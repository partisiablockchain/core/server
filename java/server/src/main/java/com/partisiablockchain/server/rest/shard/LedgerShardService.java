package com.partisiablockchain.server.rest.shard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.FailureCause;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.transaction.ExecutableTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializer;
import com.secata.stream.SafeDataOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

/** Service for accessing a specific shard. */
public final class LedgerShardService implements ShardService {

  private static final ObjectMapper mapper = StateObjectMapper.createObjectMapper();
  private final Map<String, BlockchainLedger> ledgers;

  /**
   * Service for accessing a specific shard.
   *
   * @param ledgers a map from shard name to the ledger for that shard
   */
  public LedgerShardService(Map<String, BlockchainLedger> ledgers) {
    this.ledgers = ledgers;
  }

  @Override
  public ShardState getShardState(String shardId) {
    BlockchainLedger ledger = ledgers.get(shardId);
    ImmutableChainState chainState = ledger.getChainState();
    Map<ChainPluginType, JsonNode> plugins = new HashMap<>();
    JsonNode consensusPlugin =
        mapper.valueToTree(ledger.getChainState().getLocalConsensusPluginState());
    plugins.put(ChainPluginType.CONSENSUS, consensusPlugin);
    JsonNode accountPlugin =
        mapper.valueToTree(ledger.getChainState().getLocalAccountPluginState());
    plugins.put(ChainPluginType.ACCOUNT, accountPlugin);
    return new ShardState(chainState.getGovernanceVersion(), plugins);
  }

  @Override
  public ShardBlock getShardBlock(String shardId, Hash hash) {
    BlockchainLedger ledger = ledgers.get(shardId);
    if (hash == null) {
      Block block = ledger.getLatestBlock();
      return createShardBlock(block);
    }
    Block block = ledger.getBlock(hash);
    if (block != null) {
      return createShardBlock(block);
    } else {
      return null;
    }
  }

  private static ShardBlock createShardBlock(Block block) {
    return new ShardBlock(
        block.identifier(),
        block.getParentBlock(),
        block.getCommitteeId(),
        block.getBlockTime(),
        block.getProductionTime(),
        block.getEventTransactions(),
        block.getTransactions(),
        block.getState(),
        block.getProducerIndex());
  }

  @Override
  public ShardTransaction getShardTransaction(String shardId, Hash hash) {
    BlockchainLedger ledger = ledgers.get(shardId);
    ExecutedTransaction transaction = ledger.getTransaction(hash);

    if (transaction != null) {
      return convertTransaction(
          transaction,
          ledger,
          this::createTransactionPointers,
          ledger::getTransactionFailure,
          true);
    }
    BlockchainLedger.PossiblyFinalizedTransaction proposedTransaction =
        ledger.getExecutedTransaction(hash);
    if (proposedTransaction != null) {
      return convertTransaction(
          proposedTransaction.transaction(),
          ledger,
          this::createTransactionPointers,
          ledger::getTransactionFailure,
          false);
    }
    if (ledger.getPending().containsKey(hash)) {
      SignedTransaction signedTransaction = ledger.getPending().get(hash);
      return new ShardTransaction(
          hash, false, SafeDataOutputStream.serialize(signedTransaction::write), null);
    }
    return null;
  }

  private List<ShardTransaction.ShardExecutionStatus.ShardTransactionPointer>
      createTransactionPointers(BlockchainLedger ledger, Collection<Hash> eventHashes) {
    return eventHashes.stream()
        .map(
            event ->
                new ShardTransaction.ShardExecutionStatus.ShardTransactionPointer(
                    event, ledger.getEventTransaction(event).getEvent().getDestinationShard()))
        .toList();
  }

  private static ShardTransaction convertTransaction(
      ExecutedTransaction transaction,
      BlockchainLedger ledger,
      BiFunction<
              BlockchainLedger,
              Collection<Hash>,
              List<ShardTransaction.ShardExecutionStatus.ShardTransactionPointer>>
          statuses,
      Function<Hash, FailureCause> failureLookup,
      boolean finalized) {
    boolean executionSucceeded = transaction.didExecutionSucceed();
    ExecutableTransaction inner = transaction.getInner();
    boolean isEvent = !(inner instanceof SignedTransaction);
    List<ShardTransaction.ShardExecutionStatus.ShardTransactionPointer> transactionPointers =
        statuses.apply(ledger, transaction.getEvents());
    ShardTransaction.ShardExecutionStatus.ShardFailure failure = null;
    if (!executionSucceeded) {
      FailureCause cause = failureLookup.apply(transaction.getInner().identifier());
      failure =
          new ShardTransaction.ShardExecutionStatus.ShardFailure(
              cause.getErrorMessage(), cause.getStackTrace());
    }

    return new ShardTransaction(
        transaction.getInner().identifier(),
        isEvent,
        SafeDataOutputStream.serialize(transaction::write),
        new ShardTransaction.ShardExecutionStatus(
            transaction.getBlockHash(),
            transaction.didExecutionSucceed(),
            finalized,
            transactionPointers,
            ledger.getTransactionCost(transaction.getInner().identifier()),
            failure));
  }

  @Override
  public ShardJar getShardJar(String shardId, Hash hash) {
    BlockchainLedger ledger = ledgers.get(shardId);
    StateSerializer stateSerializer = new StateSerializer(ledger.getStateStorage(), true);
    try {
      LargeByteArray read = stateSerializer.read(hash, LargeByteArray.class);
      if (read == null) {
        return null;
      } else {
        return new ShardJar(read.getData());
      }
    } catch (NullPointerException e) {
      return null;
    }
  }
}
