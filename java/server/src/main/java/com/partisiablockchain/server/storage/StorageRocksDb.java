package com.partisiablockchain.server.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.storage.Storage;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithResource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.rocksdb.Options;
import org.rocksdb.RocksDB;
import org.rocksdb.RocksIterator;
import org.rocksdb.Statistics;
import org.rocksdb.StatsLevel;

/**
 * Storage implementation based on rocks db, creates a database file in the supplied root directory.
 *
 * @see RocksDB
 */
public final class StorageRocksDb implements Storage {

  static {
    RocksDB.loadLibrary();
  }

  private final RocksDB db;
  private final boolean immutable;

  /**
   * Creates a new StorageRocksDB with a single DB in the supplied directory.
   *
   * @param path the path for the db file
   * @param immutable true if ignore overwrite
   */
  public StorageRocksDb(RootDirectory path, boolean immutable) {
    this.immutable = immutable;

    db =
        ExceptionConverter.call(
            () -> RocksDB.open(createOptions(), path.createFile("rocks.db").getAbsolutePath()),
            "Error in DB open");
  }

  static Options createOptions() {
    Options options = new Options();
    options.setCreateIfMissing(true);
    options.setKeepLogFileNum(10);
    options.setMaxLogFileSize(10 * 1024 * 1024);

    Statistics statistics = new Statistics();
    statistics.setStatsLevel(StatsLevel.ALL);
    options.setStatistics(statistics);
    return options;
  }

  @Override
  public <T> T read(Hash hash, Function<SafeDataInputStream, T> reader) {
    byte[] bytes = readRaw(hash);
    if (bytes != null) {
      return reader.apply(SafeDataInputStream.createFromBytes(bytes));
    } else {
      return null;
    }
  }

  private byte[] lookup(Hash key) {
    return ExceptionConverter.call(() -> db.get(key.getBytes()), "Error in looking up hash");
  }

  @Override
  public <T> Stream<T> stream(Function<SafeDataInputStream, T> reader) {
    return WithResource.apply(
        db::newIterator,
        rocksIterator -> {
          List<Hash> keys = getKeys(rocksIterator);
          return StreamSupport.stream(
              Spliterators.spliteratorUnknownSize(
                  iterator(keys, reader),
                  Spliterator.IMMUTABLE | Spliterator.NONNULL | Spliterator.ORDERED),
              false);
        },
        "Error in preparing for stream iteration");
  }

  private List<Hash> getKeys(RocksIterator rocksIterator) {
    List<Hash> keys = new ArrayList<>();
    rocksIterator.seekToFirst();
    while (rocksIterator.isValid()) {
      keys.add(Hash.read(SafeDataInputStream.createFromBytes(rocksIterator.key())));
      rocksIterator.next();
    }
    return keys;
  }

  private <T> Iterator<T> iterator(
      List<Hash> rocksIterator, Function<SafeDataInputStream, T> reader) {
    Iterator<Hash> inner = rocksIterator.iterator();
    return new Iterator<>() {
      @Override
      public boolean hasNext() {
        return inner.hasNext();
      }

      @Override
      public T next() {
        return read(inner.next(), reader);
      }
    };
  }

  @Override
  public byte[] readRaw(Hash hash) {
    return lookup(hash);
  }

  @Override
  public boolean has(Hash hash) {
    byte[] bytes = lookup(hash);
    return bytes != null;
  }

  @Override
  public boolean isEmpty() {
    return WithResource.apply(
        db::newIterator,
        rocksIterator -> {
          rocksIterator.seekToFirst();
          return !rocksIterator.isValid();
        },
        "Error when checking for empty");
  }

  @Override
  public void remove(Hash hash) {
    ExceptionConverter.run(() -> db.delete(hash.getBytes()), "Error in delete");
  }

  @Override
  public void write(Hash hash, Consumer<SafeDataOutputStream> writer) {
    boolean shouldWrite = !immutable || !has(hash);
    if (shouldWrite) {
      ExceptionConverter.run(
          () -> db.put(hash.getBytes(), SafeDataOutputStream.serialize(writer)), "Error in write");
    }
  }

  @Override
  public void clear() {
    WithResource.accept(
        db::newIterator,
        rocksIterator -> {
          rocksIterator.seekToFirst();
          if (rocksIterator.isValid()) {
            final byte[] firstKey = rocksIterator.key();
            rocksIterator.seekToLast();
            final byte[] lastKey = rocksIterator.key();
            db.deleteRange(firstKey, lastKey);
            db.delete(lastKey);
          }
        },
        "Error when clearing db");
  }
}
