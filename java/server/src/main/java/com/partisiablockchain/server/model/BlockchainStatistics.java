package com.partisiablockchain.server.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/** Data transfer object for aggregate information for the added blocks. */
public final class BlockchainStatistics {

  /** Timestamp of earliest block included in statistics. */
  public long earliestBlockProductionTime;

  /** Timestamp of latest block included in statistics. */
  public long latestBlockProductionTime;

  /** Number of transactions in the interval. */
  public long transactionCount;

  /** Number of events in the interval. */
  public long eventTransactionCount;

  /** Number of blocks in the interval. */
  public long blockCount;

  /** Number of reset blocks in the interval. */
  public long resetBlockCount;

  /** Information about the active producers in the interval. */
  public Map<Long, Committee> committees;

  private static BlockchainStatistics createFromBuilder(Builder builder) {
    BlockchainStatistics blockchainStatistics = new BlockchainStatistics();
    blockchainStatistics.earliestBlockProductionTime = builder.earliestBlockProductionTime;
    blockchainStatistics.latestBlockProductionTime = builder.latestBlockProductionTime;
    blockchainStatistics.blockCount = builder.blockCount;
    blockchainStatistics.committees =
        builder.committees.entrySet().stream()
            .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().build()));
    blockchainStatistics.eventTransactionCount = builder.eventTransactionCount;
    blockchainStatistics.resetBlockCount = builder.resetBlockCount;
    blockchainStatistics.transactionCount = builder.transactionCount;
    return blockchainStatistics;
  }

  /** Builder for the statistics data transfer object. */
  public static final class Builder {

    private Block nextBlock;

    Long latestBlockProductionTime;
    long earliestBlockProductionTime;
    long blockCount;
    long resetBlockCount;
    long transactionCount;
    long eventTransactionCount;
    Map<Long, Committee.Builder> committees = new HashMap<>();

    /**
     * Add a block to the builder. Assumes that the blocks are appended with the newest block first.
     *
     * @param block the block to add.
     */
    public void addBlock(Block block) {
      if (isEmpty()) {
        latestBlockProductionTime = block.getProductionTime();
      }
      earliestBlockProductionTime = block.getProductionTime();

      blockCount++;
      short blockProducerIndex = block.getProducerIndex();
      if (blockProducerIndex == -1) {
        resetBlockCount++;
      }
      transactionCount += block.getTransactions().size();
      eventTransactionCount += block.getEventTransactions().size();

      Committee.Builder committeeBuilder =
          committees.getOrDefault(block.getCommitteeId(), new Committee.Builder());

      BlockProducer.Builder blockProducerBuilder =
          committeeBuilder.producers.getOrDefault(blockProducerIndex, new BlockProducer.Builder());

      blockProducerBuilder.incrementBlocksProduced();
      long finalizationTime = calculateFinalizationTime(block, nextBlock);
      blockProducerBuilder.addFinalizationTime(finalizationTime);
      committeeBuilder.setBlockProducer(blockProducerIndex, blockProducerBuilder);
      committees.put(block.getCommitteeId(), committeeBuilder);

      nextBlock = block;
    }

    private long calculateFinalizationTime(Block currentBlock, Block nextBlock) {
      if (nextBlock == null || !hasTransactions(currentBlock)) {
        return -1;
      }
      return nextBlock.getProductionTime() - currentBlock.getProductionTime();
    }

    private boolean hasTransactions(Block block) {
      int totalTransactions = block.getTransactions().size() + block.getEventTransactions().size();
      return totalTransactions > 0;
    }

    /**
     * Build the statistics record.
     *
     * @return the statistics record
     * @throws WebApplicationException with status {@link Response.Status#NOT_FOUND} if no blocks
     *     has been added
     */
    public BlockchainStatistics build() {
      if (isEmpty()) {
        throw new WebApplicationException(Response.Status.NOT_FOUND);
      }
      return createFromBuilder(this);
    }

    private boolean isEmpty() {
      return latestBlockProductionTime == null;
    }
  }
}
