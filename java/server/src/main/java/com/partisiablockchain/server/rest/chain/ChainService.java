package com.partisiablockchain.server.rest.chain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import java.util.List;
import java.util.Map;

/** Interface for communicating with a chain. */
public interface ChainService {

  /**
   * Get the state of the chain.
   *
   * @param blockTime get the chain state for the given block time. If null get the latest block
   *     time.
   * @return the current state of the chain
   */
  ChainState getChainState(Long blockTime);

  /**
   * Get a chain account by address.
   *
   * @param address the address of the account
   * @param blockTime get the chain state for the given block time. If null get the latest block
   *     time.
   * @return the state of the account
   */
  ChainAccount getChainAccount(BlockchainAddress address, Long blockTime);

  /**
   * Get a chain contract by address.
   *
   * @param address the address of the contract
   * @param blockTime get the chain state for the given block time. If null get the latest block
   *     time.
   * @return the state of the contract
   */
  ChainContract getChainContract(BlockchainAddress address, Long blockTime);

  /**
   * Get the spawned transaction.
   *
   * @param serializedTransaction the information about the transaction
   * @return the state of the transaction which is added
   */
  ChainTransactionPointer putChainTransaction(byte[] serializedTransaction);

  /**
   * Get value in an avl tree from a key.
   *
   * @param address the address of the contract.
   * @param treeId the tree id of the avl tree.
   * @param key the key.
   * @param blockTime get the chain state for the given block time. If null get the latest block
   *     time.
   * @return the corresponding value.
   */
  byte[] getChainContractAvlValue(
      BlockchainAddress address, int treeId, byte[] key, Long blockTime);

  /**
   * Retrieves the next n entries of an avl tree from a key (Excluding the key itself). If key is
   * null, gets the first n entries.
   *
   * @param address the address of the contract.
   * @param treeId the tree id of the avl tree.
   * @param key the key to base the search from. If null, the first n entries are retrieved.
   * @param n the number of elements to get.
   * @param skip the number of elements to skip before collecting entries
   * @param blockTime get the chain state for the given block time. If null get the latest block
   *     time.
   * @return list of the next n entries. May return fewer than n entries if there are fewer entries
   *     left in the tree.
   */
  List<Map.Entry<byte[], byte[]>> getAvlNextN(
      BlockchainAddress address, int treeId, byte[] key, int n, int skip, Long blockTime);

  /**
   * Gets the modified keys for an AvlTree between two block times. Returns a sorted list of
   * modified keys starting from a supplied key
   *
   * @param address the address of the contract.
   * @param treeId the tree id of the avl tree.
   * @param blockTime1 the first block time to get the avl tree from
   * @param blockTime2 the second block time to get the avl tree from
   * @param key the key to base the search from. If null, starts from the first element.
   * @param n the max number of elements to get.
   * @return list of modified keys. May return fewer than n entries if there are fewer modified
   *     entries left in the tree.
   */
  List<byte[]> getAvlModifiedKeys(
      BlockchainAddress address, int treeId, long blockTime1, long blockTime2, byte[] key, int n);

  /**
   * Get size information of an avl tree.
   *
   * @param address the address of the contract.
   * @param treeId the tree id of the avl tree.
   * @param blockTime get the chain state for the given block time. If null get the latest block
   *     time.
   * @return the size of the avl tree.
   */
  int getAvlInformation(BlockchainAddress address, int treeId, Long blockTime);
}
