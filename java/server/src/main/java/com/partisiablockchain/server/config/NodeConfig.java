package com.partisiablockchain.server.config;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.flooding.Address;
import java.util.List;
import java.util.stream.Collectors;

/** Configuration for an entire node, including list of modules. */
public record NodeConfig(
    List<ModuleConfig> modules,
    KademliaConfig kademliaConfig,
    int restPort,
    int floodingPort,
    String persistentPeer,
    List<String> knownPeers) {

  /**
   * A generic configuration for a module, points to the specific module being loaded in {@link
   * #fullyQualifiedClass}.
   */
  public record ModuleConfig(String fullyQualifiedClass, JsonNode configuration) {}

  /**
   * Returns the know peers.
   *
   * @return List of known peers
   */
  public List<Address> knownPeersAsAddress() {
    if (knownPeers == null) {
      return List.of();
    }

    return knownPeers.stream().map(Address::parseAddress).collect(Collectors.toList());
  }
}
