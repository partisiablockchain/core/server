package com.partisiablockchain.server.config;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.server.model.Genesis;
import com.partisiablockchain.server.storage.StorageRocksDb;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.rest.ObjectMapperProvider;
import java.io.File;

/** Config of the server. */
public final class ConfigFiles {

  private static final ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);

  private final File dataDir;
  private final String genesisState;
  private final File genesisFile;
  private final File nodeConfigFile;

  private ConfigFiles(
      String nodeConfigFile, String genesisFile, String dataDir, String genesisState) {
    this.genesisFile = loadAsFile(genesisFile);
    this.nodeConfigFile = loadAsFile(nodeConfigFile);
    this.dataDir = loadAsFile(dataDir);
    this.genesisState = genesisState;
  }

  private static File loadAsFile(String genesisFile) {
    // This is a file defined by the user in the docker - run by the user.
    // nosemgrep
    return new File(genesisFile);
  }

  /**
   * Creates the config files from three string argument. The files as
   *
   * <ol>
   *   <li>Node config file
   *   <li>Genesis file
   *   <li>Data directory for resuming previous operations
   * </ol>
   *
   * @param args the arguments
   * @return the config files
   */
  public static ConfigFiles createFromArgs(String[] args) {
    if (args.length > 3) {
      return new ConfigFiles(args[0], args[1], args[2], args[3]);
    } else {
      throw new IllegalArgumentException(
          "Need arguments for both node config, genesis and data directory");
    }
  }

  /**
   * Returns the config of the node.
   *
   * @return config for the node
   */
  public NodeConfig getNodeConfig() {
    return read(NodeConfig.class, nodeConfigFile);
  }

  /**
   * Returns the root directory.
   *
   * @return The root directory
   */
  public RootDirectory getDataDirectory() {
    return new RootDirectory(dataDir, StorageRocksDb::new);
  }

  /**
   * Returns the genesis state.
   *
   * @return the genesis state
   */
  public String getGenesisFile() {
    return genesisState;
  }

  /**
   * Returns the chainid.
   *
   * @return the chainid
   */
  public String getChainId() {
    return readGenesis().chainId();
  }

  private Genesis readGenesis() {
    return read(Genesis.class, genesisFile);
  }

  private <T> T read(Class<T> valueType, File nodeConfigFile) {
    return ExceptionConverter.call(
        () -> mapper.readValue(nodeConfigFile, valueType), "Unable to read config file");
  }
}
