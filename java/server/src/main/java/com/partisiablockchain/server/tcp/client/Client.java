package com.partisiablockchain.server.tcp.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.ClassLoaderHelper;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.contract.ContractType;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateStorage;
import com.partisiablockchain.server.tcp.Connection;
import com.partisiablockchain.server.tcp.packet.ContractCreated;
import com.partisiablockchain.server.tcp.packet.ContractCreatedResponse;
import com.partisiablockchain.server.tcp.packet.ContractRemoved;
import com.partisiablockchain.server.tcp.packet.ContractStateRequest;
import com.partisiablockchain.server.tcp.packet.ContractStateResponse;
import com.partisiablockchain.server.tcp.packet.ContractUpdated;
import com.partisiablockchain.server.tcp.packet.Handshake;
import com.partisiablockchain.server.tcp.packet.Packet;
import com.partisiablockchain.server.tcp.packet.SerializedStateRequest;
import com.partisiablockchain.server.tcp.packet.SerializedStateResponse;
import com.partisiablockchain.server.tcp.threads.ScheduledSingleThreadExecutor;
import com.partisiablockchain.server.tcp.threads.ScheduledSingleThreadExecutorImpl;
import com.partisiablockchain.server.tcp.threads.SharedPoolSingleThreadExecutorDecorator;
import com.partisiablockchain.server.tcp.threads.WaitForState;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.WithCloseableResources;
import java.io.Closeable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A client connected through TCP to other blockchain nodes. */
public final class Client implements Closeable {

  private static final Logger logger = LoggerFactory.getLogger(Client.class);

  private final List<Hash> enabledBinders;
  private final ScheduledSingleThreadExecutor executor;
  private final Map<ContractStateRequest, WaitForState<StateAndBinaries>> contractState =
      new ConcurrentHashMap<>();
  private Connection connection;
  private final Address toConnect;
  private final FetchingStateStorage storage;
  private final ContractDispatch dispatch;
  private final List<BlockchainAddress> contracts;

  Client(
      Supplier<Executor> executor,
      List<Hash> enabledBinders,
      Address toConnect,
      CreateContractListener listener,
      StateStorage stateStorage,
      ScheduledSingleThreadExecutor scheduledExecutor,
      Set<String> systemClassLoadedNamespaces) {
    this(
        executor,
        enabledBinders,
        toConnect,
        listener,
        stateStorage,
        scheduledExecutor,
        systemClassLoadedNamespaces,
        null);
  }

  Client(
      Supplier<Executor> executorSupplier,
      List<Hash> enabledBinders,
      Address toConnect,
      CreateContractListener listener,
      StateStorage stateStorage,
      ScheduledSingleThreadExecutor scheduledExecutor,
      Set<String> systemClassLoadedNamespaces,
      List<BlockchainAddress> contracts) {
    if (enabledBinders != null && enabledBinders.isEmpty()) {
      throw new IllegalArgumentException("TCP Client given no binders to listen to!");
    }

    this.executor = Objects.requireNonNull(scheduledExecutor);
    this.enabledBinders = enabledBinders;
    this.toConnect = Objects.requireNonNull(toConnect);
    this.storage = new FetchingStateStorage(this::requestState, stateStorage);
    this.dispatch =
        new ContractDispatch(
            Objects.requireNonNull(executorSupplier),
            this::getContractState,
            new StateSerializer(this.storage, true),
            Objects.requireNonNull(listener),
            Objects.requireNonNull(systemClassLoadedNamespaces));
    this.contracts = contracts;
    scheduleConnect();
  }

  /**
   * Create a new client that connects to a list of shards.
   *
   * @param enabledBinders the binders that events should be sent for, or null to not filter.
   *     Nullable.
   * @param shards addresses of all shards. Not nullable.
   * @param listener listener invoked for created contracts. Not nullable.
   * @param stateStorage storage to use for storing fetched states. Not nullable.
   * @param systemClassLoadedNamespaces class namespaces that should be loaded by the system
   *     classloader. Not nullable.
   * @return closable to close the clients. Never null.
   */
  public static WithCloseableResources createShardClient(
      List<Hash> enabledBinders,
      List<Address> shards,
      CreateContractListener listener,
      StateStorage stateStorage,
      Set<String> systemClassLoadedNamespaces) {
    return createShardClient(
        enabledBinders, shards, listener, stateStorage, systemClassLoadedNamespaces, null);
  }

  /**
   * Create a new client that connects to a list of shards, keeping track of specified contracts
   * only.
   *
   * @param enabledBinders the binders that events should be sent for, or null to not filter
   * @param shards addresses of all shards
   * @param listener listener invoked for created contracts
   * @param stateStorage storage to use for storing fetched states
   * @param systemClassLoadedNamespaces class namespaces that should be loaded by the system
   *     classloader
   * @param contracts the contracts that are relevant for this client or null if this client should
   *     listen on all contracts
   * @return closable to close the clients
   */
  public static WithCloseableResources createShardClient(
      List<Hash> enabledBinders,
      List<Address> shards,
      CreateContractListener listener,
      StateStorage stateStorage,
      Set<String> systemClassLoadedNamespaces,
      List<BlockchainAddress> contracts) {
    return createClient(
        enabledBinders, shards, listener, stateStorage, systemClassLoadedNamespaces, contracts);
  }

  private static WithCloseableResources createClient(
      List<Hash> enabledBinders,
      List<Address> shards,
      CreateContractListener listener,
      StateStorage stateStorage,
      Set<String> systemClassLoadedNamespaces,
      List<BlockchainAddress> contracts) {
    SharedPoolSingleThreadExecutorDecorator executor =
        new SharedPoolSingleThreadExecutorDecorator("ShardClients");
    List<Client> clients =
        createClients(
            enabledBinders,
            shards,
            listener,
            stateStorage,
            executor,
            systemClassLoadedNamespaces,
            contracts);
    return new ClientResources(clients, executor);
  }

  static List<Client> createClients(
      List<Hash> enabledBinders,
      List<Address> shards,
      CreateContractListener listener,
      StateStorage stateStorage,
      Supplier<Executor> executor,
      Set<String> systemClassLoadedNamespaces,
      List<BlockchainAddress> contracts) {
    if (shards.isEmpty()) {
      throw new IllegalArgumentException("TCP Client given no shards to listen to!");
    }

    List<Client> clients =
        shards.stream()
            .map(
                address ->
                    new Client(
                        executor,
                        enabledBinders,
                        address,
                        listener,
                        stateStorage,
                        new ScheduledSingleThreadExecutorImpl("Connector"),
                        systemClassLoadedNamespaces,
                        contracts))
            .toList();
    return clients;
  }

  @Override
  public void close() {
    executor.close();
    if (connection != null) {
      connection.close();
    }
  }

  private void scheduleConnect() {
    executor.schedule(this::checkConnection, 1, TimeUnit.SECONDS);
  }

  @SuppressWarnings("AddressSelection")
  private void checkConnection() {
    if (!isConnected()) {
      try {
        Socket socket =
            ExceptionConverter.call(
                // Intentional suppressed, Intended only for internal use in a deployment
                // nosemgrep
                () -> new Socket(toConnect.hostname(), toConnect.port()));
        List<BlockchainAddress> activeListeners = dispatch.activeListeners();
        Handshake handshake = new Handshake(enabledBinders, activeListeners, contracts);
        handshake.write(new SafeDataOutputStream(socket.getOutputStream()));
        this.connection =
            Connection.createConnection(
                this::incoming,
                socket.getInputStream(),
                socket.getOutputStream(),
                this::scheduleConnect);
        logger.trace(
            "Connected to server {} ({} fetching state, {} contract states)",
            toConnect,
            storage.fetching().size(),
            contractState.keySet().size());
        storage.fetching().forEach(this::requestState);
        contractState.keySet().forEach(this::sendStateRequest);
      } catch (Exception e) {
        logger.debug("Unable to connect, rechecking in 5 seconds");
        executor.schedule(this::checkConnection, 5, TimeUnit.SECONDS);
      }
    }
  }

  boolean isConnected() {
    return this.connection != null && this.connection.isAlive();
  }

  private void requestState(Hash hash) {
    logger.trace("Fetching state {}", hash);
    Packet<SerializedStateRequest> packet =
        new Packet<>(Packet.Type.SERIALIZED_STATE_REQUEST, new SerializedStateRequest(hash));
    sendPacket(packet);
  }

  void sendPacket(Packet<?> packet) {
    logger.trace("Sending packet: {}", packet.getType());
    if (isConnected()) {
      connection.send(packet);
    }
  }

  private void createContract(BlockchainAddress address, ContractCreated event) {
    logger.trace("Contract created {}", address);
    dispatch.create(
        event,
        createListener -> {
          logger.trace("Create listener for contract {}: {}", address, createListener);
          sendPacket(
              new Packet<>(
                  Packet.Type.CONTRACT_CREATED_RESPONSE,
                  new ContractCreatedResponse(address, createListener)));
        });
  }

  void incoming(Packet<?> packet) {
    logger.trace("Packet received: {}", packet.getType());
    if (packet.getType() == Packet.Type.CONTRACT_CREATED) {
      ContractCreated event = (ContractCreated) packet.getPayload();
      BlockchainAddress address = event.getAddress();
      createContract(address, event);
    } else if (packet.getType() == Packet.Type.CONTRACT_UPDATED) {
      ContractUpdated event = (ContractUpdated) packet.getPayload();
      dispatch.update(event);
    } else if (packet.getType() == Packet.Type.CONTRACT_REMOVED) {
      ContractRemoved event = (ContractRemoved) packet.getPayload();
      dispatch.remove(event);
    } else if (packet.getType() == Packet.Type.CONTRACT_STATE_RESPONSE) {
      ContractStateResponse response = (ContractStateResponse) packet.getPayload();
      notifyContractState(response);
    } else if (packet.getType() == Packet.Type.SERIALIZED_STATE_RESPONSE) {
      SerializedStateResponse response = (SerializedStateResponse) packet.getPayload();
      storage.received(response.getData());
    } else {
      throw new IllegalArgumentException(
          "Received unexpected packet type: " + packet.getType().getType());
    }
  }

  private StateAndBinaries getContractState(BlockchainAddress contract, long blockTime) {
    ContractStateRequest request = new ContractStateRequest(contract, blockTime);
    WaitForState<StateAndBinaries> wait =
        contractState.computeIfAbsent(request, this::sendStateRequest);
    return wait.waitForState();
  }

  private WaitForState<StateAndBinaries> sendStateRequest(
      ContractStateRequest contractStateRequest) {
    sendPacket(new Packet<>(Packet.Type.CONTRACT_STATE_REQUEST, contractStateRequest));
    return new WaitForState<>();
  }

  private void notifyContractState(ContractStateResponse response) {
    notifyState(
        contractState,
        response.getRequest(),
        new StateAndBinaries(
            response.getBinderHash(), response.getContractHash(), response.getStateHash()));
  }

  private <K, V> void notifyState(Map<K, WaitForState<V>> pending, K request, V response) {
    WaitForState<V> remove = pending.remove(request);
    if (remove != null) {
      remove.setState(response);
    }
  }

  private static final class ContractDispatch {

    private final ClassLoaderHelper classLoaders;

    private final Supplier<Executor> executors;
    private final BiFunction<BlockchainAddress, Long, StateAndBinaries> stateHistory;
    private final StateSerializer serializer;
    private final CreateContractListener listener;
    private final Map<BlockchainAddress, ContractEventDispatcher> listeners =
        new ConcurrentHashMap<>();
    private final Executor executor;

    private ContractDispatch(
        Supplier<Executor> executors,
        BiFunction<BlockchainAddress, Long, StateAndBinaries> stateHistory,
        StateSerializer serializer,
        CreateContractListener listener,
        Set<String> systemClassLoadedNamespaces) {
      this.executors = Objects.requireNonNull(executors);
      this.executor = Objects.requireNonNull(executors.get());
      this.stateHistory = Objects.requireNonNull(stateHistory);
      this.serializer = Objects.requireNonNull(serializer);
      this.listener = Objects.requireNonNull(listener);
      this.classLoaders = new ClassLoaderHelper(systemClassLoadedNamespaces);
    }

    void create(ContractCreated event, Consumer<Boolean> callback) {
      executor.execute(
          () -> {
            Hash stateHash = event.getStateHash();
            BlockchainAddress address = event.getAddress();
            Hash binderHash = event.getBinderHash();
            Hash abiHash = event.getAbiHash();
            Hash contractHash = event.getContractHash();
            int codeSize = event.getCodeSize();
            ContractType type = ContractType.fromAddress(address);
            BlockchainContract<?, ?> blockchainContract =
                createBlockchainContract(type, binderHash, contractHash);
            ContractEventDispatcher.StateLookup stateLookup =
                new InnerStateLookup(type, blockchainContract, binderHash, contractHash);

            StateSerializable currentState =
                blockchainContract.getContractSerialization().read(serializer, stateHash);
            Listener created =
                listener.createContract(
                    blockchainContract,
                    blockTime ->
                        stateLookup.read(this.stateHistory.apply(address, blockTime)).state(),
                    address,
                    CoreContractState.create(binderHash, contractHash, abiHash, codeSize),
                    currentState);
            boolean listen = created != null;
            if (listen) {
              listeners.put(
                  address,
                  new ContractEventDispatcher(
                      executors.get(), created, currentState, blockchainContract, stateLookup));
            }
            callback.accept(listen);
          });
    }

    void update(ContractUpdated event) {
      listeners
          .get(event.getAddress())
          .update(
              new StateAndBinaries(
                  event.getBinderHash(), event.getContractHash(), event.getStateHash()));
    }

    void remove(ContractRemoved event) {
      listeners.remove(event.getAddress()).remove();
    }

    List<BlockchainAddress> activeListeners() {
      return new ArrayList<>(listeners.keySet());
    }

    private BlockchainContract<?, ?> createBlockchainContract(
        ContractType type, Hash binder, Hash contract) {
      LargeByteArray binderJar = serializer.read(binder, LargeByteArray.class);
      LargeByteArray contractJar = serializer.read(contract, LargeByteArray.class);
      return classLoaders.blockchainContract(type, binderJar, contractJar);
    }

    private final class InnerStateLookup implements ContractEventDispatcher.StateLookup {

      private final ContractType type;
      private CoreContractState.ContractSerialization<?> serialization;
      private BlockchainContract<?, ?> latestBlockchainContract;
      private Hash latestBinder;
      private Hash latestContract;

      InnerStateLookup(
          ContractType type,
          BlockchainContract<?, ?> latestBlockchainContract,
          Hash latestBinder,
          Hash latestContract) {
        this.type = type;
        this.latestBlockchainContract = latestBlockchainContract;
        this.serialization = latestBlockchainContract.getContractSerialization();
        this.latestBinder = latestBinder;
        this.latestContract = latestContract;
      }

      @Override
      public ContractEventDispatcher.StateAndContract read(StateAndBinaries stateAndBinary) {
        boolean cachedSerializerMatches =
            Objects.equals(latestBinder, stateAndBinary.binder())
                && Objects.equals(latestContract, stateAndBinary.contract());
        if (!cachedSerializerMatches) {
          latestBlockchainContract =
              createBlockchainContract(type, stateAndBinary.binder(), stateAndBinary.contract());
          latestBinder = stateAndBinary.binder();
          latestContract = stateAndBinary.contract();
          serialization = latestBlockchainContract.getContractSerialization();
        }
        StateSerializable state = serialization.read(serializer, stateAndBinary.state());
        return new ContractEventDispatcher.StateAndContract(state, latestBlockchainContract);
      }
    }
  }

  /** Listener that is invoked for all new contracts. */
  public interface CreateContractListener {

    /**
     * Invoked for each new contract.
     *
     * @param contract the created contract
     * @param history utility for accessing historic states of this contract
     * @param address the location of the contract
     * @param core the core information about the contract
     * @param state the current state of the contract
     * @return a listener to register or null to not lister for the contract
     */
    Listener createContract(
        BlockchainContract<?, ?> contract,
        StateHistory history,
        BlockchainAddress address,
        CoreContractState core,
        StateSerializable state);
  }

  /** A contract listener. Either of the {@link #update} methods should be implemented. */
  public interface Listener {

    /**
     * Called when the contract was updated.
     *
     * @param state the new state of the contract
     */
    default void update(StateSerializable state) {
      throw new RuntimeException("Not implemented.");
    }

    /**
     * Called when the contract was updated.
     *
     * @param state the new state of the contract
     * @param blockchainContract the possible new blockchain contract
     */
    default void update(StateSerializable state, BlockchainContract<?, ?> blockchainContract) {
      update(state);
    }

    /** Called when the contract was removed. */
    void remove();
  }

  /** Interface for getting historic states. */
  public interface StateHistory {

    /**
     * Get the state of a contract for a specific block time.
     *
     * @param blockTime the block time to get state for
     * @return the state of the contract at the requested time
     */
    StateSerializable getState(long blockTime);
  }

  /** Set of closable clients and other related resources. */
  public static final class ClientResources implements WithCloseableResources {
    private final List<Client> clients;
    private final SharedPoolSingleThreadExecutorDecorator executor;

    private ClientResources(
        List<Client> clients, SharedPoolSingleThreadExecutorDecorator executor) {
      this.clients = Objects.requireNonNull(clients);
      this.executor = executor;
    }

    @Override
    public Stream<AutoCloseable> resources() {
      return Stream.concat(clients.stream(), Stream.of(executor));
    }
  }
}
