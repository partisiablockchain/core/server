package com.partisiablockchain.server.rest.chain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.server.rest.PluginTypeConverter;
import com.partisiablockchain.server.rest.ShardConverter;
import com.partisiablockchain.server.rest.api.ChainControllerApi;
import com.partisiablockchain.server.rest.chain.ChainState.ChainFeature;
import com.partisiablockchain.server.rest.chain.ChainState.ChainPlugin;
import com.partisiablockchain.server.rest.model.Account;
import com.partisiablockchain.server.rest.model.AvlInformation;
import com.partisiablockchain.server.rest.model.AvlStateEntry;
import com.partisiablockchain.server.rest.model.AvlStateValue;
import com.partisiablockchain.server.rest.model.Chain;
import com.partisiablockchain.server.rest.model.Contract;
import com.partisiablockchain.server.rest.model.Feature;
import com.partisiablockchain.server.rest.model.Plugin;
import com.partisiablockchain.server.rest.model.SerializedTransaction;
import com.partisiablockchain.server.rest.model.TransactionPointer;
import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;

/** Controller for the chain. */
public final class ChainController implements ChainControllerApi {

  private final ChainService chainService;

  /**
   * Controller for communicating with a chain.
   *
   * @param chainService service the controller
   */
  @Inject
  public ChainController(ChainService chainService) {
    this.chainService = chainService;
  }

  /**
   * Get information about the chain.
   *
   * @return the current chain state
   */
  @Override
  public Chain getChain(Long blockTime) {
    ChainState chainState = chainService.getChainState(blockTime);

    Map<String, Plugin> plugins = new HashMap<>();
    Map<ChainPluginType, ChainPlugin> chainPlugins = chainState.plugins();
    for (ChainPluginType chainPluginType : chainPlugins.keySet()) {
      ChainPlugin chainPluginState = chainPlugins.get(chainPluginType);

      Plugin plugin =
          new Plugin().jarId(chainPluginState.jarId().toString()).state(chainPluginState.state());
      String pluginType = PluginTypeConverter.convertToString(chainPluginType);

      plugins.put(pluginType, plugin);
    }

    List<Feature> features = new ArrayList<>();
    for (ChainFeature chainFeatureState : chainState.features()) {
      Feature feature = new Feature();
      feature.setName(chainFeatureState.name());
      feature.setValue(chainFeatureState.value());
      features.add(feature);
    }

    List<String> shards = new ArrayList<>();
    for (String shard : chainState.shards()) {
      shards.add(ShardConverter.convertToGovShard(shard));
    }

    return new Chain()
        .chainId(chainState.chainId())
        .governanceVersion(chainState.governanceVersion())
        .shards(shards)
        .features(features)
        .plugins(plugins);
  }

  /**
   * Get a specific account by address.
   *
   * @param address address of the account
   * @return the account
   */
  @Override
  public Account getAccount(String address, Long blockTime) {
    BlockchainAddress blockchainAddress = BlockchainAddress.fromString(address);
    if (blockchainAddress.getType() != BlockchainAddress.Type.ACCOUNT) {
      throw new BadRequestException("Not a valid account address");
    }

    ChainAccount chainAccount = chainService.getChainAccount(blockchainAddress, blockTime);

    Account account = new Account();
    account.setShardId(chainAccount.shardId());
    account.setNonce(chainAccount.nonce());
    account.setAccount(chainAccount.account());
    return account;
  }

  /**
   * Get a specific contract by address.
   *
   * @param address of the contract
   * @return the contract
   */
  @Override
  public Contract getContract(String address, Long blockTime) {
    BlockchainAddress blockchainAddress = BlockchainAddress.fromString(address);
    if (blockchainAddress.getType() == BlockchainAddress.Type.ACCOUNT) {
      throw new BadRequestException("Not a valid contract address");
    }

    ChainContract chainContract = chainService.getChainContract(blockchainAddress, blockTime);

    return new Contract()
        .shardId(ShardConverter.convertToGovShard(chainContract.shardId()))
        .serializedContract(chainContract.serializedContract())
        .address(chainContract.address().writeAsString())
        .jar(chainContract.jar().toString())
        .binder(chainContract.binder().toString())
        .account(chainContract.account())
        .storageLength(chainContract.storageLength())
        .abi(chainContract.abi());
  }

  @Override
  public AvlInformation getContractAvlInformation(String address, Integer treeId, Long blockTime) {
    BlockchainAddress blockchainAddress = BlockchainAddress.fromString(address);
    if (blockchainAddress.getType() == BlockchainAddress.Type.ACCOUNT) {
      throw new BadRequestException("Not a valid contract address");
    }

    int size = chainService.getAvlInformation(blockchainAddress, treeId, blockTime);
    return new AvlInformation().size(size);
  }

  @Override
  public List<String> getContractAvlModifiedKeys(
      String address, Integer treeId, List<Long> blockTime, String key, Integer n) {
    BlockchainAddress blockchainAddress = assertValidContractAvlNextRequest(address, n);
    byte[] keyBytes = key != null ? HexFormat.of().parseHex(key) : null;
    if (blockTime.size() != 2) {
      throw new BadRequestException("Expects exactly two block times");
    }

    List<byte[]> avlModified =
        chainService.getAvlModifiedKeys(
            blockchainAddress, treeId, blockTime.get(0), blockTime.get(1), keyBytes, n);

    return avlModified.stream().map(bytes -> HexFormat.of().formatHex(bytes)).toList();
  }

  @Override
  public AvlStateValue getContractAvlValue(
      String address, Integer treeId, String key, Long blockTime) {
    BlockchainAddress blockchainAddress = BlockchainAddress.fromString(address);
    if (blockchainAddress.getType() == BlockchainAddress.Type.ACCOUNT) {
      throw new BadRequestException("Not a valid contract address");
    }
    byte[] chainContractAvlValue =
        chainService.getChainContractAvlValue(
            blockchainAddress, treeId, HexFormat.of().parseHex(key), blockTime);
    return new AvlStateValue().data(chainContractAvlValue);
  }

  @Override
  public List<AvlStateEntry> getContractAvlNextN(
      String address, Integer treeId, String key, Integer n, Integer skip, Long blockTime) {
    BlockchainAddress blockchainAddress = assertValidContractAvlNextRequest(address, n);
    byte[] keyBytes = key != null ? HexFormat.of().parseHex(key) : null;

    List<Map.Entry<byte[], byte[]>> avlNextN =
        chainService.getAvlNextN(blockchainAddress, treeId, keyBytes, n, skip, blockTime);

    return getAvlStateEntries(avlNextN);
  }

  /**
   * Checks that the address is a valid contract blockchain address, and that requested number of
   * entries to get is not greater than 100.
   *
   * @param address hex encoded address
   * @param n number of requested avl entries to get
   * @return the parsed blockchain address
   */
  private static BlockchainAddress assertValidContractAvlNextRequest(String address, Integer n) {
    BlockchainAddress blockchainAddress = BlockchainAddress.fromString(address);
    if (blockchainAddress.getType() == BlockchainAddress.Type.ACCOUNT) {
      throw new BadRequestException("Not a valid contract address");
    }
    if (n > 100) {
      throw new BadRequestException("Can fetch at most 100 entries");
    }
    return blockchainAddress;
  }

  private static List<AvlStateEntry> getAvlStateEntries(List<Map.Entry<byte[], byte[]>> avlNextN) {
    ArrayList<AvlStateEntry> avlEntries = new ArrayList<>();
    for (Map.Entry<byte[], byte[]> entry : avlNextN) {
      AvlStateEntry avlStateEntry =
          new AvlStateEntry().key(HexFormat.of().formatHex(entry.getKey())).value(entry.getValue());
      avlEntries.add(avlStateEntry);
    }
    return avlEntries;
  }

  /**
   * Put a transaction on the chain.
   *
   * @param serializedTransaction the transaction to put on the chain
   * @return an object containing the information about the added transaction
   */
  @Override
  public TransactionPointer putTransaction(SerializedTransaction serializedTransaction) {
    if (serializedTransaction == null) {
      throw new BadRequestException("Serialized transaction is not valid");
    }

    ChainTransactionPointer chainTransaction =
        chainService.putChainTransaction(serializedTransaction.getPayload());

    TransactionPointer transactionPointer =
        new TransactionPointer()
            .identifier(chainTransaction.identifier().toString())
            .destinationShardId(chainTransaction.destinationShard());
    return transactionPointer;
  }
}
