package com.partisiablockchain.server.tcp.threads;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.coverage.WithCloseableResources;
import com.secata.tools.thread.SharedPoolSingleThreadedExecution;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Decorator for {@link SharedPoolSingleThreadedExecution} making it into a {@link Executor}. */
public final class SharedPoolSingleThreadExecutorDecorator
    implements WithCloseableResources, Supplier<Executor> {

  private static final Logger logger =
      LoggerFactory.getLogger(SharedPoolSingleThreadExecutorDecorator.class);
  private final SharedPoolSingleThreadedExecution execution;

  /**
   * Create a new decorator.
   *
   * @param name name of shared pool
   */
  public SharedPoolSingleThreadExecutorDecorator(String name) {
    execution = SharedPoolSingleThreadedExecution.named(name);
  }

  /**
   * Create a new single threaded executor using this pool.
   *
   * @return the new executor
   */
  @SuppressWarnings("FutureReturnValueIgnored")
  public Executor singleThreadExecutor() {
    SharedPoolSingleThreadedExecution.SingleThreadExecutor executor =
        execution.createSingleThreadedExecutor();
    return r ->
        executor.execute(
            Executors.callable(
                () ->
                    ExceptionLogger.handle(
                        logger::warn, r::run, "Error in single threaded execution")));
  }

  @Override
  public Stream<AutoCloseable> resources() {
    return Stream.of(execution);
  }

  @Override
  public Executor get() {
    return singleThreadExecutor();
  }
}
