package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Packet with the data response for a {@link SerializedStateRequest}. */
public final class SerializedStateResponse implements DataStreamSerializable {

  private final byte[] data;

  /**
   * Create a new response.
   *
   * @param data the data for the request
   */
  public SerializedStateResponse(byte[] data) {
    this.data = data.clone();
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeDynamicBytes(data);
  }

  /**
   * Read from stream.
   *
   * @param stream to read from
   * @return the read object
   */
  public static SerializedStateResponse read(SafeDataInputStream stream) {
    return new SerializedStateResponse(stream.readDynamicBytes());
  }

  /**
   * Get the data for the response.
   *
   * @return the data
   */
  public byte[] getData() {
    return data.clone();
  }
}
