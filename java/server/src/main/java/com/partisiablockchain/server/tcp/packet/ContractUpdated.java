package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Packet indicating that a contract has been updated. */
public final class ContractUpdated implements DataStreamSerializable {

  private final BlockchainAddress address;
  private final Hash binderHash;
  private final Hash contractHash;
  private final Hash stateHash;

  /**
   * Create a new contract updated packet.
   *
   * @param address the address fo the updated contract
   * @param binderHash the identifier of the contract binder
   * @param contractHash the identifier of the contract code
   * @param stateHash the new state identifer of the updated contract
   */
  public ContractUpdated(
      BlockchainAddress address, Hash binderHash, Hash contractHash, Hash stateHash) {
    this.address = address;
    this.binderHash = binderHash;
    this.contractHash = contractHash;
    this.stateHash = stateHash;
  }

  /**
   * Get the address of the updated contract.
   *
   * @return the address
   */
  public BlockchainAddress getAddress() {
    return address;
  }

  /**
   * Get the state identifier of the updated contract.
   *
   * @return the state identifier
   */
  public Hash getStateHash() {
    return stateHash;
  }

  /**
   * Get the contract identifier of the updated contract.
   *
   * @return the contract identifier
   */
  public Hash getContractHash() {
    return contractHash;
  }

  /**
   * Get the binder identifier of the updated contract.
   *
   * @return the binder identifier
   */
  public Hash getBinderHash() {
    return binderHash;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    address.write(stream);
    stateHash.write(stream);
    binderHash.write(stream);
    contractHash.write(stream);
  }

  /**
   * Read from stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static ContractUpdated read(SafeDataInputStream stream) {
    BlockchainAddress address = BlockchainAddress.read(stream);
    Hash stateHash = Hash.read(stream);
    Hash binderHash = Hash.read(stream);
    Hash contractHash = Hash.read(stream);
    return new ContractUpdated(address, binderHash, contractHash, stateHash);
  }
}
