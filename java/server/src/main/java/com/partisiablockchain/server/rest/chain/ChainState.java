package com.partisiablockchain.server.rest.chain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.crypto.Hash;
import java.util.List;
import java.util.Map;

/** Record used for transfering chain state between service and controller. */
public record ChainState(
    String chainId,
    long governanceVersion,
    List<String> shards,
    List<ChainFeature> features,
    Map<ChainPluginType, ChainPlugin> plugins) {

  /** Record used for transfering chain plugin state. */
  public record ChainPlugin(Hash jarId, JsonNode state) {}

  /** Record used for transfering chain feature value. */
  public record ChainFeature(String name, String value) {}
}
