package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.stream.SafeListStream;
import java.util.List;

/** Handshake sent from client to server when a connection has been established. */
public final class Handshake implements DataStreamSerializable {

  private final List<Hash> binders;
  private final List<BlockchainAddress> existingListeners;
  private final List<BlockchainAddress> contractAddresses;

  /**
   * Create a new handshake.
   *
   * @param binders the contract binders to listen for or null to listen for any binder
   * @param existingListeners contracts already listening to - used when reconnecting
   */
  public Handshake(List<Hash> binders, List<BlockchainAddress> existingListeners) {
    this(binders, existingListeners, null);
  }

  /**
   * Create a new handshake with a specified lists of contract addresses to keep track of.
   *
   * @param binders the contract binders to listen for or null to listen for any binder
   * @param existingListeners contracts already listening to - used when reconnecting
   * @param contractAddresses the contracts the client should receive updates on or null if client
   *     listens on all contracts
   */
  public Handshake(
      List<Hash> binders,
      List<BlockchainAddress> existingListeners,
      List<BlockchainAddress> contractAddresses) {
    this.binders = binders;
    this.existingListeners = existingListeners;
    this.contractAddresses = contractAddresses;
  }

  /**
   * Read from stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static Handshake read(SafeDataInputStream stream) {
    List<Hash> binders = stream.readOptional(Hash.LIST_SERIALIZER::readDynamic);
    List<BlockchainAddress> existing = BlockchainAddress.LIST_SERIALIZER.readDynamic(stream);
    List<BlockchainAddress> contractAddresses =
        stream.readOptional(BlockchainAddress.LIST_SERIALIZER::readDynamic);
    return new Handshake(binders, existing, contractAddresses);
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeOptional(SafeListStream.primitive(Hash.LIST_SERIALIZER::writeDynamic), binders);
    BlockchainAddress.LIST_SERIALIZER.writeDynamic(stream, existingListeners);
    stream.writeOptional(
        SafeListStream.primitive(BlockchainAddress.LIST_SERIALIZER::writeDynamic),
        contractAddresses);
  }

  /**
   * Is this binder identifier enabled.
   *
   * @param binderIdentifier the id of the binder to test
   * @return true if the client wants to be notified for this binder
   */
  public boolean isBinderEnabled(Hash binderIdentifier) {
    return binders == null || binders.contains(binderIdentifier);
  }

  /**
   * Returns true if either the contractAddresses list is null (i.e. the client listens on all
   * contracts) or if it was initialized and the specified contract is in the list.
   *
   * @param contract The contract to check
   * @return True if the contract is in the list or if the list is null. False otherwise
   */
  public boolean isContractEnabled(BlockchainAddress contract) {
    return contractAddresses == null || contractAddresses.contains(contract);
  }

  /**
   * Get a list of existing listeners that the client has.
   *
   * @return the existing listeners.
   */
  public List<BlockchainAddress> getExisting() {
    return existingListeners;
  }
}
