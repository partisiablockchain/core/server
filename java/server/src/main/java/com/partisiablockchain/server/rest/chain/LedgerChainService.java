package com.partisiablockchain.server.rest.chain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.AccountPluginState;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.server.rest.ModelFactory;
import com.partisiablockchain.server.rest.ShardConverter;
import com.partisiablockchain.server.rest.StateAbiSerializer;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** Service for accessing a ledgers chain state. */
public final class LedgerChainService implements ChainService {

  private static final ObjectMapper mapper = StateObjectMapper.createObjectMapper();

  private final Map<String, BlockchainLedger> ledgers;

  /**
   * Service for accessing a ledgers chain state.
   *
   * @param ledgers a map from shard name to the ledger for that shard
   */
  public LedgerChainService(Map<String, BlockchainLedger> ledgers) {
    this.ledgers = ledgers;
  }

  @Override
  public ChainState getChainState(Long blockTime) {
    BlockchainLedger ledger = ledgers.get(null);
    String chainId = ledger.getChainId();
    ImmutableChainState chainState = loadChainState(ledger, blockTime);
    long governanceVersion = chainState.getGovernanceVersion();
    List<String> shards = chainState.getActiveShards().stream().toList();
    List<ChainState.ChainFeature> features = new ArrayList<>();
    AvlTree<String, String> chainFeatures = chainState.getFeatures();
    for (String key : chainFeatures.keySet()) {
      features.add(new ChainState.ChainFeature(key, chainFeatures.getValue(key)));
    }
    Map<ChainPluginType, ChainState.ChainPlugin> plugins = new HashMap<>();
    Set<ChainPluginType> chainPluginTypes = chainState.getChainPluginTypes();
    for (ChainPluginType type : chainPluginTypes) {
      StateSerializable globalPluginState = chainState.getGlobalPluginState(type);
      JsonNode state = toJsonNodeIfNonNull(globalPluginState);
      plugins.put(type, new ChainState.ChainPlugin(chainState.getPluginJarIdentifier(type), state));
    }
    return new ChainState(chainId, governanceVersion, shards, features, plugins);
  }

  @Override
  public ChainAccount getChainAccount(BlockchainAddress blockchainAddress, Long blockTime) {
    String shardId = getTargetShard(blockchainAddress);
    BlockchainLedger ledger = ledgers.get(shardId);
    ImmutableChainState chainState = loadChainState(ledger, blockTime);
    AccountState account = chainState.getAccount(blockchainAddress);
    if (account != null) {
      AccountPluginState<?, ?, ?> localAccountPluginState = chainState.getLocalAccountPluginState();
      StateSerializable accountState =
          localAccountPluginState != null
              ? localAccountPluginState.getAccount(blockchainAddress)
              : null;
      JsonNode state = toJsonNodeIfNonNull(accountState);
      return new ChainAccount(ShardConverter.convertToGovShard(shardId), state, account.getNonce());
    } else {
      throw new NotFoundException(
          "Unable to find account '%s'".formatted(blockchainAddress.writeAsString()));
    }
  }

  private String getTargetShard(BlockchainAddress address) {
    BlockchainLedger ledger = ledgers.get(null);
    ImmutableChainState chainState = ledger.getChainState();
    return chainState.getRoutingPlugin().route(chainState.getActiveShards(), address);
  }

  @Override
  public ChainContract getChainContract(BlockchainAddress contractAddress, Long blockTime) {
    String shardId = getTargetShard(contractAddress);
    BlockchainLedger ledger = ledgers.get(shardId);
    ImmutableChainState chainState = loadChainState(ledger, blockTime);
    CoreContractState contract = chainState.getCoreContractState(contractAddress);
    if (contract == null) {
      throw new NotFoundException(
          "Unable to find contract at '%s'".formatted(contractAddress.writeAsString()));
    } else {
      final StateSerializer stateSerializer = new StateSerializer(ledger.getStateStorage(), true);
      StateSerializable contractState = chainState.getContractState(contractAddress);
      AccountPluginState<?, ?, ?> localAccountPluginState = chainState.getLocalAccountPluginState();
      StateSerializable accountState =
          localAccountPluginState != null
              ? localAccountPluginState.getContract(contractAddress)
              : null;
      JsonNode contractStorageAccountPlugin = toJsonNodeIfNonNull(accountState);
      LargeByteArray abi = stateSerializer.read(contract.getAbiIdentifier(), LargeByteArray.class);
      byte[] serializedContract =
          contractState != null
              ? StateAbiSerializer.serializeV2(contractState, contractAddress.getType())
              : null;
      return new ChainContract(
          ShardConverter.convertToGovShard(shardId),
          serializedContract,
          contractAddress,
          contract.getContractIdentifier(),
          contract.getBinderIdentifier(),
          contractStorageAccountPlugin,
          chainState.getContractStorageLength(contractAddress),
          abi.getData());
    }
  }

  @Override
  public ChainTransactionPointer putChainTransaction(byte[] bytes) {
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(bytes);
    SignedTransaction recoverTransaction =
        SignedTransaction.read(ledgers.get(null).getChainId(), stream);

    BlockchainLedger ledger = ledgers.get(getTargetShard(recoverTransaction.getSender()));
    SignedTransaction signedTransaction =
        SignedTransaction.read(ledger.getChainId(), SafeDataInputStream.createFromBytes(bytes));
    boolean success = ledger.addPendingTransaction(signedTransaction);
    if (!success) {
      throw new BadRequestException(
          "The received transaction is invalid in the current state of the blockchain.");
    } else {
      BlockchainAddress targetShardAddress = signedTransaction.getSender();
      String targetShard = getTargetShard(targetShardAddress);
      return new ChainTransactionPointer(
          signedTransaction.identifier(), ShardConverter.convertToGovShard(targetShard));
    }
  }

  @Override
  public byte[] getChainContractAvlValue(
      BlockchainAddress address, int treeId, byte[] key, Long blockTime) {
    String shardId = getTargetShard(address);
    BlockchainLedger ledger = ledgers.get(shardId);
    ImmutableChainState chainState = loadChainState(ledger, blockTime);
    CoreContractState contract = chainState.getCoreContractState(address);
    if (contract == null) {
      throw new NotFoundException(
          "Unable to find contract at '%s'".formatted(address.writeAsString()));
    } else {
      StateSerializable contractState = chainState.getContractState(address);
      byte[] avlTreeValue =
          ModelFactory.getAvlTreeValue(address.getType(), contractState, treeId, key);
      if (avlTreeValue == null) {
        throw new NotFoundException(
            "Unable to find avl value for key '%s'".formatted(HexFormat.of().formatHex(key)));
      }
      return avlTreeValue;
    }
  }

  @Override
  public List<Map.Entry<byte[], byte[]>> getAvlNextN(
      BlockchainAddress address, int treeId, byte[] key, int n, int skip, Long blockTime) {
    String shardId = getTargetShard(address);
    BlockchainLedger ledger = ledgers.get(shardId);
    ImmutableChainState chainState = loadChainState(ledger, blockTime);
    CoreContractState contract = chainState.getCoreContractState(address);
    if (contract == null) {
      throw new NotFoundException(
          "Unable to find contract at '%s'".formatted(address.writeAsString()));
    } else {
      StateSerializable contractState = chainState.getContractState(address);
      return ModelFactory.createAvlTreeNextN(
          address.getType(), contractState, treeId, key, n, skip);
    }
  }

  @Override
  public List<byte[]> getAvlModifiedKeys(
      BlockchainAddress address, int treeId, long blockTime1, long blockTime2, byte[] key, int n) {
    String shardId = getTargetShard(address);
    BlockchainLedger ledger = ledgers.get(shardId);
    ImmutableChainState chainState1 = loadChainState(ledger, blockTime1);
    ImmutableChainState chainState2 = loadChainState(ledger, blockTime2);
    StateSerializable contractState1 = chainState1.getContractState(address);
    StateSerializable contractState2 = chainState2.getContractState(address);
    if (contractState1 == null || contractState2 == null) {
      throw new NotFoundException(
          "Unable to find contract at '%s'".formatted(address.writeAsString()));
    }
    return ModelFactory.createAvlTreeModifiedKeys(
        address.getType(), contractState1, contractState2, treeId, key, n);
  }

  @Override
  public int getAvlInformation(BlockchainAddress address, int treeId, Long blockTime) {
    String shardId = getTargetShard(address);
    BlockchainLedger ledger = ledgers.get(shardId);
    ImmutableChainState chainState = loadChainState(ledger, blockTime);
    CoreContractState contract = chainState.getCoreContractState(address);
    if (contract == null) {
      throw new NotFoundException(
          "Unable to find contract at '%s'".formatted(address.writeAsString()));
    } else {
      StateSerializable contractState = chainState.getContractState(address);
      return ModelFactory.getAvlInformation(address.getType(), contractState, treeId);
    }
  }

  private static JsonNode toJsonNodeIfNonNull(StateSerializable stateSerializable) {
    if (stateSerializable != null) {
      return mapper.valueToTree(stateSerializable);
    } else {
      return null;
    }
  }

  private ImmutableChainState loadChainState(BlockchainLedger ledger, Long blockTime) {
    ImmutableChainState chainState;
    if (blockTime != null) {
      chainState = ledger.getState(blockTime);
      if (chainState == null) {
        throw new BadRequestException("No chain state for block time '%d'".formatted(blockTime));
      }
    } else {
      chainState = ledger.getChainState();
    }
    return chainState;
  }
}
