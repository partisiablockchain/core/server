package com.partisiablockchain.server.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/** Information about a block producer. */
public final class BlockProducer {

  /** Number of produced blocks. */
  public long blocksProduced;

  /** Median block finalization time. */
  public double medianFinalizationTime;

  private static BlockProducer createFromBuilder(BlockProducer.Builder builder) {
    BlockProducer blockProducer = new BlockProducer();
    blockProducer.blocksProduced = builder.blocksProduced;
    Collections.sort(builder.finalizationTimes);
    blockProducer.medianFinalizationTime = findMedian(builder.finalizationTimes);
    return blockProducer;
  }

  /**
   * Find the median of a sorted list of numbers. If the size of the list of numbers is odd, we
   * return the middle one, and if the size is even, we return the mean of the two middle values.
   *
   * @param sortedNumbers the sorted list of numbers
   * @return the median
   */
  static double findMedian(List<Long> sortedNumbers) {
    if (sortedNumbers.size() == 0) {
      return -1;
    }
    int middleIndex = (int) Math.ceil((double) sortedNumbers.size() / 2) - 1;
    if (sortedNumbers.size() % 2 != 0) {
      return sortedNumbers.get(middleIndex);
    } else {
      return (double) (sortedNumbers.get(middleIndex) + sortedNumbers.get(middleIndex + 1)) / 2;
    }
  }

  /** Builder for the block producer data transfer object. */
  public static final class Builder {

    long blocksProduced;
    ArrayList<Long> finalizationTimes = new ArrayList<>();

    void incrementBlocksProduced() {
      blocksProduced++;
    }

    void addFinalizationTime(long finalizationTime) {
      if (finalizationTime > -1) {
        finalizationTimes.add(finalizationTime);
      }
    }

    /**
     * Build the block producer record.
     *
     * @return the block producer record
     */
    public BlockProducer build() {
      return createFromBuilder(this);
    }
  }
}
