package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Response to a {@link ContractStateRequest} containing the state of the contract. */
public final class ContractStateResponse implements DataStreamSerializable {

  private final ContractStateRequest request;
  private final Hash binderHash;
  private final Hash contractHash;
  private final Hash stateHash;

  /**
   * Create a new response.
   *
   * @param request the request that this is response for
   * @param binderHash the identifier of the contract binder
   * @param contractHash the identifier of the contract code
   * @param stateHash the state of the contract
   */
  public ContractStateResponse(
      ContractStateRequest request, Hash binderHash, Hash contractHash, Hash stateHash) {
    this.request = request;
    this.binderHash = binderHash;
    this.contractHash = contractHash;
    this.stateHash = stateHash;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    request.write(stream);
    binderHash.write(stream);
    contractHash.write(stream);
    stateHash.write(stream);
  }

  /**
   * Read from stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static ContractStateResponse read(SafeDataInputStream stream) {
    ContractStateRequest request = ContractStateRequest.read(stream);
    Hash binderHash = Hash.read(stream);
    Hash contractHash = Hash.read(stream);
    Hash stateHash = Hash.read(stream);
    return new ContractStateResponse(request, binderHash, contractHash, stateHash);
  }

  /**
   * Get the request corresponding to this response.
   *
   * @return the request
   */
  public ContractStateRequest getRequest() {
    return request;
  }

  /**
   * Get the state hash for the contract.
   *
   * @return the state hash
   */
  public Hash getStateHash() {
    return stateHash;
  }

  /**
   * Get the binder hash for the contract.
   *
   * @return the binder hash
   */
  public Hash getBinderHash() {
    return binderHash;
  }

  /**
   * Get the contract hash for the contract.
   *
   * @return the contract hash
   */
  public Hash getContractHash() {
    return contractHash;
  }
}
