package com.partisiablockchain.server.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/** Data transfer object for information about a committee and its producers. */
public final class Committee {

  /** Information about the active producers of the committee. */
  public Map<Short, BlockProducer> producers;

  private static Committee createFromBuilder(Committee.Builder builder) {
    Committee committee = new Committee();
    committee.producers =
        builder.producers.entrySet().stream()
            .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().build()));
    return committee;
  }

  /** Builder for the committee data transfer object. */
  public static final class Builder {

    Map<Short, BlockProducer.Builder> producers = new HashMap<>();

    void setBlockProducer(short producerIndex, BlockProducer.Builder builder) {
      producers.put(producerIndex, builder);
    }

    /**
     * Build the committee record.
     *
     * @return the committee record
     */
    public Committee build() {
      return createFromBuilder(this);
    }
  }
}
