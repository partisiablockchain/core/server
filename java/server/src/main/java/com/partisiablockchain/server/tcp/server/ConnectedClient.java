package com.partisiablockchain.server.tcp.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.ChainState;
import com.partisiablockchain.blockchain.DelayedBlockchainListener;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.StateHash;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.server.tcp.ClosableConnection;
import com.partisiablockchain.server.tcp.Connection;
import com.partisiablockchain.server.tcp.packet.ContractCreated;
import com.partisiablockchain.server.tcp.packet.ContractCreatedResponse;
import com.partisiablockchain.server.tcp.packet.ContractRemoved;
import com.partisiablockchain.server.tcp.packet.ContractStateRequest;
import com.partisiablockchain.server.tcp.packet.ContractStateResponse;
import com.partisiablockchain.server.tcp.packet.ContractUpdated;
import com.partisiablockchain.server.tcp.packet.Handshake;
import com.partisiablockchain.server.tcp.packet.Packet;
import com.partisiablockchain.server.tcp.packet.SerializedStateRequest;
import com.partisiablockchain.server.tcp.packet.SerializedStateResponse;
import com.partisiablockchain.server.tcp.threads.WaitForState;
import com.secata.tools.coverage.ThrowingRunnable;
import com.secata.tools.coverage.WithCloseableResources;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class ConnectedClient implements BlockchainLedger.Listener, WithCloseableResources {

  private static final Logger logger = LoggerFactory.getLogger(ConnectedClient.class);

  private boolean initial = true;
  private final ClosableConnection connection;
  private final BlockchainLedger ledger;
  private final Handshake handshake;
  private final Map<BlockchainAddress, WaitForState<Boolean>> waitingForContracts =
      new ConcurrentHashMap<>();
  private final Set<BlockchainAddress> listeners =
      Collections.newSetFromMap(new ConcurrentHashMap<>());

  ConnectedClient(
      InputStream input, OutputStream output, BlockchainLedger ledger, Handshake handshake) {
    this(
        (incoming, onClose) -> Connection.createConnection(incoming, input, output, onClose),
        ledger,
        handshake);
  }

  ConnectedClient(ClosableConnection connection, BlockchainLedger ledger, Handshake handshake) {
    this((incoming, onClose) -> connection, ledger, handshake);
  }

  private ConnectedClient(
      BiFunction<Consumer<Packet<?>>, ThrowingRunnable, ClosableConnection> connection,
      BlockchainLedger ledger,
      Handshake handshake) {
    this.ledger = ledger;
    this.handshake = handshake;
    DelayedBlockchainListener listener = new DelayedBlockchainListener(this);
    ledger.attach(listener);
    this.connection = connection.apply(this::incoming, () -> ledger.detach(listener));
  }

  void incoming(Packet<?> packet) {
    if (packet.getType() == Packet.Type.CONTRACT_CREATED_RESPONSE) {
      ContractCreatedResponse event = (ContractCreatedResponse) packet.getPayload();
      waitingForContracts.remove(event.getAddress()).setState(event.isRegisterListener());
    } else if (packet.getType() == Packet.Type.CONTRACT_STATE_REQUEST) {
      ContractStateRequest request = (ContractStateRequest) packet.getPayload();
      ImmutableChainState state = ledger.getState(request.getBlockTime());
      CoreContractState coreContractState = state.getCoreContractState(request.getAddress());
      connection.send(
          new Packet<>(
              Packet.Type.CONTRACT_STATE_RESPONSE,
              new ContractStateResponse(
                  request,
                  coreContractState.getBinderIdentifier(),
                  coreContractState.getContractIdentifier(),
                  StateHash.getStateHash(state, request.getAddress()))));
    } else if (packet.getType() == Packet.Type.SERIALIZED_STATE_REQUEST) {
      SerializedStateRequest request = (SerializedStateRequest) packet.getPayload();
      logger.trace("Received request for state: " + request.getHash());
      connection.send(
          new Packet<>(
              Packet.Type.SERIALIZED_STATE_RESPONSE,
              new SerializedStateResponse(ledger.getStateStorage().read(request.getHash()))));
    } else {
      throw new IllegalArgumentException(
          "Received unexpected packet type: " + packet.getType().getType());
    }
  }

  @Override
  public void newFinalState(ImmutableChainState state, BlockchainLedger.UpdateEvent contracts) {
    if (initial) {
      initial = false;
      List<BlockchainAddress> existing = handshake.getExisting();
      listeners.addAll(existing);

      HashSet<BlockchainAddress> newContracts = new HashSet<>(contracts.newContracts);
      newContracts.removeAll(existing);
      handleCreateEvents(newContracts, state);

      HashSet<BlockchainAddress> removedContracts = new HashSet<>(existing);
      removedContracts.removeAll(state.getContracts());
      handleRemoveEvents(removedContracts);

      // Trigger update for all the existing listener since they might have been updated
      // while unconnected
      HashSet<BlockchainAddress> updated = new HashSet<>(existing);
      updated.removeAll(removedContracts);
      handleUpdateEvents(state, updated);
    } else {
      handleCreateEvents(contracts.newContracts, state);

      handleUpdateEvents(state, contracts.updatedContracts);

      handleRemoveEvents(contracts.removedContract);
    }
  }

  private void handleCreateEvents(Collection<BlockchainAddress> contracts, ChainState state) {
    for (BlockchainAddress contract : contracts) {
      if (handshake.isContractEnabled(contract)) {
        // If the contract was removed in the same block as it was created this will be null
        CoreContractState coreContractState = state.getCoreContractState(contract);
        boolean exists = coreContractState != null;
        if (exists && handshake.isBinderEnabled(coreContractState.getBinderIdentifier())) {
          WaitForState<Boolean> waitObject =
              waitingForContracts.computeIfAbsent(contract, ignored -> new WaitForState<>());
          logger.trace("Sending CONTRACT_CREATED for {}", contract);
          connection.send(
              new Packet<>(
                  Packet.Type.CONTRACT_CREATED,
                  new ContractCreated(
                      contract,
                      coreContractState.getBinderIdentifier(),
                      coreContractState.getContractIdentifier(),
                      coreContractState.getAbiIdentifier(),
                      coreContractState.computeStorageLength(),
                      StateHash.getStateHash(state, contract))));
          Boolean createListener = waitObject.waitForState();
          logger.trace("Response for CONTRACT_CREATED for {}: {}", contract, createListener);
          if (createListener) {
            listeners.add(contract);
          }
        }
      }
    }
  }

  private void handleUpdateEvents(ChainState state, Collection<BlockchainAddress> contracts) {
    for (BlockchainAddress contract : contracts) {
      if (listeners.contains(contract)) {
        Hash stateHash = StateHash.getStateHash(state, contract);
        if (stateHash != null) {
          CoreContractState coreContractState = state.getCoreContractState(contract);
          connection.send(
              new Packet<>(
                  Packet.Type.CONTRACT_UPDATED,
                  new ContractUpdated(
                      contract,
                      coreContractState.getBinderIdentifier(),
                      coreContractState.getContractIdentifier(),
                      stateHash)));
        }
      }
    }
  }

  private void handleRemoveEvents(Collection<BlockchainAddress> contracts) {
    for (BlockchainAddress contract : contracts) {
      if (listeners.remove(contract)) {
        connection.send(new Packet<>(Packet.Type.CONTRACT_REMOVED, new ContractRemoved(contract)));
      }
    }
  }

  @Override
  public Stream<AutoCloseable> resources() {
    return Stream.of(connection);
  }

  public boolean isClosed() {
    return !connection.isAlive();
  }
}
