package com.partisiablockchain.server.tcp.threads;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.thread.ExecutorFactory;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Implementation of {@link ScheduledSingleThreadExecutor} based on a {@link
 * ScheduledExecutorService}.
 */
public final class ScheduledSingleThreadExecutorImpl implements ScheduledSingleThreadExecutor {

  private final ScheduledExecutorService scheduledExecutorService;

  /**
   * Construct a new single threaded scheduled executor.
   *
   * @param name name of the executor
   */
  public ScheduledSingleThreadExecutorImpl(String name) {
    scheduledExecutorService = ExecutorFactory.newScheduledSingleThread(name);
  }

  @Override
  @SuppressWarnings("FutureReturnValueIgnored")
  public void schedule(Runnable runnable, long delay, TimeUnit unit) {
    scheduledExecutorService.schedule(runnable, delay, unit);
  }

  @Override
  public void close() {
    scheduledExecutorService.shutdownNow();
  }
}
