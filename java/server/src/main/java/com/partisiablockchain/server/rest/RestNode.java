package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.server.ServerModule;
import com.partisiablockchain.server.config.ApiConfig;
import com.partisiablockchain.server.rest.resources.BlockchainResource;
import com.partisiablockchain.server.rest.resources.FeatureResource;
import com.partisiablockchain.server.rest.resources.HealthResource;
import com.partisiablockchain.server.rest.resources.MetricsResource;
import com.partisiablockchain.storage.RootDirectory;

/** Server module for exposing the reader node rest api. */
public final class RestNode implements ServerModule<ApiConfig> {

  @Override
  public void register(
      RootDirectory rootDirectory,
      BlockchainLedger blockchainLedger,
      ServerConfig serverConfig,
      ApiConfig apiConfig) {
    if (apiConfig.enabled) {
      serverConfig.registerRestComponent(new HealthResource());
      serverConfig.registerRestComponent(new FeatureResource(blockchainLedger));
      serverConfig.registerRestComponent(new BlockchainResource(blockchainLedger));
      serverConfig.registerRestComponent(new MetricsResource(blockchainLedger));
    }
  }

  @Override
  public Class<ApiConfig> configType() {
    return ApiConfig.class;
  }
}
