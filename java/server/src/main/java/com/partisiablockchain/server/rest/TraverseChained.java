package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.dto.traversal.AvlTraverse;
import com.partisiablockchain.dto.traversal.FieldTraverse;
import com.partisiablockchain.dto.traversal.Traverse;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.ExceptionConverter;
import java.lang.reflect.Field;
import java.util.List;

/** Helper class for traversing the chain. */
public abstract class TraverseChained implements TraverseChainElement {

  private final TraverseChainElement next;

  private TraverseChained(TraverseChainElement next) {
    this.next = next;
  }

  /**
   * Takes a list of Traverse and reverse iterates it to build a new chained Traverse structure
   * where each traverse points to their next. The final - root object is returned which is the
   * first in the list and should be resolved first.
   *
   * @param path the the path to convert
   * @return the root of the traverse stack
   */
  public static TraverseChainElement create(List<Traverse> path) {
    TraverseChainElement previous = object -> object;
    for (int i = path.size() - 1; i >= 0; i--) {
      Traverse traverseRecord = path.get(i);
      previous = create(traverseRecord, previous);
    }
    return previous;
  }

  private static TraverseChained create(Traverse traverse, TraverseChainElement next) {
    if (traverse instanceof FieldTraverse) {
      return new FieldTraversal((FieldTraverse) traverse, next);
    } else if (traverse instanceof AvlTraverse) {
      return new AvlTraversal((AvlTraverse) traverse, next);
    } else {
      throw new IllegalArgumentException("Invalid Traverse record: " + traverse);
    }
  }

  @Override
  public Object traversePath(Object object) {
    Object traverse = traverseObject(object);
    return next.traversePath(traverse);
  }

  /**
   * Abstract method for traversing an object.
   *
   * @param object to be traversed
   * @return the object which was traversed to
   */
  protected abstract Object traverseObject(Object object);

  static final class AvlTraversal extends TraverseChained {

    private AvlTraverse avlTraverse;

    AvlTraversal(AvlTraverse avlTraverse, TraverseChainElement next) {
      super(next);
      this.avlTraverse = avlTraverse;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object traverseObject(Object object) {
      return ((AvlTree) object).getValue(avlTraverse.keyType().convert(avlTraverse.key()));
    }
  }

  static final class FieldTraversal extends TraverseChained {

    private FieldTraverse fieldTraverse;

    FieldTraversal(FieldTraverse fieldTraverse, TraverseChainElement next) {
      super(next);
      this.fieldTraverse = fieldTraverse;
    }

    @Override
    public Object traverseObject(Object current) {
      Class<?> clazz = current.getClass();
      Field field =
          ExceptionConverter.call(
              () -> clazz.getDeclaredField(fieldTraverse.name()), "Field does not exist");
      field.setAccessible(true);
      return ExceptionConverter.call(() -> field.get(current), "Could not access field");
    }
  }
}
