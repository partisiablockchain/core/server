package com.partisiablockchain.server.tcp.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateStorage;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

final class FetchingStateStorage implements StateStorage {

  private static final Logger logger = LoggerFactory.getLogger(FetchingStateStorage.class);
  private final Consumer<Hash> fetchCallback;
  private final Set<Hash> fetching = new HashSet<>();
  private final StateStorage delegate;

  FetchingStateStorage(Consumer<Hash> fetchCallback, StateStorage delegate) {
    this.fetchCallback = Objects.requireNonNull(fetchCallback);
    this.delegate = Objects.requireNonNull(delegate);
  }

  @Override
  public boolean write(Hash hash, Consumer<SafeDataOutputStream> consumer) {
    throw new UnsupportedOperationException("Writing is not permitted");
  }

  @Override
  public <S> S read(Hash hash, Function<SafeDataInputStream, S> function) {
    S read = delegate.read(hash, function);
    if (read == null) {
      fetch(hash);
      return delegate.read(hash, function);
    } else {
      return read;
    }
  }

  private synchronized void fetch(Hash hash) {
    if (fetching.add(hash)) {
      fetchCallback.accept(hash);
    }
    while (fetching.contains(hash)) {
      ExceptionConverter.run(this::wait, "Unexpected interrupt");
    }
  }

  synchronized void received(byte[] bytes) {
    Hash hash = Hash.create(s -> s.write(bytes));
    logger.trace("Received state for {}", hash);
    delegate.write(hash, s -> s.write(bytes));
    fetching.remove(hash);
    this.notifyAll();
  }

  public synchronized Set<Hash> fetching() {
    return Set.copyOf(fetching);
  }
}
