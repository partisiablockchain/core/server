package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import java.util.function.Predicate;

/** Searcher for a traversal of blocks. */
public final class BlockSearcher {
  private final Predicate<Block> searchFunction;

  private BlockSearcher(Predicate<Block> searchFunction) {
    this.searchFunction = searchFunction;
  }

  /**
   * Create a new searcher for blocks.
   *
   * @param searchFunction function applied to a searched block indicating if the search is over
   * @return the searcher
   */
  public static BlockSearcher create(Predicate<Block> searchFunction) {
    return new BlockSearcher(searchFunction);
  }

  /**
   * Handle the current block and indicate if search is done.
   *
   * @param block the block to handle
   * @return true if the traversal is done otherwise false
   */
  public boolean handleBlock(Block block) {
    return searchFunction.test(block);
  }
}
