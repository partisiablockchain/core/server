package com.partisiablockchain.server.tcp.threads;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionLogger;
import java.io.Closeable;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** An object that ensures that a collection of closeables are only closed once. */
public final class OneTimeClosable implements Closeable {

  private static final Logger logger = LoggerFactory.getLogger(OneTimeClosable.class);

  private final List<AutoCloseable> closeables;
  private boolean closed = false;

  /**
   * Create a new OneTimeClosable.
   *
   * @param closeables the list of closable that are wrapped
   */
  public OneTimeClosable(List<AutoCloseable> closeables) {
    this.closeables = closeables;
  }

  @Override
  public synchronized void close() {
    if (!closed) {
      closed = true;
      closeables.forEach(
          closeable ->
              ExceptionLogger.handle(logger::warn, closeable::close, "Unable to close resource"));
    }
  }

  /**
   * Has this been closed.
   *
   * @return true if already closed
   */
  public boolean isClosed() {
    return closed;
  }
}
