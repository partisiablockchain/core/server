package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/** Packet to request the state of a contract. */
public final class ContractStateRequest implements DataStreamSerializable {

  private final BlockchainAddress address;
  private final long blockTime;

  /**
   * Create a new request.
   *
   * @param address the contract to request the state of
   * @param blockTime the block time to get the state for
   */
  public ContractStateRequest(BlockchainAddress address, long blockTime) {
    this.address = address;
    this.blockTime = blockTime;
  }

  /**
   * Get the address of the request.
   *
   * @return the address
   */
  public BlockchainAddress getAddress() {
    return address;
  }

  /**
   * Get the block time of the request.
   *
   * @return the block time
   */
  public long getBlockTime() {
    return blockTime;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    address.write(stream);
    stream.writeLong(blockTime);
  }

  /**
   * Read from stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static ContractStateRequest read(SafeDataInputStream stream) {
    BlockchainAddress address = BlockchainAddress.read(stream);
    long blockTime = stream.readLong();
    return new ContractStateRequest(address, blockTime);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContractStateRequest that = (ContractStateRequest) o;
    return blockTime == that.blockTime && Objects.equals(address, that.address);
  }

  @Override
  public int hashCode() {
    return Objects.hash(address, blockTime);
  }
}
