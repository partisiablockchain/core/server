package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.BlockchainLedger.Listener;
import com.partisiablockchain.blockchain.BlockchainLedger.UpdateEvent;
import com.partisiablockchain.blockchain.ChainState;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.server.ServerModule.ContractListener;
import com.partisiablockchain.server.ServerModule.CreateContractListener;
import com.secata.tools.thread.SharedPoolSingleThreadedExecution;
import com.secata.tools.thread.SharedPoolSingleThreadedExecution.SingleThreadExecutor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class ContractDispatcher implements Listener {

  private final SharedPoolSingleThreadedExecution singleThreadedExecution;
  private final List<CreateContractListener> createContractListeners;
  private final Map<BlockchainAddress, ContractEventDispatcher> listeners;

  ContractDispatcher(List<CreateContractListener> createContractListeners) {
    this.createContractListeners = createContractListeners;
    this.listeners = new HashMap<>();
    this.singleThreadedExecution =
        SharedPoolSingleThreadedExecution.named(getClass().getSimpleName());
  }

  @Override
  public void newFinalState(ImmutableChainState state, UpdateEvent contracts) {
    handleCreateEvents(contracts.newContracts, state);

    handleUpdateEvents(state, contracts.updatedContracts);

    handleRemoveEvents(contracts.removedContract);
  }

  private void handleCreateEvents(Collection<BlockchainAddress> contracts, ChainState state) {
    for (BlockchainAddress contract : contracts) {
      CoreContractState coreContractState = state.getCoreContractState(contract);
      StateSerializable contractState = state.getContractState(contract);
      List<ContractListener> contractListeners = new ArrayList<>();
      for (CreateContractListener listener : createContractListeners) {
        ContractListener listenerContract =
            listener.createContract(contract, coreContractState, contractState);
        if (listenerContract != null) {
          contractListeners.add(listenerContract);
        }
      }
      if (!contractListeners.isEmpty()) {
        ContractEventDispatcher contractEventDispatcher =
            new ContractEventDispatcher(
                singleThreadedExecution.createSingleThreadedExecutor(),
                contractListeners,
                coreContractState,
                contractState);
        listeners.put(contract, contractEventDispatcher);
      }
    }
  }

  private void handleUpdateEvents(ChainState state, Collection<BlockchainAddress> contracts) {
    for (BlockchainAddress contract : contracts) {
      if (listeners.get(contract) != null) {
        listeners
            .get(contract)
            .update(state.getCoreContractState(contract), state.getContractState(contract));
      }
    }
  }

  private void handleRemoveEvents(Collection<BlockchainAddress> contracts) {
    for (BlockchainAddress contract : contracts) {
      if (listeners.get(contract) != null) {
        listeners.remove(contract).remove();
      }
    }
  }

  static final class ContractEventDispatcher {

    private final List<ContractListener> contractListeners;
    private final SingleThreadExecutor executor;

    ContractEventDispatcher(
        SingleThreadExecutor singleThreadedExecutor,
        List<ContractListener> contractListeners,
        CoreContractState coreContractState,
        StateSerializable contractState) {
      this.executor = singleThreadedExecutor;
      this.contractListeners = List.copyOf(contractListeners);
      update(coreContractState, contractState);
    }

    @SuppressWarnings("FutureReturnValueIgnored")
    void remove() {
      executor.execute(
          () -> {
            contractListeners.forEach(ContractListener::remove);
            return null;
          });
    }

    @SuppressWarnings("FutureReturnValueIgnored")
    void update(CoreContractState coreContractState, StateSerializable contractState) {
      executor.execute(
          () -> {
            contractListeners.forEach(
                contractListener -> contractListener.update(coreContractState, contractState));
            return null;
          });
    }
  }
}
