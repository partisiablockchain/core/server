package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.FailureCause;
import com.partisiablockchain.blockchain.TransactionCost;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutableTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.ContractType;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.server.model.ResetBlock;
import com.partisiablockchain.server.rest.resources.BlockchainResource;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import jakarta.ws.rs.NotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/** Factory to help with creating objects. */
public final class ModelFactory {

  static final ObjectMapper CONTRACT_MAPPER = StateObjectMapper.createObjectMapper();

  private ModelFactory() {}

  /**
   * Create a ExecutedTransaction transfer record.
   *
   * @param transaction which should be transferred
   * @param block which should be transferred
   * @param finalized whether it is finialized or not
   * @param events which should be transfered
   * @param transactionCostLookup the function for looking up transaction cost
   * @param failureLookup for looking up the failure, if the transaction did not succeed
   * @return the ExexutedTransaction record
   */
  public static com.partisiablockchain.dto.ExecutedTransaction createExecutedTransaction(
      ExecutedTransaction transaction,
      Block block,
      boolean finalized,
      Function<Collection<Hash>, List<Event>> events,
      Function<Hash, TransactionCost> transactionCostLookup,
      Function<Hash, FailureCause> failureLookup) {
    String blockHashString = transaction.getBlockHash().toString();
    long blockTime = block.getBlockTime();
    long productionTime = block.getProductionTime();
    boolean executionSucceeded = transaction.didExecutionSucceed();
    ExecutableTransaction executableTransaction = transaction.getInner();
    byte[] transactionPayload = SafeDataOutputStream.serialize(executableTransaction::write);
    String identifier = executableTransaction.identifier().toString();

    boolean isEvent = false;
    String from = null;
    if (executableTransaction instanceof SignedTransaction) {
      from = ((SignedTransaction) executableTransaction).getSender().writeAsString();
      isEvent = false;
    } else {
      isEvent = true;
    }
    List<Event> newEvents = events.apply(transaction.getEvents());

    com.partisiablockchain.dto.FailureCause failureCause = null;
    if (!transaction.didExecutionSucceed()) {
      final FailureCause failureCauseLookup =
          failureLookup.apply(transaction.getInner().identifier());
      com.partisiablockchain.dto.FailureCause theFailureCause =
          new com.partisiablockchain.dto.FailureCause(
              failureCauseLookup.getErrorMessage(), failureCauseLookup.getStackTrace());
      failureCause = theFailureCause;
    }

    com.partisiablockchain.dto.TransactionCost cost =
        createTransactionCost(transactionCostLookup.apply(executableTransaction.identifier()));

    com.partisiablockchain.dto.ExecutedTransaction executedTransaction =
        new com.partisiablockchain.dto.ExecutedTransaction(
            transactionPayload,
            blockHashString,
            productionTime,
            blockTime,
            from,
            identifier,
            executionSucceeded,
            newEvents,
            finalized,
            isEvent,
            cost,
            failureCause);
    return executedTransaction;
  }

  /**
   * Create a transaction cost.
   *
   * @param cost to be transferred
   * @return the transaction cost
   */
  public static com.partisiablockchain.dto.TransactionCost createTransactionCost(
      TransactionCost cost) {
    if (cost == null) {
      return null;
    }

    return new com.partisiablockchain.dto.TransactionCost(
        cost.getAllocatedForEvents(),
        cost.getCpu(),
        cost.getRemaining(),
        cost.getPaidByContract(),
        cost.getNetworkCosts());
  }

  /**
   * Create a Block transfer record.
   *
   * @param block to be transferred
   * @return the Block record
   */
  public static com.partisiablockchain.dto.BlockState createBlock(Block block) {
    if (block == null) {
      throw new NotFoundException();
    }
    return new com.partisiablockchain.dto.BlockState(
        block.getParentBlock().toString(),
        block.getCommitteeId(),
        block.getBlockTime(),
        block.getProductionTime(),
        block.getTransactions().stream().map(Hash::toString).collect(Collectors.toList()),
        block.getEventTransactions().stream().map(Hash::toString).collect(Collectors.toList()),
        block.identifier().toString(),
        block.getState().toString(),
        block.getProducerIndex());
  }

  /**
   * Create a new reset block.
   *
   * @param resetBlock the reset block
   * @param previousBlock previous block
   * @return a reset block containing the two blocks
   */
  public static ResetBlock createResetBlock(Block resetBlock, Block previousBlock) {
    return new ResetBlock(createBlock(resetBlock), createBlock(previousBlock));
  }

  /**
   * Create a account transfer record.
   *
   * @param accountState to be transferred
   * @return the AccountState record
   */
  public static com.partisiablockchain.dto.AccountState createAccountState(
      AccountState accountState) {
    if (accountState == null) {
      throw new NotFoundException();
    }
    return new com.partisiablockchain.dto.AccountState(accountState.getNonce());
  }

  /**
   * Create a contractState tranfer record.
   *
   * @param address of the contract state
   * @param coreContractState state of the core contract
   * @param state of the contract
   * @param abi of the contract
   * @param storageLength of the contract
   * @param stateOutput serialize state to abi state format
   * @return the ContractState record
   */
  public static com.partisiablockchain.dto.ContractState createContractState(
      BlockchainAddress address,
      CoreContractState coreContractState,
      StateSerializable state,
      LargeByteArray abi,
      Long storageLength,
      BlockchainResource.StateOutput stateOutput) {
    ContractType type = ContractType.fromAddress(address);
    String jarHash = coreContractState.getContractIdentifier().toString();
    JsonNode jsonState;
    if (stateOutput == BlockchainResource.StateOutput.NONE || state == null) {
      jsonState = JsonNodeFactory.instance.nullNode();
    } else if (stateOutput == BlockchainResource.StateOutput.JSON) {
      jsonState = CONTRACT_MAPPER.valueToTree(state);
    } else if (stateOutput == BlockchainResource.StateOutput.AVL_BINARY) {
      jsonState = serializeAvlBinary(state, type);
    } else if (address.getType() == BlockchainAddress.Type.CONTRACT_PUBLIC) {
      jsonState = CONTRACT_MAPPER.valueToTree(state).get("state").get("data");
    } else {
      jsonState = JsonNodeFactory.instance.binaryNode(StateAbiSerializer.serialize(state));
    }
    return new com.partisiablockchain.dto.ContractState(
        type, address.writeAsString(), jarHash, storageLength, jsonState, abi.getData());
  }

  /**
   * Serializes state to avl binary format.
   *
   * @param state state to serialize.
   * @param type type of the contract.
   * @return serialized state.
   */
  private static JsonNode serializeAvlBinary(StateSerializable state, ContractType type) {
    ObjectNode json = JsonNodeFactory.instance.objectNode();
    if (type == ContractType.PUBLIC) {
      StateAccessor stateAccessor = StateAccessor.create(state);
      json.put("state", stateAccessor.get("state").cast(LargeByteArray.class).getData());
      if (stateAccessor.hasField("avlTrees")) {
        byte[] avlBytes = getAvlBytes(state);
        json.put("avlTrees", avlBytes);
      }
    } else if (type == ContractType.ZERO_KNOWLEDGE) {
      StateAccessor stateAccessor = StateAccessor.create(state).get("openState");
      json.put("state", stateAccessor.get("openState").cast(LargeByteArray.class).getData());
      if (stateAccessor.hasField("avlTrees")) {
        byte[] avlBytes = getAvlBytes(stateAccessor.cast(StateSerializable.class));
        json.put("avlTrees", avlBytes);
      }
    } else {
      json.put("state", StateAbiSerializer.serialize(state));
    }
    return json;
  }

  private static byte[] getAvlBytes(StateSerializable state) {
    StateAccessor stateAccessor = StateAccessor.create(state);
    AvlTree<?, ?> avlTree = stateAccessor.get("avlTrees").cast(AvlTree.class);
    Type genericType =
        ExceptionConverter.call(
            () -> state.getClass().getDeclaredField("avlTrees").getGenericType());
    return StateAbiSerializer.serializeAvlTree(avlTree, genericType);
  }

  /**
   * Create an event from a executable event.
   *
   * @param event The executable event, which needs to be created
   * @return The created event
   */
  public static Event createEvent(ExecutableEvent event) {
    if (event == null) {
      throw new NotFoundException();
    }
    return new Event(event.identifier().toString(), event.getEvent().getDestinationShard());
  }

  /**
   * Creates a list of n next entries in the given avl tree based from the given key skipping 'skip'
   * number.
   *
   * @param <T> type of the key in the Avl Tree. Should always be ComparableByteArray.
   * @param addressType type of address. Can be public of zk contract.
   * @param state contract state.
   * @param treeId tree id of avl tree.
   * @param key key to base the search from.
   * @param n number of entries to get.
   * @param skip number of elements to skip
   * @return list of n next entries.
   */
  public static <T extends Comparable<T>> List<Map.Entry<byte[], byte[]>> createAvlTreeNextN(
      BlockchainAddress.Type addressType,
      StateSerializable state,
      int treeId,
      byte[] key,
      int n,
      int skip) {
    StateAccessor innerTree = getAvlTree(addressType, state, treeId);

    Class<T> keyType = getKeyType(innerTree);
    AvlTree<T, LargeByteArray> tree =
        innerTree.get("avlTree").typedAvlTree(keyType, LargeByteArray.class);
    T actualKey = convertAvlKey(keyType, key);
    List<Map.Entry<T, LargeByteArray>> nextN = tree.getNextN(actualKey, n, skip);

    return nextN.stream()
        .map(
            entry ->
                Map.entry(
                    StateAccessor.create((StateSerializable) entry.getKey())
                        .get("data")
                        .cast(LargeByteArray.class)
                        .getData(),
                    entry.getValue().getData()))
        .toList();
  }

  /**
   * Gets the value corresponding to the given key.
   *
   * @param <T> type of the key in the Avl Tree. Should always be ComparableByteArray.
   * @param addressType type of address. Can be public of zk contract.
   * @param state contract state.
   * @param treeId tree id of avl tree.
   * @param key key to base the search from.
   * @return the value corresponding to the key or null if it doesn't exist.
   */
  public static <T extends Comparable<T>> byte[] getAvlTreeValue(
      BlockchainAddress.Type addressType, StateSerializable state, int treeId, byte[] key) {
    StateAccessor innerTree = getAvlTree(addressType, state, treeId);
    Class<T> keyType = getKeyType(innerTree);
    AvlTree<T, LargeByteArray> tree =
        innerTree.get("avlTree").typedAvlTree(keyType, LargeByteArray.class);
    T actualKey = convertAvlKey(keyType, key);
    LargeByteArray large = tree.getValue(actualKey);

    return large == null ? null : large.getData();
  }

  /**
   * Gets the modified keys of avl tree with tree id between two states.
   *
   * @param <T> type of the key in the Avl Tree. Should always be ComparableByteArray.
   * @param addressType type of address. Can be public of zk contract.
   * @param state1 first contract state
   * @param state2 second contract state
   * @param treeId tree id of avl tree
   * @param key key to base the search from
   * @param n max number of elements to get
   * @return list of at most n modified keys
   */
  public static <@ImmutableTypeParameter T extends Comparable<T>>
      List<byte[]> createAvlTreeModifiedKeys(
          BlockchainAddress.Type addressType,
          StateSerializable state1,
          StateSerializable state2,
          int treeId,
          byte[] key,
          int n) {
    StateAccessor innerTree1 = getAvlTree(addressType, state1, treeId);
    StateAccessor innerTree2 = getAvlTree(addressType, state2, treeId);

    Class<T> keyType = getKeyType(innerTree1);
    AvlTree<T, LargeByteArray> tree1 =
        innerTree1.get("avlTree").typedAvlTree(keyType, LargeByteArray.class);
    AvlTree<T, LargeByteArray> tree2 =
        innerTree2.get("avlTree").typedAvlTree(keyType, LargeByteArray.class);
    T actualKey = convertAvlKey(keyType, key);
    List<T> keysDiff = AvlTree.modifiedKeys(tree1, tree2, actualKey, n);
    return keysDiff.stream()
        .map(
            k ->
                StateAccessor.create((StateSerializable) k)
                    .get("data")
                    .cast(LargeByteArray.class)
                    .getData())
        .toList();
  }

  /**
   * Get size of an avl tree map.
   *
   * @param addressType type of address. Can be public of zk contract.
   * @param state contract state.
   * @param treeId tree id of avl tree.
   * @return size of the avl tree map
   */
  public static int getAvlInformation(
      BlockchainAddress.Type addressType, StateSerializable state, int treeId) {
    StateAccessor avlTree = getAvlTree(addressType, state, treeId);
    return avlTree.get("avlTree").cast(AvlTree.class).size();
  }

  /**
   * Gets the avl tree with given tree id of the contract state.
   *
   * @param addressType type of contract.
   * @param state state of the contract
   * @param treeId tree id of desired avl tree.
   * @return the inner avl tree.
   */
  private static StateAccessor getAvlTree(
      BlockchainAddress.Type addressType, StateSerializable state, int treeId) {
    StateAccessor stateAccessor = StateAccessor.create(state);
    if (addressType == BlockchainAddress.Type.CONTRACT_ZK) {
      stateAccessor = stateAccessor.get("openState");
    }
    if (!stateAccessor.hasField("avlTrees")) {
      throw new NotFoundException("Contract does not contain avl trees");
    }
    StateAccessor innerTree = stateAccessor.get("avlTrees").getTreeValue(treeId);
    if (innerTree == null) {
      throw new NotFoundException("Avl Tree does not contain tree id " + treeId);
    }
    return innerTree;
  }

  /**
   * Get the key type of the avl tree. Should always be ComparableByteArray.
   *
   * @param <T> type of the key in the Avl Tree. Should always be ComparableByteArray.
   * @param innerTree the inner tree.
   * @return the key type.
   */
  private static <T extends Comparable<T>> Class<T> getKeyType(StateAccessor innerTree) {
    Type genericType =
        ExceptionConverter.call(
            () ->
                innerTree
                    .cast(StateSerializable.class)
                    .getClass()
                    .getDeclaredField("avlTree")
                    .getGenericType());
    Type[] typeArguments = ((ParameterizedType) genericType).getActualTypeArguments();
    @SuppressWarnings("unchecked")
    Class<T> keyType = (Class<T>) typeArguments[0];
    return keyType;
  }

  /**
   * Convert key bytes into an object of keyType.
   *
   * @param <T> type of the key in the Avl Tree. Should always be ComparableByteArray.
   * @param keyType type of T
   * @param key the key as bytes
   * @return the avl key
   */
  private static <T extends Comparable<T>> T convertAvlKey(Class<T> keyType, byte[] key) {
    if (key == null) {
      return null;
    } else {
      Constructor<T> cons =
          ExceptionConverter.call(() -> keyType.getDeclaredConstructor(byte[].class));
      return ExceptionConverter.call(() -> cons.newInstance(key));
    }
  }
}
