package com.partisiablockchain.server.rest.resources;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.account.AccountState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.AvlStateValue;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.dto.Feature;
import com.partisiablockchain.dto.IncomingTransaction;
import com.partisiablockchain.dto.JarState;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.server.rest.ModelFactory;
import com.partisiablockchain.server.rest.TraverseChainElement;
import com.partisiablockchain.server.rest.TraverseChained;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HexFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Main public-facing REST API for {@link BlockchainLedger} inspection and interactions.
 *
 * <p>This REST resource allows for inspecting the current blockchain state (fx. {@link
 * getContract}), and for interacting with the blockchain by sending transactions ({@link
 * #pushTransaction}).
 */
@Path("/blockchain")
public final class BlockchainResource {

  private static final Logger logger = LoggerFactory.getLogger(BlockchainResource.class);
  private final BlockchainLedger blockchain;
  private final StateSerializer stateSerializer;

  /**
   * Create a new blockchain resource.
   *
   * @param blockchain the blockchain to access. Not nullable.
   */
  public BlockchainResource(BlockchainLedger blockchain) {
    this.blockchain = blockchain;
    stateSerializer = new StateSerializer(blockchain.getStateStorage(), true);
  }

  /**
   * This is a status page for uptime statistics.
   *
   * @return 204 No Content when everything is good
   */
  @GET
  public Response status() {
    return Response.noContent().build();
  }

  /**
   * Get the chain identifier for the running node.
   *
   * @return the chain id as a record
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/chainId")
  public ChainId chainId() {
    return new ChainId(blockchain.getChainId());
  }

  /**
   * Given an account address this method returns the account state for the given account. If the
   * account does not exist the method yields a 404 Not Found.
   *
   * @param address the address of the account state
   * @return the account state
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/account/{address}")
  public com.partisiablockchain.dto.AccountState getAccountState(
      @PathParam("address") String address) {
    AccountState state = blockchain.getAccountState(BlockchainAddress.fromString(address));
    return ModelFactory.createAccountState(state);
  }

  /**
   * Retrieve sub-state of the account plugin's global state by traversing its fields.
   *
   * @param path the record containing the path to the desired sub-state
   * @return the sub-state as a JsonNode
   */
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/accountPlugin/global")
  public JsonNode traverseGlobalAccountState(TraversePath path) {
    return traverseState(
        path, blockchain.getChainState().getGlobalPluginState(ChainPluginType.ACCOUNT));
  }

  /**
   * Retrieve sub-state of the account plugin's local state by traversing its fields.
   *
   * @param path the record containing the path to the desired sub-state
   * @param blockTime get the contract state for the given block time
   * @return the sub-state as a JsonNode
   */
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/accountPlugin/local")
  public JsonNode traverseLocalAccountState(
      TraversePath path, @DefaultValue("-1") @QueryParam("blockTime") long blockTime) {
    ImmutableChainState chainState = loadChainState(blockTime);
    return traverseState(path, chainState.getLocalAccountPluginState());
  }

  /**
   * Retrieve sub-state of the consensus plugin's global state by traversing its fields.
   *
   * @param path the record containing the path to the desired sub-state
   * @return the sub-state as a JsonNode
   */
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/consensusPlugin/global")
  public JsonNode traverseGlobalConsensusState(TraversePath path) {
    StateSerializable globalConsensusPluginState =
        blockchain.getChainState().getGlobalPluginState(ChainPluginType.CONSENSUS);
    return traverseState(path, globalConsensusPluginState);
  }

  /**
   * Retrieve sub-state of the consensus plugin's local state by traversing its fields.
   *
   * @param path the record containing the path to the desired sub-state
   * @return the sub-state as a JsonNode
   */
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/consensusPlugin/local")
  public JsonNode traverseLocalConsensusState(TraversePath path) {
    StateSerializable localConsensusPluginState =
        blockchain.getChainState().getLocalConsensusPluginState();
    return traverseState(path, localConsensusPluginState);
  }

  private JsonNode traverseState(TraversePath path, Object pluginState) {
    ObjectMapper objectMapper = StateObjectMapper.createObjectMapper();
    TraverseChainElement root = TraverseChained.create(path.path());
    return objectMapper.valueToTree(root.traversePath(pluginState));
  }

  /**
   * Get the consensus plugin jar for the running chain.
   *
   * @return the consensus plugin jar data object
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/consensusJar")
  public JarState consensusJar() {
    LargeByteArray consensusPluginJar =
        blockchain.getChainState().getPluginJar(ChainPluginType.CONSENSUS);
    if (consensusPluginJar == null) {
      throw new NotFoundException();
    } else {
      return new JarState(consensusPluginJar.getData());
    }
  }

  /**
   * Retrieve the value of a feature from the blockchain.
   *
   * @param feature the key of the feature
   * @return the current value of theo feature
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/features/{feature}")
  public Feature getFeatures(@PathParam("feature") String feature) {
    return new Feature(feature, blockchain.getChainState().getFeature(feature));
  }

  /**
   * Retrieve the latest block for the running chain.
   *
   * @return the block
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks/latest")
  public com.partisiablockchain.dto.BlockState getLatestBlock() {
    return ModelFactory.createBlock(blockchain.getLatestBlock());
  }

  /**
   * Retrieve the latest n blocks before a given included blockTime. If only k &lt; n blocks
   * occurred k blocks will be returned.
   *
   * @param number number of blocks to get, must be smaller than 100.
   * @param blockTime the blockTime to search from (included).
   * @return the list of blocks
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks/{number}/{blockTime}")
  public List<com.partisiablockchain.dto.BlockState> getBlocksBeforeBlockTime(
      @PathParam("number") int number, @PathParam("blockTime") long blockTime) {
    if (number > 100) {
      throw new BadRequestException("Can only fetch up to 100 blocks.");
    }
    List<com.partisiablockchain.dto.BlockState> blocks = new ArrayList<>();
    for (int i = 0; i < number; i++) {
      Block block = blockchain.getBlock(blockTime - i);
      blocks.add(ModelFactory.createBlock(block));
    }
    return blocks;
  }

  /**
   * Retrieves a block from a hash.
   *
   * @param hash the hash of the block
   * @return the block
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks/{blockHash}")
  public com.partisiablockchain.dto.BlockState getBlock(@PathParam("blockHash") String hash) {
    return ModelFactory.createBlock(blockchain.getBlock(Hash.fromString(hash)));
  }

  /**
   * Retrieves a contract state given a contract address.
   *
   * <p>404 Not Found if the contract does not exist.
   *
   * <p>400 Bad Request if the address specified is of the wrong type e.g. an account address.
   *
   * @param address the address of the contract
   * @param blockTime get the contract state for the given block time
   * @param requireContractState require serializedContract JsonNode in the returned record
   * @param stateOutput what format to output the state in
   * @return the contract state
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/contracts/{address}")
  public ContractState getContract(
      @PathParam("address") String address,
      @DefaultValue("-1") @QueryParam("blockTime") long blockTime,
      @DefaultValue("true") @QueryParam("requireContractState") boolean requireContractState,
      @DefaultValue("DEFAULT") @QueryParam("stateOutput") StateOutput stateOutput) {
    BlockchainAddress key = BlockchainAddress.fromString(address);
    ImmutableChainState chainState = loadChainState(blockTime);
    CoreContractState coreContractState = chainState.getCoreContractState(key);
    assertContractAddress(key, coreContractState);
    LargeByteArray abi =
        stateSerializer.read(coreContractState.getAbiIdentifier(), LargeByteArray.class);
    if (stateOutput == StateOutput.DEFAULT) {
      if (requireContractState) {
        stateOutput = StateOutput.JSON;
      } else {
        stateOutput = StateOutput.NONE;
      }
    }
    if (stateOutput != StateOutput.NONE) {
      return ModelFactory.createContractState(
          key,
          coreContractState,
          chainState.getContractState(key),
          abi,
          chainState.getContractStorageLength(key),
          stateOutput);
    } else {
      return ModelFactory.createContractState(
          key, coreContractState, null, abi, chainState.getContractStorageLength(key), stateOutput);
    }
  }

  /** Type of state to be returned when querying getContract. */
  public enum StateOutput {
    /** Output state in JSON format. */
    JSON,
    /** Output state in binary format. */
    BINARY,
    /** Output state with serialized AvlTrees. */
    AVL_BINARY,
    /** Do not output state. */
    NONE,
    /** Default value, will default to requireContractState parameter. */
    DEFAULT;

    /**
     * Create a StateOutput from a case-insensitive string.
     *
     * @param value input string.
     * @return StateOutput corresponding to the input. Returns null if invalid input.
     */
    public static StateOutput fromString(String value) {
      try {
        return valueOf(value.toUpperCase(Locale.ENGLISH));
      } catch (Exception e) {
        return null;
      }
    }
  }

  /**
   * Retrieves the next n entries of an avl tree from a key (Excluding the key itself).
   *
   * @param address the address of the contract.
   * @param blockTime get the contract state for the given block time.
   * @param treeId the tree id of the avl tree.
   * @param key the key to base the search from.
   * @param n the number of elements to get.
   * @param skip the number of elements to skip before collecting entries
   * @return the next n entries.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/contracts/{address}/avl/{treeId}/next/{key}")
  public List<Map.Entry<byte[], byte[]>> getContractAvlNextN(
      @PathParam("address") String address,
      @DefaultValue("-1") @QueryParam("blockTime") long blockTime,
      @PathParam("treeId") int treeId,
      @PathParam("key") String key,
      @DefaultValue("10") @QueryParam("n") int n,
      @DefaultValue("0") @QueryParam("skip") int skip) {
    return getAvlNextN(address, blockTime, treeId, HexFormat.of().parseHex(key), n, skip);
  }

  /**
   * Retrieves the first n entries of an avl tree.
   *
   * @param address the address of the contract.
   * @param blockTime get the contract state for the given block time.
   * @param treeId the tree id of the avl tree.
   * @param n the number of elements to get. The max allowed is 100.
   * @param skip the number of elements to skip before collecting entries
   * @return the next n entries.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/contracts/{address}/avl/{treeId}/next")
  public List<Map.Entry<byte[], byte[]>> getContractAvlFirstN(
      @PathParam("address") String address,
      @DefaultValue("-1") @QueryParam("blockTime") long blockTime,
      @PathParam("treeId") int treeId,
      @DefaultValue("10") @QueryParam("n") int n,
      @DefaultValue("0") @QueryParam("skip") int skip) {
    return getAvlNextN(address, blockTime, treeId, null, n, skip);
  }

  /**
   * Retrieves the next n entries of an avl tree from a key (Excluding the key itself). If key is
   * null, gets the first n entries.
   *
   * @param address the address of the contract.
   * @param blockTime get the contract state for the given block time.
   * @param treeId the tree id of the avl tree.
   * @param key the key to base the search from.
   * @param n the number of elements to get. The max allowed is 100.
   * @return the next n entries.
   */
  private List<Map.Entry<byte[], byte[]>> getAvlNextN(
      String address, long blockTime, int treeId, byte[] key, int n, int skip) {
    BlockchainAddress addressKey = BlockchainAddress.fromString(address);
    ImmutableChainState chainState = loadChainState(blockTime);
    CoreContractState coreContractState = chainState.getCoreContractState(addressKey);
    assertContractAddress(addressKey, coreContractState);
    if (n > 100) {
      throw new BadRequestException("Can fetch at most 100 entries");
    }
    StateSerializable state = chainState.getContractState(addressKey);
    return ModelFactory.createAvlTreeNextN(addressKey.getType(), state, treeId, key, n, skip);
  }

  /**
   * Get value in an avl tree from a key. Throws 404 if key is not present in tree.
   *
   * @param address the address of the contract.
   * @param blockTime get the contract state for the given block time.
   * @param treeId the tree id of the avl tree.
   * @param key the key.
   * @return the value.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/contracts/{address}/avl/{treeId}/{key}")
  public AvlStateValue getContractAvlValue(
      @PathParam("address") String address,
      @DefaultValue("-1") @QueryParam("blockTime") long blockTime,
      @PathParam("treeId") int treeId,
      @PathParam("key") String key) {
    BlockchainAddress addressKey = BlockchainAddress.fromString(address);
    ImmutableChainState chainState = loadChainState(blockTime);
    CoreContractState coreContractState = chainState.getCoreContractState(addressKey);
    assertContractAddress(addressKey, coreContractState);
    StateSerializable state = chainState.getContractState(addressKey);
    byte[] result =
        ModelFactory.getAvlTreeValue(
            addressKey.getType(), state, treeId, HexFormat.of().parseHex(key));
    if (result == null) {
      throw new NotFoundException("Value does not exist in tree");
    }
    return new AvlStateValue(result);
  }

  /**
   * Retrieves the modified keys for the AvlTree with tree id between two block times. Only returns
   * modified keys greater than provided key. Returns at most n keys.
   *
   * @param address the address of the contract.
   * @param treeId the tree id of the avl tree.
   * @param blockTime1 the first block time.
   * @param blockTime2 the second block time.
   * @param key key to base the search from.
   * @param n the max number of elements to get. The max allowed is 100.
   * @return the modified keys.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/contracts/{address}/avl/{treeId}/diff/{blockTime1}/{blockTime2}/{key}")
  public List<byte[]> getContractAvlModifiedKeys(
      @PathParam("address") String address,
      @PathParam("treeId") int treeId,
      @PathParam("blockTime1") long blockTime1,
      @PathParam("blockTime2") long blockTime2,
      @PathParam("key") String key,
      @DefaultValue("10") @QueryParam("n") int n) {
    return getContractAvlModifiedKeysImpl(
        address, treeId, blockTime1, blockTime2, HexFormat.of().parseHex(key), n);
  }

  /**
   * Retrieves the modified keys for the AvlTree with tree id between two block times. Returns at
   * most n keys.
   *
   * @param address the address of the contract.
   * @param treeId the tree id of the avl tree.
   * @param blockTime1 the first block time.
   * @param blockTime2 the second block time.
   * @param n the max number of elements to get. The max allowed is 100.
   * @return the modified keys.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/contracts/{address}/avl/{treeId}/diff/{blockTime1}/{blockTime2}")
  public List<byte[]> getContractAvlModifiedKeys(
      @PathParam("address") String address,
      @PathParam("treeId") int treeId,
      @PathParam("blockTime1") long blockTime1,
      @PathParam("blockTime2") long blockTime2,
      @DefaultValue("10") @QueryParam("n") int n) {
    return getContractAvlModifiedKeysImpl(address, treeId, blockTime1, blockTime2, null, n);
  }

  private List<byte[]> getContractAvlModifiedKeysImpl(
      String address, int treeId, long blockTime1, long blockTime2, byte[] key, int n) {
    BlockchainAddress addressKey = BlockchainAddress.fromString(address);
    ImmutableChainState chainState1 = loadChainState(blockTime1);
    ImmutableChainState chainState2 = loadChainState(blockTime2);
    CoreContractState coreContractState1 = chainState1.getCoreContractState(addressKey);
    CoreContractState coreContractState2 = chainState2.getCoreContractState(addressKey);
    assertContractAddress(addressKey, coreContractState1);
    assertContractAddress(addressKey, coreContractState2);
    if (n > 100) {
      throw new BadRequestException("Can fetch at most 100 entries");
    }
    StateSerializable state1 = chainState1.getContractState(addressKey);
    StateSerializable state2 = chainState2.getContractState(addressKey);
    return ModelFactory.createAvlTreeModifiedKeys(
        addressKey.getType(), state1, state2, treeId, key, n);
  }

  private ImmutableChainState loadChainState(long blockTime) {
    ImmutableChainState chainState;
    if (blockTime != -1) {
      chainState = blockchain.getState(blockTime);
      if (chainState == null) {
        throw new BadRequestException("No chain state for given block time");
      }
    } else {
      chainState = blockchain.getChainState();
    }
    return chainState;
  }

  private static void assertContractAddress(
      BlockchainAddress addressKey, CoreContractState coreContractState) {
    if (addressKey.getType() == BlockchainAddress.Type.ACCOUNT) {
      throw new BadRequestException("Address does not correspond to a contract");
    }
    if (coreContractState == null) {
      throw new NotFoundException();
    }
  }

  /**
   * Retrieve sub-state of contract from address by traversing its fields.
   *
   * @param address the address of the contract
   * @param path the record containing the path to the desired sub-state
   * @return the sub-state as a JsonNode
   */
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/contracts/{address}")
  public JsonNode traverseContractState(@PathParam("address") String address, TraversePath path) {
    BlockchainAddress key = BlockchainAddress.fromString(address);
    StateSerializable contractState = blockchain.getChainState().getContractState(key);
    return traverseState(path, contractState);
  }

  private List<Event> events(Collection<Hash> events) {
    return events.stream().map(id -> new Event(id.toString(), shardLookup(id))).toList();
  }

  private String shardLookup(Hash eventHash) {
    return blockchain.getEventTransaction(eventHash).getEvent().getDestinationShard();
  }

  private List<com.partisiablockchain.dto.ExecutedTransaction> transactionsToRecords(
      List<ExecutedTransaction> transactions, Block block) {
    return transactions.stream()
        .map(transaction -> transactionToRecord(transaction, block))
        .toList();
  }

  private com.partisiablockchain.dto.ExecutedTransaction transactionToRecord(
      ExecutedTransaction transaction, Block block) {
    return ModelFactory.createExecutedTransaction(
        transaction,
        block,
        true,
        this::events,
        blockchain::getTransactionCost,
        blockchain::getTransactionFailure);
  }

  /**
   * Retrieve a block given the block time.
   *
   * @param blockTime the block time of the block
   * @return the block
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks/blockTime/{blockTime}")
  public com.partisiablockchain.dto.BlockState getBlockFromNumber(
      @PathParam("blockTime") long blockTime) {
    return ModelFactory.createBlock(blockchain.getBlock(blockTime));
  }

  /**
   * Retrieve all transactions of specific block.
   *
   * @param hash the hash of the block
   * @return the list of transactions
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks/{blockHash}/transactions")
  public List<com.partisiablockchain.dto.ExecutedTransaction> getTransactionsForBlock(
      @PathParam("blockHash") String hash) {
    Hash blockHash = Hash.fromString(hash);
    Block block = blockchain.getBlock(blockHash);
    if (block == null) {
      throw new NotFoundException();
    }
    List<ExecutedTransaction> transactions = blockchain.getTransactionsForBlock(blockHash);
    transactions.addAll(getEventTransactionsForBlock(block));
    return transactionsToRecords(transactions, block);
  }

  List<ExecutedTransaction> getEventTransactionsForBlock(Block block) {
    List<ExecutedTransaction> transactions = new ArrayList<>();
    ImmutableChainState state = blockchain.getState(block.getBlockTime());
    AvlTree<Hash, Boolean> executionStatus = state.getExecutedState().getExecutionStatus();
    AvlTree<Hash, Hash> spawnedEvents = state.getExecutedState().getSpawnedEvents();
    for (Hash event : spawnedEvents.keySet()) {
      if (executionStatus.containsKey(event)) {
        transactions.add(blockchain.getTransaction(event));
      }
    }
    return transactions;
  }

  /**
   * Retrieve the latest n transactions. If only k &lt; n transactions occurred k transactions will
   * be returned. Will at most search through 1000 blocks.
   *
   * @param number number of transactions to get, must be smaller than 100.
   * @param includeEvents allow events in the returned list of transactions.
   * @return the list of transactions
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/transaction/latest/{number}")
  public List<com.partisiablockchain.dto.ExecutedTransaction> getLatestTransactions(
      @PathParam("number") int number,
      @DefaultValue("false") @QueryParam("includeEvents") boolean includeEvents) {
    Block currentBlock = blockchain.getLatestBlock();
    return getTransactionsBeforeBlock(number, currentBlock.getBlockTime(), "", includeEvents);
  }

  /**
   * Retrieve the latest n transactions before a given included blockTime and optionally
   * transactionHash. If only k &lt; n transactions occurred k transactions will be returned. Will
   * at most search through 1000 blocks.
   *
   * @param number number of transactions to get, must be smaller than 100.
   * @param blockTime the blockTime to search from (included).
   * @param transactionHash optional transactionHash to search from (excluded).
   * @param includeEvents allow events in the returned list of transactions.
   * @return the list of transactions
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/transaction/latest/{number}/{blockTime}")
  public List<com.partisiablockchain.dto.ExecutedTransaction> getTransactionsBeforeBlock(
      @PathParam("number") int number,
      @PathParam("blockTime") long blockTime,
      @DefaultValue("") @QueryParam("transactionHash") String transactionHash,
      @DefaultValue("false") @QueryParam("includeEvents") boolean includeEvents) {
    if (number > 100) {
      throw new BadRequestException("Can only fetch up to 100 transactions.");
    }

    Block currentBlock = blockchain.getBlock(blockTime);

    List<com.partisiablockchain.dto.ExecutedTransaction> transactionsFromFirstBlock =
        retrieveTransactionsFromFirstBlock(currentBlock, transactionHash, number, includeEvents);

    int numberOfRemainingTransactions = number - transactionsFromFirstBlock.size();

    List<com.partisiablockchain.dto.ExecutedTransaction> remainingTransactions =
        retrieveRemainingTransactions(
            blockchain.getBlock(currentBlock.getParentBlock()),
            numberOfRemainingTransactions,
            includeEvents);

    List<com.partisiablockchain.dto.ExecutedTransaction> transactions = new ArrayList<>();
    transactions.addAll(transactionsFromFirstBlock);
    transactions.addAll(remainingTransactions);
    return transactions;
  }

  private List<com.partisiablockchain.dto.ExecutedTransaction> retrieveTransactionsFromFirstBlock(
      Block block, String transactionHash, int number, boolean includeEvents) {
    List<com.partisiablockchain.dto.ExecutedTransaction> transactionsFromFirstBlock =
        getTransactionsFromBlock(block.identifier(), includeEvents);
    int endIndex = Math.min(transactionsFromFirstBlock.size(), number);
    if (!transactionHash.isEmpty()) {
      int startIndex = getIndexOfTransaction(transactionHash, transactionsFromFirstBlock) + 1;
      endIndex = Math.min(transactionsFromFirstBlock.size(), endIndex + startIndex);
      return transactionsFromFirstBlock.subList(startIndex, endIndex);
    } else {
      return transactionsFromFirstBlock.subList(0, endIndex);
    }
  }

  private List<com.partisiablockchain.dto.ExecutedTransaction> retrieveRemainingTransactions(
      Block startBlock, int number, boolean includeEvents) {
    List<com.partisiablockchain.dto.ExecutedTransaction> transactions = new ArrayList<>();
    int maxNumberOfBlocks = 1000;
    Block currentBlock = startBlock;
    for (int i = 1; i < maxNumberOfBlocks; i++) {
      if (currentBlock == null) {
        return transactions;
      }
      List<com.partisiablockchain.dto.ExecutedTransaction> signedTransactions =
          getTransactionsFromBlock(currentBlock.identifier(), includeEvents);
      for (com.partisiablockchain.dto.ExecutedTransaction signedTransaction : signedTransactions) {
        if (transactions.size() == number) {
          return transactions;
        }
        transactions.add(signedTransaction);
      }
      currentBlock = blockchain.getBlock(currentBlock.getParentBlock());
    }
    return transactions;
  }

  private int getIndexOfTransaction(
      String transactionHash, List<com.partisiablockchain.dto.ExecutedTransaction> transactions) {
    for (int i = 0; i < transactions.size(); i++) {
      com.partisiablockchain.dto.ExecutedTransaction t = transactions.get(i);
      if (t.identifier().equals(transactionHash)) {
        return i;
      }
    }
    return -1;
  }

  private List<com.partisiablockchain.dto.ExecutedTransaction> getTransactionsFromBlock(
      Hash blockIdentifier, boolean includeEvents) {
    Block block = blockchain.getBlock(blockIdentifier);
    List<ExecutedTransaction> newTransactions = blockchain.getTransactionsForBlock(blockIdentifier);
    newTransactions.addAll(getEventTransactionsForBlock(block));
    List<com.partisiablockchain.dto.ExecutedTransaction> newTransactionRecords =
        transactionsToRecords(newTransactions, block);
    if (includeEvents) {
      return newTransactionRecords;
    } else {
      return newTransactionRecords.stream().filter(e -> !e.isEvent()).toList();
    }
  }

  /**
   * Retrieve the latest block time that occurred before the given UTC time. Throws
   * NotFoundException if no such block exist. If the given time is -1 returns the latest block
   * time.
   *
   * @param newestProductionTime the UTC time in millis, default -1.
   * @return the blockTime of the latest block before the given UTC time.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks/latestBlockTime")
  public Long getBlockTimeFromUtc(
      @DefaultValue("-1") @QueryParam("utcTime") long newestProductionTime) {
    return searchForBlockFromEpocTime(newestProductionTime);
  }

  @SuppressWarnings("ConstantConditions")
  private long searchForBlockFromEpocTime(long newestProductionTime) {
    Block block = blockchain.getLatestBlock();
    if (newestProductionTime == -1) {
      return block.getBlockTime();
    }
    Block genesis = blockchain.getBlock(0);
    if (newestProductionTime < genesis.getProductionTime()) {
      throw new NotFoundException(
          "Queried for block before "
              + newestProductionTime
              + ", but oldest block was created at time "
              + genesis.getProductionTime());
    }
    long step = 100;
    Block newerBlock = block;
    while (block.getProductionTime() > newestProductionTime) {
      newerBlock = block;
      long olderBlockTime = Math.max(block.getBlockTime() - step, 0);
      block = blockchain.getBlock(olderBlockTime);
      step = step * 2;
    }
    // now we know the block is between newerBlock and block, binary search
    while (newerBlock.getBlockTime() > block.getBlockTime() + 1) {
      long midTime = (newerBlock.getBlockTime() + block.getBlockTime()) / 2;
      Block mid = blockchain.getBlock(midTime);
      if (mid.getProductionTime() > newestProductionTime) {
        newerBlock = mid;
      } else {
        block = mid;
      }
    }

    return block.getBlockTime();
  }

  /**
   * Retrieve transactions of the latest block.
   *
   * @return the list of transactions
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks/latest/transactions")
  public List<com.partisiablockchain.dto.ExecutedTransaction> getTransactionsFromLatestBlock() {
    Block block = blockchain.getLatestBlock();
    List<ExecutedTransaction> transactions = blockchain.getTransactionsForBlock(block.identifier());
    return transactionsToRecords(transactions, block);
  }

  /**
   * Get a the transaction for the given hash. If no transaction is found it returns a 404 Not
   * Found.
   *
   * @param transactionHash the hash of the transaction
   * @return the transaction
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/transaction/{transactionHash}")
  public com.partisiablockchain.dto.ExecutedTransaction getTransaction(
      @PathParam("transactionHash") String transactionHash) {
    Hash hash = Hash.fromString(transactionHash);
    ExecutedTransaction transaction = blockchain.getTransaction(hash);
    if (transaction == null) {
      throw new NotFoundException();
    }
    Block block = blockchain.getBlock(transaction.getBlockHash());
    return transactionToRecord(transaction, block);
  }

  /**
   * Retrieve an event from hash.
   *
   * @param eventIdentifier the hash of the event
   * @return the event
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/event/{eventIdentifier}")
  public Event getEventTransaction(@PathParam("eventIdentifier") String eventIdentifier) {
    Hash hash = Hash.fromString(eventIdentifier);
    ExecutableEvent event = blockchain.getEventTransaction(hash);
    return ModelFactory.createEvent(event);
  }

  /**
   * Push transaction to the blockchain.
   *
   * @param transaction the transaction to push
   */
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/transaction")
  public void pushTransaction(IncomingTransaction transaction) {
    SignedTransaction signedTransaction =
        SafeDataInputStream.deserialize(
            stream -> SignedTransaction.read(chainId().chainId(), stream),
            transaction.transactionPayload());
    addPendingTransaction(signedTransaction);
  }

  /**
   * This sends an event transaction through this node.
   *
   * <p>400 Bad Request if the event is invalid.
   *
   * @param event the transaction to send
   */
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Path("/event")
  public void pushEvent(IncomingTransaction event) {
    FloodableEvent floodableEvent =
        SafeDataInputStream.readFully(event.transactionPayload(), FloodableEvent::read);
    if (!blockchain.addPendingEvent(floodableEvent)) {
      logger.warn("Invalid event {} not added to pending", floodableEvent);
      throw new BadRequestException("Invalid event");
    }
  }

  private void addPendingTransaction(SignedTransaction signedTransaction) {
    if (!blockchain.addPendingTransaction(signedTransaction)) {
      logger.warn("Invalid transaction {} not added to pending", signedTransaction);
      throw new BadRequestException("Transaction is not valid in current state");
    }
  }
}
