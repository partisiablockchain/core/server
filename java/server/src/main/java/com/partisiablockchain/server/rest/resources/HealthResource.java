package com.partisiablockchain.server.rest.resources;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.partisiablockchain.server.rest.api.HealthResourceApi;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/** Resource for healthchecks. */
@Produces(MediaType.APPLICATION_JSON)
@Path("/health")
public final class HealthResource implements HealthResourceApi {

  private static final ObjectMapper mapper = new ObjectMapper();

  /** Create a new healthcheck resource. */
  public HealthResource() {}

  /**
   * Used for healthchecks.
   *
   * @return 200 OK when up
   */
  @Override
  @GET
  public Response getHealth() {
    ObjectNode json = mapper.createObjectNode();
    json.put("status", "up");
    return Response.status(Response.Status.OK).entity(json).build();
  }
}
