package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.DelayedBlockchainListener;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.flooding.NetworkConfig;
import com.partisiablockchain.kademlia.Kademlia;
import com.partisiablockchain.server.ServerModule.CreateContractListener;
import com.partisiablockchain.server.ServerModule.ServerConfig;
import com.partisiablockchain.server.config.ConfigFiles;
import com.partisiablockchain.server.config.KademliaConfig;
import com.partisiablockchain.server.config.NodeConfig;
import com.partisiablockchain.server.config.NodeConfig.ModuleConfig;
import com.partisiablockchain.server.rest.ExceptionToResponse;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.rest.AliveCheck;
import com.secata.tools.rest.AliveCheckBinder;
import com.secata.tools.rest.ObjectMapperProvider;
import com.secata.tools.rest.RestServer;
import com.secata.tools.rest.RootResource;
import com.secata.tools.thread.ShutdownHook;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.BooleanSupplier;
import java.util.function.LongSupplier;
import java.util.function.Supplier;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Main class for the pbc, bootstraps every available module and loads configurations. */
public final class Main {

  private static final ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);
  private static final Logger logger = LoggerFactory.getLogger(Main.class);

  private Main() {}

  /**
   * Entry point for starting the rest endpoints interacting with the blockchain.
   *
   * @param args arguments used for {@link ConfigFiles#createFromArgs}
   */
  public static void main(String[] args) {
    ConfigFiles configFiles = ConfigFiles.createFromArgs(args);
    runWithBlockchain(
        configFiles.getDataDirectory(),
        configFiles.getChainId(),
        configFiles.getGenesisFile(),
        configFiles.getNodeConfig());
  }

  static NetworkConfig createNetworkConfig(NodeConfig nodeConfig, Kademlia kademlia) {
    List<Supplier<Address>> addressSuppliers = new ArrayList<>();

    List<Address> knownPeers = nodeConfig.knownPeersAsAddress();
    if (!knownPeers.isEmpty()) {
      addressSuppliers.add(KnownPeers.create(knownPeers));
    }

    if (kademlia != null) {
      addressSuppliers.add(kademlia::getRandomAddress);
    }

    Address persistentConnection = null;
    if (nodeConfig.persistentPeer() != null) {
      persistentConnection = Address.parseAddress(nodeConfig.persistentPeer());
    }

    Address nodeAddress = new Address("localhost", nodeConfig.floodingPort());
    return new NetworkConfig(nodeAddress, joinSuppliers(addressSuppliers), persistentConnection);
  }

  static Supplier<Address> joinSuppliers(List<Supplier<Address>> addressSupplier) {
    if (addressSupplier.isEmpty()) {
      return FunctionUtility.nullSupplier();
    } else {
      return () -> {
        int supplier = ThreadLocalRandom.current().nextInt(addressSupplier.size());
        return addressSupplier.get(supplier).get();
      };
    }
  }

  /** Same as default entrypoint but parameterized with a blockchain. */
  private static void runWithBlockchain(
      RootDirectory rootDirectory, String chainId, String genesisFile, NodeConfig nodeConfig) {
    logger.info("Booting PBC blockchain");

    CollectingServerConfig serverConfig = new CollectingServerConfig();
    BlockchainLedger blockchain =
        initializeBlockchain(rootDirectory, chainId, genesisFile, nodeConfig, serverConfig);

    configureModules(rootDirectory, serverConfig, blockchain, nodeConfig.modules());

    RestServer restServer = restServer(serverConfig, blockchain, nodeConfig.restPort());

    startApp(restServer, serverConfig.closeable, new ShutdownHook());
    logger.info("PBC blockchain shut down");
  }

  static void configureModules(
      RootDirectory rootDirectory,
      CollectingServerConfig serverConfig,
      BlockchainLedger blockchain,
      List<ModuleConfig> modules) {
    for (ModuleConfig module : modules) {
      ServerModule<?> serverModule = loadServerModule(module);
      configureServerModule(
          serverModule, module.configuration(), rootDirectory, serverConfig, blockchain);
    }
    blockchain.attach(
        new DelayedBlockchainListener(
            new ContractDispatcher(serverConfig.createContractListeners)));
  }

  static RestServer restServer(
      CollectingServerConfig serverConfig, BlockchainLedger blockchain, int restPort) {
    if (serverConfig.hasRest()) {
      serverConfig.addRestAliveCheck(checkLatestBlock(blockchain));
      ResourceConfig resourceConfig = createConfigForBlockchainNode(serverConfig);
      return new RestServer(restPort, resourceConfig);
    } else {
      return null;
    }
  }

  static BlockchainLedger initializeBlockchain(
      RootDirectory rootDirectory,
      String chainId,
      String genesisFile,
      NodeConfig nodeConfig,
      CollectingServerConfig serverConfig) {
    Kademlia kademlia = null;
    KademliaConfig kademliaConfig = nodeConfig.kademliaConfig();
    if (kademliaConfig != null) {
      kademlia =
          new Kademlia(
              kademliaConfig.getOrCreateKademliaKey(),
              nodeConfig.floodingPort(),
              kademliaConfig.getKnownNodes());
      serverConfig.registerCloseable(kademlia);
    }

    NetworkConfig networkConfig = createNetworkConfig(nodeConfig, kademlia);
    BlockchainLedger blockchain =
        BlockchainLedger.createBlockChain(networkConfig, rootDirectory, genesisFile);
    serverConfig.registerCloseable(blockchain);
    return blockchain;
  }

  static <T extends ServerModuleConfigDto> void configureServerModule(
      ServerModule<T> serverModule,
      JsonNode configuration,
      RootDirectory rootDirectory,
      CollectingServerConfig collectingServerConfig,
      BlockchainLedger blockchain) {
    T loadedConfig =
        ExceptionConverter.call(
            () -> mapper.treeToValue(configuration, serverModule.configType()),
            "Unable to read module configuration");
    if (loadedConfig.enabled) {
      logger.info("Registering server module {}", serverModule.getClass().getSimpleName());
      serverModule.register(rootDirectory, blockchain, collectingServerConfig, loadedConfig);
    } else {
      logger.info("Ignoring disabled server module {}", serverModule);
    }
  }

  private static ServerModule<?> loadServerModule(ModuleConfig module) {
    Class<?> moduleClass =
        ExceptionConverter.call(
            () -> Class.forName(module.fullyQualifiedClass()), "Unable to locate module class");
    return ExceptionConverter.call(
        () -> (ServerModule<?>) moduleClass.getDeclaredConstructor().newInstance(),
        "Unable to instantiate module. Did you remember a public constructor?");
  }

  static BooleanSupplier checkLatestBlock(BlockchainLedger blockchain) {
    return () -> isRecentProductionTime(blockchain.getLatestBlock().getProductionTime());
  }

  static boolean isRecentProductionTime(long productionTime) {
    return isRecentProductionTime(productionTime, System::currentTimeMillis);
  }

  static boolean isRecentProductionTime(long productionTime, LongSupplier now) {
    long blockAge = now.getAsLong() - productionTime;
    return blockAge < 150 * 1000;
  }

  static ResourceConfig createConfigForBlockchainNode(CollectingServerConfig serverConfig) {
    ResourceConfig resourceConfig = new ResourceConfig();
    serverConfig.components.forEach(resourceConfig::register);
    serverConfig.componentClasses.forEach(resourceConfig::register);
    resourceConfig.register(RootResource.class);
    resourceConfig.register(ExceptionToResponse.class);
    resourceConfig.register(new AliveCheckBinder(serverConfig.getRestAlive()));
    return resourceConfig;
  }

  /** Starts the jersey APP based on the resource config and the port to expose rest. */
  static void startApp(RestServer app, List<AutoCloseable> resources, ShutdownHook shutdownHooks) {
    if (app != null) {
      shutdownHooks.register(app);
      ExceptionConverter.run(app::start, "App crashed");
    }
    resources.forEach(shutdownHooks::register);
  }

  static final class KnownPeers implements Supplier<Address> {

    private final List<Address> peers;
    private int index;

    static KnownPeers create(List<Address> peers) {
      return new KnownPeers(peers);
    }

    private KnownPeers(List<Address> peers) {
      index = ThreadLocalRandom.current().nextInt(peers.size());
      this.peers = peers;
    }

    @Override
    public Address get() {
      return peers.get(index++ % peers.size());
    }
  }

  static final class CollectingServerConfig implements ServerConfig {

    final List<AutoCloseable> closeable = new ArrayList<>();
    final List<BooleanSupplier> alive = new ArrayList<>();
    final List<Class<?>> componentClasses = new ArrayList<>();
    final List<Object> components = new ArrayList<>();
    final List<CreateContractListener> createContractListeners = new ArrayList<>();

    @Override
    public void registerCloseable(AutoCloseable autoCloseable) {
      closeable.add(autoCloseable);
    }

    @Override
    public void registerRestComponent(Class<?> componentClass) {
      componentClasses.add(componentClass);
    }

    @Override
    public void registerRestComponent(Object o) {
      components.add(o);
    }

    @Override
    public void addRestAliveCheck(BooleanSupplier aliveCheck) {
      alive.add(aliveCheck);
    }

    @Override
    public void addCreateListener(CreateContractListener createContractListener) {
      createContractListeners.add(createContractListener);
    }

    boolean hasRest() {
      return !components.isEmpty() || !componentClasses.isEmpty() || !alive.isEmpty();
    }

    AliveCheck getRestAlive() {
      return () -> {
        for (BooleanSupplier isAlive : alive) {
          if (!isAlive.getAsBoolean()) {
            return false;
          }
        }
        return true;
      };
    }
  }
}
