package com.partisiablockchain.server.tcp.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.ExceptionLogger;
import com.secata.tools.thread.ExecutorFactory;
import com.secata.tools.thread.ThreadedLoop;
import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.function.BiConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Connector used to accept client connections. */
public final class ServerConnector implements Closeable {

  private static final Logger logger = LoggerFactory.getLogger(ServerConnector.class);

  private final ExecutorService executor = ExecutorFactory.newCached("ContractServer");

  private final BiConsumer<InputStream, OutputStream> creator;
  private final ServerSocket serverSocket;
  private final ThreadedLoop serverLoop;

  ServerConnector(int port, BiConsumer<InputStream, OutputStream> creator) {
    this(ExceptionConverter.call(() -> new ServerSocket(port), "Unable to bind"), creator);
  }

  ServerConnector(ServerSocket serverSocket, BiConsumer<InputStream, OutputStream> creator) {
    this.serverSocket = serverSocket;
    this.creator = creator;
    this.serverLoop =
        ThreadedLoop.create(() -> socketAccepted(serverSocket.accept()), "Server")
            .onException(this::close)
            .logger(logger::trace, "Error")
            .start();
  }

  @SuppressWarnings("FutureReturnValueIgnored")
  private void socketAccepted(final Socket accept) {
    executor.submit(
        () ->
            ExceptionConverter.run(
                () -> creator.accept(accept.getInputStream(), accept.getOutputStream()),
                "Unable to get socket streams"));
  }

  @Override
  public void close() {
    if (serverLoop.isRunning()) {
      serverLoop.stop();
      executor.shutdownNow();
      ExceptionLogger.handle(logger::debug, serverSocket::close, "Unable to close socket");
    }
  }

  boolean isClosed() {
    return !serverLoop.isRunning();
  }
}
