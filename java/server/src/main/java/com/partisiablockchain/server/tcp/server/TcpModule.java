package com.partisiablockchain.server.tcp.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.server.ServerModule;
import com.partisiablockchain.server.ServerModuleConfigDto;
import com.partisiablockchain.storage.RootDirectory;

/** Module for creating a {@link Server}. */
public final class TcpModule implements ServerModule<TcpModule.TcpModuleConfig> {

  @Override
  public void register(
      RootDirectory rootDirectory,
      BlockchainLedger blockchainLedger,
      ServerConfig serverConfig,
      TcpModuleConfig moduleConfig) {
    Server server = new Server(blockchainLedger, moduleConfig.port);
    serverConfig.registerCloseable(server);
  }

  @Override
  public Class<TcpModuleConfig> configType() {
    return TcpModuleConfig.class;
  }

  /** Config for {@link TcpModule}. */
  public static final class TcpModuleConfig extends ServerModuleConfigDto {

    /** The port to bind. */
    public int port;
  }
}
