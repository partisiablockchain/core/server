package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Packet indicating that a contract has been created. */
public final class ContractCreated implements DataStreamSerializable {

  private final BlockchainAddress address;

  private final Hash binderHash;
  private final Hash contractHash;
  private final Hash abiHash;
  private final int codeSize;

  private final Hash stateHash;

  /**
   * Create a new ContractCreated payload.
   *
   * @param address the address of the contract
   * @param binderHash the identifier of the contract binder
   * @param contractHash the identifier of the contract code
   * @param abiHash the identifier of the ABI
   * @param codeSize the code size of the contract
   * @param stateHash identifier of the current contract state
   */
  public ContractCreated(
      BlockchainAddress address,
      Hash binderHash,
      Hash contractHash,
      Hash abiHash,
      int codeSize,
      Hash stateHash) {
    this.address = address;
    this.binderHash = binderHash;
    this.contractHash = contractHash;
    this.abiHash = abiHash;
    this.codeSize = codeSize;
    this.stateHash = stateHash;
  }

  /**
   * Get the address of the created contract.
   *
   * @return the address
   */
  public BlockchainAddress getAddress() {
    return address;
  }

  /**
   * Get the binder identifier of the created contract.
   *
   * @return the binder identifier
   */
  public Hash getBinderHash() {
    return binderHash;
  }

  /**
   * Get the contract identifier of the created contract.
   *
   * @return the contract identifier
   */
  public Hash getContractHash() {
    return contractHash;
  }

  /**
   * Get the abi identifier of the created contract.
   *
   * @return the abi identifier
   */
  public Hash getAbiHash() {
    return abiHash;
  }

  /**
   * Get the side of the code of the created contract.
   *
   * @return the side of the code
   */
  public int getCodeSize() {
    return codeSize;
  }

  /**
   * Get the identifier of current state of the created contract.
   *
   * @return the identifier of current state
   */
  public Hash getStateHash() {
    return stateHash;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    address.write(stream);
    binderHash.write(stream);
    contractHash.write(stream);
    abiHash.write(stream);
    stream.writeInt(codeSize);
    stateHash.write(stream);
  }

  /**
   * Read from stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static ContractCreated read(SafeDataInputStream stream) {
    BlockchainAddress address = BlockchainAddress.read(stream);

    Hash binderHash = Hash.read(stream);
    Hash contractHash = Hash.read(stream);
    Hash abiHash = Hash.read(stream);
    int codeSize = stream.readInt();

    Hash stateHash = Hash.read(stream);
    return new ContractCreated(address, binderHash, contractHash, abiHash, codeSize, stateHash);
  }
}
