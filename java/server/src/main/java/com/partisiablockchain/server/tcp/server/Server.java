package com.partisiablockchain.server.tcp.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.server.tcp.packet.Handshake;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.ExceptionLogger;
import java.io.Closeable;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A TCP server accepting clients to listen to the local blockchain. */
public final class Server implements Closeable {

  private static final Logger logger = LoggerFactory.getLogger(Server.class);
  private final BlockchainLedger ledger;
  private final ServerConnector connector;

  private final List<ConnectedClient> clients = Collections.synchronizedList(new ArrayList<>());

  /**
   * Create a new TCP server.
   *
   * @param ledger the underlying ledger
   * @param port port to bind
   */
  public Server(BlockchainLedger ledger, int port) {
    this.ledger = ledger;
    this.connector = new ServerConnector(port, this::createConnectedClient);
  }

  void createConnectedClient(InputStream input, OutputStream output) {
    try {
      Handshake handshake = Handshake.read(new SafeDataInputStream(input));
      clients.add(new ConnectedClient(input, output, ledger, handshake));
    } catch (Exception e) {
      logger.debug("Error while reading handshake", e);
      ExceptionLogger.handle(logger::debug, input::close, "Unable to close socket");
    }
    clients.removeIf(ConnectedClient::isClosed);
  }

  @Override
  public void close() {
    connector.close();
    clients.forEach(ConnectedClient::close);
  }

  int clients() {
    return clients.size();
  }

  boolean isClosed() {
    return connector.isClosed();
  }
}
