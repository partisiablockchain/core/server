package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** Converter for shard naming, primarily used for governance shard. */
public final class ShardConverter {

  /** Name of the governance shard. */
  public static final String governanceShardName = "Gov";

  /**
   * If the shardId is null, we should convert to using the governance shard name.
   *
   * @param shardId the shard id to check
   * @return the converted shard id
   */
  public static String convertToGovShard(String shardId) {
    if (shardId == null) {
      return governanceShardName;
    } else {
      return shardId;
    }
  }

  /**
   * If the shardId is Gov, we should convert to using the internal representation.
   *
   * @param shardId the shard id to check
   * @return the converted shard id
   */
  public static String convertFromGovShard(String shardId) {
    if (shardId.equals(governanceShardName)) {
      return null;
    } else {
      return shardId;
    }
  }
}
