package com.partisiablockchain.server.rest.shard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.server.rest.PluginTypeConverter;
import com.partisiablockchain.server.rest.ShardConverter;
import com.partisiablockchain.server.rest.api.ShardControllerApi;
import com.partisiablockchain.server.rest.model.Block;
import com.partisiablockchain.server.rest.model.ExecutedTransaction;
import com.partisiablockchain.server.rest.model.ExecutionStatus;
import com.partisiablockchain.server.rest.model.Failure;
import com.partisiablockchain.server.rest.model.Jar;
import com.partisiablockchain.server.rest.model.Shard;
import com.partisiablockchain.server.rest.model.TransactionCost;
import com.partisiablockchain.server.rest.model.TransactionPointer;
import com.partisiablockchain.server.rest.shard.ShardTransaction.ShardExecutionStatus;
import com.partisiablockchain.server.rest.shard.ShardTransaction.ShardExecutionStatus.ShardFailure;
import com.partisiablockchain.server.rest.shard.ShardTransaction.ShardExecutionStatus.ShardTransactionPointer;
import jakarta.inject.Inject;
import jakarta.ws.rs.NotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Controller for communicating with shards. */
public final class ShardController implements ShardControllerApi {

  private final ShardService shardService;

  /**
   * Controller for communicating with a shard.
   *
   * @param shardService service for the controller
   */
  @Inject
  public ShardController(ShardService shardService) {
    this.shardService = shardService;
  }

  /**
   * Get information about a specific shard.
   *
   * @param shardId the id of the shard
   * @return the shard
   */
  @Override
  public Shard getShard(String shardId) {
    ShardState shardState = shardService.getShardState(ShardConverter.convertFromGovShard(shardId));

    Map<String, JsonNode> plugins = new HashMap<>();
    Map<ChainPluginType, JsonNode> shardPlugins = shardState.plugins();
    for (ChainPluginType chainPluginType : shardPlugins.keySet()) {
      String pluginType = PluginTypeConverter.convertToString(chainPluginType);
      plugins.put(pluginType, shardPlugins.get(chainPluginType));
    }

    return new Shard().governanceVersion(shardState.governanceVersion()).plugins(plugins);
  }

  /**
   * Get the latest block.
   *
   * @param shardId the id of the shard
   * @return the block
   */
  @Override
  public Block getLatestBlock(String shardId) {
    return getBlock(shardId, null);
  }

  /**
   * Get a specific block by id.
   *
   * @param shardId the id of the shard
   * @param blockId the id of the block
   * @return the block
   */
  @Override
  public Block getBlock(String shardId, String blockId) {
    Hash identifier = null;
    if (blockId != null) {
      identifier = Hash.fromString(blockId);
    }

    ShardBlock shardBlock =
        shardService.getShardBlock(ShardConverter.convertFromGovShard(shardId), identifier);
    if (shardBlock == null) {
      throw new NotFoundException();
    }

    return new Block()
        .identifier(shardBlock.identifier().toString())
        .parentBlock(shardBlock.parentBlock().toString())
        .committeeId(shardBlock.committeeId())
        .blockTime(shardBlock.blockTime())
        .events(shardBlock.events().stream().map(Hash::toString).toList())
        .transactions(shardBlock.transactions().stream().map(Hash::toString).toList())
        .state(shardBlock.state().toString())
        .productionTime(shardBlock.productionTime())
        .producer((int) shardBlock.producer());
  }

  /**
   * Get a specific transaction by id.
   *
   * @param shardId the id of the shard
   * @param transactionId the id of the transaction
   * @return the transaction
   */
  @Override
  public ExecutedTransaction getTransaction(String shardId, String transactionId) {
    Hash identifier = Hash.fromString(transactionId);

    ShardTransaction shardTransaction =
        shardService.getShardTransaction(ShardConverter.convertFromGovShard(shardId), identifier);
    if (shardTransaction == null) {
      throw new NotFoundException();
    }

    Hash transactionIdentifier = shardTransaction.identifier();
    boolean isEvent = shardTransaction.isEvent();
    byte[] content = shardTransaction.content();

    ShardExecutionStatus shardExecutionStatus = shardTransaction.executionStatus();
    if (shardExecutionStatus == null) {
      return new ExecutedTransaction()
          .identifier(transactionIdentifier.toString())
          .isEvent(isEvent)
          .content(content);
    }

    Hash blockId = shardExecutionStatus.blockId();
    boolean success = shardExecutionStatus.success();
    boolean finalized = shardExecutionStatus.finalized();
    List<TransactionPointer> transactionPointers = new ArrayList<>();
    for (ShardTransactionPointer shardTransactionPointer : shardExecutionStatus.events()) {
      transactionPointers.add(
          new TransactionPointer()
              .identifier(shardTransactionPointer.identifier().toString())
              .destinationShardId(
                  ShardConverter.convertToGovShard(shardTransactionPointer.destinationShard())));
    }
    ShardFailure shardFailure = shardExecutionStatus.failure();
    Failure failure;
    if (shardFailure != null) {
      failure =
          new Failure()
              .errorMessage(shardFailure.errorMessage())
              .stackTrace(shardFailure.stackTrace());
    } else {
      failure = null;
    }

    TransactionCost cost = createTransactionCost(shardExecutionStatus.cost());

    ExecutionStatus executionStatus =
        new ExecutionStatus()
            .blockId(blockId.toString())
            .success(success)
            .finalized(finalized)
            .events(transactionPointers)
            .transactionCost(cost)
            .failure(failure);

    return new ExecutedTransaction()
        .identifier(transactionIdentifier.toString())
        .isEvent(isEvent)
        .content(content)
        .executionStatus(executionStatus);
  }

  /**
   * Get a specific jar by id.
   *
   * @param shardId the id of the shard
   * @param jarId the id of the jar
   * @return the jar
   */
  @Override
  public Jar getJar(String shardId, String jarId) {
    Hash identifier = Hash.fromString(jarId);

    ShardJar shardJar =
        shardService.getShardJar(ShardConverter.convertFromGovShard(shardId), identifier);
    if (shardJar == null) {
      throw new NotFoundException();
    }

    return new Jar().data(shardJar.data());
  }

  /*
   * Create a transaction cost.
   *
   * @param cost to be transferred
   * @return the transaction cost
   */
  static TransactionCost createTransactionCost(
      com.partisiablockchain.blockchain.TransactionCost cost) {
    if (cost == null) {
      return null;
    }

    Map<String, Long> networkFees = new HashMap<>();
    for (Map.Entry<Hash, Long> entry : cost.getNetworkCosts().entrySet()) {
      networkFees.put(entry.getKey().toString(), entry.getValue());
    }

    return new TransactionCost()
        .allocatedForEvents(cost.getAllocatedForEvents())
        .cpu(cost.getCpu())
        .remaining(cost.getRemaining())
        .paidByContract(cost.getPaidByContract())
        .networkFees(networkFees);
  }
}
