package com.partisiablockchain.server.config;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.kademlia.KademliaKey;
import com.partisiablockchain.kademlia.KademliaNode;
import com.secata.tools.coverage.ExceptionConverter;
import java.net.InetAddress;
import java.util.Base64;
import java.util.List;

/** The configuration for the kademlia layer. */
public record KademliaConfig(String kademliaKey, TargetConfig config) {

  /** Containing the config for kademlia node. */
  public record TargetConfig(String kademliaId, String host, int port) {}

  /**
   * Get or create a new kademlia key.
   *
   * @return The found or created kademlia key
   */
  public KademliaKey getOrCreateKademliaKey() {
    if (kademliaKey != null) {
      byte[] kademliaKeyRaw = Base64.getDecoder().decode(kademliaKey);
      return new KademliaKey(kademliaKeyRaw);
    } else {
      return new KademliaKey();
    }
  }

  /**
   * Get the known nodes.
   *
   * @return List of known nodes
   */
  @SuppressWarnings("AddressSelection")
  public List<KademliaNode> getKnownNodes() {
    if (config == null) {
      return List.of();
    }
    byte[] knownNodeKeyBytes = Base64.getDecoder().decode(config.kademliaId());
    return List.of(
        new KademliaNode(
            new KademliaKey(knownNodeKeyBytes),
            ExceptionConverter.call(
                () -> InetAddress.getByName(config.host()), "Could not get InetAddress by name"),
            config.port));
  }
}
