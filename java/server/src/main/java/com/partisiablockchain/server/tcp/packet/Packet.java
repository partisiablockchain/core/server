package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.function.Function;

/**
 * Anything that can be sent through the TCP listener connection.
 *
 * @param <T> the payload type
 */
public final class Packet<T extends DataStreamSerializable> {

  private final Type<T> type;
  private final T payload;

  /**
   * Create a new packet.
   *
   * @param type the type of the packet
   * @param payload the payload of the packet
   */
  public Packet(Type<T> type, T payload) {
    this.type = type;
    this.payload = payload;
  }

  /**
   * Send this packet through the supplied stream.
   *
   * @param stream stream to send through
   */
  public void send(SafeDataOutputStream stream) {
    stream.writeByte(type.getType());
    payload.write(stream);
  }

  /**
   * Get the payload of this packet.
   *
   * @return the payload
   */
  public T getPayload() {
    return payload;
  }

  /**
   * Get the type of this packet.
   *
   * @return the packet type
   */
  public Type<T> getType() {
    return type;
  }

  /**
   * Possible payload types.
   *
   * @param <T> the payload type
   */
  public static final class Type<T extends DataStreamSerializable> {

    /** Request for a serialized state. */
    public static final Type<SerializedStateRequest> SERIALIZED_STATE_REQUEST =
        new Type<>(0, SerializedStateRequest::read);

    /** Response with a serialized state. */
    public static final Type<SerializedStateResponse> SERIALIZED_STATE_RESPONSE =
        new Type<>(1, SerializedStateResponse::read);

    /** Request for a contract state. */
    public static final Type<ContractStateRequest> CONTRACT_STATE_REQUEST =
        new Type<>(2, ContractStateRequest::read);

    /** Response for a contract state. */
    public static final Type<ContractStateResponse> CONTRACT_STATE_RESPONSE =
        new Type<>(3, ContractStateResponse::read);

    /** Contract created message. */
    public static final Type<ContractCreated> CONTRACT_CREATED =
        new Type<>(4, ContractCreated::read);

    /** Response to a contract created message. */
    public static final Type<ContractCreatedResponse> CONTRACT_CREATED_RESPONSE =
        new Type<>(5, ContractCreatedResponse::read);

    /** Contract updated event. */
    public static final Type<ContractUpdated> CONTRACT_UPDATED =
        new Type<>(6, ContractUpdated::read);

    /** Contract removed event. */
    public static final Type<ContractRemoved> CONTRACT_REMOVED =
        new Type<>(7, ContractRemoved::read);

    private static final List<Type<?>> values =
        List.of(
            SERIALIZED_STATE_REQUEST,
            SERIALIZED_STATE_RESPONSE,
            CONTRACT_STATE_REQUEST,
            CONTRACT_STATE_RESPONSE,
            CONTRACT_CREATED,
            CONTRACT_CREATED_RESPONSE,
            CONTRACT_UPDATED,
            CONTRACT_REMOVED);

    private final int type;
    private final Function<SafeDataInputStream, T> reader;

    private Type(int type, Function<SafeDataInputStream, T> reader) {
      this.type = type;
      this.reader = reader;
    }

    /**
     * Parse a packet of this type from the supplied stream.
     *
     * @param stream the stream to read from
     * @return the parsed packet
     */
    public Packet<T> parse(SafeDataInputStream stream) {
      T payload = reader.apply(stream);
      return new Packet<>(this, payload);
    }

    /**
     * Get the type id of this packet.
     *
     * @return type id
     */
    public int getType() {
      return type;
    }

    /**
     * Parses a type from the given ordinal.
     *
     * @param type the ordinal type
     * @return the type for this ordinal
     */
    public static Type<?> parseInt(int type) {
      return values.get(type);
    }
  }
}
