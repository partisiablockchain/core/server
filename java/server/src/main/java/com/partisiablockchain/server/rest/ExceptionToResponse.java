package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import jakarta.annotation.Priority;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ExceptionMapper that logs any exceptions from the application and converts to appropriate
 * response.
 */
@Priority(1000)
public final class ExceptionToResponse implements ExceptionMapper<Throwable> {

  private static final Logger logger = LoggerFactory.getLogger(ExceptionToResponse.class);

  @Override
  public Response toResponse(Throwable exception) {
    Response response;
    if (exception instanceof WebApplicationException) {
      response = ((WebApplicationException) exception).getResponse();
      logger.info("Application threw a WebApplicationException. Status=" + response.getStatus());
    } else if (exception instanceof IllegalArgumentException) {
      logger.info("Application threw an IllegalArgumentException", exception);
      response = Response.status(Response.Status.BAD_REQUEST).build();
    } else if (exception instanceof IllegalStateException) {
      logger.info("Application threw an IllegalStateException", exception);
      response = Response.status(Response.Status.BAD_REQUEST).build();
    } else {
      logger.info("Error in application. Application threw exception.", exception);
      response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }
    return response;
  }
}
