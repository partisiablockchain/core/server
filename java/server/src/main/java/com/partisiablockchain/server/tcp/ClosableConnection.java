package com.partisiablockchain.server.tcp;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.server.tcp.packet.Packet;
import java.io.Closeable;

/** A connection that can be closed. */
public interface ClosableConnection extends Closeable {

  /**
   * Send a packet to this connect.
   *
   * @param packet the packet to send
   */
  void send(Packet<?> packet);

  /**
   * Check if this connection is alive.
   *
   * @return true if alive
   */
  boolean isAlive();
}
