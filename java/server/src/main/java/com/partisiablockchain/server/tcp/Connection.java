package com.partisiablockchain.server.tcp;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.server.tcp.packet.Packet;
import com.partisiablockchain.server.tcp.threads.OneTimeClosable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ThrowingRunnable;
import com.secata.tools.thread.ThreadedLoop;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A connection to a client. */
public final class Connection implements ClosableConnection {

  private static final Logger logger = LoggerFactory.getLogger(Connection.class);

  private final LinkedBlockingQueue<Packet<?>> outbound;
  private OneTimeClosable closer;

  private Connection(
      final Consumer<Packet<?>> consumer,
      InputStream input,
      OutputStream output,
      ThrowingRunnable onClose) {
    this.outbound = new LinkedBlockingQueue<>();
    // Initialization is done in a method to synchronize initialization and close since the threads
    // otherwise might call close before we are fully initialized.
    initialize(consumer, input, output, onClose, this.outbound);
  }

  private synchronized void initialize(
      final Consumer<Packet<?>> consumer,
      InputStream input,
      OutputStream output,
      ThrowingRunnable onClose,
      LinkedBlockingQueue<Packet<?>> outbound) {
    SafeDataInputStream inputStream = new SafeDataInputStream(input);
    ThreadedLoop receiverThread =
        ThreadedLoop.create(
                () -> {
                  int type = inputStream.readUnsignedByte();
                  Packet.Type<?> packetType = Packet.Type.parseInt(type);
                  Packet<?> packet = packetType.parse(inputStream);
                  consumer.accept(packet);
                },
                "Receiver")
            .onException(this::close)
            .logger(logger::trace, "Connection problem ")
            .start();

    SafeDataOutputStream outputStream = new SafeDataOutputStream(output);
    ThreadedLoop senderThread =
        ThreadedLoop.create(() -> outbound.take().send(outputStream), "Sender")
            .onException(this::close)
            .logger(logger::trace, "Connection problem ")
            .start();
    this.closer =
        new OneTimeClosable(
            List.of(input, output, receiverThread::stop, senderThread::stop, onClose::run));
  }

  /**
   * Create a new connection.
   *
   * @param incoming callback for incoming messages
   * @param input input stream
   * @param output output stream
   * @param onClose callback to call on close
   * @return the created connection
   */
  public static Connection createConnection(
      Consumer<Packet<?>> incoming,
      InputStream input,
      OutputStream output,
      ThrowingRunnable onClose) {
    return new Connection(incoming, input, output, onClose);
  }

  @Override
  public void send(Packet<?> packet) {
    this.outbound.add(packet);
  }

  @Override
  public boolean isAlive() {
    return !closer.isClosed();
  }

  @Override
  public synchronized void close() {
    closer.close();
  }
}
