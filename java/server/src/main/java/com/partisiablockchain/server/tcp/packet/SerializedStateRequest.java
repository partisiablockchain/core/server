package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Packet requesting a serialized state. */
public final class SerializedStateRequest implements DataStreamSerializable {

  private final Hash stateHash;

  /**
   * Create a new request.
   *
   * @param stateHash the state hash to get
   */
  public SerializedStateRequest(Hash stateHash) {
    this.stateHash = stateHash;
  }

  /**
   * Get the hash that is requested.
   *
   * @return the hash
   */
  public Hash getHash() {
    return stateHash;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    stateHash.write(stream);
  }

  /**
   * Read from stream.
   *
   * @param stream stream to read from
   * @return the read object
   */
  public static SerializedStateRequest read(SafeDataInputStream stream) {
    return new SerializedStateRequest(Hash.read(stream));
  }
}
