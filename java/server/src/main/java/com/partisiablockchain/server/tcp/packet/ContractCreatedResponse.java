package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Response to {@link ContractCreated} indicating if a listener should be registered. */
public final class ContractCreatedResponse implements DataStreamSerializable {

  private final BlockchainAddress address;
  private final boolean registerListener;

  /**
   * Create a new response for a created contract.
   *
   * @param address the address of the contract
   * @param registerListener true to register a listener
   */
  public ContractCreatedResponse(BlockchainAddress address, boolean registerListener) {
    this.address = address;
    this.registerListener = registerListener;
  }

  /**
   * Get the address of the contract that this is a response for.
   *
   * @return the contract address
   */
  public BlockchainAddress getAddress() {
    return address;
  }

  /**
   * Should register a listener.
   *
   * @return true to register listener
   */
  public boolean isRegisterListener() {
    return registerListener;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    address.write(stream);
    stream.writeBoolean(registerListener);
  }

  /**
   * Read from stream.
   *
   * @param stream the stream to read from
   * @return the read object
   */
  public static ContractCreatedResponse read(SafeDataInputStream stream) {
    BlockchainAddress address = BlockchainAddress.read(stream);
    boolean registerListener = stream.readBoolean();
    return new ContractCreatedResponse(address, registerListener);
  }
}
