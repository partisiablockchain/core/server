package com.partisiablockchain.server.tcp.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.serialization.StateSerializable;
import java.util.concurrent.Executor;

final class ContractEventDispatcher {

  private final StateLookup stateLookup;
  private final Client.Listener listener;
  private final Executor executor;

  ContractEventDispatcher(
      Executor singleThreadedExecutor,
      Client.Listener listener,
      StateSerializable contractState,
      BlockchainContract<?, ?> blockchainContract,
      StateLookup stateLookup) {
    this.listener = listener;
    this.executor = singleThreadedExecutor;
    this.stateLookup = stateLookup;
    executor.execute(() -> listener.update(contractState, blockchainContract));
  }

  void remove() {
    executor.execute(listener::remove);
  }

  void update(StateAndBinaries state) {
    executor.execute(
        () -> {
          StateAndContract currentState = stateLookup.read(state);
          listener.update(currentState.state, currentState.blockchainContract);
        });
  }

  interface StateLookup {
    StateAndContract read(StateAndBinaries identifier);
  }

  record StateAndContract(StateSerializable state, BlockchainContract<?, ?> blockchainContract) {}
}
