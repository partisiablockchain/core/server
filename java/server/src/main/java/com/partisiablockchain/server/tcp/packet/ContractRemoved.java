package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;

/** Packet indicating that a contract has been removed. */
public final class ContractRemoved implements DataStreamSerializable {

  private final BlockchainAddress address;

  /**
   * Create a new contract removed packet.
   *
   * @param address the address of the removed contract
   */
  public ContractRemoved(BlockchainAddress address) {
    this.address = address;
  }

  /**
   * Get the address of the removed contract.
   *
   * @return the address
   */
  public BlockchainAddress getAddress() {
    return address;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    address.write(stream);
  }

  /**
   * Read from stream.
   *
   * @param stream to read from
   * @return the read object
   */
  public static ContractRemoved read(SafeDataInputStream stream) {
    BlockchainAddress address = BlockchainAddress.read(stream);
    return new ContractRemoved(address);
  }
}
