package com.partisiablockchain.server.rest.resources;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.server.model.BlockchainStatistics;
import com.partisiablockchain.server.model.ResetBlock;
import com.partisiablockchain.server.model.ServerRuntime;
import com.partisiablockchain.server.rest.BlockSearcher;
import com.partisiablockchain.server.rest.ModelFactory;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Predicate;

/** Rest resource for receiving metrics of pbc. */
@Path("/metrics")
public final class MetricsResource {

  static final int MAX_BLOCKS_INTERVAL = 1000;
  private final BlockchainLedger blockchain;

  /**
   * Create a new metrics resource.
   *
   * @param blockchain the blockchain to get metrics for
   */
  public MetricsResource(BlockchainLedger blockchain) {
    this.blockchain = blockchain;
  }

  /**
   * Retrieve the blocks finalized on the chain within the last {@code timeFrameMinutes}.
   *
   * @param timeFrameMinutes the time frame in which to look for blocks
   * @return the block list
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks/{timeFrameMinutes}")
  public BlockchainStatistics getLatestBlocks(@PathParam("timeFrameMinutes") int timeFrameMinutes) {
    long endTime = getEndTime(timeFrameMinutes);
    BlockchainStatistics.Builder builder = new BlockchainStatistics.Builder();
    BlockSearcher allBlocksSearcher = BlockSearcher.create(falsePredicate(builder::addBlock));
    traverseBlocksUntil(endTime, allBlocksSearcher);
    return builder.build();
  }

  /**
   * Retrieve the blocks finalized on the chain between with block times between {@code
   * blockTimeFrom} and {@code blockTimeTo} both inclusive. Setting {@code blockTimeTo} to -1 will
   * attempt to get the latest block time on the blockchain. Interval is restricted to be at most
   * {@code MAX_BLOCKS_INTERVAL}.
   *
   * @param blockTimeFrom lower bound on block time
   * @param blockTimeTo upper bound on block time
   * @return blocks statistics for the interval
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks")
  public BlockchainStatistics getBlocksFromBlockTimeInterval(
      @QueryParam("from") long blockTimeFrom, @QueryParam("to") long blockTimeTo) {
    if (blockTimeTo == -1) {
      blockTimeTo = blockchain.getLatestBlock().getBlockTime();
    }
    validateBlockInterval(blockTimeFrom, blockTimeTo);

    BlockchainStatistics.Builder builder = new BlockchainStatistics.Builder();
    for (long i = blockTimeFrom; i <= blockTimeTo; i++) {
      Block currentBlock = blockchain.getBlock(i);
      builder.addBlock(currentBlock);
    }
    return builder.build();
  }

  private void validateBlockInterval(long blockTimeFrom, long blockTimeTo) {
    boolean fromIsLarger = blockTimeFrom > blockTimeTo;
    boolean intervalIsTooLarge = blockTimeTo - blockTimeFrom + 1 > MAX_BLOCKS_INTERVAL;
    boolean toBlockIsNull = blockchain.getBlock(blockTimeTo) == null;
    if (fromIsLarger) {
      throw new IllegalArgumentException("'from' has to be equal or less than 'to'");
    } else if (intervalIsTooLarge) {
      throw new IllegalArgumentException("Interval is larger than " + MAX_BLOCKS_INTERVAL);
    } else if (toBlockIsNull) {
      throw new RuntimeException("'to' was larger than latest block time");
    }
  }

  private <T> Predicate<T> falsePredicate(Consumer<T> consumer) {
    return t -> {
      consumer.accept(t);
      return false;
    };
  }

  private long getEndTime(int timeFrameMinutes) {
    return System.currentTimeMillis() - timeFrameMinutes * 60_000L;
  }

  private void traverseBlocksUntil(long endTime, BlockSearcher blockSearcher) {
    Block currentBlock = blockchain.getLatestBlock();
    while (currentBlock != null && isWithinTimeframe(endTime, currentBlock)) {
      if (blockSearcher.handleBlock(currentBlock)) {
        return;
      }
      currentBlock = blockchain.getBlock(currentBlock.getParentBlock());
    }
  }

  boolean isWithinTimeframe(long endTime, Block currentBlock) {
    return currentBlock.getProductionTime() >= endTime;
  }

  /**
   * Search for the latest reset block in the blocks finalized on the chain within the last {@code
   * timeFrameMinutes}.
   *
   * @param timeFrameMinutes the time frame in which to look for blocks
   * @return the latest reset block and the previous block
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/blocks/reset/{timeFrameMinutes}")
  public ResetBlock getLatestResetBlock(@PathParam("timeFrameMinutes") int timeFrameMinutes) {
    long endTime = getEndTime(timeFrameMinutes);
    AtomicReference<ResetBlock> resetBlock = new AtomicReference<>();
    BlockSearcher resetBlockSearcher = createResetBlockSearcher(resetBlock);
    traverseBlocksUntil(endTime, resetBlockSearcher);
    return resetBlock.get();
  }

  BlockSearcher createResetBlockSearcher(AtomicReference<ResetBlock> resetBlock) {
    return BlockSearcher.create(
        block -> {
          if (block.getProducerIndex() == -1) {
            Block previous = blockchain.getBlock(block.getParentBlock());
            resetBlock.set(ModelFactory.createResetBlock(block, previous));
            return true;
          } else {
            return false;
          }
        });
  }

  /**
   * Retrieve the runtime of the blockchain.
   *
   * @return the runtime
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/runtime")
  public ServerRuntime getRuntime() {
    Runtime runtime = Runtime.getRuntime();
    long freeMemory = runtime.freeMemory();
    long totalMemory = runtime.totalMemory();
    long maxMemory = runtime.maxMemory();
    int availableProcessors = runtime.availableProcessors();
    return new ServerRuntime(freeMemory, totalMemory, maxMemory, availableProcessors);
  }
}
