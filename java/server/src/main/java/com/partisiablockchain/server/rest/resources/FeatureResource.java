package com.partisiablockchain.server.rest.resources;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.dto.Feature;
import com.partisiablockchain.tree.AvlTree;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/** Rest resource for all features. */
@Path("/features")
@Produces(MediaType.APPLICATION_JSON)
public final class FeatureResource {

  private final BlockchainLedger blockchainLedger;

  /**
   * Resource for returning features on the blockchain.
   *
   * @param blockchainLedger the blockchain
   */
  public FeatureResource(BlockchainLedger blockchainLedger) {
    this.blockchainLedger = blockchainLedger;
  }

  /**
   * Returns all features and their state on the blockchain.
   *
   * @return Json containing feature and its state
   */
  @GET
  public List<Feature> getFeatures() {
    List<Feature> features = new ArrayList<>();
    AvlTree<String, String> avlTree = blockchainLedger.getChainState().getFeatures();
    avlTree
        .keySet()
        .forEach(
            key -> {
              features.add(new Feature(key, avlTree.getValue(key)));
            });
    return features;
  }
}
