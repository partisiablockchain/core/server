package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.dto.PluginType;

/** Converter for chain plugin types to plugin types. */
public final class PluginTypeConverter {

  /**
   * Convert from ChainPluginType to PluginType.
   *
   * @param chainPluginType the ChainPluginType to convert
   * @return the converted plugin type
   */
  public static PluginType convert(ChainPluginType chainPluginType) {
    if (chainPluginType == ChainPluginType.ACCOUNT) {
      return PluginType.ACCOUNT;
    } else if (chainPluginType == ChainPluginType.ROUTING) {
      return PluginType.ROUTING;
    } else if (chainPluginType == ChainPluginType.CONSENSUS) {
      return PluginType.CONSENSUS;
    } else if (chainPluginType == ChainPluginType.SHARED_OBJECT_STORE) {
      return PluginType.SHARED_OBJECT_STORE;
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * Convert from ChainPluginType to string.
   *
   * @param chainPluginType the ChainPluginType to convert
   * @return the string corresponding to the ChainPluginType
   */
  public static String convertToString(ChainPluginType chainPluginType) {
    if (chainPluginType == ChainPluginType.ACCOUNT) {
      return "ACCOUNT";
    } else if (chainPluginType == ChainPluginType.ROUTING) {
      return "ROUTING";
    } else if (chainPluginType == ChainPluginType.CONSENSUS) {
      return "CONSENSUS";
    } else if (chainPluginType == ChainPluginType.SHARED_OBJECT_STORE) {
      return "SHARED_OBJECT_STORE";
    } else {
      throw new IllegalArgumentException();
    }
  }
}
