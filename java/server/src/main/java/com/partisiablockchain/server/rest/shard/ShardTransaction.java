package com.partisiablockchain.server.rest.shard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.TransactionCost;
import com.partisiablockchain.crypto.Hash;
import java.util.List;

/** Record used for transfering shard transaction between service and controller. */
@SuppressWarnings("ArrayRecordComponent")
public record ShardTransaction(
    Hash identifier, boolean isEvent, byte[] content, ShardExecutionStatus executionStatus) {

  /** Shard execution status. */
  public record ShardExecutionStatus(
      Hash blockId,
      boolean success,
      boolean finalized,
      List<ShardTransactionPointer> events,
      TransactionCost cost,
      ShardFailure failure) {

    /** Transaction pointer. */
    public record ShardTransactionPointer(Hash identifier, String destinationShard) {}

    /** Failure record. */
    public record ShardFailure(String errorMessage, String stackTrace) {}
  }
}
