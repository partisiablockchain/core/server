package com.partisiablockchain.server.rest.shard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;

/** Interface for communicating with a shard. */
public interface ShardService {

  /**
   * Get the state of the shard.
   *
   * @param shardId id of the shard
   * @return the currrent state of the shard
   */
  ShardState getShardState(String shardId);

  /**
   * Get a shard block by hash, if the hash is null the latest block is returned.
   *
   * @param shardId id of the shard
   * @param identifier of the block
   * @return the block or null if not found
   */
  ShardBlock getShardBlock(String shardId, Hash identifier);

  /**
   * Get a transaction by hash.
   *
   * @param shardId id of the shard
   * @param identifier of the transaction
   * @return the transaction or null if not found
   */
  ShardTransaction getShardTransaction(String shardId, Hash identifier);

  /**
   * Get a jar on a specific shard with specific identifier.
   *
   * @param shardId id of the shard
   * @param identifer of the jar
   * @return the jar or null if not found
   */
  ShardJar getShardJar(String shardId, Hash identifer);
}
