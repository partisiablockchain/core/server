package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.contract.ContractType;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.LargeByteArray;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** Helper to created cached class loaders. */
public final class ClassLoaderHelper {

  private final Map<Hash, WeakReference<JarClassLoader>> classLoaders;
  private final Map<ContractAndBinder, WeakReference<JarClassLoader>> contractClassLoaders;
  private final Set<String> systemClassLoadedNamespaces;

  /**
   * Create new ClassLoaderHelper.
   *
   * @param systemClassLoadedNamespaces a set of namespaces for which the classes should be loaded
   *     by the default classloader
   */
  public ClassLoaderHelper(Set<String> systemClassLoadedNamespaces) {
    this.systemClassLoadedNamespaces = Set.copyOf(systemClassLoadedNamespaces);
    classLoaders = new HashMap<>();
    contractClassLoaders = new HashMap<>();
  }

  synchronized JarClassLoader jarClassLoader(LargeByteArray jar) {
    WeakReference<JarClassLoader> classLoader = classLoaders.get(jar.getIdentifier());
    JarClassLoader jarClassLoader = getClassLoaderOrNull(classLoader);
    if (jarClassLoader == null) {
      JarClassLoader.Builder classLoaderBuilder = JarClassLoader.builder(jar.getData());
      systemClassLoadedNamespaces.forEach(classLoaderBuilder::addSystemClassLoadingNamespace);
      jarClassLoader = classLoaderBuilder.build();
      classLoaders.put(jar.getIdentifier(), new WeakReference<>(jarClassLoader));
    }
    return jarClassLoader;
  }

  synchronized JarClassLoader contractClassLoader(LargeByteArray jar, JarClassLoader binderLoader) {
    ContractAndBinder contractAndBinder = new ContractAndBinder(jar.getIdentifier(), binderLoader);
    WeakReference<JarClassLoader> classLoader = contractClassLoaders.get(contractAndBinder);
    JarClassLoader jarClassLoader = getClassLoaderOrNull(classLoader);
    if (jarClassLoader == null) {
      JarClassLoader.Builder classLoaderBuilder =
          JarClassLoader.builder(jar.getData()).defaultClassLoader(binderLoader);
      systemClassLoadedNamespaces.forEach(classLoaderBuilder::addSystemClassLoadingNamespace);
      jarClassLoader = classLoaderBuilder.build();
      contractClassLoaders.put(contractAndBinder, new WeakReference<>(jarClassLoader));
    }
    return jarClassLoader;
  }

  private JarClassLoader getClassLoaderOrNull(WeakReference<JarClassLoader> classLoader) {
    if (classLoader != null) {
      return classLoader.get();
    } else {
      return null;
    }
  }

  /**
   * Create a BlockchainContract with the supplied binder and contract.
   *
   * @param type the type of the contract
   * @param binderJar the binder of the contract
   * @param contractJar the contract of the contract
   * @return the created blockchain contract
   */
  public BlockchainContract<?, ?> blockchainContract(
      ContractType type, LargeByteArray binderJar, LargeByteArray contractJar) {
    JarClassLoader binderLoader = jarClassLoader(binderJar);

    Object binder;
    Object contract;
    if (isRawBinder(binderLoader.getMainClass())) {
      contract = contractJar.getData();
      binder = binderLoader.instantiateBinder(byte[].class, contract);
    } else {
      JarClassLoader contractLoader = contractClassLoader(contractJar, binderLoader);
      contract = contractLoader.instantiateMainClass();
      binder = binderLoader.instantiateBinder(contract.getClass().getSuperclass(), contract);
    }

    if (type == ContractType.PUBLIC) {
      return JarClassLoader.readPubBlockchainContract(binder);
    }
    if (type == ContractType.SYSTEM || type == ContractType.GOVERNANCE) {
      return JarClassLoader.readSysBlockchainContract(binder);
    }
    if (type == ContractType.ZERO_KNOWLEDGE) {
      return JarClassLoader.readZkBlockchainContract(binder);
    }
    throw new UnsupportedOperationException("Unknown contract type: " + type);
  }

  @SuppressWarnings("ReturnValueIgnored")
  private boolean isRawBinder(Class<?> type) {
    try {
      type.getDeclaredConstructor(byte[].class);
      return true;
    } catch (NoSuchMethodException e) {
      return false;
    }
  }
}
