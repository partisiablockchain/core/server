package com.partisiablockchain.kademlia;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.net.InetAddress;

/** Test helper. */
public final class KademliaHelper {

  /**
   * Get the key of the kademlia node.
   *
   * @param node the node to access
   * @return the key as bytes
   */
  public static byte[] getKeyBytes(KademliaNode node) {
    return node.getKey().toBytes();
  }

  /**
   * Get the key of the kademlia node.
   *
   * @param node the node to access
   * @return the address
   */
  public static InetAddress getAddress(KademliaNode node) {
    return node.getAddress();
  }

  /**
   * Get the port of the kademlia node.
   *
   * @param node the node to access
   * @return the port
   */
  public static int getPort(KademliaNode node) {
    return node.getPort();
  }
}
