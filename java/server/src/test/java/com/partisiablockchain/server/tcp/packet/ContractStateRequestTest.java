package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractStateRequestTest {

  @Test
  public void equals() {
    EqualsVerifier.forClass(ContractStateRequest.class).verify();
  }

  @Test
  public void readWrite() {
    ContractStateRequest request = new ContractStateRequest(TestObjects.ACCOUNT_TWO, 789L);

    ContractStateRequest read =
        SafeDataInputStream.readFully(
            SafeDataOutputStream.serialize(request), ContractStateRequest::read);
    Assertions.assertThat(read).usingRecursiveComparison().isEqualTo(request);
  }

  @Test
  public void getters() {
    ContractStateRequest request = new ContractStateRequest(TestObjects.ACCOUNT_TWO, 78992L);
    Assertions.assertThat(request.getAddress()).isEqualTo(TestObjects.ACCOUNT_TWO);
    Assertions.assertThat(request.getBlockTime()).isEqualTo(78992L);
  }
}
