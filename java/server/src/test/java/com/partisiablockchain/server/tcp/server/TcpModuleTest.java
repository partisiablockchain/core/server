package com.partisiablockchain.server.tcp.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.server.ServerModule;
import com.partisiablockchain.storage.MemoryStorage;
import java.io.File;
import java.io.IOException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;

/** Test. */
public final class TcpModuleTest {

  @TempDir private File tmp;

  @Test
  public void register() throws IOException {
    TcpModule.TcpModuleConfig config = new TcpModule.TcpModuleConfig();
    config.port = 0;
    BlockchainLedger blockchain = Mockito.mock(BlockchainLedger.class);
    ServerModule.ServerConfig resourceConfig = Mockito.mock(ServerModule.ServerConfig.class);
    TcpModule restNode = new TcpModule();
    restNode.register(MemoryStorage.createRootDirectory(tmp), blockchain, resourceConfig, config);

    Mockito.verify(resourceConfig).registerCloseable(Mockito.any(Server.class));
  }

  @Test
  public void configType() {
    Assertions.assertThat(new TcpModule().configType()).isEqualTo(TcpModule.TcpModuleConfig.class);
  }
}
