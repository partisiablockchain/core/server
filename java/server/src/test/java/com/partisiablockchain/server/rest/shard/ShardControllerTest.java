package com.partisiablockchain.server.rest.shard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.server.rest.model.Block;
import com.partisiablockchain.server.rest.model.ExecutedTransaction;
import com.partisiablockchain.server.rest.model.ExecutionStatus;
import com.partisiablockchain.server.rest.model.Failure;
import com.partisiablockchain.server.rest.model.Jar;
import com.partisiablockchain.server.rest.model.Shard;
import com.partisiablockchain.server.rest.model.TransactionCost;
import com.partisiablockchain.server.rest.model.TransactionPointer;
import jakarta.ws.rs.NotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test for shard controller. */
public final class ShardControllerTest extends CloseableTest {

  private final ShardServiceDummy shardServiceDummy = new ShardServiceDummy();
  private final ShardController shardController = new ShardController(shardServiceDummy);
  private final ObjectMapper mapper = StateObjectMapper.createObjectMapper();
  private final String shardId = "Gov";

  @Test
  public void getShard() {
    long governanceVersion = 0L;
    Map<String, JsonNode> plugins = Map.of("ACCOUNT", mapper.valueToTree("state"));

    Shard shard = new Shard().governanceVersion(governanceVersion).plugins(plugins);

    assertThat(shardController.getShard("Gov")).isEqualTo(shard);
  }

  @Test
  public void expectNotFoundWithInvalidBlockId() {
    String notFoundBlockId = shardServiceDummy.invalidBlockId.toString();
    assertThatThrownBy(() -> shardController.getBlock(shardId, notFoundBlockId))
        .isInstanceOf(NotFoundException.class);
  }

  @Test
  public void expectBlockWithValidBlockId() {
    String validBlockId = shardServiceDummy.validBlockId.toString();

    Hash identifier = shardServiceDummy.validBlockId;
    Hash parentBlock = shardServiceDummy.parentBlock;
    long committeeId = 0L;
    long blockTime = 0L;
    long productionTime = 0L;
    List<Hash> events = List.of(shardServiceDummy.eventHash);
    List<Hash> transactions = List.of(shardServiceDummy.transactionHash);
    Hash state = shardServiceDummy.blockState;
    short producer = 0;

    Block expected =
        new Block()
            .identifier(identifier.toString())
            .parentBlock(parentBlock.toString())
            .committeeId(committeeId)
            .blockTime(blockTime)
            .productionTime(productionTime)
            .events(events.stream().map(Hash::toString).toList())
            .transactions(transactions.stream().map(Hash::toString).toList())
            .state(state.toString())
            .producer((int) producer);

    assertThat(shardController.getBlock(shardId, validBlockId)).isEqualTo(expected);
  }

  @Test
  public void expectedLatestBlockWithNullBlockId() {
    Hash identifier = shardServiceDummy.latestValidBlockId;
    Hash parentBlock = shardServiceDummy.parentBlock;
    long committeeId = 0L;
    long blockTime = 0L;
    long productionTime = 0L;
    List<Hash> events = List.of(shardServiceDummy.eventHash);
    List<Hash> transactions = List.of(shardServiceDummy.transactionHash);
    Hash state = shardServiceDummy.blockState;
    short producer = 0;

    Block expected = new Block();
    expected.setIdentifier(identifier.toString());
    expected.setParentBlock(parentBlock.toString());
    expected.setCommitteeId(committeeId);
    expected.setBlockTime(blockTime);
    expected.setProductionTime(productionTime);
    expected.setEvents(events.stream().map(Hash::toString).toList());
    expected.setTransactions(transactions.stream().map(Hash::toString).toList());
    expected.setState(state.toString());
    expected.setProducer((int) producer);

    assertThat(shardController.getBlock(shardId, null)).isEqualTo(expected);
    assertThat(shardController.getLatestBlock(shardId)).isEqualTo(expected);
  }

  @Test
  public void successfulTransaction() {
    Hash identifier = shardServiceDummy.successfulTransaction;
    ExecutedTransaction expected =
        new ExecutedTransaction()
            .identifier(shardServiceDummy.validTransactionId.toString())
            .isEvent(true)
            .content(shardServiceDummy.txContent)
            .executionStatus(
                new ExecutionStatus()
                    .blockId(shardServiceDummy.exBlockId.toString())
                    .success(false)
                    .finalized(false)
                    .events(List.of())
                    .transactionCost(
                        new TransactionCost()
                            .allocatedForEvents(0L)
                            .cpu(1L)
                            .remaining(2L)
                            .paidByContract(3L)
                            .networkFees(new HashMap<>()))
                    .failure(null));
    assertThat(shardController.getTransaction(shardId, identifier.toString())).isEqualTo(expected);
  }

  @Test
  public void expectNotFoundWithInvalidTransactionid() {
    String notFoundTransactionId = shardServiceDummy.invalidTransactionId.toString();
    assertThatThrownBy(() -> shardController.getTransaction(shardId, notFoundTransactionId))
        .isInstanceOf(NotFoundException.class);
  }

  /** A pending transaction returns a null execution status. */
  @Test
  public void pendingTransaction() {
    String pendingTransactionId = shardServiceDummy.pendingTransaction.toString();
    ExecutedTransaction expected =
        new ExecutedTransaction()
            .identifier(pendingTransactionId)
            .isEvent(true)
            .content(shardServiceDummy.txContent);
    assertThat(shardController.getTransaction(shardId, pendingTransactionId)).isEqualTo(expected);
  }

  @Test
  public void expectValidTransactionWithValidTransactionId() {
    Hash identifer = shardServiceDummy.validTransactionId;
    boolean isEvent = true;
    byte[] content = shardServiceDummy.txContent;

    Failure failure = new Failure().errorMessage("message").stackTrace("trace");
    TransactionPointer txPointer =
        new TransactionPointer()
            .identifier(shardServiceDummy.txPointerId.toString())
            .destinationShardId(shardServiceDummy.destinationShard);
    TransactionCost cost =
        new TransactionCost()
            .allocatedForEvents(0L)
            .cpu(1L)
            .remaining(2L)
            .paidByContract(3L)
            .networkFees(new HashMap<>());
    ExecutionStatus executionStatus =
        new ExecutionStatus()
            .blockId(shardServiceDummy.exBlockId.toString())
            .success(true)
            .finalized(true)
            .events(List.of(txPointer))
            .transactionCost(cost)
            .failure(failure);
    ExecutedTransaction expected =
        new ExecutedTransaction()
            .identifier(identifer.toString())
            .isEvent(isEvent)
            .content(content)
            .executionStatus(executionStatus);
    assertThat(shardController.getTransaction(shardId, identifer.toString())).isEqualTo(expected);
  }

  @Test
  public void expectNotFoundWithInvalidJarId() {
    String notFoundJarId = shardServiceDummy.invalidJarId.toString();
    assertThatThrownBy(() -> shardController.getJar(shardId, notFoundJarId))
        .isInstanceOf(NotFoundException.class);
  }

  @Test
  public void expectJarWithValidJarId() {
    Jar expected = new Jar().data(shardServiceDummy.validJarData);
    assertThat(shardController.getJar(shardId, shardServiceDummy.validJarId.toString()))
        .isEqualTo(expected);
  }

  @Test
  void expect_no_transaction_cost_converted_from_null() {
    assertThat(ShardController.createTransactionCost(null)).isEqualTo(null);
  }

  @Test
  void should_convert_transaction_cost() {
    HashMap<Hash, Long> networkFees = new HashMap<>();
    networkFees.put(Hash.fromString("a".repeat(64)), 2L);
    com.partisiablockchain.blockchain.TransactionCost cost =
        new com.partisiablockchain.blockchain.TransactionCost(1, 2, 3, 4, networkFees);
    HashMap<String, Long> convertedNetworkFees = new HashMap<>();
    convertedNetworkFees.put("a".repeat(64), 2L);
    assertThat(ShardController.createTransactionCost(cost))
        .isEqualTo(
            new TransactionCost()
                .allocatedForEvents(1L)
                .cpu(2L)
                .remaining(3L)
                .paidByContract(4L)
                .networkFees(convertedNetworkFees));
  }
}
