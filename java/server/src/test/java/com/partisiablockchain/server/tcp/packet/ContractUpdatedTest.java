package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractUpdatedTest {

  @Test
  public void readWrite() {
    ContractUpdated response =
        new ContractUpdated(
            TestObjects.ACCOUNT_TWO,
            Hash.create(s -> s.writeInt(34)),
            Hash.create(s -> s.writeInt(56)),
            Hash.create(s -> s.writeInt(123)));

    ContractUpdated read =
        SafeDataInputStream.readFully(
            SafeDataOutputStream.serialize(response), ContractUpdated::read);
    Assertions.assertThat(read).usingRecursiveComparison().isEqualTo(response);
  }

  @Test
  public void getters() {
    Hash stateHash = Hash.create(s -> s.writeInt(12));
    Hash binderHash = Hash.create(s -> s.writeInt(34));
    Hash contractHash = Hash.create(s -> s.writeInt(56));
    ContractUpdated response =
        new ContractUpdated(TestObjects.ACCOUNT_TWO, binderHash, contractHash, stateHash);
    Assertions.assertThat(response.getAddress()).isEqualTo(TestObjects.ACCOUNT_TWO);
    Assertions.assertThat(response.getStateHash()).isEqualTo(stateHash);
    Assertions.assertThat(response.getBinderHash()).isEqualTo(binderHash);
    Assertions.assertThat(response.getContractHash()).isEqualTo(contractHash);
  }
}
