package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractCreatedTest {

  @Test
  public void readWrite() {
    ContractCreated contractCreated =
        new ContractCreated(
            TestObjects.ACCOUNT_FOUR,
            Hash.create(s -> s.writeInt(1)),
            Hash.create(s -> s.writeInt(2)),
            TestObjects.EMPTY_HASH,
            7,
            Hash.create(s -> s.writeInt(7)));
    ContractCreated read =
        SafeDataInputStream.readFully(
            SafeDataOutputStream.serialize(contractCreated), ContractCreated::read);
    Assertions.assertThat(read).usingRecursiveComparison().isEqualTo(contractCreated);
  }

  @Test
  public void getters() {
    BlockchainAddress address = TestObjects.ACCOUNT_FOUR;
    Hash binderHash = Hash.create(s -> s.writeInt(1));
    Hash contractHash = Hash.create(s -> s.writeInt(2));
    Hash stateHash = Hash.create(s -> s.writeInt(7));
    Hash abiHash = Hash.create(s -> s.writeInt(72));
    ContractCreated contractCreated =
        new ContractCreated(address, binderHash, contractHash, abiHash, 7, stateHash);
    Assertions.assertThat(contractCreated.getAddress()).isEqualTo(TestObjects.ACCOUNT_FOUR);
    Assertions.assertThat(contractCreated.getBinderHash()).isEqualTo(binderHash);
    Assertions.assertThat(contractCreated.getContractHash()).isEqualTo(contractHash);
    Assertions.assertThat(contractCreated.getAbiHash()).isEqualTo(abiHash);
    Assertions.assertThat(contractCreated.getCodeSize()).isEqualTo(7);
    Assertions.assertThat(contractCreated.getStateHash()).isEqualTo(stateHash);
  }
}
