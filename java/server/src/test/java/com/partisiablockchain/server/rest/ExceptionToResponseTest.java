package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import jakarta.ws.rs.ForbiddenException;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Tests for {@link ExceptionToResponse}. */
public final class ExceptionToResponseTest {

  private final ExceptionToResponse mapper = new ExceptionToResponse();

  @Test
  public void exceptionToResponse() {
    assertToResponse(new RuntimeException(), 500);
  }

  @Test
  public void illegalArgumentToResponse() {
    assertToResponse(new IllegalArgumentException(), 400);
  }

  @Test
  public void illegalStateToResponse() {
    assertToResponse(new IllegalStateException(), 400);
  }

  @Test
  public void accessForbiddenToResponse() {
    assertToResponse(new ForbiddenException("Dummy"), 403);
  }

  @Test
  public void webApplicationException() {
    WebApplicationException exception = new WebApplicationException(404);
    Response response = assertToResponse(exception, 404);
    Assertions.assertThat(response).isEqualTo(exception.getResponse());
  }

  private Response assertToResponse(Exception exception, int expectedStatus) {
    Response response = mapper.toResponse(exception);
    Assertions.assertThat(response.getStatus()).isEqualTo(expectedStatus);
    return response;
  }
}
