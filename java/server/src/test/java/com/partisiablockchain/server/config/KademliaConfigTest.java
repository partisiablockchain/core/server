package com.partisiablockchain.server.config;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.kademlia.KademliaHelper;
import com.partisiablockchain.kademlia.KademliaKey;
import com.partisiablockchain.kademlia.KademliaNode;
import com.partisiablockchain.server.config.KademliaConfig.TargetConfig;
import java.util.Base64;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class KademliaConfigTest {

  @Test
  public void values() {
    KademliaConfig kademliaConfig = createKademliaConfig();
    TargetConfig target = kademliaConfig.config();
    Assertions.assertThat(target.host()).isEqualTo("localhost");
    Assertions.assertThat(target.kademliaId())
        .isEqualTo("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB=");
    Assertions.assertThat(target.port()).isEqualTo(7777);
    Assertions.assertThat(kademliaConfig.kademliaKey())
        .isEqualTo("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=");
  }

  @Test
  public void getOrCreateKademliaKey() {
    KademliaConfig kademliaConfigWithKey = createKademliaConfig();
    KademliaKey keyFromRaw = kademliaConfigWithKey.getOrCreateKademliaKey();

    KademliaConfig otherKademliaConfigWithKey = createKademliaConfig();
    KademliaKey otherKeyFromRaw = otherKademliaConfigWithKey.getOrCreateKademliaKey();

    KademliaConfig anotherKademliaConfigWithoutKey = new KademliaConfig(null, null);
    KademliaKey createdKey = anotherKademliaConfigWithoutKey.getOrCreateKademliaKey();

    Assertions.assertThat(keyFromRaw).isNotNull();
    Assertions.assertThat(keyFromRaw).isEqualTo(otherKeyFromRaw);
    Assertions.assertThat(createdKey).isNotNull();
    Assertions.assertThat(createdKey).isNotEqualTo(otherKeyFromRaw);
  }

  @Test
  public void getKnownNodes() {
    KademliaConfig kademliaConfig = createKademliaConfig();

    List<KademliaNode> knownNodes = kademliaConfig.getKnownNodes();
    Assertions.assertThat(knownNodes).hasSize(1);

    KademliaNode knownNode = knownNodes.get(0);
    Assertions.assertThat(KademliaHelper.getAddress(knownNode).toString())
        .isEqualTo("localhost/127.0.0.1");
    Assertions.assertThat(KademliaHelper.getPort(knownNode))
        .isEqualTo(kademliaConfig.config().port());

    byte[] expectedBytes = Base64.getDecoder().decode(kademliaConfig.config().kademliaId());
    byte[] actualBytes = KademliaHelper.getKeyBytes(knownNode);
    Assertions.assertThat(actualBytes).isEqualTo(expectedBytes);
  }

  @Test
  public void noKnownNodes() {
    KademliaConfig kademliaConfig = new KademliaConfig("", null);
    Assertions.assertThat(kademliaConfig.getKnownNodes()).isEmpty();
  }

  static KademliaConfig createKademliaConfig() {
    TargetConfig targetConfig =
        new TargetConfig("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB=", "localhost", 7777);
    KademliaConfig kademliaConfig =
        new KademliaConfig("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=", targetConfig);
    return kademliaConfig;
  }
}
