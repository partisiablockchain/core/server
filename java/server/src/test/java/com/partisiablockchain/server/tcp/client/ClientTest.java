package com.partisiablockchain.server.tcp.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.ObjectCreator;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.sys.SysBinderContext;
import com.partisiablockchain.binder.sys.SysBinderContract;
import com.partisiablockchain.blockchain.contract.StringToStringContract;
import com.partisiablockchain.blockchain.contract.binder.BlockchainContract;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateBoolean;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.server.rest.resources.helper.SystemBinderJarHelper;
import com.partisiablockchain.server.tcp.packet.ContractCreated;
import com.partisiablockchain.server.tcp.packet.ContractCreatedResponse;
import com.partisiablockchain.server.tcp.packet.ContractRemoved;
import com.partisiablockchain.server.tcp.packet.ContractStateRequest;
import com.partisiablockchain.server.tcp.packet.ContractStateResponse;
import com.partisiablockchain.server.tcp.packet.ContractUpdated;
import com.partisiablockchain.server.tcp.packet.Handshake;
import com.partisiablockchain.server.tcp.packet.Packet;
import com.partisiablockchain.server.tcp.packet.SerializedStateResponse;
import com.partisiablockchain.server.tcp.threads.ScheduledSingleThreadExecutor;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.WithCloseableResources;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** Test for {@link Client}. */
public final class ClientTest extends CloseableTest {

  private static final BlockchainAddress contractAddress =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(stream -> stream.writeInt(123)));

  @Test
  public void scheduleConnectOnCreateRetryOnFailure() {
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("nowhere", 10),
                (contract, history, address, core, state) -> null,
                ObjectCreator.createMemoryStateStorage(),
                scheduler,
                Collections.emptySet()));
    Assertions.assertThat(client.isConnected()).isFalse();
    Assertions.assertThat(scheduler.latestDelay).isEqualTo(1_000);
    Runnable first = scheduler.latest;
    first.run();
    Assertions.assertThat(scheduler.latestDelay).isEqualTo(5_000);
    Runnable second = scheduler.latest;
    second.run();
    Assertions.assertThat(scheduler.latestDelay).isEqualTo(5_000);
    Assertions.assertThat(scheduler.latest).isNotSameAs(second);
  }

  @Test
  public void connectAndRetryOnFailure() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    final Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) -> null,
                ObjectCreator.createMemoryStateStorage(),
                scheduler,
                Collections.emptySet()));
    Assertions.assertThat(scheduler.latestDelay).isEqualTo(1_000);
    Runnable first = scheduler.latest;
    scheduler.latch = new CountDownLatch(1);
    first.run();
    Assertions.assertThat(client.isConnected()).isTrue();
    serverSocket.accept().close();
    Assertions.assertThat(scheduler.latch.await(1, TimeUnit.SECONDS)).isTrue();
    Assertions.assertThat(client.isConnected()).isFalse();
    Assertions.assertThat(scheduler.latestDelay).isEqualTo(1_000);
    Assertions.assertThat(scheduler.latest).isNotSameAs(first);
  }

  @Test
  public void createShardClient() {
    WithCloseableResources shardClient =
        Client.createShardClient(
            null,
            List.of(new Address("nowhere", 1), new Address("nowhere", 2)),
            (contract, history, address, core, state) -> null,
            ObjectCreator.createMemoryStateStorage(),
            Collections.emptySet());
    Assertions.assertThat(shardClient.resources()).hasSize(3).doesNotContainNull();
  }

  @Test
  public void shouldNotConnectIfAlreadyConnected() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) -> null,
                ObjectCreator.createMemoryStateStorage(),
                scheduler,
                Collections.emptySet()));
    Assertions.assertThat(client.isConnected()).isFalse();
    Runnable firstSchedule = scheduler.latest;
    firstSchedule.run();
    Assertions.assertThat(client.isConnected()).isTrue();
    serverSocket.accept();
    serverSocket.close();
    firstSchedule.run();
    Assertions.assertThat(scheduler.latest)
        .describedAs("Should not try to reconnect and thus not fail")
        .isEqualTo(firstSchedule);
  }

  @Test
  public void initializeListenerThenReconnect() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    StateStorageRaw memoryStateStorage = ObjectCreator.createMemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(memoryStateStorage, true);

    final Hash binderHash =
        stateSerializer
            .write(new LargeByteArray(SystemBinderJarHelper.getSystemBinderJar()))
            .hash();
    final Hash contractHash =
        stateSerializer
            .write(new LargeByteArray(JarBuilder.buildJar(StringToStringContract.class)))
            .hash();
    final Hash initialState = stateSerializer.write(new StateString("InitialString")).hash();
    final Hash updatedState = stateSerializer.write(new StateString("UpdatedString")).hash();

    AtomicReference<String> expectedState = new AtomicReference<>("InitialString");
    AtomicInteger updates = new AtomicInteger();
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) ->
                    new Client.Listener() {
                      @Override
                      public void update(StateSerializable state) {
                        Assertions.assertThat(state)
                            .hasFieldOrPropertyWithValue("value", expectedState.get());
                        updates.incrementAndGet();
                      }

                      @Override
                      public void remove() {}
                    },
                memoryStateStorage,
                scheduler,
                Collections.emptySet()));
    scheduler.latest.run();

    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_CREATED,
            new ContractCreated(
                contractAddress,
                binderHash,
                contractHash,
                TestObjects.EMPTY_HASH,
                123,
                initialState)));
    expectedState.set("UpdatedString");
    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_UPDATED,
            new ContractUpdated(contractAddress, binderHash, contractHash, updatedState)));

    Socket socket = serverSocket.accept();
    socket.setSoTimeout(500);
    SafeDataInputStream initialStream = new SafeDataInputStream(socket.getInputStream());
    Handshake.read(initialStream);
    Packet<?> createResponse =
        Packet.Type.parseInt(initialStream.readUnsignedByte()).parse(initialStream);
    Assertions.assertThat(createResponse.getType())
        .isEqualTo(Packet.Type.CONTRACT_CREATED_RESPONSE);
    ContractCreatedResponse payload = (ContractCreatedResponse) createResponse.getPayload();
    Assertions.assertThat(payload.isRegisterListener()).isTrue();
    Assertions.assertThat(updates).hasValue(2);

    // Close and reconnect
    scheduler.latch = new CountDownLatch(1);
    socket.close();
    Assertions.assertThat(scheduler.latch.await(1, TimeUnit.SECONDS)).isTrue();
    scheduler.latest.run();

    Socket secondSocket = serverSocket.accept();
    secondSocket.setSoTimeout(500);
    SafeDataInputStream safeDataInputStream =
        new SafeDataInputStream(secondSocket.getInputStream());
    Handshake handshake = Handshake.read(safeDataInputStream);
    Assertions.assertThat(handshake.getExisting()).containsExactly(contractAddress);
  }

  @Test
  public void updateWithNewBinderAndContractShouldCacheClassLoaderForSame() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    StateStorageRaw memoryStateStorage = ObjectCreator.createMemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(memoryStateStorage, true);

    final Hash binderHash =
        stateSerializer
            .write(new LargeByteArray(SystemBinderJarHelper.getSystemBinderJar()))
            .hash();
    final Hash contractHash =
        stateSerializer
            .write(
                new LargeByteArray(
                    JarBuilder.buildJar(StringToStringContract.class, StateString.class)))
            .hash();
    final Hash updatedBinder =
        stateSerializer.write(new LargeByteArray(JarBuilder.buildJar(MockSysBinder.class))).hash();
    // Include additional class (StateBoolean) to ensure hash changes
    final Hash updatedContract =
        stateSerializer
            .write(
                new LargeByteArray(
                    JarBuilder.buildJar(
                        StringToStringContract.class, StateString.class, StateBoolean.class)))
            .hash();
    final Hash initialState = stateSerializer.write(new StateString("InitialString")).hash();

    AtomicInteger updates = new AtomicInteger();
    AtomicBoolean newClassLoader = new AtomicBoolean(false);
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) ->
                    new Client.Listener() {
                      Class<?> stateClass;

                      @Override
                      public void update(StateSerializable state) {
                        if (stateClass != null) {
                          if (newClassLoader.get()) {
                            Assertions.assertThat(state.getClass()).isNotEqualTo(stateClass);
                          } else {
                            Assertions.assertThat(state.getClass()).isEqualTo(stateClass);
                          }
                        }

                        stateClass = state.getClass();
                        updates.incrementAndGet();
                      }

                      @Override
                      public void remove() {}
                    },
                memoryStateStorage,
                scheduler,
                Collections.emptySet()));

    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_CREATED,
            new ContractCreated(
                contractAddress,
                binderHash,
                contractHash,
                TestObjects.EMPTY_HASH,
                123,
                initialState)));
    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_UPDATED,
            new ContractUpdated(contractAddress, binderHash, contractHash, initialState)));

    newClassLoader.set(true);
    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_UPDATED,
            new ContractUpdated(contractAddress, binderHash, updatedContract, initialState)));
    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_UPDATED,
            new ContractUpdated(contractAddress, updatedBinder, updatedContract, initialState)));
    newClassLoader.set(false);
    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_UPDATED,
            new ContractUpdated(contractAddress, updatedBinder, updatedContract, initialState)));
  }

  /** You can create a listener which also receives changes to the blockchain contract. */
  @Test
  public void listenToBlockchainContractUpdates() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    StateStorageRaw memoryStateStorage = ObjectCreator.createMemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(memoryStateStorage, true);

    final Hash contractHash =
        stateSerializer
            .write(
                new LargeByteArray(
                    JarBuilder.buildJar(StringToStringContract.class, StateString.class)))
            .hash();
    final Hash binderHash =
        stateSerializer
            .write(new LargeByteArray(SystemBinderJarHelper.getSystemBinderJar()))
            .hash();
    final Hash updatedBinder =
        stateSerializer.write(new LargeByteArray(JarBuilder.buildJar(MockSysBinder.class))).hash();
    // Include additional class (StateBoolean) to ensure hash changes
    final Hash updatedContract =
        stateSerializer
            .write(
                new LargeByteArray(
                    JarBuilder.buildJar(
                        StringToStringContract.class, StateString.class, StateBoolean.class)))
            .hash();
    final Hash initialState = stateSerializer.write(new StateString("InitialString")).hash();

    final AtomicReference<BlockchainContract<?, ?>> latestBlockchainContract =
        new AtomicReference<>();
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) ->
                    new Client.Listener() {
                      @Override
                      public void update(
                          StateSerializable state, BlockchainContract<?, ?> blockchainContract) {
                        latestBlockchainContract.set(blockchainContract);
                      }

                      @Override
                      public void remove() {}
                    },
                memoryStateStorage,
                scheduler,
                Collections.emptySet()));

    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_CREATED,
            new ContractCreated(
                contractAddress,
                binderHash,
                contractHash,
                TestObjects.EMPTY_HASH,
                123,
                initialState)));
    Assertions.assertThat(latestBlockchainContract.get()).isNotNull();
    BlockchainContract<?, ?> prevReceivedBlockchainContract = latestBlockchainContract.get();
    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_UPDATED,
            new ContractUpdated(contractAddress, binderHash, contractHash, initialState)));

    Assertions.assertThat(latestBlockchainContract.get()).isNotNull();
    Assertions.assertThat(latestBlockchainContract.get()).isEqualTo(prevReceivedBlockchainContract);
    prevReceivedBlockchainContract = latestBlockchainContract.get();

    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_UPDATED,
            new ContractUpdated(contractAddress, binderHash, updatedContract, initialState)));

    Assertions.assertThat(latestBlockchainContract.get()).isNotNull();
    Assertions.assertThat(latestBlockchainContract.get())
        .isNotEqualTo(prevReceivedBlockchainContract);
    prevReceivedBlockchainContract = latestBlockchainContract.get();

    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_UPDATED,
            new ContractUpdated(contractAddress, updatedBinder, updatedContract, initialState)));

    Assertions.assertThat(latestBlockchainContract.get()).isNotNull();
    Assertions.assertThat(latestBlockchainContract.get())
        .isNotEqualTo(prevReceivedBlockchainContract);
    prevReceivedBlockchainContract = latestBlockchainContract.get();

    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_UPDATED,
            new ContractUpdated(contractAddress, updatedBinder, updatedContract, initialState)));

    Assertions.assertThat(latestBlockchainContract.get()).isNotNull();
    Assertions.assertThat(latestBlockchainContract.get()).isEqualTo(prevReceivedBlockchainContract);
  }

  private Hash createBinderHash(StateSerializer stateSerializer) {
    return stateSerializer
        .write(new LargeByteArray(SystemBinderJarHelper.getSystemBinderJar()))
        .hash();
  }

  private Hash createContractHash(StateSerializer stateSerializer) {
    return stateSerializer
        .write(new LargeByteArray(JarBuilder.buildJar(StringToStringContract.class)))
        .hash();
  }

  @Test
  public void createThenRemove() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    StateStorageRaw memoryStateStorage = ObjectCreator.createMemoryStateStorage();
    StateSerializer stateSerializer = new StateSerializer(memoryStateStorage, true);

    Hash binderHash = createBinderHash(stateSerializer);
    Hash contractHash = createContractHash(stateSerializer);
    Hash initialState = stateSerializer.write(new StateString("InitialString")).hash();

    CountDownLatch removed = new CountDownLatch(1);
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) ->
                    new Client.Listener() {
                      @Override
                      public void update(StateSerializable state) {}

                      @Override
                      public void remove() {
                        removed.countDown();
                      }
                    },
                memoryStateStorage,
                scheduler,
                Collections.emptySet()));
    scheduler.latest.run();

    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_CREATED,
            new ContractCreated(
                contractAddress,
                binderHash,
                contractHash,
                TestObjects.EMPTY_HASH,
                123,
                initialState)));
    client.incoming(
        new Packet<>(Packet.Type.CONTRACT_REMOVED, new ContractRemoved(contractAddress)));

    Assertions.assertThat(removed.await(1, TimeUnit.SECONDS)).isTrue();
  }

  @Test
  public void reconnectWithPendingRequests() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    ObjectCreator.MemoryStateStorage memoryStateStorage = ObjectCreator.createMemoryStateStorage();
    final Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) -> null,
                memoryStateStorage,
                scheduler,
                Collections.emptySet(),
                List.of(contractAddress)));
    // Connect
    scheduler.latest.run();

    Socket initialConnection = serverSocket.accept();
    initialConnection.setSoTimeout(500);
    SafeDataInputStream socketStream = new SafeDataInputStream(initialConnection.getInputStream());
    Handshake.read(socketStream);

    CountDownLatch pendingRequests = new CountDownLatch(1);

    StateSerializer stateSerializer = new StateSerializer(memoryStateStorage, true);

    Hash binderHash =
        stateSerializer
            .write(new LargeByteArray(SystemBinderJarHelper.getSystemBinderJar()))
            .hash();
    Hash contractHash =
        stateSerializer
            .write(new LargeByteArray(JarBuilder.buildJar(StringToStringContract.class)))
            .hash();
    Hash initialState = stateSerializer.write(new StateString("InitialString")).hash();
    final byte[] value = memoryStateStorage.read(binderHash);
    memoryStateStorage.remove(binderHash);

    register(
            new Thread(
                () -> {
                  // Triggers a request for the binder hash
                  client.incoming(
                      new Packet<>(
                          Packet.Type.CONTRACT_CREATED,
                          new ContractCreated(
                              contractAddress,
                              binderHash,
                              contractHash,
                              TestObjects.EMPTY_HASH,
                              123,
                              initialState)));
                  pendingRequests.countDown();
                }))
        .start();
    readPacket(socketStream);

    // Close and reconnect
    scheduler.latch = new CountDownLatch(1);
    initialConnection.close();
    Assertions.assertThat(scheduler.latch.await(1, TimeUnit.SECONDS)).isTrue();
    scheduler.latest.run();

    Socket accept = serverSocket.accept();
    accept.setSoTimeout(500);
    SafeDataInputStream safeDataInputStream = new SafeDataInputStream(accept.getInputStream());
    Handshake.read(safeDataInputStream);
    Packet<?> first = readPacket(safeDataInputStream);
    Assertions.assertThat(first.getType()).isEqualTo(Packet.Type.SERIALIZED_STATE_REQUEST);

    client.incoming(
        new Packet<>(Packet.Type.SERIALIZED_STATE_RESPONSE, new SerializedStateResponse(value)));
    Assertions.assertThat(pendingRequests.await(1, TimeUnit.SECONDS)).isTrue();

    Packet<?> createResponse =
        Packet.Type.parseInt(safeDataInputStream.readUnsignedByte()).parse(safeDataInputStream);
    Assertions.assertThat(createResponse.getType())
        .isEqualTo(Packet.Type.CONTRACT_CREATED_RESPONSE);
    ContractCreatedResponse payload = (ContractCreatedResponse) createResponse.getPayload();
    Assertions.assertThat(payload.isRegisterListener()).isFalse();
  }

  @Test
  public void getHistoricContractState() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    ObjectCreator.MemoryStateStorage memoryStateStorage = ObjectCreator.createMemoryStateStorage();
    CountDownLatch pendingRequests = new CountDownLatch(1);
    final Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) -> {
                  Objects.requireNonNull(history.getState(10));
                  pendingRequests.countDown();
                  return null;
                },
                memoryStateStorage,
                scheduler,
                Collections.emptySet(),
                List.of(contractAddress)));
    // Connect
    scheduler.latest.run();

    Socket initialConnection = serverSocket.accept();
    initialConnection.setSoTimeout(1000);
    SafeDataInputStream socketStream = new SafeDataInputStream(initialConnection.getInputStream());
    Handshake.read(socketStream);

    StateSerializer stateSerializer = new StateSerializer(memoryStateStorage, true);

    Hash binderHash =
        stateSerializer
            .write(new LargeByteArray(SystemBinderJarHelper.getSystemBinderJar()))
            .hash();
    Hash contractHash =
        stateSerializer
            .write(new LargeByteArray(JarBuilder.buildJar(StringToStringContract.class)))
            .hash();
    Hash initialState = stateSerializer.write(new StateString("InitialString")).hash();

    register(
            new Thread(
                () ->
                    client.incoming(
                        new Packet<>(
                            Packet.Type.CONTRACT_CREATED,
                            new ContractCreated(
                                contractAddress,
                                binderHash,
                                contractHash,
                                TestObjects.EMPTY_HASH,
                                123,
                                initialState)))))
        .start();
    Packet<?> packet = readPacket(socketStream);
    Assertions.assertThat(packet.getType()).isEqualTo(Packet.Type.CONTRACT_STATE_REQUEST);

    // Close and reconnect
    scheduler.latch = new CountDownLatch(1);
    initialConnection.close();
    Assertions.assertThat(scheduler.latch.await(1, TimeUnit.SECONDS)).isTrue();
    scheduler.latest.run();

    Socket accept = serverSocket.accept();
    accept.setSoTimeout(500);
    SafeDataInputStream safeDataInputStream = new SafeDataInputStream(accept.getInputStream());
    Handshake.read(safeDataInputStream);
    Packet<?> stateRequest = readPacket(safeDataInputStream);
    Assertions.assertThat(stateRequest.getType()).isEqualTo(Packet.Type.CONTRACT_STATE_REQUEST);

    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_STATE_RESPONSE,
            new ContractStateResponse(
                new ContractStateRequest(contractAddress, 10),
                binderHash,
                contractHash,
                initialState)));

    Assertions.assertThat(pendingRequests.await(1, TimeUnit.SECONDS)).isTrue();
  }

  private Packet<?> readPacket(SafeDataInputStream safeDataInputStream) {
    return Packet.Type.parseInt(safeDataInputStream.readUnsignedByte()).parse(safeDataInputStream);
  }

  @Test
  public void sendWhileNotConnectedShouldNotThrow() {
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", 1),
                (contract, history, address, core, state) -> null,
                ObjectCreator.createMemoryStateStorage(),
                new MyScheduledSingleThreadExecutor(),
                Collections.emptySet()));
    client.sendPacket(
        new Packet<>(
            Packet.Type.CONTRACT_STATE_REQUEST, new ContractStateRequest(contractAddress, 10)));
  }

  @Test
  public void unknownContractStateResponseShouldNotFail() {
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", 1),
                (contract, history, address, core, state) -> null,
                ObjectCreator.createMemoryStateStorage(),
                new MyScheduledSingleThreadExecutor(),
                Collections.emptySet()));
    client.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_STATE_RESPONSE,
            new ContractStateResponse(
                new ContractStateRequest(contractAddress, 10),
                TestObjects.EMPTY_HASH,
                TestObjects.EMPTY_HASH,
                TestObjects.EMPTY_HASH)));
  }

  @Test
  public void unknownIncomingPacketType() {
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", 1),
                (contract, history, address, core, state) -> null,
                ObjectCreator.createMemoryStateStorage(),
                new MyScheduledSingleThreadExecutor(),
                Collections.emptySet()));
    Assertions.assertThatThrownBy(
            () ->
                client.incoming(
                    new Packet<>(
                        Packet.Type.CONTRACT_STATE_REQUEST,
                        new ContractStateRequest(contractAddress, 10))))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void closeConnected() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) -> null,
                ObjectCreator.createMemoryStateStorage(),
                scheduler,
                Collections.emptySet()));
    Assertions.assertThat(client.isConnected()).isFalse();
    scheduler.latest.run();
    Socket accept = serverSocket.accept();
    accept.setSoTimeout(500);
    Handshake.read(new SafeDataInputStream(accept.getInputStream()));
    Assertions.assertThat(client.isConnected()).isTrue();
    client.close();
    Assertions.assertThat(accept.getInputStream().read()).isEqualTo(-1);
  }

  @Test
  public void closeUnconnected() throws Exception {
    ServerSocket serverSocket = register(new ServerSocket(ObjectCreator.port()));
    MyScheduledSingleThreadExecutor scheduler = new MyScheduledSingleThreadExecutor();
    Client client =
        register(
            new Client(
                () -> Runnable::run,
                null,
                new Address("localhost", serverSocket.getLocalPort()),
                (contract, history, address, core, state) -> null,
                ObjectCreator.createMemoryStateStorage(),
                scheduler,
                Collections.emptySet()));
    Assertions.assertThat(client.isConnected()).isFalse();
    client.close();
    Assertions.assertThat(scheduler.closed).isTrue();
  }

  @Test
  public void clientWithSingleBinder() {
    final Hash binderHash = Hash.create(s -> s.writeInt(2));
    final WithCloseableResources resources =
        Client.createShardClient(
            List.of(binderHash),
            List.of(new Address("localhost", 0)),
            (contract, history, address, core, state) -> null,
            ObjectCreator.createMemoryStateStorage(),
            Collections.emptySet());
    Assertions.assertThat(resources).isNotNull().isInstanceOf(Client.ClientResources.class);
  }

  @Test
  public void constructorMissingBinders() {
    Assertions.assertThatCode(
            () ->
                Client.createShardClient(
                    List.of(),
                    List.of(new Address("localhost", 0)),
                    (contract, history, address, core, state) -> null,
                    ObjectCreator.createMemoryStateStorage(),
                    Collections.emptySet()))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("TCP Client given no binders to listen to!");
  }

  @Test
  public void clientMissingShards() {
    Assertions.assertThatCode(
            () ->
                Client.createShardClient(
                    null,
                    List.of(),
                    (contract, history, address, core, state) -> null,
                    ObjectCreator.createMemoryStateStorage(),
                    Collections.emptySet()))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("TCP Client given no shards to listen to!");
  }

  @DisplayName(
      "You should override either of the update methods when implementing a Client.Listener")
  @Test
  void defaultClientImplementation() {
    Client.Listener listener =
        new Client.Listener() {
          @Override
          public void remove() {}
        };
    Assertions.assertThatThrownBy(() -> listener.update(null))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Not implemented.");
  }

  private static final class MyScheduledSingleThreadExecutor
      implements ScheduledSingleThreadExecutor {

    CountDownLatch latch;
    Runnable latest;
    long latestDelay;
    boolean closed;

    @Override
    public void schedule(Runnable runnable, long delay, TimeUnit unit) {
      if (latch != null) {
        latch.countDown();
      }
      latest = runnable;
      latestDelay = unit.toMillis(delay);
    }

    @Override
    public void close() {
      closed = true;
    }
  }

  /** Mock sys binder with StateString state. */
  public static final class MockSysBinder implements SysBinderContract<StateString> {

    /**
     * Default constructor.
     *
     * @param rawContract underlying contract
     */
    public MockSysBinder(byte[] rawContract) {
      Objects.requireNonNull(rawContract);
    }

    @Override
    public Class<StateString> getStateClass() {
      return StateString.class;
    }

    @Override
    public BinderResult<StateString, BinderEvent> create(SysBinderContext context, byte[] rpc) {
      return null;
    }

    @Override
    public BinderResult<StateString, BinderEvent> invoke(
        SysBinderContext context, StateString state, byte[] rpc) {
      return null;
    }

    @Override
    public BinderResult<StateString, BinderEvent> callback(
        SysBinderContext context, StateString state, CallbackContext callbackContext, byte[] rpc) {
      return null;
    }

    @Override
    public StateString upgrade(StateAccessor oldState, byte[] rpc) {
      return null;
    }
  }
}
