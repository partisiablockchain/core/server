package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import java.util.Objects;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class PacketTest {

  @Test
  public void parseTypes() {
    List<Packet.Type<?>> validTypes =
        List.of(
            Packet.Type.SERIALIZED_STATE_REQUEST,
            Packet.Type.SERIALIZED_STATE_RESPONSE,
            Packet.Type.CONTRACT_STATE_REQUEST,
            Packet.Type.CONTRACT_STATE_RESPONSE,
            Packet.Type.CONTRACT_CREATED,
            Packet.Type.CONTRACT_CREATED_RESPONSE,
            Packet.Type.CONTRACT_UPDATED,
            Packet.Type.CONTRACT_REMOVED);
    for (int i = 0; i < validTypes.size(); i++) {
      Packet.Type<?> type = validTypes.get(i);
      Assertions.assertThat(Packet.Type.parseInt(i)).isSameAs(type);
      Assertions.assertThat(type.getType()).isEqualTo(i);
    }
    Assertions.assertThatThrownBy(
            () -> Objects.requireNonNull(Packet.Type.parseInt(validTypes.size())))
        .isInstanceOf(IndexOutOfBoundsException.class);
  }

  @Test
  public void writeAndParse() {
    SerializedStateRequest request =
        new SerializedStateRequest(Hash.create(stream -> stream.writeInt(0)));
    Packet<SerializedStateRequest> packet =
        new Packet<>(Packet.Type.SERIALIZED_STATE_REQUEST, request);
    byte[] serialize = SafeDataOutputStream.serialize(packet::send);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(serialize);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(Packet.Type.SERIALIZED_STATE_REQUEST.getType());
    Packet<SerializedStateRequest> parse = Packet.Type.SERIALIZED_STATE_REQUEST.parse(stream);
    Assertions.assertThat(parse.getType()).isEqualTo(Packet.Type.SERIALIZED_STATE_REQUEST);
    Assertions.assertThat(parse.getPayload()).usingRecursiveComparison().isEqualTo(request);
  }
}
