package com.partisiablockchain.server.rest.chain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.server.rest.model.Account;
import com.partisiablockchain.server.rest.model.AvlInformation;
import com.partisiablockchain.server.rest.model.AvlStateEntry;
import com.partisiablockchain.server.rest.model.AvlStateValue;
import com.partisiablockchain.server.rest.model.Chain;
import com.partisiablockchain.server.rest.model.Contract;
import com.partisiablockchain.server.rest.model.Feature;
import com.partisiablockchain.server.rest.model.Plugin;
import com.partisiablockchain.server.rest.model.SerializedTransaction;
import com.partisiablockchain.server.rest.model.TransactionPointer;
import jakarta.ws.rs.BadRequestException;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Tests for ChainController. */
public final class ChainControllerTest extends CloseableTest {

  private ChainServiceDummy chainServiceDummy;
  private ChainController chainController;
  private final ObjectMapper mapper = StateObjectMapper.createObjectMapper();

  /** Setup dependencies for tests. */
  @BeforeEach
  public void setUp() {
    chainServiceDummy = new ChainServiceDummy();
    chainController = new ChainController(chainServiceDummy);
  }

  @Test
  public void expectBadRequestWithInvalidAccountAddress() {
    String invalidAddress = chainServiceDummy.validChainContractAddress.writeAsString();
    assertThatThrownBy(() -> chainController.getAccount(invalidAddress, null))
        .isInstanceOf(BadRequestException.class);
  }

  @Test
  public void expectAccountStateWithValidAccountAddress() {
    String validAddress = chainServiceDummy.validChainAccountAddress.writeAsString();
    String shardId = chainServiceDummy.shardId;
    JsonNode accountJson = chainServiceDummy.accountJson;
    Account expected = new Account().shardId(shardId).account(accountJson).nonce(0L);
    assertThat(chainController.getAccount(validAddress, null)).isEqualTo(expected);
  }

  @Test
  public void expectBadRequestWithInvalidContractAddress() {
    String invalidAddress = chainServiceDummy.validChainAccountAddress.writeAsString();
    assertThatThrownBy(() -> chainController.getContract(invalidAddress, null))
        .isInstanceOf(BadRequestException.class);
  }

  @Test
  public void expectContractStateWithValidContractAddress() {
    String shardId = chainServiceDummy.shardId;
    byte[] serializedContract = chainServiceDummy.serializedContract;
    BlockchainAddress validAddress = chainServiceDummy.validChainContractAddress;
    Hash jar = chainServiceDummy.jarId;
    Hash binder = chainServiceDummy.binder;
    JsonNode account = chainServiceDummy.accountJson;
    long storageLength = chainServiceDummy.storagelength;
    byte[] abi = chainServiceDummy.abi;

    Contract expected =
        new Contract()
            .shardId(shardId)
            .serializedContract(serializedContract)
            .address(validAddress.writeAsString())
            .jar(jar.toString())
            .binder(binder.toString())
            .account(account)
            .storageLength(storageLength)
            .abi(abi);

    assertThat(chainController.getContract(validAddress.writeAsString(), null)).isEqualTo(expected);
  }

  @Test
  public void expectExceptionWhenFailingToPutTransaction() {
    SerializedTransaction serializedTransaction = null;

    assertThatThrownBy(() -> chainController.putTransaction(serializedTransaction))
        .isInstanceOf(BadRequestException.class);
  }

  @Test
  public void expectValidChainTransactionWhenPutOfvalidTransaction() {
    SerializedTransaction serializedTransaction = new SerializedTransaction().payload(new byte[0]);
    Hash transactionHash = chainServiceDummy.transactionHash;
    String shardId = chainServiceDummy.shardId;
    TransactionPointer expected = new TransactionPointer();
    expected.setIdentifier(transactionHash.toString());
    expected.setDestinationShardId(shardId);
    assertThat(chainController.putTransaction(serializedTransaction)).isEqualTo(expected);
  }

  @Test
  public void getChain() {
    String chainId = "CHAIN ID";
    long goveranceVersion = 0L;
    List<String> shards = List.of("Gov", "Shard");

    Feature feature = new Feature().name("featureName").value("featureValue");
    List<Feature> features = List.of(feature);

    Hash jarId = chainServiceDummy.jarId;
    Plugin plugin = new Plugin().jarId(jarId.toString()).state(mapper.valueToTree("state"));
    Map<String, Plugin> plugins = Map.of("ACCOUNT", plugin);

    Chain expected =
        new Chain()
            .chainId(chainId)
            .governanceVersion(goveranceVersion)
            .shards(shards)
            .features(features)
            .plugins(plugins);

    assertThat(chainController.getChain(null)).isEqualTo(expected);
  }

  @Test
  public void expectChainGovernanceChangeWhenBumpingGovernanceVersion() {
    Chain chain = chainController.getChain(null);
    long expected = 0L;
    assertThat(chain.getGovernanceVersion()).isEqualTo(expected);

    expected = chainServiceDummy.setGovernanceVersion(1L);

    chain = chainController.getChain(null);
    assertThat(chain.getGovernanceVersion()).isEqualTo(expected);
  }

  /**
   * getContractAvlValue can get the value stored for a key in an AVL tree in a specific contract.
   */
  @Test
  void getAvlValue() {
    AvlStateValue contractAvlValue =
        chainController.getContractAvlValue(
            "010000000000000000000000000000000000000000", 0, "00", null);
    assertThat(contractAvlValue.getData()).containsExactly(42, 43, 44);
  }

  /** getContractAvlValue throws for an account address. */
  @Test
  void getAvlValueNotContract() {
    assertThatThrownBy(
            () ->
                chainController.getContractAvlValue(
                    "000000000000000000000000000000000000000000", 0, "00", null))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Not a valid contract address");
  }

  /**
   * getContractAvlNextN gets the next entries following a key for an AVL tree in a specific
   * contract.
   */
  @Test
  void getAvlNextN() {
    List<AvlStateEntry> contractAvlNextN =
        chainController.getContractAvlNextN(
            "010000000000000000000000000000000000000000", 0, "00", 5, 0, null);
    assertThat(contractAvlNextN.get(0).getKey()).isEqualTo("01");
    assertThat(contractAvlNextN.get(0).getValue()).containsExactly(2);
    assertThat(contractAvlNextN.get(1).getKey()).isEqualTo("03");
    assertThat(contractAvlNextN.get(1).getValue()).containsExactly(4);
  }

  /** getContractAvlFirstN gets the first entries for an AVL tree in a specific contract. */
  @Test
  void getAvlFirstN() {
    List<AvlStateEntry> contractAvlNextN =
        chainController.getContractAvlNextN(
            "010000000000000000000000000000000000000000", 0, null, 100, 0, null);
    assertThat(contractAvlNextN.get(0).getKey()).isEqualTo("01");
    assertThat(contractAvlNextN.get(0).getValue()).containsExactly(2);
    assertThat(contractAvlNextN.get(1).getKey()).isEqualTo("03");
    assertThat(contractAvlNextN.get(1).getValue()).containsExactly(4);
  }

  /** getContractAvlNextN throws for an account address. */
  @Test
  void getAvlNextNotContract() {
    assertThatThrownBy(
            () ->
                chainController.getContractAvlNextN(
                    "000000000000000000000000000000000000000000", 0, "00", 5, 0, null))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Not a valid contract address");
  }

  /** getContractAvlNextN throws for n greater than 100. */
  @Test
  void getAvlNextNumberTooLarge() {
    assertThatThrownBy(
            () ->
                chainController.getContractAvlNextN(
                    "010000000000000000000000000000000000000000", 0, "00", 1000, 0, null))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Can fetch at most 100 entries");
  }

  /**
   * getContractAvlModifiedKeys returns a list of keys modified for an AVL tree in a specific
   * contract.
   */
  @Test
  void getAvlModifiedKeys() {
    List<String> contractAvlModifiedKeys =
        chainController.getContractAvlModifiedKeys(
            "010000000000000000000000000000000000000000", 0, List.of(5L, 10L), null, 10);
    assertThat(contractAvlModifiedKeys).containsExactly("01", "02", "03");
  }

  /** getContractAvlModifiedKeys throws for an account address. */
  @Test
  void getAvlModifiedKeysNotContract() {
    assertThatThrownBy(
            () ->
                chainController.getContractAvlModifiedKeys(
                    "000000000000000000000000000000000000000000", 0, List.of(5L, 10L), "00", 5))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Not a valid contract address");
  }

  /** getContractAvlModifiedKeys throws for n greater than 100. */
  @Test
  void getAvlModifiedKeysNumberTooLarge() {
    assertThatThrownBy(
            () ->
                chainController.getContractAvlModifiedKeys(
                    "010000000000000000000000000000000000000000", 0, List.of(5L, 10L), "00", 1000))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Can fetch at most 100 entries");
  }

  /** getContractAvlModifiedKeys throws if supplied with more or less than 2 block times. */
  @Test
  void getAvlModifiedKeysIncorrectNumberOfBlockTimes() {
    assertThatThrownBy(
            () ->
                chainController.getContractAvlModifiedKeys(
                    "010000000000000000000000000000000000000000", 0, List.of(), "00", 10))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Expects exactly two block times");
    assertThatThrownBy(
            () ->
                chainController.getContractAvlModifiedKeys(
                    "010000000000000000000000000000000000000000", 0, List.of(5L), "00", 10))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Expects exactly two block times");
    assertThatThrownBy(
            () ->
                chainController.getContractAvlModifiedKeys(
                    "010000000000000000000000000000000000000000",
                    0,
                    List.of(5L, 10L, 15L),
                    "00",
                    10))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Expects exactly two block times");
  }

  /** getAvlInformation gets the size of the specified avl tree. */
  @Test
  void getAvlInformation() {
    AvlInformation contractAvlValue =
        chainController.getContractAvlInformation(
            "010000000000000000000000000000000000000000", 0, null);
    assertThat(contractAvlValue.getSize()).isEqualTo(10);
  }

  /** getAvlInformation throws for an account address. */
  @Test
  void getAvlInformationNotContract() {
    assertThatThrownBy(
            () ->
                chainController.getContractAvlInformation(
                    "000000000000000000000000000000000000000000", 0, null))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Not a valid contract address");
  }
}
