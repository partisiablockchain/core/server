package com.partisiablockchain.server.config;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.partisiablockchain.server.config.NodeConfig.ModuleConfig;
import com.partisiablockchain.server.rest.RestNode;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class NodeConfigTest {

  @Test
  public void values() {
    NodeConfig nodeConfig = createNodeConfig();

    Assertions.assertThat(nodeConfig.knownPeers())
        .isEqualTo(List.of("localhost:8080", "test:1234"));
    Assertions.assertThat(nodeConfig.restPort()).isEqualTo(80);
    Assertions.assertThat(nodeConfig.modules())
        .usingRecursiveFieldByFieldElementComparator()
        .containsExactly(restModule());
    Assertions.assertThat(nodeConfig.floodingPort()).isEqualTo(9999);
    Assertions.assertThat(nodeConfig.persistentPeer()).isEqualTo("peer:1");
  }

  private static NodeConfig createNodeConfig() {
    NodeConfig nodeConfig =
        new NodeConfig(
            List.of(restModule()),
            KademliaConfigTest.createKademliaConfig(),
            80,
            9999,
            "peer:1",
            List.of("localhost:8080", "test:1234"));
    return nodeConfig;
  }

  /** Helper method for getting a Module config. */
  public static ModuleConfig restModule() {
    String fullyQualifiedClass = RestNode.class.getName();
    ObjectNode jsonNodes = JsonNodeFactory.instance.objectNode();
    jsonNodes.set("enabled", JsonNodeFactory.instance.booleanNode(true));
    ModuleConfig config = new ModuleConfig(fullyQualifiedClass, jsonNodes);
    return config;
  }
}
