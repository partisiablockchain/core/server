package com.partisiablockchain.server.tcp.client;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.ObjectCreator;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateStorage;
import com.secata.stream.SafeDataInputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class FetchingStateStorageTest extends CloseableTest {

  private final StateStorage storage = ObjectCreator.createMemoryStateStorage();
  private final Hash hash = Hash.create(s -> s.writeByte(0));

  @Test
  public void shouldFetchWhenNotPresent() throws Exception {
    CountDownLatch fetch = new CountDownLatch(1);
    FetchingStateStorage fetching = new FetchingStateStorage(hash -> fetch.countDown(), storage);

    CountDownLatch valueSet = new CountDownLatch(1);
    AtomicReference<Integer> read = new AtomicReference<>();
    Thread thread =
        register(
            new Thread(
                () -> {
                  Integer value = fetching.read(hash, SafeDataInputStream::readUnsignedByte);
                  read.set(value);
                  valueSet.countDown();
                }));
    thread.start();
    Assertions.assertThat(fetch.await(1, TimeUnit.SECONDS)).isTrue();
    Assertions.assertThat(fetching.fetching()).containsExactly(hash);
    Assertions.assertThat(thread.getState()).isEqualTo(Thread.State.WAITING);
    fetching.received(new byte[] {0});
    Assertions.assertThat(valueSet.await(1, TimeUnit.SECONDS)).isTrue();

    Assertions.assertThat(read).hasValue(0);
  }

  @Test
  public void shouldOnlyFetchOnce() throws Exception {
    CountDownLatch fetch = new CountDownLatch(2);
    FetchingStateStorage fetching = new FetchingStateStorage(hash -> fetch.countDown(), storage);

    register(new Thread(() -> fetching.read(hash, SafeDataInputStream::readUnsignedByte))).start();
    register(new Thread(() -> fetching.read(hash, SafeDataInputStream::readUnsignedByte))).start();

    Assertions.assertThat(fetch.await(200, TimeUnit.MILLISECONDS)).isFalse();
  }

  @Test
  public void shouldNotFetchWhenPresent() throws Exception {
    AtomicInteger fetch = new AtomicInteger(0);
    FetchingStateStorage fetching =
        new FetchingStateStorage(hash -> fetch.incrementAndGet(), storage);

    fetching.received(new byte[] {0});
    CountDownLatch latch = new CountDownLatch(1);
    // Do the read in avoid infinite sleep in pitest
    register(
            new Thread(
                () -> {
                  Assertions.assertThat(fetching.read(hash, SafeDataInputStream::readUnsignedByte))
                      .isEqualTo(0);
                  latch.countDown();
                }))
        .start();
    Assertions.assertThat(latch.await(1, TimeUnit.SECONDS)).isTrue();
  }

  @Test
  public void write() {
    FetchingStateStorage fetching =
        new FetchingStateStorage(FunctionUtility.noOpConsumer(), storage);
    Assertions.assertThatThrownBy(() -> fetching.write(hash, hash))
        .isInstanceOf(UnsupportedOperationException.class);
  }
}
