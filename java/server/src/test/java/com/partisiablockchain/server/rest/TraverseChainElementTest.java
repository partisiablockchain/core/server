package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.google.errorprone.annotations.Immutable;
import com.google.errorprone.annotations.ImmutableTypeParameter;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.dto.traversal.AvlKeyType;
import com.partisiablockchain.dto.traversal.AvlTraverse;
import com.partisiablockchain.dto.traversal.FieldTraverse;
import com.partisiablockchain.dto.traversal.Traverse;
import com.partisiablockchain.tree.AvlTree;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test. */
public final class TraverseChainElementTest {

  @Test
  public void traverseObject() {
    long balance = 999;
    final BlockchainAddress account = TestObjects.ACCOUNT_TWO;
    final Object object =
        new Object() {
          @SuppressWarnings("unused")
          private final AvlTree<BlockchainAddress, Long> tree =
              AvlTree.create(Map.of(account, balance));
        };

    FieldTraverse fieldTraverse = new FieldTraverse("tree");

    AvlTraverse avlTraverse =
        new AvlTraverse(AvlKeyType.BLOCKCHAIN_ADDRESS, account.writeAsString());

    TraverseChainElement traverseChainElement =
        TraverseChained.create(List.of(fieldTraverse, avlTraverse));
    Object o = traverseChainElement.traversePath(object);
    assertThat((Long) o).isEqualTo(balance);
  }

  @Test
  public void invalidTraverse() {
    Traverse dummy = new Traverse() {};
    assertThatThrownBy(() -> TraverseChained.create(List.of(dummy)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Invalid Traverse record");
  }

  @Test
  public void traverseDeepObject() {

    int expectedResult = 99123;

    AvlTree<String, FieldsClass<FieldClass<Integer>>> one =
        AvlTree.create(
            Map.of(
                "one",
                new FieldsClass<>(new FieldClass<>(expectedResult), new FieldClass<>(4321))));
    AvlTree<String, FieldsClass<FieldClass<Integer>>> two =
        AvlTree.create(
            Map.of(
                "two",
                new FieldsClass<>(new FieldClass<>(1234), new FieldClass<>(expectedResult))));

    AvlTree<Integer, AvlTree<String, FieldsClass<FieldClass<Integer>>>> avl =
        AvlTree.create(
            Map.of(
                1, one,
                2, two));

    Map<
            Long,
            FieldClass<
                AvlTree<
                    String, AvlTree<Integer, AvlTree<String, FieldsClass<FieldClass<Integer>>>>>>>
        map =
            Map.of(
                7L, new FieldClass<>(AvlTree.create(Map.of("first", avl))),
                8L, new FieldClass<>(AvlTree.create()));
    AvlTree<
            Long,
            FieldClass<
                AvlTree<
                    String, AvlTree<Integer, AvlTree<String, FieldsClass<FieldClass<Integer>>>>>>>
        object = AvlTree.create(map);

    TraverseChainElement traverseChainElement =
        TraverseChained.create(
            List.of(
                createAvl("7", AvlKeyType.LONG),
                createField("field"),
                createAvl("first", AvlKeyType.STRING),
                createAvl("2", AvlKeyType.INTEGER),
                createAvl("two", AvlKeyType.STRING),
                createField("field2"),
                createField("field")));
    Object o = traverseChainElement.traversePath(object);
    assertThat((Integer) o).isEqualTo(expectedResult);
  }

  private FieldTraverse createField(String name) {
    return new FieldTraverse(name);
  }

  private AvlTraverse createAvl(String key, AvlKeyType keyType) {
    return new AvlTraverse(keyType, key);
  }

  @Immutable
  private static final class FieldClass<@ImmutableTypeParameter T> {

    @SuppressWarnings({"unused", "FieldCanBeLocal"})
    private final T field;

    FieldClass(T field) {
      this.field = field;
    }
  }

  @SuppressWarnings({"unused", "FieldCanBeLocal"})
  @Immutable
  private static final class FieldsClass<@ImmutableTypeParameter T> {

    private final T field1;
    private final T field2;

    FieldsClass(T field1, T field2) {
      this.field1 = field1;
      this.field2 = field2;
    }
  }
}
