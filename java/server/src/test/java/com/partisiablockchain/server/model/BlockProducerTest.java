package com.partisiablockchain.server.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

final class BlockProducerTest {

  @Test
  void calculateMedian() {
    assertThat(BlockProducer.findMedian(List.of(1L, 2L, 3L, 4L, 5L))).isEqualTo(3);
    assertThat(BlockProducer.findMedian(List.of(1L, 2L, 4L, 5L))).isEqualTo(3);
  }

  @Test
  void calculateMedianFinalizationTime() {
    BlockProducer.Builder builder = new BlockProducer.Builder();
    builder.addFinalizationTime(5);
    builder.addFinalizationTime(3);
    builder.addFinalizationTime(1);
    builder.addFinalizationTime(2);
    builder.addFinalizationTime(4);
    assertThat(builder.build().medianFinalizationTime).isEqualTo(3);
  }
}
