package com.partisiablockchain.server.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.server.JsonAssert;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainStatisticsTest {

  @Test
  public void values() {
    long blockTime = 1337;
    long productionTime = 10001;
    Hash stateHash = Hash.create(stream -> stream.writeInt(666));
    Hash txHash = Hash.create(stream -> stream.writeInt(1337));
    Hash parentBlockHash = Hash.create(stream -> stream.writeInt(0));
    List<Hash> transactions = new ArrayList<>(Collections.nCopies(10, txHash));
    List<Hash> eventTransactions = new ArrayList<>(Collections.nCopies(5, txHash));

    Block block =
        new Block(
            productionTime,
            blockTime,
            0,
            parentBlockHash,
            stateHash,
            eventTransactions,
            transactions,
            -1);
    BlockchainStatistics.Builder builder = new BlockchainStatistics.Builder();
    builder.addBlock(block);
    BlockchainStatistics blockchainStatistics = builder.build();
    assertThat(blockchainStatistics.earliestBlockProductionTime).isEqualTo(productionTime);
    assertThat(blockchainStatistics.latestBlockProductionTime).isEqualTo(productionTime);
    assertThat(blockchainStatistics.blockCount).isEqualTo(1);
    assertThat(blockchainStatistics.committees).hasSize(1);
    assertThat(
            blockchainStatistics
                .committees
                .get(0L)
                .producers
                .get(block.getProducerIndex())
                .blocksProduced)
        .isEqualTo(1);
    assertThat(
            blockchainStatistics
                .committees
                .get(0L)
                .producers
                .get(block.getProducerIndex())
                .medianFinalizationTime)
        .isEqualTo(-1);
    assertThat(blockchainStatistics.eventTransactionCount).isEqualTo(5);
    assertThat(blockchainStatistics.transactionCount).isEqualTo(10);
    assertThat(blockchainStatistics.resetBlockCount).isEqualTo(1);

    short producerIndex = 10;
    block =
        new Block(
            productionTime - 10,
            blockTime,
            1,
            parentBlockHash,
            stateHash,
            new ArrayList<>(),
            transactions,
            producerIndex);

    builder.addBlock(block);

    blockchainStatistics = builder.build();
    assertThat(blockchainStatistics.earliestBlockProductionTime).isEqualTo(productionTime - 10);
    assertThat(blockchainStatistics.latestBlockProductionTime).isEqualTo(productionTime);
    assertThat(blockchainStatistics.blockCount).isEqualTo(2);
    assertThat(blockchainStatistics.committees).hasSize(2);
    assertThat(blockchainStatistics.committees.get(1L).producers.get(producerIndex).blocksProduced)
        .isEqualTo(1);
    assertThat(
            blockchainStatistics
                .committees
                .get(1L)
                .producers
                .get(block.getProducerIndex())
                .medianFinalizationTime)
        .isEqualTo(10);
    assertThat(blockchainStatistics.eventTransactionCount).isEqualTo(5);
    assertThat(blockchainStatistics.transactionCount).isEqualTo(20);
    assertThat(blockchainStatistics.resetBlockCount).isEqualTo(1);

    block =
        new Block(
            productionTime - 30,
            blockTime,
            1,
            parentBlockHash,
            stateHash,
            new ArrayList<>(),
            transactions,
            producerIndex);

    builder.addBlock(block);
    blockchainStatistics = builder.build();
    assertThat(blockchainStatistics.blockCount).isEqualTo(3);
    assertThat(blockchainStatistics.committees.get(1L).producers.get(producerIndex).blocksProduced)
        .isEqualTo(2);
    assertThat(
            blockchainStatistics
                .committees
                .get(1L)
                .producers
                .get(block.getProducerIndex())
                .medianFinalizationTime)
        .isEqualTo(15);
    assertThat(blockchainStatistics.eventTransactionCount).isEqualTo(5);
    assertThat(blockchainStatistics.transactionCount).isEqualTo(30);
    assertThat(blockchainStatistics.resetBlockCount).isEqualTo(1);

    JsonAssert.assertSerializeLoop(blockchainStatistics);
  }

  @Test
  public void addBlock_hasTransactions() {
    Hash dummyHash = Hash.create(h -> h.writeInt(123));
    BlockchainStatistics.Builder builder = new BlockchainStatistics.Builder();

    Block zeroTransactionsBlock =
        new Block(0L, 0L, 0, dummyHash, dummyHash, List.of(), List.of(), 0);
    builder.addBlock(zeroTransactionsBlock);
    builder.addBlock(zeroTransactionsBlock);
    BlockchainStatistics blockchainStatistics = builder.build();
    assertThat(
            blockchainStatistics
                .committees
                .get(0L)
                .producers
                .get((short) 0L)
                .medianFinalizationTime)
        .isEqualTo(-1);

    Block onlyEventTransactionsBlock =
        new Block(0L, 0L, 0, dummyHash, dummyHash, List.of(dummyHash), List.of(), 0);
    builder.addBlock(onlyEventTransactionsBlock);
    blockchainStatistics = builder.build();
    assertThat(
            blockchainStatistics
                .committees
                .get(0L)
                .producers
                .get((short) 0L)
                .medianFinalizationTime)
        .isEqualTo(0);
  }
}
