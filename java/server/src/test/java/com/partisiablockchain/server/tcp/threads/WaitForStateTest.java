package com.partisiablockchain.server.tcp.threads;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.secata.tools.coverage.ExceptionConverter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class WaitForStateTest extends CloseableTest {

  @Test
  public void shouldWaitForValue() throws Exception {
    WaitForState<Boolean> booleanWaitForState = new WaitForState<>();
    CountDownLatch done = new CountDownLatch(1);
    Thread thread =
        register(
            new Thread(
                () -> {
                  Assertions.assertThat(booleanWaitForState.waitForState()).isTrue();
                  done.countDown();
                }));
    thread.start();
    ExceptionConverter.run(() -> Thread.sleep(100), "Unable to sleep");
    Assertions.assertThat(thread.getState()).isEqualTo(Thread.State.WAITING);
    booleanWaitForState.setState(true);
    Assertions.assertThat(done.await(1, TimeUnit.SECONDS)).isTrue();
  }
}
