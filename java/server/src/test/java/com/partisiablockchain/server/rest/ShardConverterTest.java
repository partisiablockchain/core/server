package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.CloseableTest;
import org.junit.jupiter.api.Test;

/** Test for ShardConverter. */
public final class ShardConverterTest extends CloseableTest {

  @Test
  public void classTest() {
    ShardConverter s = new ShardConverter();
    assertThat(s).isNotNull();
  }

  @Test
  public void expectGovernanceShardNameOnNull() {
    assertThat(ShardConverter.convertToGovShard(null))
        .isEqualTo(ShardConverter.governanceShardName);
  }

  @Test
  public void expectNullShardNameOnGov() {
    assertThat(ShardConverter.convertFromGovShard(ShardConverter.governanceShardName)).isNull();
  }

  @Test
  public void expectShardIdConvertingToGov() {
    String expected = "Shard";
    assertThat(ShardConverter.convertToGovShard(expected)).isEqualTo(expected);
  }

  @Test
  public void expectShardIdConvertingFromGov() {
    String expected = "Shard";
    assertThat(ShardConverter.convertFromGovShard(expected)).isEqualTo(expected);
  }
}
