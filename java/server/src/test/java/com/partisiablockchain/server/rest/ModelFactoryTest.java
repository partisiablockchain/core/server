package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.TransactionCost;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.server.rest.resources.BlockchainResource;
import com.partisiablockchain.server.rest.resources.helper.SystemBinderJarHelper;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.coverage.FunctionUtility;
import jakarta.ws.rs.NotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

/** Test. */
@SuppressWarnings("unused")
public final class ModelFactoryTest {

  @Test
  void publicGetNextN() {
    TestingAvlState state = getState();
    List<Map.Entry<byte[], byte[]>> value =
        ModelFactory.createAvlTreeNextN(
            BlockchainAddress.Type.CONTRACT_PUBLIC, state, 0, new byte[] {0}, 1, 0);
    assertThat(value).hasSize(1);
    assertThat(value.get(0).getKey()).containsExactly(1);
    assertThat(value.get(0).getValue()).containsExactly(2);
    List<Map.Entry<byte[], byte[]>> value2 =
        ModelFactory.createAvlTreeNextN(
            BlockchainAddress.Type.CONTRACT_PUBLIC, state, 0, new byte[] {0}, 2, 0);
    assertThat(value2).hasSize(2);
    assertThat(value2.get(0).getKey()).containsExactly(1);
    assertThat(value2.get(0).getValue()).containsExactly(2);
    assertThat(value2.get(1).getKey()).containsExactly(3);
    assertThat(value2.get(1).getValue()).containsExactly(4);
    List<Map.Entry<byte[], byte[]>> value3 =
        ModelFactory.createAvlTreeNextN(
            BlockchainAddress.Type.CONTRACT_PUBLIC, state, 0, new byte[] {1}, 1, 0);
    assertThat(value3).hasSize(1);
    assertThat(value3.get(0).getKey()).containsExactly(3);
    assertThat(value3.get(0).getValue()).containsExactly(4);
    List<Map.Entry<byte[], byte[]>> value4 =
        ModelFactory.createAvlTreeNextN(
            BlockchainAddress.Type.CONTRACT_PUBLIC, state, 0, null, 2, 0);
    assertThat(value4).hasSize(2);
    assertThat(value4.get(0).getKey()).containsExactly(1);
    assertThat(value4.get(0).getValue()).containsExactly(2);
    assertThat(value4.get(1).getKey()).containsExactly(3);
    assertThat(value4.get(1).getValue()).containsExactly(4);
  }

  @Test
  void zkGetNextN() {
    ZkTestingAvlState zkState = new ZkTestingAvlState(getState());
    List<Map.Entry<byte[], byte[]>> zkValue =
        ModelFactory.createAvlTreeNextN(
            BlockchainAddress.Type.CONTRACT_ZK, zkState, 0, new byte[] {0}, 1, 0);
    assertThat(zkValue).hasSize(1);
    assertThat(zkValue.get(0).getKey()).containsExactly(1);
    assertThat(zkValue.get(0).getValue()).containsExactly(2);
    List<Map.Entry<byte[], byte[]>> zkValue2 =
        ModelFactory.createAvlTreeNextN(
            BlockchainAddress.Type.CONTRACT_ZK, zkState, 0, new byte[] {0}, 2, 0);
    assertThat(zkValue2).hasSize(2);
    assertThat(zkValue2.get(0).getKey()).containsExactly(1);
    assertThat(zkValue2.get(0).getValue()).containsExactly(2);
    assertThat(zkValue2.get(1).getKey()).containsExactly(3);
    assertThat(zkValue2.get(1).getValue()).containsExactly(4);
    List<Map.Entry<byte[], byte[]>> zkValue3 =
        ModelFactory.createAvlTreeNextN(
            BlockchainAddress.Type.CONTRACT_ZK, zkState, 0, new byte[] {1}, 1, 0);
    assertThat(zkValue3).hasSize(1);
    assertThat(zkValue3.get(0).getKey()).containsExactly(3);
    assertThat(zkValue3.get(0).getValue()).containsExactly(4);
    List<Map.Entry<byte[], byte[]>> zkValue4 =
        ModelFactory.createAvlTreeNextN(BlockchainAddress.Type.CONTRACT_ZK, zkState, 0, null, 2, 0);
    assertThat(zkValue4).hasSize(2);
    assertThat(zkValue4.get(0).getKey()).containsExactly(1);
    assertThat(zkValue4.get(0).getValue()).containsExactly(2);
    assertThat(zkValue4.get(1).getKey()).containsExactly(3);
    assertThat(zkValue4.get(1).getValue()).containsExactly(4);
  }

  @Test
  void noAvlTrees() {
    assertThatThrownBy(
            () ->
                ModelFactory.createAvlTreeNextN(
                    BlockchainAddress.Type.CONTRACT_PUBLIC,
                    new StateWithoutAvl(new LargeByteArray(new byte[0])),
                    0,
                    new byte[] {0},
                    1,
                    0))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Contract does not contain avl trees");
  }

  @Test
  void incorrectTreeId() {
    assertThatThrownBy(
            () ->
                ModelFactory.createAvlTreeNextN(
                    BlockchainAddress.Type.CONTRACT_PUBLIC, getState(), 1, new byte[] {0}, 1, 0))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Avl Tree does not contain tree id 1");
  }

  @Test
  void getAvlValue() {
    TestingAvlState state = getState();
    byte[] value =
        ModelFactory.getAvlTreeValue(
            BlockchainAddress.Type.CONTRACT_PUBLIC, state, 0, new byte[] {1});
    assertThat(value).containsExactly(2);
    byte[] value2 =
        ModelFactory.getAvlTreeValue(
            BlockchainAddress.Type.CONTRACT_PUBLIC, state, 0, new byte[] {3});
    assertThat(value2).containsExactly(4);
    byte[] value3 =
        ModelFactory.getAvlTreeValue(
            BlockchainAddress.Type.CONTRACT_PUBLIC, state, 0, new byte[] {5});
    assertThat(value3).isNull();
  }

  @Test
  void getAvlValueZk() {
    ZkTestingAvlState state = new ZkTestingAvlState(getState());
    byte[] value =
        ModelFactory.getAvlTreeValue(BlockchainAddress.Type.CONTRACT_ZK, state, 0, new byte[] {1});
    assertThat(value).containsExactly(2);
    byte[] value2 =
        ModelFactory.getAvlTreeValue(BlockchainAddress.Type.CONTRACT_ZK, state, 0, new byte[] {3});
    assertThat(value2).containsExactly(4);
    byte[] value3 =
        ModelFactory.getAvlTreeValue(BlockchainAddress.Type.CONTRACT_ZK, state, 0, new byte[] {5});
    assertThat(value3).isNull();
  }

  @Test
  void noAvlTreesValue() {
    assertThatThrownBy(
            () ->
                ModelFactory.getAvlTreeValue(
                    BlockchainAddress.Type.CONTRACT_PUBLIC,
                    new StateWithoutAvl(new LargeByteArray(new byte[0])),
                    0,
                    new byte[] {0}))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Contract does not contain avl trees");
  }

  @Test
  void incorrectTreeIdValue() {
    assertThatThrownBy(
            () ->
                ModelFactory.getAvlTreeValue(
                    BlockchainAddress.Type.CONTRACT_PUBLIC, getState(), 1, new byte[] {0}))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Avl Tree does not contain tree id 1");
  }

  private static TestingAvlState getState() {
    return new TestingAvlState(
        AvlTree.create(
            Map.of(
                0,
                new AvlTreeWrapperStub(
                    AvlTree.create(
                        Map.of(
                            new ComparableByteArray(new byte[] {1}),
                            new LargeByteArray(new byte[] {2}),
                            new ComparableByteArray(new byte[] {3}),
                            new LargeByteArray(new byte[] {4})))))));
  }

  /** Testing state. */
  @Immutable
  public record ZkTestingAvlState(TestingAvlState openState) implements StateSerializable {}

  /** Testing state. */
  @Immutable
  public record TestingAvlState(AvlTree<Integer, AvlTreeWrapperStub> avlTrees)
      implements StateSerializable {}

  /** Stub to use as we do not have access to WasmInvoker. */
  @Immutable
  public record AvlTreeWrapperStub(AvlTree<ComparableByteArray, LargeByteArray> avlTree)
      implements StateSerializable {}

  /** Comparable byte array. Stub to use as we do not have access to WasmInvoker. */
  @Immutable
  public static final class ComparableByteArray
      implements Comparable<ComparableByteArray>, StateSerializable {
    private final LargeByteArray data;

    /** Serializable constructor. */
    ComparableByteArray() {
      this.data = null;
    }

    /**
     * Create comparable byte array.
     *
     * @param data inner byte array
     */
    public ComparableByteArray(byte[] data) {
      this.data = new LargeByteArray(data);
    }

    /**
     * Get the data as bytes.
     *
     * @return data as bytes
     */
    public byte[] getData() {
      return this.data.getData();
    }

    @Override
    public int compareTo(ComparableByteArray o) {
      return Arrays.compareUnsigned(getData(), o.getData());
    }
  }

  @Immutable
  private record StateWithoutAvl(LargeByteArray state) implements StateSerializable {}

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ExampleClass implements StateSerializable {
    private final boolean myBool;
    private final int state;

    ExampleClass(boolean myBool, int state) {
      this.myBool = myBool;
      this.state = state;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class WasmContract implements StateSerializable {

    private final LargeByteArray state;

    WasmContract(LargeByteArray state) {
      this.state = state;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class WasmContractAvl implements StateSerializable {
    private final LargeByteArray state;

    private final AvlTree<Integer, Integer> avlTrees;

    WasmContractAvl(LargeByteArray state, AvlTree<Integer, Integer> avlTrees) {
      this.state = state;
      this.avlTrees = avlTrees;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class OpenWasmContractAvl implements StateSerializable {
    private final LargeByteArray openState;

    private final AvlTree<Integer, Integer> avlTrees;

    OpenWasmContractAvl(LargeByteArray openState, AvlTree<Integer, Integer> avlTrees) {
      this.openState = openState;
      this.avlTrees = avlTrees;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ZkWasmContractAvl implements StateSerializable {

    private final OpenWasmContractAvl openState;

    ZkWasmContractAvl(OpenWasmContractAvl openState) {
      this.openState = openState;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class OpenWasmContract implements StateSerializable {

    private final LargeByteArray openState;

    OpenWasmContract(LargeByteArray openState) {
      this.openState = openState;
    }
  }

  @SuppressWarnings("FieldCanBeLocal")
  @Immutable
  static final class ZkWasmContract implements StateSerializable {

    private final OpenWasmContract openState;

    ZkWasmContract(OpenWasmContract openState) {
      this.openState = openState;
    }
  }

  @Test
  void createContract() throws IOException {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(FunctionUtility.noOpConsumer()));
    LargeByteArray abiBytes = new LargeByteArray(new byte[0]);
    CoreContractState core =
        CoreContractState.create(
            SystemBinderJarHelper.getSystemBinderJarIdentifier(),
            TestObjects.EMPTY_HASH,
            TestObjects.EMPTY_HASH,
            234);
    ContractState contractState =
        ModelFactory.createContractState(
            contractAddress,
            core,
            new ExampleClass(true, 0x42),
            abiBytes,
            999L,
            BlockchainResource.StateOutput.BINARY);

    assertThat(contractState.serializedContract().binaryValue())
        .isEqualTo(HexFormat.of().parseHex("0142000000"));

    byte[] encodedState = HexFormat.of().parseHex("0142000000");

    BlockchainAddress publicAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_PUBLIC, Hash.create(FunctionUtility.noOpConsumer()));
    ContractState wasmContractState =
        ModelFactory.createContractState(
            publicAddress,
            core,
            new WasmContract(new LargeByteArray(encodedState)),
            abiBytes,
            999L,
            BlockchainResource.StateOutput.BINARY);
    assertThat(wasmContractState.serializedContract().binaryValue()).containsExactly(encodedState);
  }

  @Test
  void createContractAvl() throws IOException {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(FunctionUtility.noOpConsumer()));
    LargeByteArray abiBytes = new LargeByteArray(new byte[0]);
    CoreContractState core =
        CoreContractState.create(
            SystemBinderJarHelper.getSystemBinderJarIdentifier(),
            TestObjects.EMPTY_HASH,
            TestObjects.EMPTY_HASH,
            234);
    ContractState contractState =
        ModelFactory.createContractState(
            contractAddress,
            core,
            new ExampleClass(true, 0x42),
            abiBytes,
            999L,
            BlockchainResource.StateOutput.AVL_BINARY);

    assertThat(contractState.serializedContract().get("state").binaryValue())
        .isEqualTo(HexFormat.of().parseHex("0142000000"));

    byte[] encodedState = HexFormat.of().parseHex("0142000000");

    BlockchainAddress publicAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_PUBLIC, Hash.create(FunctionUtility.noOpConsumer()));
    WasmContractAvl state = new WasmContractAvl(new LargeByteArray(encodedState), AvlTree.create());
    ContractState wasmContractStateAvl =
        ModelFactory.createContractState(
            publicAddress, core, state, abiBytes, 999L, BlockchainResource.StateOutput.AVL_BINARY);
    assertThat(wasmContractStateAvl.serializedContract().get("state").binaryValue())
        .containsExactly(encodedState);
    assertThat(wasmContractStateAvl.serializedContract().get("avlTrees").binaryValue())
        .containsExactly(HexFormat.of().parseHex("00000000"));

    ContractState wasmContractState =
        ModelFactory.createContractState(
            publicAddress,
            core,
            new WasmContract(new LargeByteArray(encodedState)),
            abiBytes,
            999L,
            BlockchainResource.StateOutput.AVL_BINARY);

    assertThat(wasmContractState.serializedContract().get("state").binaryValue())
        .containsExactly(encodedState);
    assertThat(wasmContractState.serializedContract().has("avlTrees")).isFalse();
  }

  @Test
  void createContractAvlZk() throws IOException {
    LargeByteArray abiBytes = new LargeByteArray(new byte[0]);
    CoreContractState core =
        CoreContractState.create(
            SystemBinderJarHelper.getSystemBinderJarIdentifier(),
            TestObjects.EMPTY_HASH,
            TestObjects.EMPTY_HASH,
            234);

    byte[] encodedState = HexFormat.of().parseHex("0142000000");

    BlockchainAddress secretAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_ZK, Hash.create(FunctionUtility.noOpConsumer()));

    ContractState wasmContractStateZkAvl =
        ModelFactory.createContractState(
            secretAddress,
            core,
            new ZkWasmContractAvl(
                new OpenWasmContractAvl(new LargeByteArray(encodedState), AvlTree.create())),
            abiBytes,
            999L,
            BlockchainResource.StateOutput.AVL_BINARY);

    assertThat(wasmContractStateZkAvl.serializedContract().get("state").binaryValue())
        .containsExactly(encodedState);
    assertThat(wasmContractStateZkAvl.serializedContract().get("avlTrees").binaryValue())
        .containsExactly(HexFormat.of().parseHex("00000000"));

    ContractState wasmContractStateZk =
        ModelFactory.createContractState(
            secretAddress,
            core,
            new ZkWasmContract(new OpenWasmContract(new LargeByteArray(encodedState))),
            abiBytes,
            999L,
            BlockchainResource.StateOutput.AVL_BINARY);

    assertThat(wasmContractStateZk.serializedContract().get("state").binaryValue())
        .containsExactly(encodedState);
    assertThat(wasmContractStateZk.serializedContract().has("avlTrees")).isFalse();
  }

  @Test
  void should_create_transaction_cost() {
    HashMap<Hash, Long> networkCosts = new HashMap<>();
    TransactionCost cost = new TransactionCost(1, 2, 3, 4, networkCosts);
    com.partisiablockchain.dto.TransactionCost expected =
        new com.partisiablockchain.dto.TransactionCost(1, 2, 3, 4, networkCosts);
    assertThat(ModelFactory.createTransactionCost(cost)).isEqualTo(expected);
  }
}
