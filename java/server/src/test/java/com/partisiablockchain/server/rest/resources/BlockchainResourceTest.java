package com.partisiablockchain.server.rest.resources;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainConsensusPlugin;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.FinalBlock;
import com.partisiablockchain.blockchain.PluginContext;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.StringToStringContract;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.ExecutedTransaction;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.dto.AccountState;
import com.partisiablockchain.dto.AvlStateValue;
import com.partisiablockchain.dto.ChainId;
import com.partisiablockchain.dto.ContractState;
import com.partisiablockchain.dto.Event;
import com.partisiablockchain.dto.Feature;
import com.partisiablockchain.dto.IncomingTransaction;
import com.partisiablockchain.dto.traversal.FieldTraverse;
import com.partisiablockchain.dto.traversal.TraversePath;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.server.rest.ModelFactory;
import com.partisiablockchain.server.rest.ModelFactoryTest;
import com.partisiablockchain.server.rest.resources.helper.SystemBinderJarHelper;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.Response;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.assertj.core.api.ThrowableAssert.ThrowingCallable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** Test. */
public final class BlockchainResourceTest extends CloseableTest {

  private final KeyPair producerKey = new KeyPair(BigInteger.ONE);
  private final List<KeyPair> accounts = new ArrayList<>();
  private BlockchainResource resource;
  private Block initialBlock;
  private SignedTransaction transaction;
  private BlockchainTestHelper blockchainTestHelper;
  private String producerIdentifier;
  private String chainId;
  private BlockchainAddress contractAddress;
  private byte[] jar;
  private byte[] consensusJar;
  private byte[] abi;

  private static final ObjectMapper MAPPER = StateObjectMapper.createObjectMapper();

  /** Setup default resource. */
  @BeforeEach
  public void setUp() {
    BlockchainAddress producer = producerKey.getPublic().createAddress();
    producerIdentifier = producer.writeAsString();
    jar = JarBuilder.buildJar(StringToStringContract.class);

    consensusJar = JarBuilder.buildJar(DummyConsensusPlugin.class);
    contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(FunctionUtility.noOpConsumer()));
    abi = new byte[] {0, 1, 2, 3, 4};
    blockchainTestHelper =
        new BlockchainTestHelper(
            newFolder(),
            state -> {
              StateHelper.initial(state);
              for (int i = 0; i < 10; i++) {
                KeyPair keyPair = new KeyPair(BigInteger.valueOf(i + 2));
                BlockchainAddress address = keyPair.getPublic().createAddress();
                state.createAccount(address);
                accounts.add(keyPair);
              }
              state.createContract(
                  contractAddress,
                  CoreContractState.create(
                      state.saveJar(SystemBinderJarHelper.getSystemBinderJar()),
                      state.saveJar(jar),
                      state.saveJar(abi),
                      jar.length));
              state.setContractState(contractAddress, null);
            },
            this::register);
    BlockchainLedger blockchain = blockchainTestHelper.blockchain;
    chainId = blockchain.getChainId();

    initialBlock = blockchain.getLatestBlock();
    blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainTestHelper.appendBlock(producerKey, List.of());

    initialBlock = blockchain.getLatestBlock();

    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contractAddress, new byte[] {0, 0, 0, 1, 32});
    CoreTransactionPart core =
        CoreTransactionPart.create(1L, initialBlock.getProductionTime() + 100_000, 1000_000);
    transaction = SignedTransaction.create(core, interact).sign(producerKey, chainId);

    resource = new BlockchainResource(blockchain);
  }

  @Test
  public void root() {
    Response status = resource.status();
    assertThat(status).isNotNull();
    assertThat(status.getStatus()).isEqualTo(204);
  }

  @Test
  public void getLatestBlock() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(transaction));

    com.partisiablockchain.dto.BlockState state = resource.getLatestBlock();
    assertThat(state.identifier()).isEqualTo(block.identifier().toString());
    assertThat(state.parentBlock()).isEqualTo(initialBlock.identifier().toString());
    assertThat(state.transactions()).hasSize(1);
    assertThat(state.events()).isEmpty();
  }

  @Test
  public void getBlockTimeFromUtc() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of());
    long startProdTime = block.getProductionTime();
    blockchainTestHelper.appendBlock(producerKey, List.of(), startProdTime + 1000);
    blockchainTestHelper.appendBlock(producerKey, List.of(), startProdTime + 2000);

    Long blockTime = resource.getBlockTimeFromUtc(startProdTime + 50);
    assertThat(blockTime).isEqualTo(block.getBlockTime());
  }

  @Test
  public void getBlockTimeFromUtcManyBlocks1() {
    blockchainTestHelper.appendBlock(producerKey, List.of());
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of());
    long startProdTime = block.getProductionTime();
    for (int i = 1; i <= 100; i++) {
      blockchainTestHelper.appendBlock(producerKey, List.of(), startProdTime + i);
    }

    Long blockTime = resource.getBlockTimeFromUtc(startProdTime);
    assertThat(blockTime).isEqualTo(block.getBlockTime());
  }

  @Test
  public void getBlockTimeFromUtcManyBlocks2() {
    blockchainTestHelper.appendBlock(producerKey, List.of());
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of());
    long startProdTime = block.getProductionTime();
    for (int i = 1; i <= 1000; i++) {
      blockchainTestHelper.appendBlock(producerKey, List.of(), startProdTime + i);
    }

    Long blockTime = resource.getBlockTimeFromUtc(startProdTime);
    assertThat(blockTime).isEqualTo(block.getBlockTime());
  }

  @Test
  public void getBlockTimeFromUtcBeforeGenesis() {
    blockchainTestHelper.appendBlock(producerKey, List.of());
    assertThat(resource.getBlockTimeFromUtc(0)).isNotNull();
    assertThatThrownBy(() -> resource.getBlockTimeFromUtc(-2))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Queried for block before -2, but oldest block was created at time 0");
  }

  @Test
  public void getBlockTimeFromUtcDefault() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of());
    assertThat(resource.getBlockTimeFromUtc(-1)).isEqualTo(block.getBlockTime());
  }

  @Test
  void getLatestBlockWithEvents() {
    blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    final Block block1 = blockchainTestHelper.appendBlock(producerKey, List.of());

    BlockchainLedger blockchain = blockchainTestHelper.blockchain;
    blockchainTestHelper =
        new BlockchainTestHelper(newFolder(), blockchain.getChainId(), this::register);
    resource = new BlockchainResource(blockchainTestHelper.blockchain);

    FixedList<Hash> events = blockchain.getTransaction(transaction.identifier()).getEvents();
    assertThat(events).hasSize(1);
    final ExecutableEvent event = blockchain.getEventTransaction(events.get(0));

    FloodableEvent floodableEvent =
        FloodableEvent.create(
            event,
            blockchain.getFinalizedBlock(block1.getBlockTime()),
            blockchain.getStateStorage());

    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(), List.of(floodableEvent));

    com.partisiablockchain.dto.BlockState state = resource.getLatestBlock();
    assertThat(state.identifier()).isEqualTo(block.identifier().toString());
    assertThat(state.events()).hasSize(1);
  }

  @Test
  public void getLatestBlockWithParent() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of());

    com.partisiablockchain.dto.BlockState state = resource.getLatestBlock();
    assertThat(state.identifier()).isEqualTo(block.identifier().toString());
    assertThat(state.parentBlock()).isEqualTo(initialBlock.identifier().toString());
    assertThat(state.transactions()).isEmpty();
  }

  @Test
  public void getLatestBlockWithTransaction() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(transaction));

    com.partisiablockchain.dto.BlockState state = resource.getLatestBlock();
    assertThat(state.identifier()).isEqualTo(block.identifier().toString());
    assertThat(state.parentBlock()).isEqualTo(initialBlock.identifier().toString());
    assertThat(state.transactions()).containsExactly(transaction.identifier().toString());
  }

  @Test
  public void getEmptyState() {
    assertNotFound(() -> resource.getAccountState("000000000000000000000000000000000000000001"));
  }

  @Test
  public void getStateWithNonce() {
    AccountState state = resource.getAccountState(producerIdentifier);
    assertThat(state.nonce()).isEqualTo(1);
  }

  @Test
  public void emptyGlobalAccountState() {
    JsonNode account = resource.traverseGlobalAccountState(new TraversePath(List.of()));
    assertThat(account.toString()).isEqualTo("null");
  }

  @Test
  public void emptyLocalAccountState() {
    JsonNode account = resource.traverseLocalAccountState(new TraversePath(List.of()), -1);
    assertThat(account.toString()).isEqualTo("null");
  }

  @Test
  public void emptyLocalAccountStateTime() {
    assertThatThrownBy(() -> resource.traverseLocalAccountState(new TraversePath(List.of()), 12345))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("No chain state for given block time");
  }

  @Test
  public void emptyLocalConsensusState() {
    JsonNode account = resource.traverseLocalConsensusState(new TraversePath(List.of()));
    assertThat(account.toString()).isEqualTo("null");
  }

  @Test
  public void unknownFieldInConsensusState() {
    BlockchainTestHelper blockchainTestHelper = withConsensusPlugin();
    resource = new BlockchainResource(blockchainTestHelper.blockchain);
    FieldTraverse fieldTraverse = new FieldTraverse("unknown");
    assertThatThrownBy(
            () -> resource.traverseGlobalConsensusState(new TraversePath(List.of(fieldTraverse))))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Field does not exist");
  }

  @Test
  public void consensusState() {
    JsonNode consensus = resource.traverseGlobalConsensusState(new TraversePath(List.of()));
    assertThat(consensus.toString()).isEqualTo("null");
  }

  @Test
  public void consensusJar() {
    BlockchainTestHelper blockchainTestHelper = withConsensusPlugin();
    BlockchainResource resource = new BlockchainResource(blockchainTestHelper.blockchain);
    assertThat(resource.consensusJar().data()).isEqualTo(consensusJar);
  }

  @Test
  public void putAndGetEvent() {
    blockchainTestHelper.appendBlock(producerKey, List.of(transaction));

    BlockchainLedger blockchain = blockchainTestHelper.blockchain;

    final Block block = blockchainTestHelper.appendBlock(producerKey, List.of());

    FixedList<Hash> events = blockchain.getTransaction(transaction.identifier()).getEvents();
    assertThat(events).hasSize(1);
    final ExecutableEvent event = blockchain.getEventTransaction(events.get(0));

    blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainTestHelper =
        new BlockchainTestHelper(newFolder(), blockchain.getChainId(), this::register);
    blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainTestHelper.appendBlock(producerKey, List.of());
    BlockchainResource blockchainResource = new BlockchainResource(blockchainTestHelper.blockchain);

    IncomingTransaction incomingTransaction =
        new IncomingTransaction(
            SafeDataOutputStream.serialize(
                FloodableEvent.create(
                    event,
                    blockchain.getFinalizedBlock(block.getBlockTime()),
                    blockchain.getStateStorage())));
    blockchainResource.pushEvent(incomingTransaction);
    Event eventTransaction = resource.getEventTransaction(event.identifier().toString());
    assertThat(eventTransaction.destinationShard())
        .isEqualTo(event.getEvent().getDestinationShard());
    assertThat(eventTransaction.identifier()).isEqualTo(event.identifier().toString());

    assertThatThrownBy(() -> resource.pushEvent(incomingTransaction))
        .isInstanceOf(BadRequestException.class);
  }

  @Test
  public void getEventNotFound() {
    assertThatThrownBy(
            () -> resource.getEventTransaction(Hash.create(s -> s.writeInt(1)).toString()))
        .isInstanceOf(NotFoundException.class);
  }

  private BlockchainTestHelper withConsensusPlugin() {
    return new BlockchainTestHelper(
        newFolder(),
        state ->
            state.setPlugin(
                FunctionUtility.noOpBiConsumer(),
                null,
                ChainPluginType.CONSENSUS,
                consensusJar,
                new byte[0]),
        this::register);
  }

  @Test
  public void consensusJarNotFound() {
    assertThatThrownBy(() -> resource.consensusJar()).isInstanceOf(NotFoundException.class);
  }

  @Test
  public void contractState() {
    blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    blockchainTestHelper.appendBlock(producerKey, List.of());
    TraversePath traversePath = new TraversePath(List.of(new FieldTraverse("value")));
    JsonNode consensus =
        resource.traverseContractState(contractAddress.writeAsString(), traversePath);
    assertThat(consensus.toString()).isEqualTo("\" \"");
  }

  @Test
  public void pushTransaction() {
    byte[] bytes = SafeDataOutputStream.serialize(transaction::write);
    resource.pushTransaction(createIncomingTransaction(bytes));

    assertThat(blockchainTestHelper.getPendingTransactions())
        .containsExactly(transaction.identifier());
  }

  @Test
  public void pushInvalidTransaction() {
    CoreTransactionPart core =
        CoreTransactionPart.create(2L, initialBlock.getProductionTime() + 100_000, 1000000);
    byte[] bytes =
        SafeDataOutputStream.serialize(
            SignedTransaction.create(
                    core, InteractWithContractTransaction.create(contractAddress, new byte[0]))
                .sign(producerKey, chainId));
    assertThatThrownBy(() -> resource.pushTransaction(createIncomingTransaction(bytes)))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Transaction is not valid in current state");

    assertThat(blockchainTestHelper.getPendingTransactions()).isEmpty();
  }

  @Test
  public void getSpecificBlock() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    com.partisiablockchain.dto.BlockState specificBlock =
        resource.getBlock(block.identifier().toString());
    assertThat(specificBlock.parentBlock()).isEqualTo(block.getParentBlock().toString());
    assertThat(specificBlock.blockTime()).isEqualTo(block.getBlockTime());
    assertThat(specificBlock.transactions()).containsExactly(transaction.identifier().toString());
    assertThat(specificBlock.identifier()).isEqualTo(block.identifier().toString());
  }

  @Test
  public void getSpecificBlockFromNumber() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of());
    com.partisiablockchain.dto.BlockState specificBlock =
        resource.getBlockFromNumber(block.getBlockTime());
    assertThat(specificBlock.parentBlock()).isEqualTo(block.getParentBlock().toString());
    assertThat(specificBlock.blockTime()).isEqualTo(block.getBlockTime());
    assertThat(specificBlock.transactions()).isEmpty();
    assertThat(specificBlock.identifier()).isEqualTo(block.identifier().toString());
  }

  @Test
  public void getEmptyBlock() {
    assertNotFound(() -> resource.getBlockFromNumber(5));
  }

  @Test
  public void getBlocksBeforeBlockTime() {
    Block block0 = blockchainTestHelper.appendBlock(producerKey, List.of());
    Block block1 = blockchainTestHelper.appendBlock(producerKey, List.of());
    Block block2 = blockchainTestHelper.appendBlock(producerKey, List.of());
    List<com.partisiablockchain.dto.BlockState> blocks =
        resource.getBlocksBeforeBlockTime(3, block2.getBlockTime());
    assertThat(blocks.size()).isEqualTo(3);
    verifyBlock(blocks.get(2), block0);
    verifyBlock(blocks.get(1), block1);
    verifyBlock(blocks.get(0), block2);

    assertNotFound(() -> resource.getBlocksBeforeBlockTime(100, block0.getBlockTime()));

    assertThatThrownBy(() -> resource.getBlocksBeforeBlockTime(101, 100))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Can only fetch up to 100 blocks.");
  }

  private void verifyBlock(com.partisiablockchain.dto.BlockState actual, Block expected) {
    assertThat(actual.parentBlock()).isEqualTo(expected.getParentBlock().toString());
    assertThat(actual.blockTime()).isEqualTo(expected.getBlockTime());
    assertThat(actual.transactions()).hasSize(expected.getTransactions().size());
    assertThat(actual.identifier()).isEqualTo(expected.identifier().toString());
  }

  @Test
  public void getByteTransactionsForSpecificBlock() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    blockchainTestHelper.appendBlock(producerKey, List.of());
    List<com.partisiablockchain.dto.ExecutedTransaction> transactions =
        resource.getTransactionsForBlock(block.identifier().toString());
    FixedList<Hash> events =
        blockchainTestHelper.blockchain.getTransaction(transaction.identifier()).getEvents();
    assertThat(events.size()).isEqualTo(1);
    ExecutedTransaction transaction1 =
        blockchainTestHelper.blockchain.getTransaction(events.get(0));
    verifySignedTransactions(block, transactions.subList(0, 1), List.of(transaction));
    com.partisiablockchain.dto.ExecutedTransaction inlineExecuted = transactions.get(1);
    assertThat(Hash.fromString(inlineExecuted.block())).isEqualTo(transaction1.getBlockHash());
  }

  @Test
  public void getByteTransactionsForNonExistingBlock() {
    assertNotFound(() -> resource.getTransactionsForBlock(transaction.identifier().toString()));
  }

  @Test
  public void getTransactionsFromLatestBlock() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactions =
        resource.getTransactionsFromLatestBlock();
    verifySignedTransactions(block, latestTransactions, List.of(transaction));
  }

  @Test
  public void getLatestTransactions() {
    blockchainTestHelper.appendBlock(producerKey, List.of());

    List<SignedTransaction> transactions = new ArrayList<>();
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contractAddress, new byte[] {0, 0, 0, 1, 32});
    addTransactionsOntoBlock(interact, transactions);
    final Block block1 = blockchainTestHelper.appendBlock(producerKey, transactions);

    List<SignedTransaction> transactionsForBlock2 = new ArrayList<>();
    for (int i = 0; i < 4; i++) {
      KeyPair producer = accounts.get(i);
      CoreTransactionPart core =
          CoreTransactionPart.create(2, initialBlock.getProductionTime() + 200_000, 1000_000);
      SignedTransaction signedTransaction =
          SignedTransaction.create(core, interact).sign(producer, chainId);
      transactionsForBlock2.add(signedTransaction);
    }
    final Block block2 = blockchainTestHelper.appendBlock(producerKey, transactionsForBlock2);

    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactions =
        resource.getLatestTransactions(10, false);
    assertThat(latestTransactions).hasSize(10);
    List<com.partisiablockchain.dto.ExecutedTransaction> recordFromFirstBlock =
        latestTransactions.subList(0, 4);
    verifySignedTransactions(block2, recordFromFirstBlock, transactionsForBlock2);
    List<com.partisiablockchain.dto.ExecutedTransaction> recordFromSecondBlock =
        latestTransactions.subList(4, 10);
    verifySignedTransactions(block1, recordFromSecondBlock, transactions.subList(0, 6));
  }

  @Test
  void getEventTransactionsForBlock() {
    BlockchainLedger blockchainWithExecutedEventsBeingFalse =
        BlockchainTestHelper.createLedgerWithEvents(
            newFolder(), Hash.create(s -> s.writeString("a hash")));
    register(blockchainWithExecutedEventsBeingFalse);
    List<ExecutedTransaction> transactionsExecuted =
        new BlockchainResource(blockchainWithExecutedEventsBeingFalse)
            .getEventTransactionsForBlock(
                new Block(
                    0,
                    0,
                    0,
                    Hash.create(s -> s.writeString("parent")),
                    Hash.create(s -> s.writeString("state")),
                    List.of(),
                    List.of()));
    assertThat(transactionsExecuted).isEmpty();
  }

  @Test
  public void getLatestTransactionsRequestMoreThanExistingTransactions() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactions =
        resource.getLatestTransactions(10, false);
    assertThat(latestTransactions).hasSize(1);
    verifySignedTransactions(block, latestTransactions, List.of(transaction));
  }

  @Test
  public void getLatestTransactionsOnlyN() {
    blockchainTestHelper.appendBlock(producerKey, List.of(transaction));

    KeyPair producer = accounts.get(0);
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contractAddress, new byte[] {0, 0, 0, 1, 32});
    CoreTransactionPart core =
        CoreTransactionPart.create(1, initialBlock.getProductionTime() + 100_000, 1000_000);
    SignedTransaction secondTransaction =
        SignedTransaction.create(core, interact).sign(producer, chainId);

    Block block = blockchainTestHelper.appendBlock(producer, List.of(secondTransaction));

    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactions =
        resource.getLatestTransactions(1, false);
    assertThat(latestTransactions).hasSize(1);
    verifySignedTransactions(block, latestTransactions, List.of(secondTransaction));
  }

  @Test
  public void getLatestTransactionsBlockOnlyFinal() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactions =
        resource.getLatestTransactions(1, false);
    verifySignedTransactions(block, latestTransactions, List.of(transaction));
  }

  @Test
  public void getLatestTransactionsBlockNoEvents() {
    blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    final Block block2 = blockchainTestHelper.appendBlock(producerKey, List.of());

    BlockchainLedger blockchain = blockchainTestHelper.blockchain;
    blockchainTestHelper =
        new BlockchainTestHelper(newFolder(), blockchain.getChainId(), this::register);
    resource = new BlockchainResource(blockchainTestHelper.blockchain);

    FixedList<Hash> createdEvents = blockchain.getTransaction(transaction.identifier()).getEvents();
    assertThat(createdEvents).hasSize(1);
    final ExecutableEvent firstEvent = blockchain.getEventTransaction(createdEvents.get(0));

    FloodableEvent floodableEvent =
        FloodableEvent.create(
            firstEvent,
            blockchain.getFinalizedBlock(block2.getBlockTime()),
            blockchain.getStateStorage());

    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(), List.of(floodableEvent));

    com.partisiablockchain.dto.BlockState state = resource.getLatestBlock();
    assertThat(state.identifier()).isEqualTo(block.identifier().toString());
    assertThat(state.events()).hasSize(1);

    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactions =
        resource.getLatestTransactions(1, false);
    assertThat(latestTransactions.size()).isEqualTo(0);
    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactionsWithEvents =
        resource.getLatestTransactions(1, true);
    assertThat(latestTransactionsWithEvents.size()).isEqualTo(1);
  }

  @Test
  public void getLatestTransactionsMaxNumberOfBlocks() {
    // unreachable block
    blockchainTestHelper.appendBlock(producerKey, List.of(transaction));

    KeyPair producer = accounts.get(0);
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contractAddress, new byte[] {0, 0, 0, 1, 32});
    CoreTransactionPart core =
        CoreTransactionPart.create(1, initialBlock.getProductionTime() + 100_000, 1000_000);
    SignedTransaction secondTransaction =
        SignedTransaction.create(core, interact).sign(producer, chainId);

    Block reachableBlock = blockchainTestHelper.appendBlock(producer, List.of(secondTransaction));
    for (int i = 1; i < 1000; i++) {
      blockchainTestHelper.appendBlock(producer, List.of());
    }
    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactions =
        resource.getLatestTransactions(2, false);
    verifySignedTransactions(reachableBlock, latestTransactions, List.of(secondTransaction));
  }

  @Test
  public void getLatestTransactionsMaxTransactions() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    assertThatThrownBy(() -> resource.getLatestTransactions(101, false))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Can only fetch up to 100 transactions.");
    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactions =
        resource.getLatestTransactions(100, false);
    verifySignedTransactions(block, latestTransactions, List.of(transaction));
  }

  @Test
  public void getLatestTransactionsBeforeBlock() {
    Block block1 = blockchainTestHelper.appendBlock(producerKey, List.of(transaction));

    KeyPair producer = accounts.get(0);
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contractAddress, new byte[] {0, 0, 0, 1, 32});
    CoreTransactionPart core =
        CoreTransactionPart.create(1, initialBlock.getProductionTime() + 100_000, 1000_000);
    SignedTransaction secondTransaction =
        SignedTransaction.create(core, interact).sign(producer, chainId);

    Block block2 = blockchainTestHelper.appendBlock(producer, List.of(secondTransaction));

    List<com.partisiablockchain.dto.ExecutedTransaction> latestTransactions =
        resource.getTransactionsBeforeBlock(
            2, block2.getBlockTime(), secondTransaction.identifier().toString(), false);
    verifySignedTransactions(block1, latestTransactions, List.of(transaction));
  }

  @Test
  public void getLatestTransactionsBeforeNonExistentTransaction() {
    blockchainTestHelper.appendBlock(producerKey, List.of(transaction));

    KeyPair producer = accounts.get(1);
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contractAddress, new byte[] {0, 0, 0, 1, 32});
    CoreTransactionPart core =
        CoreTransactionPart.create(1, initialBlock.getProductionTime() + 100_000, 1000_000);
    SignedTransaction secondTransaction =
        SignedTransaction.create(core, interact).sign(producer, chainId);

    Block block = blockchainTestHelper.appendBlock(producer, List.of(secondTransaction));

    List<com.partisiablockchain.dto.ExecutedTransaction> transactions =
        resource.getTransactionsBeforeBlock(
            1,
            block.getBlockTime(),
            "0000000000000000000000000000000000000000000000000000000000000000",
            false);
    verifySignedTransactions(block, transactions, List.of(secondTransaction));
  }

  @Test
  public void getLatestTransactionsBeforeLimit() {
    blockchainTestHelper.appendBlock(producerKey, List.of());

    List<SignedTransaction> transactionsForBlock1 = new ArrayList<>();
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contractAddress, new byte[] {0, 0, 0, 1, 32});
    for (int i = 0; i < 9; i++) {
      KeyPair producer = accounts.get(i);
      CoreTransactionPart core =
          CoreTransactionPart.create(1, initialBlock.getProductionTime() + 100_000, 1000_000);
      SignedTransaction signedTransaction =
          SignedTransaction.create(core, interact).sign(producer, chainId);
      transactionsForBlock1.add(signedTransaction);
    }
    final Block block1 = blockchainTestHelper.appendBlock(producerKey, transactionsForBlock1);

    List<SignedTransaction> transactionsForBlock2 = new ArrayList<>();
    for (int i = 0; i < 4; i++) {
      KeyPair producer = accounts.get(i);
      CoreTransactionPart core =
          CoreTransactionPart.create(2, initialBlock.getProductionTime() + 200_000, 1000_000);
      SignedTransaction signedTransaction =
          SignedTransaction.create(core, interact).sign(producer, chainId);
      transactionsForBlock2.add(signedTransaction);
    }
    blockchainTestHelper.appendBlock(producerKey, transactionsForBlock2);

    SignedTransaction excludedTrans = transactionsForBlock1.get(0);
    List<com.partisiablockchain.dto.ExecutedTransaction> transactions =
        resource.getTransactionsBeforeBlock(
            7, block1.getBlockTime(), excludedTrans.identifier().toString(), false);
    assertThat(transactions).hasSize(7);
    verifySignedTransactions(block1, transactions, transactionsForBlock1.subList(1, 8));
    excludedTrans = transactionsForBlock1.get(1);
    transactions =
        resource.getTransactionsBeforeBlock(
            6, block1.getBlockTime(), excludedTrans.identifier().toString(), false);
    assertThat(transactions).hasSize(6);
    verifySignedTransactions(block1, transactions, transactionsForBlock1.subList(2, 8));
  }

  private void verifySignedTransactions(
      Block block,
      List<com.partisiablockchain.dto.ExecutedTransaction> transactions,
      List<SignedTransaction> expectedTransactions) {
    assertThat(transactions).hasSize(expectedTransactions.size());
    for (int i = 0; i < transactions.size(); i++) {
      com.partisiablockchain.dto.ExecutedTransaction transaction = transactions.get(i);
      SignedTransaction specificTransaction =
          SafeDataInputStream.deserialize(
              stream -> SignedTransaction.read(chainId, stream), transaction.transactionPayload());
      assertThat(transaction.block()).isEqualTo(block.identifier().toString());
      assertThat(transaction.blockTime()).isEqualTo(block.getBlockTime());
      assertThat(transaction.productionTime()).isEqualTo(block.getProductionTime());
      assertThat(specificTransaction).isEqualTo(expectedTransactions.get(i));
    }
  }

  @Test
  public void pushByteTransaction() {
    byte[] payload = SafeDataOutputStream.serialize(transaction::write);

    IncomingTransaction byteTransaction = createIncomingTransaction(payload);
    resource.pushTransaction(byteTransaction);

    assertThat(blockchainTestHelper.getPendingTransactions())
        .containsExactly(this.transaction.identifier());
  }

  @Test
  public void getSpecificTransaction() {
    blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainTestHelper.appendBlock(producerKey, List.of());
    com.partisiablockchain.dto.ExecutedTransaction specificByteTransaction =
        resource.getTransaction(transaction.identifier().toString());
    assertThat(specificByteTransaction.finalized()).isTrue();
    assertThat(specificByteTransaction.executionSucceeded()).isTrue();
    assertThat(specificByteTransaction.events()).hasSize(1);
    assertThat(specificByteTransaction.events().get(0).destinationShard()).isNull();
    assertThat(specificByteTransaction.from())
        .isEqualTo("0035e97a5e078a5a0f28ec96d547bfee9ace803ac0");
    assertThat(specificByteTransaction.isEvent()).isFalse();
    SignedTransaction specificTransaction =
        SafeDataInputStream.deserialize(
            stream -> SignedTransaction.read(chainId, stream),
            specificByteTransaction.transactionPayload());

    assertThat(specificTransaction).isEqualTo(transaction);

    com.partisiablockchain.dto.ExecutedTransaction transaction =
        resource.getTransaction(specificByteTransaction.events().get(0).identifier());
    assertThat(transaction.events()).isEmpty();
  }

  @Test
  public void getFailedTransaction() {
    transaction =
        SignedTransaction.create(
                transaction.getCore(),
                InteractWithContractTransaction.create(contractAddress, new byte[] {0, 0, 0, 77}))
            .sign(producerKey, chainId);
    blockchainTestHelper.appendBlock(producerKey, List.of(transaction));
    blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainTestHelper.appendBlock(producerKey, List.of());
    com.partisiablockchain.dto.ExecutedTransaction specificByteTransaction =
        resource.getTransaction(transaction.identifier().toString());
    assertThat(specificByteTransaction.executionSucceeded()).isTrue();
    assertThat(specificByteTransaction.events()).hasSize(1);

    com.partisiablockchain.dto.ExecutedTransaction event =
        resource.getTransaction(specificByteTransaction.events().get(0).identifier());
    assertThat(event.executionSucceeded()).isFalse();
    assertThat(event.events()).isEmpty();
    assertThat(event.failureCause()).isNotNull();
    assertThat(event.failureCause().errorMessage()).contains("Unable to read");
    assertThat(event.failureCause().stackTrace()).contains("SafeDataInputStream.readString");
    assertThat(event.isEvent()).isTrue();
  }

  @Test
  public void getNullTransaction() {
    assertNotFound(() -> resource.getTransaction(transaction.identifier().toString()));
  }

  private void assertNotFound(ThrowingCallable callable) {
    assertThatThrownBy(callable).isInstanceOf(NotFoundException.class);
  }

  @Test
  void historicContractStates() {
    final String firstUpdate = "First";
    final String secondUpdate = "Second";
    final String thirdUpdate = "Third";

    InteractWithContractTransaction firstInteract =
        InteractWithContractTransaction.create(
            contractAddress, SafeDataOutputStream.serialize(s -> s.writeString(firstUpdate)));
    Block firstBlock = signAndAppend(firstInteract, 1);

    InteractWithContractTransaction secondInteract =
        InteractWithContractTransaction.create(
            contractAddress, SafeDataOutputStream.serialize(s -> s.writeString(secondUpdate)));
    Block secondBlock = signAndAppend(secondInteract, 2);

    InteractWithContractTransaction thirdInteract =
        InteractWithContractTransaction.create(
            contractAddress, SafeDataOutputStream.serialize(s -> s.writeString(thirdUpdate)));
    Block thirdBlock = signAndAppend(thirdInteract, 3);

    // empty block for handling transactions from third block
    blockchainTestHelper.appendBlock(producerKey, List.of());

    ContractState firstContractState =
        resource.getContract(
            contractAddress.writeAsString(),
            firstBlock.getBlockTime(),
            true,
            BlockchainResource.StateOutput.DEFAULT);
    ContractState secondContractState =
        resource.getContract(
            contractAddress.writeAsString(),
            secondBlock.getBlockTime(),
            true,
            BlockchainResource.StateOutput.DEFAULT);
    ContractState thirdContractState =
        resource.getContract(
            contractAddress.writeAsString(),
            thirdBlock.getBlockTime(),
            true,
            BlockchainResource.StateOutput.DEFAULT);

    assertThat(convertContractState(firstContractState)).isEqualTo(firstUpdate);
    assertThat(convertContractState(secondContractState)).isEqualTo(secondUpdate);
    assertThat(convertContractState(thirdContractState)).isEqualTo(thirdUpdate);
  }

  private Block signAndAppend(InteractWithContractTransaction transaction, int nonce) {
    SignedTransaction signedTransaction =
        SignedTransaction.create(
                CoreTransactionPart.create(
                    nonce, initialBlock.getProductionTime() + 100_000, 1000_000),
                transaction)
            .sign(accounts.get(0), chainId);
    return blockchainTestHelper.appendBlock(producerKey, List.of(signedTransaction));
  }

  private String convertContractState(ContractState contractState) {
    return MAPPER.convertValue(contractState.serializedContract(), StateString.class).value();
  }

  private void addTransactionsOntoBlock(
      InteractWithContractTransaction interact, List<SignedTransaction> transactionsForBlock) {
    for (int i = 0; i < 9; i++) {
      KeyPair producer = accounts.get(i);
      CoreTransactionPart core =
          CoreTransactionPart.create(1, initialBlock.getProductionTime() + 100_000, 1000_000);
      SignedTransaction signedTransaction =
          SignedTransaction.create(core, interact).sign(producer, chainId);
      transactionsForBlock.add(signedTransaction);
    }
  }

  @Test
  public void getSpecificContract() {
    ContractState specificContract =
        resource.getContract(
            contractAddress.writeAsString(), -1, true, BlockchainResource.StateOutput.DEFAULT);
    JsonNode serializedContract = specificContract.serializedContract();
    assertThat(serializedContract.toString()).isEqualTo("null");
    assertThat(specificContract.jarHash()).isNotNull();
    assertThat(specificContract.abi()).isEqualTo(abi);
    Hash expectedHash = Hash.create(s -> s.writeDynamicBytes(jar));
    assertThat(specificContract.jarHash()).isEqualTo(expectedHash.toString());
  }

  @Test
  public void getSpecificContractWithoutState() {
    ContractState specificContract =
        resource.getContract(
            contractAddress.writeAsString(), -1, false, BlockchainResource.StateOutput.DEFAULT);
    assertThat(specificContract.serializedContract()).isNullOrEmpty();
    assertThat(specificContract.jarHash()).isNotNull();
    assertThat(specificContract.abi()).isEqualTo(abi);
    Hash expectedHash = Hash.create(s -> s.writeDynamicBytes(jar));
    assertThat(specificContract.jarHash()).isEqualTo(expectedHash.toString());
  }

  @Test
  public void payingContractState() {
    LargeByteArray abiBytes = new LargeByteArray(abi);
    CoreContractState core =
        CoreContractState.create(
            SystemBinderJarHelper.getSystemBinderJarIdentifier(),
            TestObjects.EMPTY_HASH,
            TestObjects.EMPTY_HASH,
            234);
    ContractState contractState =
        ModelFactory.createContractState(
            contractAddress, core, null, abiBytes, 999L, BlockchainResource.StateOutput.NONE);
    assertThat(contractState.jarHash()).isEqualTo(TestObjects.EMPTY_HASH.toString());
  }

  @Test
  public void unableToGetChainState() {
    assertThatThrownBy(
            () ->
                resource.getContract(
                    contractAddress.writeAsString(),
                    10,
                    true,
                    BlockchainResource.StateOutput.DEFAULT))
        .hasMessage("No chain state for given block time");
  }

  @Test
  public void getEmptyContract() {
    assertNotFound(
        () ->
            resource.getContract(
                "010000000000032100000000000000000000000001",
                0,
                true,
                BlockchainResource.StateOutput.DEFAULT));
  }

  @Test
  public void getContractFromAccountId() {
    assertThatThrownBy(
            () ->
                resource.getContract(
                    TestObjects.ACCOUNT_TWO.writeAsString(),
                    0,
                    true,
                    BlockchainResource.StateOutput.DEFAULT))
        .hasMessage("Address does not correspond to a contract");
  }

  @Test
  public void getFeature() {
    Feature bootState = resource.getFeatures("BootAddress");
    assertThat(bootState).isNotNull();
    assertThat(bootState.value()).isNull();
  }

  @Test
  public void chainId() {
    ChainId chainIdRecord = resource.chainId();
    assertThat(chainIdRecord.chainId()).isEqualTo(chainId);
  }

  private IncomingTransaction createIncomingTransaction(byte[] transactionPayload) {
    return new IncomingTransaction(transactionPayload);
  }

  /** Test. */
  @Immutable
  public static final class EmptyState implements StateSerializable {}

  /** Test. */
  @Immutable
  public static final class DummyConsensusPlugin
      extends BlockchainConsensusPlugin<EmptyState, StateVoid> {

    /** Default constructor. */
    public DummyConsensusPlugin() {}

    @Override
    public BlockValidation validateLocalBlock(
        EmptyState globalState, StateVoid local, FinalBlock block) {
      return BlockValidation.createAccepted(false);
    }

    @Override
    public boolean validateExternalBlock(EmptyState globalState, FinalBlock block) {
      return false;
    }

    @Override
    public Class<StateVoid> getLocalStateClass() {
      return StateVoid.class;
    }

    @Override
    public InvokeResult<EmptyState> invokeGlobal(
        PluginContext pluginContext, EmptyState state, byte[] rpc) {
      return null;
    }

    @Override
    public StateVoid updateForBlock(
        PluginContext pluginContext, EmptyState emptyState, StateVoid stateVoid, Block block) {
      return null;
    }

    @Override
    public Class<EmptyState> getGlobalStateClass() {
      return EmptyState.class;
    }

    @Override
    public EmptyState migrateGlobal(
        StateAccessor stateAccessor, SafeDataInputStream safeDataInputStream) {
      return new EmptyState();
    }

    @Override
    public StateVoid migrateLocal(StateAccessor stateSerializable) {
      return null;
    }
  }

  @Test
  public void ensurePathParameterAlignment() {
    for (Method method : BlockchainResource.class.getDeclaredMethods()) {
      List<String> fromPath = paramsOfPathAnnotation(method);
      List<String> fromPathParam = new ArrayList<>();
      Annotation[][] parameterAnnotations = method.getParameterAnnotations();
      for (Annotation[] parameterAnnotation : parameterAnnotations) {
        for (Annotation annotation : parameterAnnotation) {
          if (annotation instanceof PathParam) {
            fromPathParam.add(((PathParam) annotation).value());
          }
        }
      }
      assertThat(fromPath)
          .describedAs(method.getName())
          .containsExactlyInAnyOrderElementsOf(fromPathParam);
    }
  }

  private List<String> paramsOfPathAnnotation(Method method) {
    Path annotation = method.getAnnotation(Path.class);
    if (annotation == null) {
      return List.of();
    }
    Pattern p = Pattern.compile("\\{(.*?)}");
    Matcher m = p.matcher(annotation.value());

    List<String> result = new ArrayList<>();
    while (m.find()) {
      result.add(m.group(1));
    }
    return result;
  }

  @Test
  void getContractBinary() {
    ContractState specificContract =
        resource.getContract(
            contractAddress.writeAsString(), -1, true, BlockchainResource.StateOutput.BINARY);
    JsonNode serializedContract = specificContract.serializedContract();
    assertThat(serializedContract.toString()).isEqualTo("null");
    assertThat(specificContract.jarHash()).isNotNull();
    assertThat(specificContract.abi()).isEqualTo(abi);
    Hash expectedHash = Hash.create(s -> s.writeDynamicBytes(jar));
    assertThat(specificContract.jarHash()).isEqualTo(expectedHash.toString());
  }

  @Test
  void stateOutputFromString() {
    assertThat(BlockchainResource.StateOutput.fromString("json"))
        .isEqualTo(BlockchainResource.StateOutput.JSON);
    assertThat(BlockchainResource.StateOutput.fromString("binary"))
        .isEqualTo(BlockchainResource.StateOutput.BINARY);
    assertThat(BlockchainResource.StateOutput.fromString("none"))
        .isEqualTo(BlockchainResource.StateOutput.NONE);
    assertThat(BlockchainResource.StateOutput.fromString("default"))
        .isEqualTo(BlockchainResource.StateOutput.DEFAULT);
    assertThat(BlockchainResource.StateOutput.fromString("invalid")).isNull();
  }

  @Test
  public void unableToGetChainStateAvl() {
    assertThatThrownBy(
            () -> resource.getContractAvlNextN(contractAddress.writeAsString(), 10, 0, "00", 1, 0))
        .hasMessage("No chain state for given block time");
  }

  @Test
  public void getEmptyContractAvl() {
    assertNotFound(
        () ->
            resource.getContractAvlNextN(
                "010000000000032100000000000000000000000001", 0, 0, "00", 1, 0));
  }

  @Test
  public void getContractAvlFromAccountId() {
    assertThatThrownBy(
            () ->
                resource.getContractAvlNextN(
                    TestObjects.ACCOUNT_TWO.writeAsString(), 0, 0, "00", 1, 0))
        .hasMessage("Address does not correspond to a contract");
  }

  @Test
  public void getContractAvlStateNextN() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    assertNotFound(
        () ->
            resource.getContract(
                address.writeAsString(), -1, true, BlockchainResource.StateOutput.DEFAULT));
    deployAvlContract(address);
    List<Map.Entry<byte[], byte[]>> entries =
        resource.getContractAvlNextN(address.writeAsString(), -1, 0, "00", 1, 0);
    assertThat(entries).hasSize(1);
    assertThat(entries.get(0).getKey()).containsExactly(1);
    assertThat(entries.get(0).getValue()).containsExactly(2);
    List<Map.Entry<byte[], byte[]>> entries2 =
        resource.getContractAvlNextN(address.writeAsString(), -1, 0, "00", 2, 0);
    assertThat(entries2).hasSize(2);
    assertThat(entries2.get(0).getKey()).containsExactly(1);
    assertThat(entries2.get(0).getValue()).containsExactly(2);
    assertThat(entries2.get(1).getKey()).containsExactly(3);
    assertThat(entries2.get(1).getValue()).containsExactly(4);
    List<Map.Entry<byte[], byte[]>> entries3 =
        resource.getContractAvlNextN(address.writeAsString(), -1, 0, "01", 1, 0);
    assertThat(entries3).hasSize(1);
    assertThat(entries3.get(0).getKey()).containsExactly(3);
    assertThat(entries3.get(0).getValue()).containsExactly(4);
  }

  @Test
  public void getContractAvlStateFirstN() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    deployAvlContract(address);
    List<Map.Entry<byte[], byte[]>> value =
        resource.getContractAvlFirstN(address.writeAsString(), -1, 0, 1, 0);
    assertThat(value).hasSize(1);
    assertThat(value.get(0).getKey()).containsExactly(1);
    assertThat(value.get(0).getValue()).containsExactly(2);
  }

  @DisplayName("Avl next can skip entries")
  @Test
  public void getContractAvlStateSkipped() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    deployAvlContract(address);
    List<Map.Entry<byte[], byte[]>> value =
        resource.getContractAvlFirstN(address.writeAsString(), -1, 0, 1, 1);
    assertThat(value).hasSize(1);
    assertThat(value.get(0).getKey()).containsExactly(3);
    assertThat(value.get(0).getValue()).containsExactly(4);
  }

  @Test
  public void getContractAvlTooLargeN() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    deployAvlContract(address);
    assertThatThrownBy(() -> resource.getContractAvlFirstN(address.writeAsString(), -1, 0, 101, 0))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Can fetch at most 100 entries");
    List<Map.Entry<byte[], byte[]>> value =
        resource.getContractAvlFirstN(address.writeAsString(), -1, 0, 100, 0);
    assertThat(value).hasSize(2);
  }

  @Test
  public void getContractAvlValue() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    deployAvlContract(address);
    AvlStateValue value = resource.getContractAvlValue(address.writeAsString(), -1, 0, "01");
    assertThat(value.data()).containsExactly(2);
    AvlStateValue value2 = resource.getContractAvlValue(address.writeAsString(), -1, 0, "03");
    assertThat(value2.data()).containsExactly(4);
    assertThatThrownBy(() -> resource.getContractAvlValue(address.writeAsString(), -1, 0, "05"))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Value does not exist in tree");
  }

  @Test
  public void unableToGetChainStateAvlValue() {
    assertThatThrownBy(
            () -> resource.getContractAvlValue(contractAddress.writeAsString(), 10, 0, "00"))
        .hasMessage("No chain state for given block time");
  }

  @Test
  public void getEmptyContractAvlValue() {
    assertNotFound(
        () ->
            resource.getContractAvlValue("010000000000032100000000000000000000000001", 0, 0, "00"));
  }

  @Test
  public void getContractAvlFromAccountIdValue() {
    assertThatThrownBy(
            () -> resource.getContractAvlValue(TestObjects.ACCOUNT_TWO.writeAsString(), 0, 0, "00"))
        .hasMessage("Address does not correspond to a contract");
  }

  @Test
  public void getContractAvlModifiedKeys() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    deployAvlContract(address);
    long blockTime1 = resource.getBlockTimeFromUtc(-1);
    InteractWithContractTransaction update =
        InteractWithContractTransaction.create(address, new byte[0]);
    signAndAppend(update, 2);
    long blockTime2 = resource.getBlockTimeFromUtc(-1);
    List<byte[]> modifiedKeys =
        resource.getContractAvlModifiedKeys(address.writeAsString(), 0, blockTime1, blockTime2, 10);
    assertThat(modifiedKeys).hasSize(2).containsExactly(new byte[] {1}, new byte[] {5});
    modifiedKeys =
        resource.getContractAvlModifiedKeys(address.writeAsString(), 0, blockTime1, blockTime2, 1);
    assertThat(modifiedKeys).hasSize(1).containsExactly(new byte[] {1});
    modifiedKeys =
        resource.getContractAvlModifiedKeys(
            address.writeAsString(), 0, blockTime1, blockTime2, "01", 10);
    assertThat(modifiedKeys).hasSize(1).containsExactly(new byte[] {5});
  }

  @Test
  public void unableToGetChainStateAvlModifiedKeys1() {
    assertThatThrownBy(
            () ->
                resource.getContractAvlModifiedKeys(contractAddress.writeAsString(), 0, 10, 0, 10))
        .hasMessage("No chain state for given block time");
  }

  @Test
  public void unableToGetChainStateAvlModifiedKeys2() {
    assertThatThrownBy(
            () ->
                resource.getContractAvlModifiedKeys(contractAddress.writeAsString(), 0, 0, 10, 10))
        .hasMessage("No chain state for given block time");
  }

  @Test
  public void getEmptyContractAvlModifiedKeys1() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    deployAvlContract(address);
    assertNotFound(
        () -> resource.getContractAvlModifiedKeys(address.writeAsString(), 0, 0, -1, 10));
  }

  @Test
  public void getEmptyContractAvlModifiedKeys2() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    deployAvlContract(address);
    assertNotFound(
        () -> resource.getContractAvlModifiedKeys(address.writeAsString(), 0, -1, 2, 10));
  }

  @Test
  public void getContractAvlFromAccountIdModifiedKeys() {
    assertThatThrownBy(
            () ->
                resource.getContractAvlModifiedKeys(
                    TestObjects.ACCOUNT_TWO.writeAsString(), 0, 0, 0, 10))
        .hasMessage("Address does not correspond to a contract");
  }

  @Test
  public void getContractAvlDiffTooLargeN() {
    BlockchainAddress address =
        BlockchainAddress.fromString("010000000000000000000000000000000000000000");
    deployAvlContract(address);
    assertThatThrownBy(
            () -> resource.getContractAvlModifiedKeys(address.writeAsString(), 0, -1, -1, 101))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("Can fetch at most 100 entries");
    List<byte[]> value =
        resource.getContractAvlModifiedKeys(address.writeAsString(), 0, -1, -1, 100);
    assertThat(value).hasSize(0);
  }

  private void deployAvlContract(BlockchainAddress address) {
    InteractWithContractTransaction deploy =
        StateHelper.DeployContract.deploy(
            address,
            SystemBinderJarHelper.getSystemBinderJar(),
            JarBuilder.buildJar(MyContract.class));
    signAndAppend(deploy, 1);
  }

  /** Test. Constant state. */
  public static final class MyContract extends SysContract<ModelFactoryTest.TestingAvlState> {
    @Override
    public ModelFactoryTest.TestingAvlState onCreate(
        SysContractContext context, SafeDataInputStream rpc) {
      return new ModelFactoryTest.TestingAvlState(
          AvlTree.create(
              Map.of(
                  0,
                  new ModelFactoryTest.AvlTreeWrapperStub(
                      AvlTree.create(
                          Map.of(
                              new ModelFactoryTest.ComparableByteArray(new byte[] {1}),
                              new LargeByteArray(new byte[] {2}),
                              new ModelFactoryTest.ComparableByteArray(new byte[] {3}),
                              new LargeByteArray(new byte[] {4})))))));
    }

    @Override
    public ModelFactoryTest.TestingAvlState onInvoke(
        SysContractContext context,
        ModelFactoryTest.TestingAvlState state,
        SafeDataInputStream rpc) {
      return new ModelFactoryTest.TestingAvlState(
          AvlTree.create(
              Map.of(
                  0,
                  new ModelFactoryTest.AvlTreeWrapperStub(
                      AvlTree.create(
                          Map.of(
                              new ModelFactoryTest.ComparableByteArray(new byte[] {1}),
                              new LargeByteArray(new byte[] {20}),
                              new ModelFactoryTest.ComparableByteArray(new byte[] {3}),
                              new LargeByteArray(new byte[] {4}),
                              new ModelFactoryTest.ComparableByteArray(new byte[] {5}),
                              new LargeByteArray(new byte[] {6})))))));
    }
  }
}
