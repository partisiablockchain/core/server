package com.partisiablockchain.server.rest.resources;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.CloseableTest;
import jakarta.ws.rs.core.Response;
import org.junit.jupiter.api.Test;

/** Test. */
public final class HealthResourceTest extends CloseableTest {

  private final HealthResource resource = new HealthResource();

  @Test
  public void getHealthStatus() {
    Response response = resource.getHealth();
    assertThat(response).isNotNull();
    assertThat(response.getStatus()).isEqualTo(200);
  }

  @Test
  public void getHealthJson() {
    Response response = resource.getHealth();
    String expectedResponseBody = "{\"status\":\"up\"}";
    String responseBody = response.getEntity().toString();
    assertThat(response).isNotNull();
    assertThat(expectedResponseBody).isEqualTo(responseBody);
  }
}
