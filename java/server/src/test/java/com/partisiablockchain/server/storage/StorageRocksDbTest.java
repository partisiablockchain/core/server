package com.partisiablockchain.server.storage;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.storage.HashStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.tools.coverage.FunctionUtility;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.rocksdb.Options;
import org.rocksdb.StatsLevel;

/** Test. */
public final class StorageRocksDbTest extends CloseableTest {

  private static final Hash EMPTY_HASH = Hash.create(FunctionUtility.noOpConsumer());
  private HashStorage<Hash> storage;

  @BeforeEach
  void setUp() {
    RootDirectory root = new RootDirectory(newFolder(), StorageRocksDb::new);
    storage = root.createHashStorage("rocks-db-hash-store", Hash::read);
  }

  @Test
  public void populateLarge() {
    HashStorage<Hash> storage = this.storage;
    testPopulation(storage, true);
  }

  @Test
  public void populateLargeSimilarHashes() {
    HashStorage<Hash> storage = this.storage;
    testPopulation(storage, false);
  }

  private void testPopulation(HashStorage<Hash> storage, boolean lower) {
    List<Hash> hashes = createTestHashes(lower);
    for (Hash hash : hashes) {
      Assertions.assertThat(storage.has(hash)).isFalse();
      storage.write(hash, hash);
    }
    // Every file must be a hash
    Stream<Hash> hashStream = storage.readEvery();
    hashStream.forEach(s -> Assertions.assertThat(hashes).contains(s));

    // Every hash should be in a file
    for (Hash hash : hashes) {
      Assertions.assertThat(storage.has(hash)).isTrue();
      Hash value = storage.read(hash);
      Assertions.assertThat(value).isEqualTo(hash);
    }
  }

  @Test
  public void writeFileTwice() {
    Hash oneHashed = TestObjects.hashNumber(1);
    this.storage.write(TestObjects.EMPTY_HASH, oneHashed);
    Assertions.assertThat(this.storage.has(TestObjects.EMPTY_HASH)).isTrue();
    this.storage.readEvery().forEach(h -> Assertions.assertThat(h).isEqualTo(oneHashed));
    Hash twoHashed = TestObjects.hashNumber(2);
    this.storage.write(TestObjects.EMPTY_HASH, twoHashed);
    this.storage.readEvery().forEach(h -> Assertions.assertThat(h).isEqualTo(oneHashed));
  }

  @Test
  public void writeWhileRead() throws InterruptedException {
    String stringHash = TestObjects.EMPTY_HASH.toString();
    final Hash firstHash = Hash.fromString(stringHash.substring(1) + "a");
    final Hash secondHash = Hash.fromString(stringHash.substring(1) + "b");

    HashStorage<Hash> storage = this.storage;
    storage.write(firstHash, TestObjects.hashNumber(5));
    CountDownLatch reading = new CountDownLatch(1);
    CountDownLatch doneReading = new CountDownLatch(1);
    AtomicReference<Hash> read = new AtomicReference<>(null);
    new Thread(
            () -> {
              Hash value = storage.read(firstHash);
              read.set(value);
              doneReading.countDown();
            })
        .start();
    Assertions.assertThat(reading.await(500, TimeUnit.MILLISECONDS)).isFalse();
    storage.write(secondHash, TestObjects.hashNumber(1));
    Assertions.assertThat(doneReading.await(1500, TimeUnit.MILLISECONDS)).isTrue();
    Assertions.assertThat(read).hasValue(TestObjects.hashNumber(5));
  }

  @Test
  public void clearStorage() {
    Assertions.assertThat(storage.isEmpty()).isTrue();
    Assertions.assertThat(this.storage.has(EMPTY_HASH)).isFalse();
    Assertions.assertThat(this.storage.read(EMPTY_HASH)).isNull();

    this.storage.write(EMPTY_HASH, EMPTY_HASH);
    Assertions.assertThat(this.storage.has(EMPTY_HASH)).isTrue();
    Assertions.assertThat(this.storage.read(EMPTY_HASH)).isNotNull();
    Assertions.assertThat(storage.isEmpty()).isFalse();
    this.storage.clear();
    Assertions.assertThat(this.storage.has(EMPTY_HASH)).isFalse();
    Assertions.assertThat(this.storage.read(EMPTY_HASH)).isNull();
    Assertions.assertThat(storage.isEmpty()).isTrue();

    Hash hash1 = TestObjects.hashNumber(12);
    this.storage.write(hash1, EMPTY_HASH);
    Hash hash2 = TestObjects.hashNumber(21);
    this.storage.write(hash2, EMPTY_HASH);
    Assertions.assertThat(this.storage.has(hash1)).isTrue();
    Assertions.assertThat(this.storage.has(hash2)).isTrue();
    Assertions.assertThat(storage.isEmpty()).isFalse();
    this.storage.clear();
    Assertions.assertThat(this.storage.has(hash1)).isFalse();
    Assertions.assertThat(this.storage.has(hash2)).isFalse();
    Assertions.assertThat(storage.isEmpty()).isTrue();

    IntStream.range(0, 100)
        .forEach(value -> storage.write(TestObjects.hashNumber(value), EMPTY_HASH));
    Assertions.assertThat(storage.isEmpty()).isFalse();
    Assertions.assertThat(storage.readEvery().mapToInt(h -> 1).sum()).isEqualTo(100);
    IntStream.range(0, 100)
        .forEach(
            value ->
                Assertions.assertThat(storage.read(TestObjects.hashNumber(value)))
                    .isEqualTo(EMPTY_HASH));
    storage.readEvery().forEach(hash -> Assertions.assertThat(hash).isEqualTo(EMPTY_HASH));

    this.storage.clear();
    Assertions.assertThat(this.storage.has(EMPTY_HASH)).isFalse();
  }

  @Test
  public void remove() {
    Assertions.assertThat(this.storage.has(EMPTY_HASH)).isFalse();
    this.storage.write(EMPTY_HASH, EMPTY_HASH);
    Assertions.assertThat(this.storage.has(EMPTY_HASH)).isTrue();
    this.storage.remove(EMPTY_HASH);
    Assertions.assertThat(this.storage.has(EMPTY_HASH)).isFalse();
  }

  static List<Hash> createTestHashes(boolean lowerEndSimilarity) {
    return IntStream.range(0, 200)
        .mapToObj(TestObjects::hashNumber)
        .flatMap(hash -> createSimilarHashes(hash, lowerEndSimilarity))
        .collect(Collectors.toList());
  }

  static Stream<Hash> createSimilarHashes(Hash hash, boolean lowerEndSimilarity) {
    return IntStream.range(0, 16)
        .mapToObj(
            c -> {
              if (lowerEndSimilarity) {
                return hash.toString().substring(1) + Integer.toHexString(c);
              } else {
                return Integer.toHexString(c) + hash.toString().substring(1);
              }
            })
        .map(Hash::fromString);
  }

  @Test
  public void createOptions() {
    Options options = StorageRocksDb.createOptions();
    Assertions.assertThat(options.createIfMissing()).isTrue();
    Assertions.assertThat(options.statistics()).isNotNull();
    Assertions.assertThat(options.statistics().statsLevel()).isEqualTo(StatsLevel.ALL);
  }
}
