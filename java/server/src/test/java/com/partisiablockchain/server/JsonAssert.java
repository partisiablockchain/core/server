package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.secata.tools.coverage.ExceptionConverter;
import org.assertj.core.api.Assertions;

/** Test. */
public final class JsonAssert {

  public static final ObjectMapper MAPPER = StateObjectMapper.createObjectMapper();

  /** Assert record serialization. */
  public static void assertSerializeLoop(Object object) {
    ExceptionConverter.run(
        () -> {
          String first = MAPPER.writeValueAsString(object);
          Object readObject = MAPPER.readValue(first, object.getClass());
          String second = MAPPER.writeValueAsString(readObject);
          Assertions.assertThat(second).isEqualTo(first);
        },
        "Json serialization failed");
  }
}
