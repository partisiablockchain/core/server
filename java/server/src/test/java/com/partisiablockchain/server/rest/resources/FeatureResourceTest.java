package com.partisiablockchain.server.rest.resources;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.dto.Feature;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Tests for checking features. */
public final class FeatureResourceTest extends CloseableTest {

  private FeatureResource featureResource;

  /** Setup of dependencies for test. */
  @BeforeEach
  public void setUp() {

    final BlockchainTestHelper blockchainTestHelper =
        new BlockchainTestHelper(
            newFolder(),
            state -> {
              StateHelper.initial(state);
              state.setFeature("OPEN_ACCOUNT_CREATION", "TEST");
            },
            this::register);
    BlockchainLedger blockchain = blockchainTestHelper.blockchain;

    featureResource = new FeatureResource(blockchain);
  }

  @Test
  public void getFeaturesValidName() {
    String expectedName = "OPEN_ACCOUNT_CREATION";
    String expectedValue = "TEST";
    final Feature expectedFeature = new Feature(expectedName, expectedValue);
    final List<Feature> features = featureResource.getFeatures();
    assertThat(features).isNotNull();
    assertThat(features).contains(expectedFeature);
    assertThat(expectedFeature.name()).isEqualTo(expectedName);
    assertThat(expectedFeature.value()).isEqualTo(expectedValue);
  }

  @Test
  public void getFeaturesWithInvalidName() {
    String expectedName = "OPEN_ACCOUNT_CREATION";
    String expectedValue = "TEST";
    final Feature expectedFeature = new Feature(expectedName, expectedValue);
    final Feature invalidFeature = new Feature("INVALID", "TEST");
    final List<Feature> features = featureResource.getFeatures();
    assertThat(features).isNotNull();
    assertThat(features).contains(expectedFeature);
    assertThat(features).doesNotContain(invalidFeature);
  }
}
