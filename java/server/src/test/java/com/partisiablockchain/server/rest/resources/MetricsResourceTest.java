package com.partisiablockchain.server.rest.resources;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.server.rest.resources.MetricsResource.MAX_BLOCKS_INTERVAL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.catchThrowableOfType;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.StringToStringContract;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.server.JsonAssert;
import com.partisiablockchain.server.model.BlockchainStatistics;
import com.partisiablockchain.server.model.ResetBlock;
import com.partisiablockchain.server.model.ServerRuntime;
import com.partisiablockchain.server.rest.BlockSearcher;
import com.partisiablockchain.server.rest.resources.helper.SystemBinderJarHelper;
import com.secata.jarutil.JarBuilder;
import com.secata.tools.coverage.FunctionUtility;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Response;
import java.math.BigInteger;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class MetricsResourceTest extends CloseableTest {

  private final KeyPair producerKey = new KeyPair(BigInteger.ONE);
  private final KeyPair otherProducerKey = new KeyPair(BigInteger.TWO);
  private MetricsResource metrics;
  private BlockchainTestHelper blockchainTestHelper;
  private byte[] jar;

  /** Setup default resource. */
  @BeforeEach
  public void setUp() {
    jar = JarBuilder.buildJar(StringToStringContract.class);

    BlockchainAddress contractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(FunctionUtility.noOpConsumer()));
    byte[] abi = new byte[] {0, 1, 2, 3, 4};
    blockchainTestHelper =
        new BlockchainTestHelper(
            newFolder(),
            state -> {
              StateHelper.initial(state);

              state.createContract(
                  contractAddress,
                  CoreContractState.create(
                      state.saveJar(SystemBinderJarHelper.getSystemBinderJar()),
                      state.saveJar(jar),
                      state.saveJar(abi),
                      jar.length));
              state.setContractState(contractAddress, null);
            },
            this::register);

    blockchainTestHelper.appendBlock(producerKey, List.of(), 10L, 1);
    blockchainTestHelper.appendBlock(producerKey, List.of(), 20L, 1);

    metrics = new MetricsResource(blockchainTestHelper.blockchain);
  }

  @Test
  public void getLatestBlocks() {
    assertNotFound(() -> metrics.getLatestBlocks(1));

    long productionTime = System.currentTimeMillis() - 65_000L;
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(), productionTime, -1);
    BlockchainStatistics blockchainStatistics = metrics.getLatestBlocks(2);
    assertThat(blockchainStatistics.blockCount).isEqualTo(1);
    assertThat(blockchainStatistics.committees.containsKey(block.getCommitteeId())).isTrue();
    assertThat(blockchainStatistics.committees.keySet().size()).isEqualTo(1);
    assertThat(
            blockchainStatistics
                .committees
                .get(block.getCommitteeId())
                .producers
                .get(block.getProducerIndex())
                .blocksProduced)
        .isEqualTo(1);

    assertNotFound(() -> metrics.getLatestBlocks(1));

    productionTime = System.currentTimeMillis() - 55_000L;
    block = blockchainTestHelper.appendBlock(producerKey, List.of(), productionTime);
    blockchainStatistics = metrics.getLatestBlocks(1);
    assertThat(blockchainStatistics.committees.containsKey(block.getCommitteeId())).isTrue();
    assertThat(blockchainStatistics.committees.keySet().size()).isEqualTo(1);
    assertThat(blockchainStatistics.blockCount).isEqualTo(1);

    Block block2 = blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainStatistics = metrics.getLatestBlocks(1);
    assertThat(blockchainStatistics.committees.containsKey(block2.getCommitteeId())).isTrue();
    assertThat(blockchainStatistics.blockCount).isEqualTo(2);
    assertThat(blockchainStatistics.committees.containsKey(block.getCommitteeId())).isTrue();
    assertThat(blockchainStatistics.committees.keySet().size()).isEqualTo(2);
  }

  @Test
  public void getLatestBlocksBackToGenesis() {
    BlockchainStatistics blockMetrics = metrics.getLatestBlocks(Integer.MAX_VALUE);
    assertThat(blockMetrics.blockCount).isEqualTo(3);
    assertThat(blockMetrics.earliestBlockProductionTime).isEqualTo(0);
  }

  /** Should only get blocks within interval. */
  @Test
  public void getBlockInterval() {
    BlockchainStatistics blockchainStatistics = metrics.getBlocksFromBlockTimeInterval(0, 2);
    assertThat(blockchainStatistics.blockCount).isEqualTo(3);
    blockchainStatistics = metrics.getBlocksFromBlockTimeInterval(0, 1);
    assertThat(blockchainStatistics.blockCount).isEqualTo(2);
    blockchainStatistics = metrics.getBlocksFromBlockTimeInterval(2, 2);
    assertThat(blockchainStatistics.blockCount).isEqualTo(1);
  }

  /** Cannot complete a request from outside of current blockTime range. */
  @Test
  public void getBlockIntervalReturnsErrorOnOutOfRangeRequest() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of());
    assertThatThrownBy(
            () ->
                metrics.getBlocksFromBlockTimeInterval(
                    block.getBlockTime(), block.getBlockTime() + 10))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("'to' was larger than latest block time");
  }

  /** Illegal argument when from is less than to. */
  @Test
  public void getBlockIntervalFromHasToBeLessThanTo() {
    assertThatThrownBy(() -> metrics.getBlocksFromBlockTimeInterval(1, 0))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("'from' has to be equal or less than 'to'");
  }

  /** 'to' argument set to -1 goes to latest block. */
  @Test
  public void getBlockIntervalMinus1GoesToLatest() {
    blockchainTestHelper.appendBlock(producerKey, List.of());
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of());
    long startProdTime = block.getProductionTime();
    for (int i = 1; i <= MAX_BLOCKS_INTERVAL; i++) {
      block = blockchainTestHelper.appendBlock(producerKey, List.of(), startProdTime + i);
    }

    BlockchainStatistics blockTimeInterval =
        metrics.getBlocksFromBlockTimeInterval(MAX_BLOCKS_INTERVAL - 10, -1);
    assertThat(blockTimeInterval.blockCount)
        .isEqualTo(block.getBlockTime() - (MAX_BLOCKS_INTERVAL - 10) + 1);
  }

  /** Cannot complete a request larger than {@code MAX_BLOCKS_INTERVAL}. */
  @Test
  public void getBlockIntervalNoMoreThanMaxInterval() {
    assertThatThrownBy(() -> metrics.getBlocksFromBlockTimeInterval(0, MAX_BLOCKS_INTERVAL + 1))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Interval is larger than " + MAX_BLOCKS_INTERVAL);
  }

  /** Can complete a request with interval size {@code MAX_BLOCKS_INTERVAL}. */
  @Test
  public void getBlockIntervalAtMaxInterval() {
    blockchainTestHelper.appendBlock(producerKey, List.of());
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of());
    long startProdTime = block.getProductionTime();
    for (int i = 1; i <= MAX_BLOCKS_INTERVAL; i++) {
      blockchainTestHelper.appendBlock(producerKey, List.of(), startProdTime + i);
    }
    BlockchainStatistics blocksFromBlockTimeInterval =
        metrics.getBlocksFromBlockTimeInterval(1, MAX_BLOCKS_INTERVAL);
    assertThat(blocksFromBlockTimeInterval.blockCount).isEqualTo(MAX_BLOCKS_INTERVAL);
  }

  private void assertNotFound(ThrowableAssert.ThrowingCallable method) {
    WebApplicationException throwable = catchThrowableOfType(WebApplicationException.class, method);
    assertThat(throwable.getResponse().getStatus())
        .isEqualTo(Response.Status.NOT_FOUND.getStatusCode());
  }

  @Test
  public void isWithinTimeFrame() {
    Block block = blockchainTestHelper.appendBlock(producerKey, List.of(), 1000L);
    assertThat(metrics.isWithinTimeframe(1000L, block)).isTrue();
  }

  @Test
  public void runtime() {
    ServerRuntime runtime = metrics.getRuntime();
    assertThat(runtime).isNotNull();
    JsonAssert.assertSerializeLoop(runtime);
  }

  @Test
  public void getLatestResetBlock() {
    long productionTime = System.currentTimeMillis() - 30_000;
    blockchainTestHelper.appendBlock(producerKey, List.of(), productionTime - 2, 1);
    blockchainTestHelper.appendBlock(producerKey, List.of(), productionTime - 1, 1);
    Block previous = blockchainTestHelper.appendBlock(producerKey, List.of(), productionTime, 1);
    Block reset = blockchainTestHelper.appendBlock(producerKey, List.of(), productionTime + 1, -1);
    ResetBlock resetBlock = metrics.getLatestResetBlock(1);
    assertThat(resetBlock).isNotNull();
    assertThat(resetBlock.resetBlock().identifier()).isEqualTo(reset.identifier().toString());
    assertThat(resetBlock.previousBlock().identifier()).isEqualTo(previous.identifier().toString());

    blockchainTestHelper.appendBlock(otherProducerKey, List.of(), productionTime + 2, 1);
    blockchainTestHelper.appendBlock(otherProducerKey, List.of(), productionTime + 3, 1);
    resetBlock = metrics.getLatestResetBlock(1);
    assertThat(resetBlock).isNotNull();
    assertThat(resetBlock.resetBlock().identifier()).isEqualTo(reset.identifier().toString());
    assertThat(resetBlock.previousBlock().identifier()).isEqualTo(previous.identifier().toString());
  }

  @Test
  public void getLatestResetBlock_noBlocks() {
    BlockchainLedger blockchainMock = Mockito.mock(BlockchainLedger.class);
    Mockito.when(blockchainMock.getLatestBlock()).thenReturn(null);
    MetricsResource metrics = new MetricsResource(blockchainMock);
    ResetBlock resetBlock = metrics.getLatestResetBlock(1);
    assertThat(resetBlock).isNull();
  }

  @Test
  public void getLatestResetBlock_stopWhenFound() {
    BlockchainLedger blockchainMock = Mockito.mock(BlockchainLedger.class);
    Block blockMock = Mockito.mock(Block.class);
    Mockito.when(blockMock.getProducerIndex()).thenReturn((short) -1);
    Hash dummyHash = Hash.create(h -> h.writeInt(123));
    Mockito.when(blockMock.getParentBlock()).thenReturn(dummyHash);
    Mockito.when(blockMock.identifier()).thenReturn(dummyHash);
    Mockito.when(blockMock.getState()).thenReturn(dummyHash);
    Mockito.when(blockchainMock.getLatestBlock()).thenReturn(blockMock);
    Mockito.when(blockchainMock.getBlock(Mockito.eq(dummyHash))).thenReturn(blockMock);
    MetricsResource metrics = new MetricsResource(blockchainMock);

    AtomicReference<ResetBlock> resetBlockReference = new AtomicReference<>();
    BlockSearcher resetBlockSearcher = metrics.createResetBlockSearcher(resetBlockReference);
    assertThat(resetBlockSearcher.handleBlock(blockMock)).isTrue();
  }
}
