package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SerializedStateRequestTest {

  @Test
  public void readWrite() {
    SerializedStateRequest response = new SerializedStateRequest(Hash.create(s -> s.writeInt(11)));

    SerializedStateRequest read =
        SafeDataInputStream.readFully(
            SafeDataOutputStream.serialize(response), SerializedStateRequest::read);
    Assertions.assertThat(read).usingRecursiveComparison().isEqualTo(response);
  }

  @Test
  public void getters() {
    Hash stateHash = Hash.create(s -> s.writeInt(11));
    SerializedStateRequest response = new SerializedStateRequest(stateHash);
    Assertions.assertThat(response.getHash()).isEqualTo(stateHash);
  }
}
