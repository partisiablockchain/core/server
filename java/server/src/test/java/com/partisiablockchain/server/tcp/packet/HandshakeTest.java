package com.partisiablockchain.server.tcp.packet;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class HandshakeTest {

  @Test
  public void readWrite() {
    Hash first = Hash.create(s -> s.writeInt(123));
    Hash second = Hash.create(s -> s.writeInt(456));
    Handshake response =
        new Handshake(
            List.of(first, second), List.of(TestObjects.ACCOUNT_FOUR, TestObjects.ACCOUNT_TWO));

    Handshake read =
        SafeDataInputStream.readFully(SafeDataOutputStream.serialize(response), Handshake::read);
    Assertions.assertThat(read).usingRecursiveComparison().isEqualTo(response);
  }

  @Test
  public void readWriteNullBinder() {
    Handshake response =
        new Handshake(null, List.of(TestObjects.ACCOUNT_FOUR, TestObjects.ACCOUNT_TWO));
    Assertions.assertThat(response.isBinderEnabled(Hash.create(s -> s.writeInt(11)))).isTrue();

    Handshake read =
        SafeDataInputStream.readFully(SafeDataOutputStream.serialize(response), Handshake::read);
    Assertions.assertThat(read).usingRecursiveComparison().isEqualTo(response);
  }

  @Test
  public void getters() {
    Hash first = Hash.create(s -> s.writeInt(123));
    Hash second = Hash.create(s -> s.writeInt(456));
    Handshake response =
        new Handshake(
            List.of(first, second), List.of(TestObjects.ACCOUNT_FOUR, TestObjects.ACCOUNT_TWO));
    Assertions.assertThat(response.getExisting())
        .containsExactly(TestObjects.ACCOUNT_FOUR, TestObjects.ACCOUNT_TWO);
    Assertions.assertThat(response.isBinderEnabled(first)).isTrue();
    Assertions.assertThat(response.isBinderEnabled(second)).isTrue();
    Assertions.assertThat(response.isBinderEnabled(Hash.create(s -> s.writeInt(0)))).isFalse();
  }
}
