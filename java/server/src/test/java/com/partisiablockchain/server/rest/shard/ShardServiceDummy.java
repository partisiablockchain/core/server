package com.partisiablockchain.server.rest.shard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.TransactionCost;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.server.rest.shard.ShardTransaction.ShardExecutionStatus;
import com.partisiablockchain.server.rest.shard.ShardTransaction.ShardExecutionStatus.ShardFailure;
import com.partisiablockchain.server.rest.shard.ShardTransaction.ShardExecutionStatus.ShardTransactionPointer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Dummy implementation for shard service. */
public final class ShardServiceDummy implements ShardService {

  private final ObjectMapper mapper = StateObjectMapper.createObjectMapper();
  private long governanceVersion = 0L;
  public final Hash invalidBlockId = Hash.create(h -> h.writeInt(1));
  public final Hash validBlockId = Hash.create(h -> h.writeInt(2));
  public final Hash latestValidBlockId = Hash.create(h -> h.writeInt(3));
  public final Hash parentBlock = Hash.create(h -> h.writeInt(4));
  public final Hash eventHash = Hash.create(h -> h.writeInt(5));
  public final Hash transactionHash = Hash.create(h -> h.writeInt(6));
  public final Hash blockState = Hash.create(h -> h.writeInt(7));
  public final Hash invalidTransactionId = Hash.create(h -> h.writeInt(8));
  public final Hash validTransactionId = Hash.create(h -> h.writeInt(9));
  public final Hash txPointerId = Hash.create(h -> h.writeInt(10));
  public final String destinationShard = "Gov";
  public final Hash exBlockId = Hash.create(h -> h.writeInt(11));
  public final byte[] txContent = new byte[4];
  public final Hash invalidJarId = Hash.create(h -> h.writeInt(12));
  public final Hash validJarId = Hash.create(h -> h.writeInt(13));
  public final Hash successfulTransaction = Hash.create(h -> h.writeInt(14));
  public final Hash pendingTransaction = Hash.create(h -> h.writeInt(15));
  public final byte[] validJarData = new byte[4];

  @Override
  public ShardState getShardState(String shardId) {
    JsonNode pluginState = mapper.valueToTree("state");
    Map<ChainPluginType, JsonNode> plugins = Map.of(ChainPluginType.ACCOUNT, pluginState);

    return new ShardState(governanceVersion, plugins);
  }

  long setGovernanceVersion(long newValue) {
    governanceVersion = newValue;
    return governanceVersion;
  }

  @Override
  public ShardBlock getShardBlock(String shardId, Hash identifier) {
    Hash blockId = null;
    if (identifier == null) {
      blockId = latestValidBlockId;
    } else if (identifier.equals(validBlockId)) {
      blockId = validBlockId;
    } else if (identifier.equals(invalidBlockId)) {
      return null;
    }

    return new ShardBlock(
        blockId,
        parentBlock,
        0L,
        0L,
        0L,
        List.of(eventHash),
        List.of(transactionHash),
        blockState,
        (short) 0);
  }

  @Override
  public ShardTransaction getShardTransaction(String shardId, Hash identifier) {
    if (identifier.equals(invalidTransactionId)) {
      return null;
    }
    TransactionCost cost = new TransactionCost(0, 1, 2, 3, new HashMap<Hash, Long>());
    if (identifier.equals(successfulTransaction)) {
      return new ShardTransaction(
          validTransactionId,
          true,
          txContent,
          new ShardExecutionStatus(exBlockId, false, false, List.of(), cost, null));
    }

    if (identifier.equals(pendingTransaction)) {
      return new ShardTransaction(pendingTransaction, true, txContent, null);
    }

    ShardFailure shardFailure = new ShardFailure("message", "trace");
    ShardTransactionPointer shardTransactionPointer =
        new ShardTransactionPointer(txPointerId, null);
    ShardExecutionStatus shardExecutionStatus =
        new ShardExecutionStatus(
            exBlockId, true, true, List.of(shardTransactionPointer), cost, shardFailure);

    return new ShardTransaction(validTransactionId, true, txContent, shardExecutionStatus);
  }

  @Override
  public ShardJar getShardJar(String shardId, Hash identifier) {
    if (identifier.equals(invalidJarId)) {
      return null;
    }

    return new ShardJar(validJarData);
  }
}
