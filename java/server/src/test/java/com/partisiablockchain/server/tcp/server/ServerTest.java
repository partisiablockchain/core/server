package com.partisiablockchain.server.tcp.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.ObjectCreator;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.server.tcp.packet.Handshake;
import com.secata.stream.SafeDataOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ServerTest extends CloseableTest {

  @Test
  public void closeOnError() {
    Server server =
        new Server(
            new BlockchainTestHelper(newFolder(), this::register).blockchain, ObjectCreator.port());

    AtomicBoolean closed = new AtomicBoolean();
    ByteArrayInputStream stream =
        new ByteArrayInputStream(new byte[0]) {
          @Override
          public void close() {
            closed.set(true);
          }
        };
    server.createConnectedClient(stream, new ByteArrayOutputStream());
    Assertions.assertThat(closed.get()).isTrue();
    Assertions.assertThat(server.clients()).isEqualTo(0);
  }

  @Test
  public void readHandshake() {
    Server server =
        new Server(
            new BlockchainTestHelper(newFolder(), this::register).blockchain, ObjectCreator.port());

    ObjectCreator.BlockingInput stream =
        ObjectCreator.blockingInput(
            this, SafeDataOutputStream.serialize(new Handshake(null, List.of())));

    Assertions.assertThat(server.clients()).isEqualTo(0);
    server.createConnectedClient(stream, new ByteArrayOutputStream());
    Assertions.assertThat(server.clients()).isEqualTo(1);
    Assertions.assertThat(server.isClosed()).isFalse();
    server.close();

    Assertions.assertThat(server.isClosed()).isTrue();
    Assertions.assertThat(stream.isClosed()).isTrue();
  }
}
