package com.partisiablockchain.server.tcp;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.ObjectCreator;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.server.tcp.packet.ContractRemoved;
import com.partisiablockchain.server.tcp.packet.Packet;
import com.partisiablockchain.server.tcp.packet.SerializedStateRequest;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ConnectionTest extends CloseableTest {

  private final Hash hash = Hash.create(s -> s.writeByte(0));

  @Test
  public void shouldCallConsumerWithReadData() throws Exception {
    CountDownLatch incoming = new CountDownLatch(1);
    register(
        Connection.createConnection(
            packet -> incoming.countDown(),
            ObjectCreator.blockingInput(
                this,
                SafeDataOutputStream.serialize(
                    s ->
                        new Packet<>(
                                Packet.Type.SERIALIZED_STATE_REQUEST,
                                new SerializedStateRequest(hash))
                            .send(s))),
            new ByteArrayOutputStream(),
            () -> {}));
    Assertions.assertThat(incoming.await(1, TimeUnit.SECONDS)).isTrue();
  }

  @Test
  public void shouldSendOutbound() throws Exception {
    Packet<SerializedStateRequest> packet =
        new Packet<>(Packet.Type.SERIALIZED_STATE_REQUEST, new SerializedStateRequest(hash));
    byte[] expected = SafeDataOutputStream.serialize(packet::send);
    CountDownLatch missingBytes = new CountDownLatch(expected.length);
    byte[] actual = new byte[expected.length];
    OutputStream output =
        new OutputStream() {
          @Override
          public void write(int b) {
            actual[actual.length - (int) missingBytes.getCount()] = (byte) b;
            missingBytes.countDown();
          }
        };

    Connection connection =
        register(
            Connection.createConnection(
                FunctionUtility.noOpConsumer(),
                ObjectCreator.blockingInput(this),
                output,
                () -> {}));
    connection.send(packet);
    Assertions.assertThat(missingBytes.await(1, TimeUnit.SECONDS)).isTrue();
    Assertions.assertThat(actual).isEqualTo(expected);
  }

  @Test
  public void closeOnError() throws Exception {
    CountDownLatch closed = new CountDownLatch(1);
    Connection connection =
        register(
            Connection.createConnection(
                FunctionUtility.noOpConsumer(),
                ObjectCreator.blockingInput(this),
                new OutputStream() {
                  @Override
                  public void write(int b) {
                    throw new RuntimeException("expected");
                  }
                },
                closed::countDown));
    connection.send(
        new Packet<>(Packet.Type.CONTRACT_REMOVED, new ContractRemoved(TestObjects.ACCOUNT_TWO)));
    Assertions.assertThat(closed.await(1, TimeUnit.SECONDS)).isTrue();
  }

  @Test
  public void onlyCloseOnce() {
    AtomicInteger closed = new AtomicInteger();
    Connection connection =
        register(
            Connection.createConnection(
                FunctionUtility.noOpConsumer(),
                ObjectCreator.blockingInput(this),
                new ByteArrayOutputStream(),
                closed::incrementAndGet));
    Assertions.assertThat(closed).hasValue(0);
    connection.close();
    Assertions.assertThat(closed).hasValue(1);
    connection.close();
    Assertions.assertThat(closed).hasValue(1);
  }
}
