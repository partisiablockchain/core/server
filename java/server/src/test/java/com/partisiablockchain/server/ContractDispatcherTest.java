package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedger.UpdateEvent;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.contract.StringToStringContract;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.server.ContractDispatcher.ContractEventDispatcher;
import com.partisiablockchain.server.ServerModule.ContractListener;
import com.partisiablockchain.server.rest.resources.helper.SystemBinderJarHelper;
import com.secata.jarutil.JarBuilder;
import com.secata.tools.thread.SharedPoolSingleThreadedExecution;
import com.secata.tools.thread.SharedPoolSingleThreadedExecution.SingleThreadExecutor;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ContractDispatcherTest extends CloseableTest {

  static final BlockchainAddress contractAddress =
      BlockchainAddress.fromString("010000000000000000000000000000000000000014");
  private final KeyPair producerKey = new KeyPair(BigInteger.ONE);
  private BlockchainLedger blockchain;

  /** Setup default resource. */
  @BeforeEach
  void setUp() {
    BlockchainTestHelper blockchainTestHelper =
        new BlockchainTestHelper(newFolder(), StateHelper::initial, this::register);

    blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainTestHelper.appendBlock(producerKey, List.of());

    String chainId = blockchainTestHelper.blockchain.getChainId();
    SignedTransaction deployTransaction = createDeployTransaction(chainId, producerKey);

    blockchainTestHelper.appendBlock(producerKey, List.of(deployTransaction));
    blockchainTestHelper.appendBlock(producerKey, List.of());

    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contractAddress, new byte[] {0, 0, 0, 1, 32});
    CoreTransactionPart core =
        CoreTransactionPart.create(
            2L,
            blockchainTestHelper.blockchain.getLatestBlock().getProductionTime() + 100_000,
            1000000);
    SignedTransaction interactTransaction =
        SignedTransaction.create(core, interact).sign(producerKey, chainId);

    blockchainTestHelper.appendBlock(producerKey, List.of(interactTransaction));
    blockchainTestHelper.appendBlock(producerKey, List.of());

    InteractWithContractTransaction remove = StateHelper.DeployContract.remove(contractAddress);
    core =
        CoreTransactionPart.create(
            3L,
            blockchainTestHelper.blockchain.getLatestBlock().getProductionTime() + 100_000,
            1000000);
    SignedTransaction removeTransaction =
        SignedTransaction.create(core, remove).sign(producerKey, chainId);
    blockchainTestHelper.appendBlock(producerKey, List.of(removeTransaction));
    blockchainTestHelper.appendBlock(producerKey, List.of());
    blockchainTestHelper.appendBlock(producerKey, List.of());

    blockchain = blockchainTestHelper.blockchain;
  }

  static SignedTransaction createDeployTransaction(String chainId, KeyPair producerKey) {
    byte[] jar = JarBuilder.buildJar(StringToStringContract.class);

    byte[] publicBinderJar = SystemBinderJarHelper.getSystemBinderJar();
    InteractWithContractTransaction deploy =
        StateHelper.DeployContract.deploy(contractAddress, publicBinderJar, jar);

    SignedTransaction deployTransaction =
        SignedTransaction.create(
                CoreTransactionPart.create(1L, System.currentTimeMillis() + 100_000, 1000000),
                deploy)
            .sign(producerKey, chainId);
    return deployTransaction;
  }

  @Test
  public void createNewContract() {
    ContractDispatcher contractDispatcher =
        new ContractDispatcher(
            List.of(
                (blockchainAddress, coreContractState, stateSerializable) -> {
                  checkCreate(blockchainAddress, coreContractState);
                  return null;
                }));
    runState(new ContractDispatcher(List.of()), 0);
    runState(contractDispatcher, 1);
  }

  private void checkCreate(
      BlockchainAddress blockchainAddress, CoreContractState coreContractState) {
    Assertions.assertThat(blockchainAddress).isEqualTo(contractAddress);
    Assertions.assertThat(coreContractState.getBinderIdentifier())
        .isEqualTo(SystemBinderJarHelper.getSystemBinderJarIdentifier());
  }

  @Test
  public void noChange() {
    ContractDispatcher contractDispatcher =
        new ContractDispatcher(
            List.of(
                (blockchainAddress, coreContractState, stateSerializable) -> {
                  checkCreate(blockchainAddress, coreContractState);
                  return null;
                }));
    runState(contractDispatcher, 1);
  }

  @Test
  public void noEventsIfNoListeners() {
    ContractDispatcher contractDispatcher =
        new ContractDispatcher(
            List.of(
                (blockchainAddress, coreContractState, stateSerializable) -> {
                  Assertions.assertThat(coreContractState.getBinderIdentifier())
                      .isEqualTo(SystemBinderJarHelper.getSystemBinderJarIdentifier());
                  return null;
                }));
    runAllStates(contractDispatcher);
  }

  private void runAllStates(ContractDispatcher contractDispatcher) {
    runState(contractDispatcher, 0);
    runState(contractDispatcher, 1);
    runState(contractDispatcher, 2);
    runState(contractDispatcher, 3);
    runState(contractDispatcher, 4);
    runState(contractDispatcher, 5);
    runState(contractDispatcher, 6);
    runState(contractDispatcher, 7);
    runState(contractDispatcher, 8);
  }

  private void runState(ContractDispatcher contractDispatcher, int i) {
    runState(contractDispatcher, i, i == 0);
  }

  private void runState(ContractDispatcher contractDispatcher, int i, boolean first) {
    ImmutableChainState state = blockchain.getState(i);
    UpdateEvent updateEvent;
    if (first) {
      updateEvent = new UpdateEvent(state.getContracts(), Set.of(), Set.of());
    } else {
      ImmutableChainState previousState = blockchain.getState(i - 1);
      Set<BlockchainAddress> previousStateContracts = previousState.getContracts();
      Set<BlockchainAddress> currentContracts = state.getContracts();

      Set<BlockchainAddress> newContracts = new HashSet<>(currentContracts);
      newContracts.removeAll(previousStateContracts);

      Set<BlockchainAddress> oldContracts = new HashSet<>(previousStateContracts);
      oldContracts.removeAll(currentContracts);

      Set<BlockchainAddress> contracts = new HashSet<>(currentContracts);
      contracts.removeAll(newContracts);
      contracts.removeIf(contract -> !state.hasContractChanged(contract, previousState));

      updateEvent = new UpdateEvent(newContracts, contracts, oldContracts);
    }

    contractDispatcher.newFinalState(state, updateEvent);
  }

  @Test
  public void dispatchEvents() throws InterruptedException {
    AtomicInteger updated = new AtomicInteger();
    AtomicInteger removed = new AtomicInteger();

    List<BlockchainAddress> created = new ArrayList<>();
    final ContractListener contractListener = new TestContractListener(updated, removed);

    ContractDispatcher contractDispatcher =
        new ContractDispatcher(
            List.of(
                (blockchainAddress, coreContractState, stateSerializable) -> {
                  created.add(blockchainAddress);
                  if (blockchainAddress.equals(contractAddress)) {
                    return contractListener;
                  } else {
                    return null;
                  }
                }));
    runAllStates(contractDispatcher);
    Assertions.assertThat(created).containsExactly(StateHelper.DEPLOY, contractAddress);

    Thread.sleep(1500);

    Assertions.assertThat(updated.get()).isEqualTo(2);
    Assertions.assertThat(removed.get()).isEqualTo(1);
  }

  @Test
  public void dispatchEventsAtTheEnd() throws InterruptedException {
    AtomicInteger updated = new AtomicInteger();
    AtomicInteger removed = new AtomicInteger();
    List<BlockchainAddress> created = new ArrayList<>();

    final ContractListener contractListener = new TestContractListener(updated, removed);

    ContractDispatcher contractDispatcher =
        new ContractDispatcher(
            List.of(
                (blockchainAddress, coreContractState, stateSerializable) -> {
                  created.add(blockchainAddress);
                  if (blockchainAddress.equals(contractAddress)) {
                    return contractListener;
                  } else {
                    return null;
                  }
                }));
    runState(contractDispatcher, 4, true);

    Assertions.assertThat(created).containsExactlyInAnyOrder(StateHelper.DEPLOY, contractAddress);

    Thread.sleep(1500);

    Assertions.assertThat(updated.get()).isEqualTo(1);
    Assertions.assertThat(removed.get()).isEqualTo(0);
  }

  @Test
  public void interactionEvents() throws Exception {
    AtomicInteger updated = new AtomicInteger();
    AtomicInteger removed = new AtomicInteger();

    final ContractListener contractListener = new TestContractListener(updated, removed);

    Assertions.assertThat(updated.get()).isEqualTo(0);
    Assertions.assertThat(removed.get()).isEqualTo(0);

    SharedPoolSingleThreadedExecution test =
        SharedPoolSingleThreadedExecution.named("interactionEvents");
    SingleThreadExecutor singleThreadedExecutor = test.createSingleThreadedExecutor();
    final ContractEventDispatcher contractDispatcher =
        new ContractEventDispatcher(
            singleThreadedExecutor,
            List.of(contractListener, contractListener),
            blockchain.getState(4).getCoreContractState(contractAddress),
            blockchain.getState(4).getContractState(contractAddress));
    Callable<Void> callable = () -> null;
    singleThreadedExecutor.execute(callable).get();

    Assertions.assertThat(updated.get()).isEqualTo(2);
    Assertions.assertThat(removed.get()).isEqualTo(0);

    contractDispatcher.update(
        blockchain.getState(4).getCoreContractState(contractAddress),
        blockchain.getState(4).getContractState(contractAddress));
    singleThreadedExecutor.execute(callable).get();

    Assertions.assertThat(updated.get()).isEqualTo(4);
    Assertions.assertThat(removed.get()).isEqualTo(0);

    contractDispatcher.remove();
    singleThreadedExecutor.execute(callable).get();

    Assertions.assertThat(updated.get()).isEqualTo(4);
    Assertions.assertThat(removed.get()).isEqualTo(2);
  }

  private static final class TestContractListener implements ContractListener {

    private final AtomicInteger updated;
    private final AtomicInteger removed;

    public TestContractListener(AtomicInteger updated, AtomicInteger removed) {
      this.updated = updated;
      this.removed = removed;
    }

    @Override
    public void update(CoreContractState coreContractState, StateSerializable stateSerializable) {
      updated.incrementAndGet();
    }

    @Override
    public void remove() {
      removed.incrementAndGet();
    }
  }
}
