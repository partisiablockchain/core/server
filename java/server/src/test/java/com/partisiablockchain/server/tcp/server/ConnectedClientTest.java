package com.partisiablockchain.server.tcp.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.ObjectCreator;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.StateHash;
import com.partisiablockchain.blockchain.StateHelper;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.StringToStringContract;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateString;
import com.partisiablockchain.server.rest.resources.helper.SystemBinderJarHelper;
import com.partisiablockchain.server.tcp.ClosableConnection;
import com.partisiablockchain.server.tcp.packet.ContractCreatedResponse;
import com.partisiablockchain.server.tcp.packet.ContractRemoved;
import com.partisiablockchain.server.tcp.packet.ContractStateRequest;
import com.partisiablockchain.server.tcp.packet.Handshake;
import com.partisiablockchain.server.tcp.packet.Packet;
import com.partisiablockchain.server.tcp.packet.SerializedStateRequest;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataOutputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

/** Test. */
public final class ConnectedClientTest extends CloseableTest {

  private final BlockchainAddress contractAddress =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(stream -> stream.writeInt(123)));

  private final BlockchainAddress contractAddress2 =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(stream -> stream.writeInt(456)));
  private BlockchainLedger blockchain;
  private BlockchainTestHelper blockchainTestHelper;

  /** Init blockchain. */
  @BeforeEach
  public void setUp() {
    blockchainTestHelper =
        new BlockchainTestHelper(
            newFolder(),
            mut -> {
              StateHelper.initial(mut);
              Hash binder = mut.saveJar(SystemBinderJarHelper.getSystemBinderJar());
              Hash contract = mut.saveJar(JarBuilder.buildJar(StringToStringContract.class));
              mut.createContract(
                  contractAddress,
                  CoreContractState.create(binder, contract, TestObjects.EMPTY_HASH, 134));
              mut.createContract(
                  contractAddress2,
                  CoreContractState.create(binder, contract, TestObjects.EMPTY_HASH, 567));
              mut.setContractState(contractAddress, new StateString("MyState"));
              mut.setContractState(contractAddress2, new StateString("MyNewState"));
            },
            this::register);
    blockchainTestHelper.appendBlock(new KeyPair(BigInteger.ONE), List.of());
    blockchain = blockchainTestHelper.blockchain;
  }

  @Test
  public void createUpdateRemove() throws Exception {
    MyConnection connection = new MyConnection(1);

    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(null, List.of()));
    final CountDownLatch done = new CountDownLatch(1);
    // Trigger initial without any contracts
    connectedClient.newFinalState(
        blockchain.latest().getState(),
        new BlockchainLedger.UpdateEvent(List.of(), List.of(), List.of()));
    register(
            new Thread(
                () -> {
                  connectedClient.newFinalState(
                      blockchain.latest().getState(),
                      new BlockchainLedger.UpdateEvent(
                          List.of(contractAddress), List.of(), List.of()));
                  done.countDown();
                }))
        .start();
    Assertions.assertThat(connection.latch.await(1, TimeUnit.SECONDS)).isTrue();
    connectedClient.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_CREATED_RESPONSE,
            new ContractCreatedResponse(contractAddress, true)));
    Assertions.assertThat(done.await(1, TimeUnit.SECONDS)).isTrue();
    connectedClient.newFinalState(
        blockchain.latest().getState(),
        new BlockchainLedger.UpdateEvent(List.of(), List.of(contractAddress), List.of()));
    Assertions.assertThat(connection.messages).hasSize(2);
    connectedClient.newFinalState(
        blockchain.latest().getState(),
        new BlockchainLedger.UpdateEvent(List.of(), List.of(), List.of(contractAddress)));
    Assertions.assertThat(connection.messages).hasSize(3);
  }

  @Test
  public void createNonExisting() {
    MyConnection connection = new MyConnection(1);

    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(null, List.of()));
    BlockchainAddress nonExisting =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(stream -> stream.writeInt(7878)));
    connectedClient.newFinalState(
        blockchain.latest().getState(),
        new BlockchainLedger.UpdateEvent(List.of(nonExisting), List.of(), List.of(nonExisting)));
  }

  @Test
  public void updateAndRemoveInSameBlock() throws Exception {
    MyConnection connection = new MyConnection(1);

    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(null, List.of()));
    register(
            new Thread(
                () ->
                    connectedClient.newFinalState(
                        blockchain.latest().getState(),
                        new BlockchainLedger.UpdateEvent(
                            List.of(contractAddress), List.of(), List.of()))))
        .start();
    Assertions.assertThat(connection.latch.await(1, TimeUnit.SECONDS)).isTrue();
    connectedClient.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_CREATED_RESPONSE,
            new ContractCreatedResponse(contractAddress, true)));

    BlockchainTestHelper emptyChain = new BlockchainTestHelper(newFolder(), this::register);
    connectedClient.newFinalState(
        emptyChain.blockchain.getChainState(),
        new BlockchainLedger.UpdateEvent(
            List.of(), List.of(contractAddress), List.of(contractAddress)));
  }

  @Test
  public void stateRequest() {
    MyConnection connection = new MyConnection();
    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(null, List.of()));
    Hash stateHash =
        Objects.requireNonNull(
            StateHash.getStateHash(blockchain.latest().getState(), contractAddress));
    connectedClient.incoming(
        new Packet<>(Packet.Type.SERIALIZED_STATE_REQUEST, new SerializedStateRequest(stateHash)));
    Assertions.assertThat(connection.messages).hasSize(1);
  }

  @Test
  public void contractStateRequest() {
    MyConnection connection = new MyConnection();

    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(null, List.of()));
    connectedClient.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_STATE_REQUEST, new ContractStateRequest(contractAddress, 0)));
    Assertions.assertThat(connection.messages).hasSize(1);
  }

  @Test
  public void illegalPacketType() {
    ClosableConnection connection = new MyConnection();
    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(null, List.of()));
    Assertions.assertThatThrownBy(
            () ->
                connectedClient.incoming(
                    new Packet<>(
                        Packet.Type.CONTRACT_REMOVED, new ContractRemoved(contractAddress))))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void defaultConstructorAttachAndDetach() {
    BlockchainLedger ledger = Mockito.mock(BlockchainLedger.class);
    ConnectedClient connectedClient =
        new ConnectedClient(
            ObjectCreator.blockingInput(this),
            new ByteArrayOutputStream(),
            ledger,
            new Handshake(List.of(), List.of(contractAddress)));
    Mockito.verify(ledger).attach(Mockito.any());
    connectedClient.close();
    Mockito.verify(ledger).detach(Mockito.any());
  }

  @Test
  public void updateRemoveNotListening() {
    List<Packet<?>> messages = new ArrayList<>();
    MyConnection connection = new MyConnection();
    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(null, List.of()));
    // Trigger initial
    connectedClient.newFinalState(
        blockchain.getChainState(),
        new BlockchainLedger.UpdateEvent(List.of(), List.of(), List.of()));
    connectedClient.newFinalState(
        blockchain.getChainState(),
        new BlockchainLedger.UpdateEvent(
            List.of(), List.of(contractAddress), List.of(contractAddress)));
    Assertions.assertThat(messages).hasSize(0);
  }

  @Test
  public void createInitialNotListening() throws Exception {
    MyConnection connection = new MyConnection(1);

    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(null, List.of()));
    CountDownLatch done = triggerCreateEvent(connectedClient);
    Assertions.assertThat(connection.latch.await(1, TimeUnit.SECONDS)).isTrue();
    connectedClient.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_CREATED_RESPONSE,
            new ContractCreatedResponse(contractAddress, false)));
    Assertions.assertThat(done.await(1, TimeUnit.SECONDS)).isTrue();
    connectedClient.newFinalState(
        blockchain.latest().getState(),
        new BlockchainLedger.UpdateEvent(List.of(), List.of(contractAddress), List.of()));
    Assertions.assertThat(connection.messages).hasSize(1);
  }

  private CountDownLatch triggerCreateEvent(ConnectedClient connectedClient) {
    CountDownLatch done = new CountDownLatch(1);
    register(
            new Thread(
                () -> {
                  connectedClient.newFinalState(
                      blockchain.latest().getState(),
                      new BlockchainLedger.UpdateEvent(
                          List.of(contractAddress), List.of(), List.of()));
                  done.countDown();
                }))
        .start();
    return done;
  }

  @Test
  public void updateAndRemoveInitial() {
    MyConnection connection = new MyConnection(1);

    ConnectedClient connectedClient =
        new ConnectedClient(
            connection,
            blockchain,
            new Handshake(
                null,
                List.of(
                    contractAddress,
                    BlockchainAddress.fromHash(
                        BlockchainAddress.Type.CONTRACT_PUBLIC, TestObjects.EMPTY_HASH))));
    connectedClient.newFinalState(
        blockchain.latest().getState(),
        new BlockchainLedger.UpdateEvent(
            List.of(contractAddress), List.of(contractAddress), List.of()));
    Assertions.assertThat(connection.messages).hasSize(2);
    Assertions.assertThat(connection.messages.get(1).getType())
        .isEqualTo(Packet.Type.CONTRACT_UPDATED);
  }

  @Test
  public void binderNotEnabled() throws Exception {
    MyConnection connection = new MyConnection(1);

    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(List.of(), List.of()));
    CountDownLatch done = triggerCreateEvent(connectedClient);
    Assertions.assertThat(done.await(1, TimeUnit.SECONDS)).isTrue();
  }

  @Test
  public void isClosed() {
    MyConnection connection = new MyConnection(1);

    ConnectedClient connectedClient =
        new ConnectedClient(connection, blockchain, new Handshake(List.of(), List.of()));
    Assertions.assertThat(connectedClient.isClosed()).isFalse();
    connection.close();
    Assertions.assertThat(connectedClient.isClosed()).isTrue();
  }

  @Test
  public void serverRespectsContractListFromClient() throws InterruptedException {
    MyConnection connection = new MyConnection(1);
    List<BlockchainAddress> contractsToUpdateOn = List.of(contractAddress);

    ConnectedClient connectedClient =
        new ConnectedClient(
            connection, blockchain, new Handshake(null, List.of(), contractsToUpdateOn));

    final CountDownLatch done = new CountDownLatch(1);

    register(
            new Thread(
                () -> {
                  connectedClient.newFinalState(
                      blockchain.latest().getState(),
                      new BlockchainLedger.UpdateEvent(
                          List.of(contractAddress, contractAddress2), List.of(), List.of()));
                  done.countDown();
                }))
        .start();
    assertAwait(connection.latch);
    connectedClient.incoming(
        new Packet<>(
            Packet.Type.CONTRACT_CREATED_RESPONSE,
            new ContractCreatedResponse(contractAddress, true)));
    assertAwait(done);

    connectedClient.newFinalState(
        blockchain.latest().getState(),
        new BlockchainLedger.UpdateEvent(List.of(), List.of(contractAddress2), List.of()));
    Assertions.assertThat(connection.messages).hasSize(1);

    connectedClient.newFinalState(
        blockchain.latest().getState(),
        new BlockchainLedger.UpdateEvent(
            List.of(), List.of(contractAddress), List.of(contractAddress)));
    Assertions.assertThat(connection.messages).hasSize(3);

    connectedClient.newFinalState(
        blockchain.latest().getState(),
        new BlockchainLedger.UpdateEvent(List.of(), List.of(), List.of(contractAddress2)));
    Assertions.assertThat(connection.messages).hasSize(3);
  }

  private void assertAwait(CountDownLatch latch) throws InterruptedException {
    Assertions.assertThat(latch.await(1, TimeUnit.SECONDS)).isTrue();
  }

  private static final class MyConnection implements ClosableConnection {

    private final List<Packet<?>> messages = new ArrayList<>();
    private final CountDownLatch latch;
    private boolean closed;

    public MyConnection() {
      this(0);
    }

    public MyConnection(int count) {
      this.latch = new CountDownLatch(count);
    }

    @Override
    public void close() {
      closed = true;
    }

    @Override
    public void send(Packet<?> packet) {
      SafeDataOutputStream.serialize(packet::send);
      messages.add(packet);
      this.latch.countDown();
    }

    @Override
    public boolean isAlive() {
      return !closed;
    }
  }
}
