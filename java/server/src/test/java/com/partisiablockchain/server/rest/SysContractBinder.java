package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderEventGroup;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.CallResult;
import com.partisiablockchain.binder.CallReturnValue;
import com.partisiablockchain.binder.sys.BinderDeploy;
import com.partisiablockchain.binder.sys.SysBinderContext;
import com.partisiablockchain.binder.sys.SysBinderContract;
import com.partisiablockchain.binder.sys.SystemInteraction;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEvent;
import com.partisiablockchain.contract.ContractEventGroup;
import com.partisiablockchain.contract.ContractEventInteraction;
import com.partisiablockchain.contract.EventCreatorImpl;
import com.partisiablockchain.contract.EventManagerImpl;
import com.partisiablockchain.contract.InteractionBuilder;
import com.partisiablockchain.contract.sys.ContractEventDeploy;
import com.partisiablockchain.contract.sys.Deploy;
import com.partisiablockchain.contract.sys.DeployBuilder;
import com.partisiablockchain.contract.sys.DeployBuilderImpl;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.Governance;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Default binder contract for binding system contracts with the blockchain.
 *
 * @param <OpenT> the state type of the contract
 */
public final class SysContractBinder<OpenT extends StateSerializable>
    implements SysBinderContract<OpenT> {

  private final SysContract<OpenT> contract;

  /**
   * Construct a new binder.
   *
   * @param contract the associated contract
   */
  public SysContractBinder(SysContract<OpenT> contract) {
    this.contract = contract;
  }

  @SuppressWarnings("unchecked")
  @Override
  public Class<OpenT> getStateClass() {
    Type genericSuperclass = contract.getClass().getGenericSuperclass();
    Type[] typeArguments = ((ParameterizedType) genericSuperclass).getActualTypeArguments();
    return (Class<OpenT>) typeArguments[0];
  }

  @Override
  public OpenT upgrade(StateAccessor oldState, byte[] rpc) {
    return contract.onUpgrade(oldState, SafeDataInputStream.createFromBytes(rpc));
  }

  private BinderResult<OpenT, BinderEvent> result(
      SysContractContextImpl contractContext, OpenT state) {
    return new SysBinderResultImpl(contractContext, state);
  }

  @Override
  public BinderResult<OpenT, BinderEvent> create(SysBinderContext context, byte[] rpc) {
    SysContractContextImpl contractContext = new SysContractContextImpl(context);
    return SafeDataInputStream.readFully(
        rpc, stream -> result(contractContext, contract.onCreate(contractContext, stream)));
  }

  @Override
  public BinderResult<OpenT, BinderEvent> invoke(
      SysBinderContext context, OpenT state, byte[] rpc) {
    SysContractContextImpl contractContext = new SysContractContextImpl(context);
    return SafeDataInputStream.readFully(
        rpc, stream -> result(contractContext, contract.onInvoke(contractContext, state, stream)));
  }

  @Override
  public BinderResult<OpenT, BinderEvent> callback(
      SysBinderContext context, OpenT state, CallbackContext callbackContext, byte[] rpc) {
    SysContractContextImpl contractContext = new SysContractContextImpl(context);
    return SafeDataInputStream.readFully(
        rpc,
        stream ->
            result(
                contractContext,
                contract.onCallback(contractContext, state, callbackContext, stream)));
  }

  static final class SysContractContextImpl implements SysContractContext {

    private final SysBinderContext context;
    private final List<ContractEvent> invocations;
    private byte[] result;
    private ContractEventGroup remoteCalls;

    SysContractContextImpl(SysBinderContext context) {
      this.context = context;
      this.invocations = new ArrayList<>();
    }

    List<BinderEvent> getInvocations() {
      return invocations.stream().map(this::convert).toList();
    }

    private BinderEvent convert(ContractEvent event) {
      if (event instanceof ContractEventInteraction interaction) {
        return new BinderInteraction(
            interaction.contract,
            bytes(interaction.rpc),
            interaction.originalSender,
            interaction.allocatedCost,
            interaction.costFromContract);
      } else if (event instanceof ContractEventDeploy deployEvent) {
        Deploy deploy = deployEvent.deploy;
        return new BinderDeploy(
            deploy.contract,
            deploy.binderJar,
            deploy.contractJar,
            deploy.abi,
            bytes(deploy.rpc),
            deployEvent.originalSender,
            deployEvent.allocatedCost);
      } else /* System event */ {
        return ((SystemEvent) event).interaction;
      }
    }

    private byte[] bytes(DataStreamSerializable rpc) {
      if (rpc != null) {
        return SafeDataOutputStream.serialize(rpc);
      } else {
        return null;
      }
    }

    CallResult getRemoteCalls() {
      if (remoteCalls == null) {
        if (result != null) {
          return new CallReturnValue(result);
        } else {
          return null;
        }
      }
      List<BinderEvent> binderInteractions =
          remoteCalls.contractEvents.stream().map(this::convert).toList();
      if (remoteCalls.callbackRpc != null) {
        return new BinderEventGroup<>(
            remoteCalls.callbackRpc,
            remoteCalls.callbackCost,
            remoteCalls.callbackCostFromContract,
            binderInteractions);
      } else {
        return null;
      }
    }

    @Override
    public BlockchainAddress getContractAddress() {
      return context.getContractAddress();
    }

    @Override
    public SystemEventCreator getInvocationCreator() {
      return new SystemEventCreatorImpl(invocations);
    }

    @Override
    public SystemEventManager getRemoteCallsCreator() {
      remoteCalls = new ContractEventGroup();
      return new SystemEventManagerImpl(remoteCalls);
    }

    @Override
    public void setResult(DataStreamSerializable result) {
      this.result = SafeDataOutputStream.serialize(result);
    }

    @Override
    public long getBlockTime() {
      return context.getBlockTime();
    }

    @Override
    public long getBlockProductionTime() {
      return context.getBlockProductionTime();
    }

    @Override
    public BlockchainAddress getFrom() {
      return context.getFrom();
    }

    @Override
    public Hash getCurrentTransactionHash() {
      return context.getCurrentTransactionHash();
    }

    @Override
    public Hash getOriginalTransactionHash() {
      return context.getOriginalTransactionHash();
    }

    @Override
    public StateSerializable getGlobalAccountPluginState() {
      return context.getGlobalAccountPluginState();
    }

    @Override
    public PluginInteractionCreator getAccountPluginInteractions() {
      return context.getAccountPluginInteractions();
    }

    @Override
    public StateSerializable getGlobalConsensusPluginState() {
      return context.getGlobalConsensusPluginState();
    }

    @Override
    public PluginInteractionCreator getConsensusPluginInteractions() {
      return context.getConsensusPluginInteractions();
    }

    @Override
    public Governance getGovernance() {
      return context.getGovernance();
    }

    @Override
    public String getFeature(String key) {
      return context.getFeature(key);
    }

    @Override
    public void registerDeductedByocFees(
        Unsigned256 amount, String symbol, FixedList<BlockchainAddress> nodes) {
      context.registerDeductedByocFees(amount, symbol, nodes);
    }

    @Override
    public void payServiceFees(long gas, BlockchainAddress target) {
      context.payServiceFees(gas, target);
    }

    @Override
    public void payInfrastructureFees(long gas, BlockchainAddress target) {
      context.payInfrastructureFees(gas, target);
    }
  }

  private final class SysBinderResultImpl implements BinderResult<OpenT, BinderEvent> {

    private final OpenT openT;
    private final List<BinderEvent> invocations;
    private final CallResult callResult;

    SysBinderResultImpl(SysContractContextImpl contractContext, OpenT openT) {
      this.openT = openT;
      this.invocations = contractContext.getInvocations();
      this.callResult = contractContext.getRemoteCalls();
    }

    @Override
    public OpenT getState() {
      return openT;
    }

    @Override
    public List<BinderEvent> getInvocations() {
      return invocations;
    }

    @Override
    public CallResult getCallResult() {
      return callResult;
    }
  }

  abstract static class SystemEventCreatorAbstract implements SystemEventCreator {

    private final List<ContractEvent> invocations;
    private final EventCreatorImpl eventCreator;

    public SystemEventCreatorAbstract(List<ContractEvent> invocations) {
      this.invocations = invocations;
      this.eventCreator = new EventCreatorImpl(invocations::add);
    }

    private void addSystemEvent(SystemInteraction e) {
      invocations.add(new SystemEvent(e));
    }

    @Override
    public void createAccount(BlockchainAddress address) {
      SystemInteraction createAccount = new SystemInteraction.CreateAccount(address);
      addSystemEvent(createAccount);
    }

    @Override
    public void createShard(String shardId) {
      addSystemEvent(new SystemInteraction.CreateShard(shardId));
    }

    @Override
    public void removeShard(String shardId) {
      addSystemEvent(new SystemInteraction.RemoveShard(shardId));
    }

    @Override
    public void updateLocalAccountPluginState(LocalPluginStateUpdate update) {
      addSystemEvent(new SystemInteraction.UpdateLocalAccountPluginState(update));
    }

    @Override
    public void updateContextFreeAccountPluginState(String shardId, byte[] rpc) {
      addSystemEvent(new SystemInteraction.UpdateContextFreeAccountPluginState(shardId, rpc));
    }

    @Override
    public void updateGlobalAccountPluginState(GlobalPluginStateUpdate update) {
      addSystemEvent(new SystemInteraction.UpdateGlobalAccountPluginState(update));
    }

    @Override
    public void updateAccountPlugin(byte[] pluginJar, byte[] rpc) {
      addSystemEvent(new SystemInteraction.UpdateAccountPlugin(pluginJar, rpc));
    }

    @Override
    public void updateLocalConsensusPluginState(LocalPluginStateUpdate update) {
      addSystemEvent(new SystemInteraction.UpdateLocalConsensusPluginState(update));
    }

    @Override
    public void updateGlobalConsensusPluginState(GlobalPluginStateUpdate update) {
      addSystemEvent(new SystemInteraction.UpdateGlobalConsensusPluginState(update));
    }

    @Override
    public void updateConsensusPlugin(byte[] pluginJar, byte[] rpc) {
      addSystemEvent(new SystemInteraction.UpdateConsensusPlugin(pluginJar, rpc));
    }

    @Override
    public void updateRoutingPlugin(byte[] pluginJar, byte[] rpc) {
      addSystemEvent(new SystemInteraction.UpdateRoutingPlugin(pluginJar, rpc));
    }

    @Override
    public DeployBuilder deployContract(BlockchainAddress contract) {
      return new DeployBuilderImpl(invocations::add, contract);
    }

    @Override
    public void removeContract(BlockchainAddress address) {
      addSystemEvent(new SystemInteraction.RemoveContract(address));
    }

    @Override
    public void exists(BlockchainAddress address) {
      addSystemEvent(new SystemInteraction.Exists(address));
    }

    @Override
    public void setFeature(String key, String value) {
      addSystemEvent(new SystemInteraction.SetFeature(key, value));
    }

    @Override
    public void upgradeSystemContract(
        byte[] contractJar, byte[] binderJar, byte[] rpc, BlockchainAddress contractAddress) {
      upgradeSystemContract(contractJar, binderJar, new byte[0], rpc, contractAddress);
    }

    @Override
    public void upgradeSystemContract(
        byte[] contractJar,
        byte[] binderJar,
        byte[] abi,
        byte[] rpc,
        BlockchainAddress contractAddress) {
      addSystemEvent(
          new SystemInteraction.UpgradeSystemContract(
              contractJar, binderJar, binderJar, abi, contractAddress));
    }

    @Override
    public InteractionBuilder invoke(BlockchainAddress contract) {
      return eventCreator.invoke(contract);
    }
  }

  static final class SystemEventCreatorImpl extends SystemEventCreatorAbstract {

    SystemEventCreatorImpl(List<ContractEvent> invocations) {
      super(invocations);
    }
  }

  static final class SystemEventManagerImpl extends SystemEventCreatorAbstract
      implements SystemEventManager {

    private final EventManagerImpl eventManager;

    public SystemEventManagerImpl(ContractEventGroup eventGroup) {
      super(eventGroup.contractEvents);
      this.eventManager = new EventManagerImpl(eventGroup);
    }

    @Override
    public void registerCallback(DataStreamSerializable rpc, long allocatedCost) {
      eventManager.registerCallback(rpc, allocatedCost);
    }

    @Override
    public void registerCallbackWithCostFromRemaining(DataStreamSerializable rpc) {
      eventManager.registerCallbackWithCostFromRemaining(rpc);
    }

    @Override
    public void registerCallbackWithCostFromContract(
        DataStreamSerializable rpc, long allocatedCost) {
      eventManager.registerCallbackWithCostFromContract(rpc, allocatedCost);
    }
  }

  static final class SystemEvent implements ContractEvent {

    private final SystemInteraction interaction;

    SystemEvent(SystemInteraction interaction) {
      this.interaction = interaction;
    }
  }
}
