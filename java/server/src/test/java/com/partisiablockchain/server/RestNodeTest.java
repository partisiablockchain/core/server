package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.server.ServerModule.ServerConfig;
import com.partisiablockchain.server.config.ApiConfig;
import com.partisiablockchain.server.rest.RestNode;
import com.partisiablockchain.server.rest.resources.BlockchainResource;
import com.partisiablockchain.server.rest.resources.FeatureResource;
import com.partisiablockchain.server.rest.resources.HealthResource;
import com.partisiablockchain.server.rest.resources.MetricsResource;
import com.partisiablockchain.storage.MemoryStorage;
import java.io.File;
import java.io.IOException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;

/** Test. */
public final class RestNodeTest {

  @TempDir private File tmp;

  @Test
  public void registerEnabled() throws IOException {
    ApiConfig config = new ApiConfig();
    config.enabled = true;
    final BlockchainLedger blockchain = Mockito.mock(BlockchainLedger.class, Mockito.RETURNS_MOCKS);
    ServerConfig resourceConfig = Mockito.mock(ServerConfig.class);
    RestNode restNode = new RestNode();
    restNode.register(MemoryStorage.createRootDirectory(tmp), blockchain, resourceConfig, config);

    Mockito.verify(resourceConfig).registerRestComponent(Mockito.any(HealthResource.class));
    Mockito.verify(resourceConfig).registerRestComponent(Mockito.any(FeatureResource.class));
    Mockito.verify(resourceConfig).registerRestComponent(Mockito.any(BlockchainResource.class));
    Mockito.verify(resourceConfig).registerRestComponent(Mockito.any(MetricsResource.class));
  }

  @Test
  public void registerDisabled() throws IOException {
    ApiConfig config = new ApiConfig();
    config.enabled = false;
    BlockchainLedger blockchain = Mockito.mock(BlockchainLedger.class);
    ServerConfig resourceConfig = Mockito.mock(ServerConfig.class);
    RestNode restNode = new RestNode();
    restNode.register(MemoryStorage.createRootDirectory(tmp), blockchain, resourceConfig, config);

    Mockito.verifyNoMoreInteractions(resourceConfig);
  }

  @Test
  public void configType() {
    Assertions.assertThat(new RestNode().configType()).isEqualTo(ApiConfig.class);
  }
}
