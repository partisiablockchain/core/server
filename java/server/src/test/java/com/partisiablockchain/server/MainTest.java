package com.partisiablockchain.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.ObjectCreator;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainTestHelper;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.flooding.NetworkConfig;
import com.partisiablockchain.flooding.NetworkConfigAccess;
import com.partisiablockchain.kademlia.Kademlia;
import com.partisiablockchain.kademlia.KademliaKey;
import com.partisiablockchain.kademlia.KademliaNode;
import com.partisiablockchain.server.Main.CollectingServerConfig;
import com.partisiablockchain.server.Main.KnownPeers;
import com.partisiablockchain.server.config.ApiConfig;
import com.partisiablockchain.server.config.KademliaConfig;
import com.partisiablockchain.server.config.KademliaConfig.TargetConfig;
import com.partisiablockchain.server.config.NodeConfig;
import com.partisiablockchain.server.config.NodeConfig.ModuleConfig;
import com.partisiablockchain.server.config.NodeConfigTest;
import com.partisiablockchain.server.rest.ExceptionToResponse;
import com.partisiablockchain.server.rest.RestNode;
import com.partisiablockchain.server.rest.resources.BlockchainResource;
import com.partisiablockchain.server.tcp.server.TcpModule;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.coverage.ThrowingRunnable;
import com.secata.tools.rest.ObjectMapperProvider;
import com.secata.tools.rest.RestServer;
import com.secata.tools.rest.RootResource;
import com.secata.tools.thread.ShutdownHook;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.core.Response;
import java.io.File;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.time.Duration;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.assertj.core.api.Assertions;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mockito;

/** Test of {@link Main}. */
public final class MainTest extends CloseableTest {

  private static final String edgeKademlia = "QWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWE=";
  private final int producerFlooding = ObjectCreator.port();
  private final ShutdownHook shutdownHook = new ShutdownHook();
  private boolean failed = false;

  @TempDir private File temp;

  @Test
  public void runWithBlockchainRemovedMutation() throws Exception {
    ThrowingRunnable wrapped =
        () ->
            Main.main(
                new String[] {
                  nodeConfigToPath(edgeConfig()),
                  defaultGenesis(),
                  temp.getAbsolutePath(),
                  createGenesisFile("ServerUnitTestSuite")
                });

    assertVoidMethodMutation(wrapped);
  }

  @Test
  public void initializeBlockchain() throws Exception {
    NodeConfig nodeConfig = createNodeConfig(0, null, null, new KademliaConfig(null, null));
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    String chainId = UUID.randomUUID().toString();
    BlockchainLedger blockchainLedger =
        Main.initializeBlockchain(
            MemoryStorage.createRootDirectory(newFolder()),
            chainId,
            createGenesisFile(chainId),
            nodeConfig,
            serverConfig);
    assertThat(blockchainLedger).isNotNull();
    assertThat(serverConfig.closeable).hasSize(2);
    assertThat(serverConfig.closeable).contains(blockchainLedger);
    assertThat(blockchainLedger.getChainId()).isEqualTo(chainId);
    for (AutoCloseable autoCloseable : serverConfig.closeable) {
      autoCloseable.close();
    }
  }

  @Test
  public void initializeBlockchainWithoutKademlia() throws Exception {
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    String chainId = UUID.randomUUID().toString();
    BlockchainLedger blockchainLedger =
        Main.initializeBlockchain(
            MemoryStorage.createRootDirectory(newFolder()),
            chainId,
            createGenesisFile(chainId),
            createNodeConfig(),
            serverConfig);
    assertThat(blockchainLedger).isNotNull();
    assertThat(serverConfig.closeable).hasSize(1);
    assertThat(serverConfig.closeable).contains(blockchainLedger);
    assertThat(blockchainLedger.getChainId()).isEqualTo(chainId);
    for (AutoCloseable autoCloseable : serverConfig.closeable) {
      autoCloseable.close();
    }
  }

  @Test
  public void initializeBlockchainShard() throws Exception {
    int tcpPort = ObjectCreator.port();
    runMasterWithRest(tcpPort);

    CollectingServerConfig serverConfig = new CollectingServerConfig();
    String chainId = UUID.randomUUID().toString();
    NodeConfig nodeConfig = createNodeConfig();
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(newFolder());
    BlockchainLedger blockchainLedger =
        Main.initializeBlockchain(
            rootDirectory,
            chainId,
            createGenesisFile("ServerUnitTestSuite"),
            nodeConfig,
            serverConfig);
    assertThat(blockchainLedger).isNotNull();
    assertThat(serverConfig.closeable).hasSize(1);
    assertThat(serverConfig.closeable).contains(blockchainLedger);
    assertThat(blockchainLedger.getChainId()).isEqualTo("ServerUnitTestSuite");
    for (AutoCloseable autoCloseable : serverConfig.closeable) {
      autoCloseable.close();
    }
  }

  private String createGenesisFile(String chainId) {
    return BlockchainTestHelper.createDefaultGenesis(chainId);
  }

  @Test
  public void createNetworkConfigWithoutPersistentPeer() {
    NodeConfig nodeConfig = createNodeConfig(0, null, List.of("localhost:7777"));
    NetworkConfig networkConfig = Main.createNetworkConfig(nodeConfig, null);
    assertThat(NetworkConfigAccess.getPersistent(networkConfig)).isNull();
    assertThat(NetworkConfigAccess.getSupplier(networkConfig).get())
        .isEqualTo(new Address("localhost", 7777));
  }

  @Test
  public void createNetworkConfigWithPersistentPeer() {
    NodeConfig nodeConfig = createNodeConfig(0, "localhost:8888", List.of("localhost:7777"));
    NetworkConfig networkConfig = Main.createNetworkConfig(nodeConfig, null);
    assertThat(NetworkConfigAccess.getPersistent(networkConfig)).isNotNull();
    assertThat(NetworkConfigAccess.getSupplier(networkConfig).get())
        .isEqualTo(new Address("localhost", 7777));
  }

  @Test
  public void createNetworkConfigWithKademlia() throws UnknownHostException {
    NodeConfig nodeConfig = createNodeConfig();
    InetAddress localhost = InetAddress.getByName("localhost");
    List<KademliaNode> knownKademlias =
        List.of(new KademliaNode(new KademliaKey(), localhost, 7777));
    NetworkConfig networkConfig =
        Main.createNetworkConfig(
            nodeConfig, new Kademlia(new KademliaKey(), ObjectCreator.port(), knownKademlias));
    assertThat(NetworkConfigAccess.getPersistent(networkConfig)).isNull();
    assertThat(NetworkConfigAccess.getSupplier(networkConfig).get())
        .isEqualTo(new Address(localhost.getHostAddress(), 7777));
  }

  @Test
  public void createNetworkConfigWithKnownPeers() {
    NodeConfig nodeConfig = createNodeConfig(0, null, List.of("localhost:7777"));
    NetworkConfig networkConfig = Main.createNetworkConfig(nodeConfig, null);
    assertThat(NetworkConfigAccess.getPersistent(networkConfig)).isNull();
    assertThat(NetworkConfigAccess.getSupplier(networkConfig).get())
        .isEqualTo(new Address("localhost", 7777));
  }

  @Test
  public void noRest() {
    BlockchainTestHelper blockchainTestHelper =
        new BlockchainTestHelper(newFolder(), this::register);
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    assertThat(serverConfig.hasRest()).isFalse();
    assertThat(Main.restServer(serverConfig, blockchainTestHelper.blockchain, 1000)).isNull();
  }

  @Test
  public void withRest() {
    BlockchainTestHelper blockchainTestHelper =
        new BlockchainTestHelper(newFolder(), this::register);
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    serverConfig.registerRestComponent(new BlockchainResource(blockchainTestHelper.blockchain));
    assertThat(serverConfig.hasRest()).isTrue();
    assertThat(Main.restServer(serverConfig, blockchainTestHelper.blockchain, 1000))
        .isInstanceOf(RestServer.class);
    assertThat(serverConfig.alive).hasSize(1);
  }

  @Test
  public void startWithRest() throws InterruptedException {
    runMasterWithRest(ObjectCreator.port());
  }

  private void runMasterWithRest(int tcpPort) throws InterruptedException {

    TcpModule.TcpModuleConfig config = new TcpModule.TcpModuleConfig();

    config.port = tcpPort;
    List<ModuleConfig> modules =
        List.of(
            createModule(TcpModule.class, config), createModule(RestNode.class, new ApiConfig()));
    NodeConfig nodeConfig = createNodeConfig(0, null, List.of(), null, modules);
    start(nodeConfig);
    waitForNode(nodeConfig.restPort(), "", Response.Status.NO_CONTENT);
  }

  @Test
  public void createConfigForBlockchainNodeComponentClass() {
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    serverConfig.registerRestComponent(BlockchainResource.class);
    ResourceConfig config = Main.createConfigForBlockchainNode(serverConfig);
    assertThat(config.isRegistered(BlockchainResource.class)).isTrue();
    assertThat(config.isRegistered(ExceptionToResponse.class)).isTrue();
  }

  @Test
  public void createConfigForBlockchainNodeComponentObject() {
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    final BlockchainLedger blockchainLedger =
        Mockito.mock(BlockchainLedger.class, Mockito.RETURNS_MOCKS);
    BlockchainResource registered = new BlockchainResource(blockchainLedger);
    serverConfig.registerRestComponent(registered);
    ResourceConfig config = Main.createConfigForBlockchainNode(serverConfig);
    assertThat(config.isRegistered(registered)).isTrue();
  }

  @Test
  public void createContractListener() {
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    serverConfig.addCreateListener((address, coreState, contractState) -> null);
    assertThat(serverConfig.createContractListeners).hasSize(1);
  }

  @Test
  public void withDisabledModule() {
    FailingModuleConfig withDisabled = new FailingModuleConfig();
    withDisabled.enabled = false;
    Main.configureServerModule(
        new FailingModule(),
        new ObjectMapper().valueToTree(withDisabled),
        MemoryStorage.createRootDirectory(newFolder()),
        new CollectingServerConfig(),
        Mockito.mock(BlockchainLedger.class));

    FailingModuleConfig withEnabled = new FailingModuleConfig();
    withEnabled.enabled = true;
    Assertions.assertThatThrownBy(
            () ->
                Main.configureServerModule(
                    new FailingModule(),
                    new ObjectMapper().valueToTree(withEnabled),
                    MemoryStorage.createRootDirectory(newFolder()),
                    new CollectingServerConfig(),
                    Mockito.mock(BlockchainLedger.class)))
        .hasMessageContaining("Expected");
  }

  private <T extends ServerModuleConfigDto> ModuleConfig createModule(
      Class<? extends ServerModule<T>> restNodeClass, T moduleConfig) {
    return new NodeConfig.ModuleConfig(
        restNodeClass.getName(), new ObjectMapper().valueToTree(moduleConfig));
  }

  @Test
  public void emptySuppliers() {
    Supplier<Address> addressSupplier = Main.joinSuppliers(List.of());
    assertThat(addressSupplier).isNotNull();
    assertThat(addressSupplier.get()).isNull();
  }

  @Test
  public void nonEmptySuppliers() {
    Address expected = new Address("localhost", 1337);
    Supplier<Address> addressSupplier = Main.joinSuppliers(List.of(() -> expected));
    assertThat(addressSupplier).isNotNull();
    assertThat(addressSupplier.get()).isEqualTo(expected);
  }

  @Test
  public void peersShouldBeSuppliedFromRandomIndex() {
    List<Address> addresses =
        List.of(
            new Address("localhost", 7777),
            new Address("localhost", 8888),
            new Address("localhost", 1888),
            new Address("localhost", 2888),
            new Address("localhost", 3888),
            new Address("localhost", 4888));
    Address first = KnownPeers.create(addresses).get();

    List<Address> allFirsts =
        IntStream.range(0, 100)
            .mapToObj(i -> KnownPeers.create(addresses).get())
            .collect(Collectors.toList());

    assertThat(allFirsts).anyMatch(Predicate.not(first::equals));
  }

  @Test
  public void producersShouldBeSuppliedInOrder() {
    List<Address> addresses =
        List.of(
            new Address("localhost", 7777),
            new Address("localhost", 8888),
            new Address("localhost", 1888),
            new Address("localhost", 2888),
            new Address("localhost", 3888),
            new Address("localhost", 4888));
    Supplier<Address> producers = KnownPeers.create(addresses);
    Address first = producers.get();
    Address second = producers.get();

    assertThat(addresses.get((addresses.indexOf(first) + 1) % 6)).isEqualTo(second);
  }

  @Test
  public void nullSupplier() {
    assertThat(FunctionUtility.<Address>nullSupplier().get()).isNull();
  }

  @Test
  public void startAppCalledWithNull() {
    Main.startApp(null, List.of(() -> {}), shutdownHook);
    assertThat(shutdownHook.closeables()).hasSize(1);
  }

  @Test
  public void closeResourcesAfter() throws Exception {
    int restPort = ObjectCreator.port();
    AtomicBoolean closed = new AtomicBoolean(false);
    Thread thread =
        new Thread(
            () -> {
              ResourceConfig resourceConfig = new ResourceConfig();
              resourceConfig.register(RootResource.class);
              RestServer restServer = new RestServer(restPort, resourceConfig);
              Main.startApp(restServer, List.of(() -> closed.set(true)), shutdownHook);
            });
    thread.start();
    waitForNode(restPort, "", Response.Status.NO_CONTENT);
    assertThat(shutdownHook.closeables()).hasSize(2);
    shutdownHook
        .closeables()
        .forEach(autoCloseable -> ExceptionConverter.run(autoCloseable::close, "Error"));
    assertThat(closed.get()).isTrue();
  }

  @Test
  public void recentProductionTime() {
    assertThat(Main.isRecentProductionTime(0)).isFalse();

    long boundary = Duration.ofSeconds(150).toMillis();
    int productionTime = 1_000_000;
    assertThat(Main.isRecentProductionTime(productionTime, () -> productionTime + 1)).isTrue();
    assertThat(Main.isRecentProductionTime(productionTime, () -> productionTime - 1)).isTrue();
    assertThat(Main.isRecentProductionTime(productionTime, () -> productionTime + boundary - 1))
        .isTrue();
    assertThat(Main.isRecentProductionTime(productionTime, () -> productionTime + boundary))
        .isFalse();
  }

  @Test
  public void checkLatestBlock() {
    BlockchainTestHelper blockchainTestHelper =
        new BlockchainTestHelper(newFolder(), this::register);
    BooleanSupplier booleanSupplier = Main.checkLatestBlock(blockchainTestHelper.blockchain);
    assertThat(booleanSupplier.getAsBoolean()).isFalse();
    blockchainTestHelper.appendBlock(new KeyPair(BigInteger.ONE), List.of());
    assertThat(booleanSupplier.getAsBoolean()).isTrue();
  }

  @Test
  public void configureModulesWithListener() throws InterruptedException {
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    CountDownLatch countDownLatch = new CountDownLatch(1);
    serverConfig.addCreateListener(
        (address, coreState, contractState) -> {
          assertThat(address).isEqualTo(ContractDispatcherTest.contractAddress);
          countDownLatch.countDown();
          return null;
        });
    BlockchainTestHelper blockchainTestHelper =
        new BlockchainTestHelper(newFolder(), this::register);
    Main.configureModules(
        MemoryStorage.createRootDirectory(newFolder()),
        serverConfig,
        blockchainTestHelper.blockchain,
        List.of());
    KeyPair producerKey = new KeyPair(BigInteger.ONE);
    SignedTransaction deployTransaction =
        ContractDispatcherTest.createDeployTransaction(
            blockchainTestHelper.blockchain.getChainId(), producerKey);
    blockchainTestHelper.appendBlock(producerKey, List.of(deployTransaction));
    assertThat(blockchainTestHelper.blockchain.getLatestBlock().getTransactions()).hasSize(1);
    assertThat(blockchainTestHelper.blockchain.getLatestBlock().getEventTransactions()).isEmpty();
    blockchainTestHelper.appendBlock(producerKey, List.of());
    assertThat(blockchainTestHelper.blockchain.getLatestBlock().getTransactions()).isEmpty();
    assertThat(blockchainTestHelper.blockchain.getLatestBlock().getEventTransactions()).hasSize(0);
    blockchainTestHelper.appendBlock(producerKey, List.of());
    assertThat(blockchainTestHelper.blockchain.getLatestBlock().getTransactions()).isEmpty();
    assertThat(blockchainTestHelper.blockchain.getLatestBlock().getEventTransactions()).isEmpty();
    assertThat(countDownLatch.await(3, TimeUnit.SECONDS)).isTrue();
  }

  @Test
  public void serverConfigRestAlive() {
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    serverConfig.addRestAliveCheck(() -> true);
    assertThat(serverConfig.getRestAlive().check()).isEqualTo(true);
  }

  @Test
  public void serverConfigRestDead() {
    CollectingServerConfig serverConfig = new CollectingServerConfig();
    serverConfig.addRestAliveCheck(() -> false);
    assertThat(serverConfig.getRestAlive().check()).isEqualTo(false);
  }

  @Test
  public void serverConfigHasRest() {
    CollectingServerConfig withRestClass = new CollectingServerConfig();
    withRestClass.registerRestComponent(BlockchainResource.class);
    assertThat(withRestClass.hasRest()).isTrue();

    CollectingServerConfig withRestObject = new CollectingServerConfig();
    final BlockchainLedger blockchainLedger =
        Mockito.mock(BlockchainLedger.class, Mockito.RETURNS_MOCKS);
    withRestObject.registerRestComponent(new BlockchainResource(blockchainLedger));
    assertThat(withRestObject.hasRest()).isTrue();

    CollectingServerConfig withAlive = new CollectingServerConfig();
    withAlive.addRestAliveCheck(() -> true);
    assertThat(withAlive.hasRest()).isTrue();
  }

  static String defaultGenesis() throws URISyntaxException {
    return new File(Main.class.getResource("genesis.json").toURI()).getAbsolutePath();
  }

  private static void assertVoidMethodMutation(ThrowingRunnable wrapped)
      throws InterruptedException {
    AtomicBoolean value = new AtomicBoolean();
    Runnable runnable =
        () -> {
          ExceptionConverter.run(wrapped, "Stuff");
          value.set(true);
        };

    Thread thread = new Thread(runnable);
    thread.start();

    Thread.sleep(100);

    assertThat(value).isFalse();

    thread.interrupt();
    thread.join();
  }

  private static KademliaConfig createKademliaConfig(
      String kademliaKey, KademliaConfig.TargetConfig target) {
    KademliaConfig config = new KademliaConfig(kademliaKey, target);
    return config;
  }

  private static KademliaConfig.TargetConfig createKademliaTargetConfig(
      String kademliaId, String host, int port) {
    TargetConfig config = new TargetConfig(kademliaId, host, port);
    return config;
  }

  private NodeConfig createNodeConfig() {
    return createNodeConfig(ObjectCreator.port());
  }

  private NodeConfig createNodeConfig(int floodingPort) {
    return createNodeConfig(floodingPort, null);
  }

  private NodeConfig createNodeConfig(int floodingPort, String persistentConnection) {
    return createNodeConfig(floodingPort, persistentConnection, List.of());
  }

  private NodeConfig createNodeConfig(
      int floodingPort, String persistentConnection, List<String> knownPeers) {
    return createNodeConfig(floodingPort, persistentConnection, knownPeers, null);
  }

  private NodeConfig createNodeConfig(
      int floodingPort,
      String persistentConnection,
      List<String> knownPeers,
      KademliaConfig kademliaConfig) {
    return createNodeConfig(floodingPort, persistentConnection, knownPeers, kademliaConfig, null);
  }

  private NodeConfig createNodeConfig(
      int floodingPort,
      String persistentConnection,
      List<String> knownPeers,
      KademliaConfig kademliaConfig,
      List<ModuleConfig> modules) {
    return new NodeConfig(
        modules,
        kademliaConfig,
        ObjectCreator.port(),
        floodingPort,
        persistentConnection,
        knownPeers);
  }

  @SuppressWarnings("unused")
  private void start(NodeConfig config) {
    startMain(nodeConfigToPath(config));
  }

  private void startMain(String nodeConfigPath) {
    Thread thread =
        register(
            new Thread(
                () -> {
                  try {
                    Main.main(
                        new String[] {
                          nodeConfigPath,
                          defaultGenesis(),
                          newFolder().getAbsolutePath(),
                          createGenesisFile("ServerUnitTestSuite")
                        });
                  } catch (Exception e) {
                    System.out.println("Error while doing things");
                    e.printStackTrace();
                    failed = true;
                  }
                }));
    thread.start();
    register(thread);
  }

  private void waitForNode(int port, String path, Response.Status expectedStatus)
      throws InterruptedException {
    Invocation get =
        ClientBuilder.newClient()
            .target("http://localhost:" + port + "/" + path)
            .request()
            .buildGet();

    long start = System.currentTimeMillis();

    boolean waiting = true;
    while (!failed && waiting && diff(start) < 60_000) {
      try {
        Response invoke = get.invoke();
        waiting = invoke.getStatus() != expectedStatus.getStatusCode();
      } catch (Exception e) {
        System.out.println("Server not up.");
      }

      Thread.sleep(250);
    }
    assertThat(failed).isFalse();
    assertThat(waiting).isFalse();
  }

  private long diff(long start) {
    return System.currentTimeMillis() - start;
  }

  private NodeConfig edgeConfig() {
    int floodingPort = ObjectCreator.port();
    String persistentPeer = "localhost:" + producerFlooding;
    KademliaConfig kademliaConfig = createKademliaConfig(edgeKademlia, null);
    return createNodeConfig(floodingPort, persistentPeer, List.of(), kademliaConfig);
  }

  @SuppressWarnings("unused")
  private NodeConfig readerConfig() {
    NodeConfig config = createNodeConfig();
    KademliaConfig kademliaConfig =
        createKademliaConfig(
            null, createKademliaTargetConfig(edgeKademlia, "localhost", producerFlooding));
    List<ModuleConfig> modules = List.of(NodeConfigTest.restModule());
    return createNodeConfig(0, null, List.of(), kademliaConfig, modules);
  }

  /**
   * Write a NodeConfig to a temporary file and return its absolute path.
   *
   * @param config the record to write
   * @return the path to the new temporary file
   */
  private static String nodeConfigToPath(Object config) {
    return ExceptionConverter.call(
        () -> {
          ObjectMapper mapper = new ObjectMapperProvider().getContext(Object.class);
          File configFile = File.createTempFile("nodeconfig", ".json");
          configFile.deleteOnExit();
          mapper.writeValue(configFile, config);
          return configFile.getAbsolutePath();
        },
        "Unable to write config node config to file");
  }

  /** Test module. */
  public static final class FailingModule implements ServerModule<FailingModuleConfig> {

    @Override
    public void register(
        RootDirectory rootDirectory,
        BlockchainLedger blockchainLedger,
        ServerConfig serverConfig,
        FailingModuleConfig failingModuleConfig) {
      throw new RuntimeException("Expected");
    }

    @Override
    public Class<FailingModuleConfig> configType() {
      return FailingModuleConfig.class;
    }
  }

  /** Test config. */
  public static final class FailingModuleConfig extends ServerModuleConfigDto {}
}
