package com.partisiablockchain.server.model;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ServerRuntimeTest {

  @Test
  public void values() {
    long free = 9861358L;
    long total = 17869512L;
    long max = 978165L;
    int availableProcessors = 1;
    ServerRuntime serverRuntime = new ServerRuntime(free, total, max, availableProcessors);

    Assertions.assertThat(serverRuntime.freeMemory()).isEqualTo(free);
    Assertions.assertThat(serverRuntime.totalMemory()).isEqualTo(total);
    Assertions.assertThat(serverRuntime.maxMemory()).isEqualTo(max);
    Assertions.assertThat(serverRuntime.availableProcessors()).isEqualTo(availableProcessors);
  }
}
