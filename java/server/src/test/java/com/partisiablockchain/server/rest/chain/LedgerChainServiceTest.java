package com.partisiablockchain.server.rest.chain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ConsensusPluginHelper;
import com.partisiablockchain.blockchain.Features;
import com.partisiablockchain.blockchain.FeePluginHelper;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.MutableChainState;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.server.rest.ModelFactoryTest;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.NotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LedgerChainServiceTest {

  public static final int CHAIN_STATE_MINT = 127;

  private BlockchainLedger ledger;
  private BlockchainLedger shard1;

  private ChainService service;

  private static final String CHAIN_ID = "Shard1";
  private static final String SUB_CHAIN_ID = "Shard1";

  public static final BlockchainAddress TEST_MINT_CONTRACT_ADDRESS =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV,
          Hash.create(s -> s.writeString("TestMintContractAddress")));
  public static final BlockchainAddress TEST_CONTRACT_ADDRESS =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV,
          Hash.create(s -> s.writeString("TestContractAddress")));

  /**
   * Setup.
   *
   * @throws IOException exception.
   */
  @BeforeEach
  public void setup() throws IOException {
    setUpWithFeatures(List.of(Features.FEATURE_OPEN_ACCOUNT_CREATION));
  }

  private void setUpWithFeatures(List<String> features) throws IOException {
    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(
            Files.createTempDirectory("myDirectory").toFile(), new ConcurrentHashMap<>());
    ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(features),
            CHAIN_ID,
            SUB_CHAIN_ID);
    shard1 =
        BlockchainLedgerHelper.createLedger(
            rootDirectory, BlockchainLedgerHelper.createNetworkConfig(), mutable -> {}, "Shard1");
    HashMap<String, BlockchainLedger> ledgers = new HashMap<>();
    ledgers.put(null, ledger);
    ledgers.put("Shard1", shard1);
    service = new LedgerChainService(ledgers);
  }

  @Test
  public void getChainAccount() throws IOException {
    // Add mpc tokens to an account such that it appears in the account plugin.
    mintTokens();
    ChainAccount chainAccount =
        service.getChainAccount(TestObjects.KEY_PAIR_ONE.getPublic().createAddress(), null);
    assertThat(chainAccount.nonce())
        .isEqualTo(
            shard1
                .getChainState()
                .getAccount(TestObjects.KEY_PAIR_ONE.getPublic().createAddress())
                .getNonce());
    assertThat(chainAccount.shardId()).isEqualTo(CHAIN_ID);
    assertThat(chainAccount.account().isNull()).isFalse();
    assertThat(chainAccount.account().get("mpcTokens").asLong()).isEqualTo(100);
    ChainAccount nonChainAccount =
        service.getChainAccount(
            new KeyPair(BigInteger.valueOf(12345)).getPublic().createAddress(), null);
    assertThat(nonChainAccount).isNotNull();
    assertThat(nonChainAccount.nonce()).isEqualTo(1);
    // disable open account creation
    setUpWithFeatures(List.of());
    assertThatThrownBy(
            () ->
                service.getChainAccount(
                    new KeyPair(BigInteger.valueOf(12345)).getPublic().createAddress(), null))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Unable to find account '00379d09b55c7b8fbc58353bd612e223cbf3465e17'");
  }

  @Test
  public void getChainContract() {
    ChainContract testContract = service.getChainContract(TEST_CONTRACT_ADDRESS, null);
    ImmutableChainState chainState = ledger.getChainState();
    assertThat(testContract.shardId()).isEqualTo(CHAIN_ID);
    assertThat(testContract.address()).isEqualTo(TEST_CONTRACT_ADDRESS);
    CoreContractState coreContractState = chainState.getCoreContractState(TEST_CONTRACT_ADDRESS);
    StateSerializer stateSerializer = new StateSerializer(ledger.getStateStorage(), true);
    assertThat(testContract.jar()).isEqualTo(coreContractState.getContractIdentifier());
    assertThat(testContract.storageLength())
        .isEqualTo(chainState.getContractStorageLength(TEST_CONTRACT_ADDRESS));
    assertThat(testContract.abi())
        .isEqualTo(
            stateSerializer
                .read(coreContractState.getAbiIdentifier(), LargeByteArray.class)
                .getData());
    assertThat(testContract.account().get("storage").intValue()).isEqualTo(10);
    assertThat(testContract.serializedContract()).isNotNull();
    assertThat(testContract.serializedContract()).containsExactly(10, 0, 0, 0);
    assertThatThrownBy(
            () ->
                service.getChainContract(
                    BlockchainAddress.fromHash(
                        BlockchainAddress.Type.CONTRACT_GOV,
                        Hash.create(s -> s.writeString("Unknown"))),
                    null))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Unable to find contract at '04099bc6cfa05c9d52512a6675e278a67a5c87de06'");
  }

  @Test
  public void getChainState() {
    ChainState state = service.getChainState(null);
    assertThat(state.chainId()).isEqualTo(CHAIN_ID);
    ImmutableChainState chainState = ledger.getChainState();
    assertThat(state.governanceVersion()).isEqualTo(chainState.getGovernanceVersion());
    assertThat(state.shards().get(0)).isEqualTo("Shard1");
    assertThat(state.shards().size()).isEqualTo(1);
    assertThat(state.features().size()).isEqualTo(1);
    assertThat(state.features().get(0).name()).isEqualTo(Features.FEATURE_OPEN_ACCOUNT_CREATION);
    assertThat(state.features().get(0).value()).isEqualTo("OK");
  }

  @Test
  public void getChainTransaction() {
    SignedTransaction transaction = createMintForChainState(shard1, TestObjects.KEY_PAIR_ONE, 1000);
    ChainTransactionPointer transactionPointer =
        service.putChainTransaction(SafeDataOutputStream.serialize(transaction::write));
    List<SignedTransaction> pendingTransactions = shard1.getPendingTransactions();
    assertThat(pendingTransactions.size()).isEqualTo(1);
    SignedTransaction pendingTransaction = pendingTransactions.get(0);
    assertThat(pendingTransaction.identifier()).isEqualTo(transactionPointer.identifier());
    assertThat(
            shard1
                .getChainState()
                .getRoutingPlugin()
                .route(
                    shard1.getChainState().getActiveShards(),
                    pendingTransaction.getTransaction().getTargetContract()))
        .isEqualTo(transactionPointer.destinationShard());

    // Create a failing transaction
    CoreTransactionPart core =
        CoreTransactionPart.create(
            shard1.getAccountState(TestObjects.KEY_PAIR_ONE.getPublic().createAddress()).getNonce(),
            0,
            0);
    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(TestObjects.CONTRACT_MPC_TOKEN, new byte[0]);
    SignedTransaction failingTransaction =
        SignedTransaction.create(core, interact)
            .sign(TestObjects.KEY_PAIR_ONE, shard1.getChainId());
    assertThatThrownBy(
            () ->
                service.putChainTransaction(
                    SafeDataOutputStream.serialize(failingTransaction::write)))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("The received transaction is invalid in the current state of the blockchain.");
  }

  /**
   * If specifying a block time when getting account gives the account data at the specified block
   * time.
   */
  @Test
  void getAccountAtBlockTime() {
    mintTokens();
    mintTokens();
    JsonNode accountAtZero =
        service.getChainAccount(TestObjects.KEY_PAIR_ONE.getPublic().createAddress(), 0L).account();
    long mpcTokensAtTwo =
        service
            .getChainAccount(TestObjects.KEY_PAIR_ONE.getPublic().createAddress(), 2L)
            .account()
            .get("mpcTokens")
            .asLong();
    long mpcTokensNow =
        service
            .getChainAccount(TestObjects.KEY_PAIR_ONE.getPublic().createAddress(), null)
            .account()
            .get("mpcTokens")
            .asLong();
    assertThat(accountAtZero).isNull();
    assertThat(mpcTokensAtTwo).isEqualTo(100);
    assertThat(mpcTokensNow).isEqualTo(200);
  }

  private void mintTokens() {
    SignedTransaction mintForChainState =
        createMintForChainState(shard1, TestObjects.KEY_PAIR_ONE, 100);
    boolean valid = shard1.addPendingTransaction(mintForChainState);
    assertThat(valid).isTrue();
    BlockchainLedgerHelper.appendBlock(shard1, List.of(mintForChainState), List.of());
    BlockchainLedgerHelper.appendBlock(shard1);
  }

  /** Specifying a block time that does not exist throws a bad request. */
  @Test
  void illegalBlockTime() {
    assertThatThrownBy(() -> service.getChainState(500L))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("No chain state for block time '500'");
  }

  /**
   * If missing account plugin, then getAccount will return a ChainAccount with account field null.
   */
  @Test
  void missingAccountPluginGetAccount() throws Exception {
    BlockchainAddress testContractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(s -> s.writeString("TestAccountAddress")));
    LedgerChainService ledgerChainService =
        setupEmptyChain(m -> m.createAccount(testContractAddress));

    ChainAccount chainAccount = ledgerChainService.getChainAccount(testContractAddress, null);
    assertThat(chainAccount.account()).isNull();
    assertThat(chainAccount.nonce()).isEqualTo(1);
    assertThat(chainAccount.shardId()).isEqualTo("Gov");
  }

  /**
   * If missing account plugin, then getContract will return a ChainContract with account field
   * null.
   */
  @Test
  void missingAccountPluginGetContract() throws Exception {
    byte[] testChainContract = JarBuilder.buildJar(LedgerChainServiceTest.TestContract.class);
    Consumer<MutableChainState> createContract =
        mutable ->
            BlockchainLedgerHelper.createContract(
                TEST_CONTRACT_ADDRESS,
                () -> testChainContract,
                () -> new LedgerChainServiceTest.TestContractState(10),
                mutable);
    LedgerChainService ledgerChainService = setupEmptyChain(createContract);

    ChainContract chainContract = ledgerChainService.getChainContract(TEST_CONTRACT_ADDRESS, null);
    assertThat(chainContract.account()).isNull();
    assertThat(chainContract.serializedContract()).isNotNull();
  }

  /**
   * If contract has null state, then getContract will return a ChainContract with
   * serializedContract field null.
   */
  @Test
  void getContractNullState() throws Exception {
    byte[] testChainContract = JarBuilder.buildJar(LedgerChainServiceTest.TestContract.class);
    Consumer<MutableChainState> createContract =
        mutable ->
            BlockchainLedgerHelper.createContract(
                TEST_CONTRACT_ADDRESS, () -> testChainContract, () -> null, mutable);
    LedgerChainService ledgerChainService = setupEmptyChain(createContract);

    ChainContract chainAccount = ledgerChainService.getChainContract(TEST_CONTRACT_ADDRESS, null);
    assertThat(chainAccount.serializedContract()).isNull();
  }

  /**
   * If a plugin has null state, then getChainState will return that plugin with its state field
   * null.
   */
  @Test
  void pluginWithoutState() throws Exception {
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable ->
                mutable.setPlugin(
                    FunctionUtility.noOpBiConsumer(),
                    null,
                    ChainPluginType.CONSENSUS,
                    ConsensusPluginHelper.createJar(),
                    SafeDataOutputStream.serialize(
                        TestObjects.KEY_PAIR_ONE.getPublic().createAddress())));
    ChainState chainState = ledgerChainService.getChainState(null);
    assertThat(chainState.plugins().get(ChainPluginType.CONSENSUS).state()).isNull();
  }

  /**
   * If there exist an account plugin, but it does not have an entry for an address, then getAccount
   * for that address returns ChainAccount with account field null.
   */
  @Test
  void accountPluginMissingEntryForAccount() throws Exception {
    BlockchainAddress testContractAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.ACCOUNT, Hash.create(s -> s.writeString("TestAccountAddress")));
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              mutable.createAccount(testContractAddress);
              mutable.setPlugin(
                  FunctionUtility.noOpBiConsumer(),
                  null,
                  ChainPluginType.ACCOUNT,
                  FeePluginHelper.createJar(),
                  new byte[0]);
            });
    ChainAccount chainAccount = ledgerChainService.getChainAccount(testContractAddress, null);
    assertThat(chainAccount.account()).isNull();
  }

  /**
   * If there exist an account plugin, but it does not have an entry for a contract, then
   * getContract for that address returns ChainContract with account field null.
   */
  @Test
  void accountPluginMissingEntryForContract() throws Exception {
    byte[] testChainContract = JarBuilder.buildJar(LedgerChainServiceTest.TestContract.class);
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              BlockchainLedgerHelper.createContract(
                  TEST_CONTRACT_ADDRESS,
                  () -> testChainContract,
                  () -> new LedgerChainServiceTest.TestContractState(10),
                  mutable);
              mutable.setPlugin(
                  FunctionUtility.noOpBiConsumer(),
                  null,
                  ChainPluginType.ACCOUNT,
                  FeePluginHelper.createJar(),
                  new byte[0]);
            });
    ChainContract chainContract = ledgerChainService.getChainContract(TEST_CONTRACT_ADDRESS, null);
    assertThat(chainContract.account()).isNull();
  }

  /**
   * Calling getChainContractAvlValue on a contract with avl tree, gets the corresponding value to
   * the specified key.
   */
  @Test
  void getAvlValue() throws Exception {
    byte[] avlChainContract = JarBuilder.buildJar(LedgerChainServiceTest.AvlContract.class);
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              BlockchainLedgerHelper.createContract(
                  TEST_CONTRACT_ADDRESS,
                  () -> avlChainContract,
                  LedgerChainServiceTest::getAvlState,
                  mutable);
            });
    byte[] avlValue =
        ledgerChainService.getChainContractAvlValue(TEST_CONTRACT_ADDRESS, 0, new byte[] {1}, null);
    assertThat(avlValue).containsExactly(2);
    byte[] avlValue2 =
        ledgerChainService.getChainContractAvlValue(TEST_CONTRACT_ADDRESS, 0, new byte[] {3}, null);
    assertThat(avlValue2).containsExactly(4);
  }

  /**
   * Calling getChainContractAvlValue on a contract with avl tree, with a key that does not exist in
   * the tree, throws not found.
   */
  @Test
  void getAvlValueNotPresent() throws Exception {
    byte[] avlChainContract = JarBuilder.buildJar(LedgerChainServiceTest.AvlContract.class);
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              BlockchainLedgerHelper.createContract(
                  TEST_CONTRACT_ADDRESS,
                  () -> avlChainContract,
                  LedgerChainServiceTest::getAvlState,
                  mutable);
            });
    assertThatThrownBy(
            () ->
                ledgerChainService.getChainContractAvlValue(
                    TEST_CONTRACT_ADDRESS, 0, new byte[] {5}, null))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Unable to find avl value for key '05'");
  }

  /**
   * Calling getChainContractAvlValue on an address for which no contract exists, throws not found.
   */
  @Test
  void getAvlValueMissingContract() throws Exception {
    LedgerChainService ledgerChainService = setupEmptyChain(FunctionUtility.noOpConsumer());
    assertThatThrownBy(
            () ->
                ledgerChainService.getChainContractAvlValue(
                    TEST_CONTRACT_ADDRESS, 0, new byte[] {1}, null))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Unable to find contract at '040a95cea619ec443a88e333edd16de52e348b72d8'");
  }

  /**
   * Calling getAvlNextN on a contract with avl tree, gets the next entries after the specified key
   * (or the first entries if null is given).
   */
  @Test
  void getAvlNextN() throws Exception {
    byte[] avlChainContract = JarBuilder.buildJar(LedgerChainServiceTest.AvlContract.class);
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              BlockchainLedgerHelper.createContract(
                  TEST_CONTRACT_ADDRESS,
                  () -> avlChainContract,
                  LedgerChainServiceTest::getAvlState,
                  mutable);
            });
    List<Map.Entry<byte[], byte[]>> avlEntries =
        ledgerChainService.getAvlNextN(TEST_CONTRACT_ADDRESS, 0, null, 5, 0, null);
    assertThat(avlEntries.get(0).getKey()).containsExactly(1);
    assertThat(avlEntries.get(0).getValue()).containsExactly(2);
    assertThat(avlEntries.get(1).getKey()).containsExactly(3);
    assertThat(avlEntries.get(1).getValue()).containsExactly(4);
    List<Map.Entry<byte[], byte[]>> avlEntries2 =
        ledgerChainService.getAvlNextN(TEST_CONTRACT_ADDRESS, 0, new byte[] {1}, 5, 0, null);
    assertThat(avlEntries2.get(0).getKey()).containsExactly(3);
    assertThat(avlEntries2.get(0).getValue()).containsExactly(4);
  }

  @DisplayName("Avl next can skip entries")
  @Test
  void getAvlNextSkip() throws Exception {
    byte[] avlChainContract = JarBuilder.buildJar(LedgerChainServiceTest.AvlContract.class);
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              BlockchainLedgerHelper.createContract(
                  TEST_CONTRACT_ADDRESS,
                  () -> avlChainContract,
                  LedgerChainServiceTest::getAvlState,
                  mutable);
            });
    List<Map.Entry<byte[], byte[]>> avlEntries =
        ledgerChainService.getAvlNextN(TEST_CONTRACT_ADDRESS, 0, null, 5, 1, null);
    assertThat(avlEntries.get(0).getKey()).containsExactly(3);
    assertThat(avlEntries.get(0).getValue()).containsExactly(4);
    List<Map.Entry<byte[], byte[]>> avlEntries2 =
        ledgerChainService.getAvlNextN(TEST_CONTRACT_ADDRESS, 0, new byte[] {1}, 5, 1, null);
    assertThat(avlEntries2).hasSize(0);
  }

  /** Calling getAvlNextN on an address for which no contract exists, throws not found. */
  @Test
  void getAvlNextMissingContract() throws Exception {
    LedgerChainService ledgerChainService = setupEmptyChain(FunctionUtility.noOpConsumer());
    assertThatThrownBy(
            () ->
                ledgerChainService.getAvlNextN(
                    TEST_CONTRACT_ADDRESS, 0, new byte[] {1}, 5, 0, null))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Unable to find contract at '040a95cea619ec443a88e333edd16de52e348b72d8'");
  }

  /**
   * Calling getAvlModifiedKeys on a contract with two block times returns the list of modified keys
   * between the two block times.
   */
  @Test
  void getAvlModifiedKeys() throws Exception {
    byte[] avlChainContract = JarBuilder.buildJar(LedgerChainServiceTest.AvlContract.class);
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              BlockchainLedgerHelper.createContract(
                  TEST_CONTRACT_ADDRESS,
                  () -> avlChainContract,
                  LedgerChainServiceTest::getAvlState,
                  mutable);
              mutable.createAccount(TestObjects.KEY_PAIR_ONE.getPublic().createAddress());
            });
    SignedTransaction signedTransaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            ledger,
            TEST_CONTRACT_ADDRESS,
            TestObjects.KEY_PAIR_ONE,
            FunctionUtility.noOpConsumer());
    BlockchainLedgerHelper.appendBlock(ledger, List.of(signedTransaction), List.of());
    assertThat(ledgerChainService.getAvlModifiedKeys(TEST_CONTRACT_ADDRESS, 0, 0, 1, null, 10))
        .containsExactly(new byte[] {3}, new byte[] {5});
    assertThat(ledgerChainService.getAvlModifiedKeys(TEST_CONTRACT_ADDRESS, 0, 0, 1, null, 1))
        .containsExactly(new byte[] {3});
    assertThat(
            ledgerChainService.getAvlModifiedKeys(
                TEST_CONTRACT_ADDRESS, 0, 0, 1, new byte[] {3}, 10))
        .containsExactly(new byte[] {5});
    assertThat(ledgerChainService.getAvlModifiedKeys(TEST_CONTRACT_ADDRESS, 0, 0, 0, null, 10))
        .isEmpty();
    assertThat(ledgerChainService.getAvlModifiedKeys(TEST_CONTRACT_ADDRESS, 0, 1, 1, null, 10))
        .isEmpty();
  }

  /** Calling getAvlModifiedKeys with a block time that does not exist throws bad request. */
  @Test
  void getAvlModifiedKeysIllegalBlockTime() throws Exception {
    byte[] avlChainContract = JarBuilder.buildJar(LedgerChainServiceTest.AvlContract.class);
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              BlockchainLedgerHelper.createContract(
                  TEST_CONTRACT_ADDRESS,
                  () -> avlChainContract,
                  LedgerChainServiceTest::getAvlState,
                  mutable);
              mutable.createAccount(TestObjects.KEY_PAIR_ONE.getPublic().createAddress());
            });
    assertThatThrownBy(
            () -> ledgerChainService.getAvlModifiedKeys(TEST_CONTRACT_ADDRESS, 0, 0, 500, null, 10))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("No chain state for block time '500'");
    assertThatThrownBy(
            () -> ledgerChainService.getAvlModifiedKeys(TEST_CONTRACT_ADDRESS, 0, 500, 0, null, 10))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("No chain state for block time '500'");
  }

  /**
   * Calling getAvlModifiedKeys with a block time, where the contract did not exist yet throws not
   * found.
   */
  @Test
  void getAvlModifiedKeysUnableToFind() throws Exception {
    final LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              mutable.createAccount(TestObjects.KEY_PAIR_ONE.getPublic().createAddress());
            });
    byte[] avlChainContract = JarBuilder.buildJar(LedgerChainServiceTest.AvlContract.class);

    ExecutableEvent deployEvent =
        BlockchainLedgerHelper.createDeployEvent(
            null,
            null,
            1L,
            TestObjects.ACCOUNT_ONE,
            TEST_CONTRACT_ADDRESS,
            avlChainContract,
            new byte[0]);
    FloodableEvent floodableEvent =
        BlockchainLedgerHelper.createFloodableEvent(
                ledger.getChainId(), TestObjects.KEY_PAIR_ONE, null, deployEvent)
            .get(0);
    ledger.addPendingEvent(floodableEvent);
    BlockchainLedgerHelper.appendBlock(ledger, List.of(), List.of(floodableEvent));
    SignedTransaction signedTransaction =
        BlockchainLedgerHelper.createInteractWithContractTransaction(
            ledger,
            TEST_CONTRACT_ADDRESS,
            TestObjects.KEY_PAIR_ONE,
            FunctionUtility.noOpConsumer());
    ledger.addPendingTransaction(signedTransaction);
    BlockchainLedgerHelper.appendBlock(ledger, List.of(signedTransaction), List.of());

    assertThatThrownBy(
            () -> ledgerChainService.getAvlModifiedKeys(TEST_CONTRACT_ADDRESS, 0, 0, 1, null, 10))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Unable to find contract at '040a95cea619ec443a88e333edd16de52e348b72d8'");
    assertThatThrownBy(
            () -> ledgerChainService.getAvlModifiedKeys(TEST_CONTRACT_ADDRESS, 0, 1, 0, null, 10))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Unable to find contract at '040a95cea619ec443a88e333edd16de52e348b72d8'");
  }

  /**
   * Calling getAvlInformation on a contract with an avl tree with two entries, returns a size of 2.
   */
  @Test
  void getAvlInformation() throws Exception {
    byte[] avlChainContract = JarBuilder.buildJar(LedgerChainServiceTest.AvlContract.class);
    LedgerChainService ledgerChainService =
        setupEmptyChain(
            mutable -> {
              BlockchainLedgerHelper.createContract(
                  TEST_CONTRACT_ADDRESS,
                  () -> avlChainContract,
                  LedgerChainServiceTest::getAvlState,
                  mutable);
            });
    int size = ledgerChainService.getAvlInformation(TEST_CONTRACT_ADDRESS, 0, null);
    assertThat(size).isEqualTo(2);
  }

  /** Calling getAvlInformation on an address for which no contract exists, throws not found. */
  @Test
  void getAvlInformationMissingContract() throws Exception {
    LedgerChainService ledgerChainService = setupEmptyChain(FunctionUtility.noOpConsumer());
    assertThatThrownBy(() -> ledgerChainService.getAvlInformation(TEST_CONTRACT_ADDRESS, 0, null))
        .isInstanceOf(NotFoundException.class)
        .hasMessage("Unable to find contract at '040a95cea619ec443a88e333edd16de52e348b72d8'");
  }

  private LedgerChainService setupEmptyChain(Consumer<MutableChainState> stateBuilder)
      throws Exception {
    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(
            Files.createTempDirectory("myDirectory").toFile(), new ConcurrentHashMap<>());
    BlockchainLedger ledger =
        BlockchainLedgerHelper.createLedger(
            rootDirectory, BlockchainLedgerHelper.createNetworkConfig(), stateBuilder, null);
    this.ledger = ledger;
    Map<String, BlockchainLedger> stringBlockchainLedgerMap = new HashMap<>();
    stringBlockchainLedgerMap.put(null, ledger);
    return new LedgerChainService(stringBlockchainLedgerMap);
  }

  /**
   * Creates a signed transaction to the mint contract, on the given ledger by the sender.
   *
   * @param ledger transaction being sent on
   * @param sender of transaction
   * @param amount amount to mint
   * @return signed transaction
   */
  public static SignedTransaction createMintForChainState(
      BlockchainLedger ledger, KeyPair sender, long amount) {
    return BlockchainLedgerHelper.createInteractWithContractTransaction(
        ledger, TEST_MINT_CONTRACT_ADDRESS, sender, s -> s.writeLong(amount));
  }

  /** Test contract with not null state. */
  public static final class TestContract extends SysContract<TestContractState> {}

  /** Test state such that serialized contract is not null. */
  public static final class TestContractState implements StateSerializable {
    public final int stateValue;

    /** Construct state with default value 0. */
    @SuppressWarnings("unused")
    public TestContractState() {
      this.stateValue = 0;
    }

    /**
     * Construct state with specified value.
     *
     * @param stateValue the value
     */
    public TestContractState(int stateValue) {
      this.stateValue = stateValue;
    }
  }

  /** Test contract for minting mpc tokens. */
  public static final class MintTokensChainServiceContract extends SysContract<StateVoid> {

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      long tokens = rpc.readLong();
      SystemEventCreator invocationCreator = context.getInvocationCreator();
      invocationCreator.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              context.getFrom(),
              SafeDataOutputStream.serialize(
                  s -> {
                    s.writeByte(CHAIN_STATE_MINT);
                    s.writeLong(tokens);
                  })));
      return state;
    }
  }

  /** Test. Constant state. */
  public static final class AvlContract extends SysContract<ModelFactoryTest.TestingAvlState> {

    @Override
    public ModelFactoryTest.TestingAvlState onCreate(
        SysContractContext context, SafeDataInputStream rpc) {
      return getAvlState();
    }

    @Override
    public ModelFactoryTest.TestingAvlState onInvoke(
        SysContractContext context,
        ModelFactoryTest.TestingAvlState state,
        SafeDataInputStream rpc) {
      return new ModelFactoryTest.TestingAvlState(
          AvlTree.create(
              Map.of(
                  0,
                  new ModelFactoryTest.AvlTreeWrapperStub(
                      AvlTree.create(
                          Map.of(
                              new ModelFactoryTest.ComparableByteArray(new byte[] {1}),
                              new LargeByteArray(new byte[] {2}),
                              new ModelFactoryTest.ComparableByteArray(new byte[] {3}),
                              new LargeByteArray(new byte[] {42}),
                              new ModelFactoryTest.ComparableByteArray(new byte[] {5}),
                              new LargeByteArray(new byte[] {6})))))));
    }
  }

  /**
   * Create a default avl state matching a pub contract.
   *
   * @return default avl state
   */
  public static ModelFactoryTest.TestingAvlState getAvlState() {
    return new ModelFactoryTest.TestingAvlState(
        AvlTree.create(
            Map.of(
                0,
                new ModelFactoryTest.AvlTreeWrapperStub(
                    AvlTree.create(
                        Map.of(
                            new ModelFactoryTest.ComparableByteArray(new byte[] {1}),
                            new LargeByteArray(new byte[] {2}),
                            new ModelFactoryTest.ComparableByteArray(new byte[] {3}),
                            new LargeByteArray(new byte[] {4})))))));
  }

  @SuppressWarnings({"FieldCanBeLocal", "unused"})
  @Immutable
  static final class WasmContract implements StateSerializable {
    private final LargeByteArray state;

    private final AvlTree<Integer, Integer> avlTrees;

    WasmContract(LargeByteArray state, AvlTree<Integer, Integer> avlTrees) {
      this.state = state;
      this.avlTrees = avlTrees;
    }
  }
}
