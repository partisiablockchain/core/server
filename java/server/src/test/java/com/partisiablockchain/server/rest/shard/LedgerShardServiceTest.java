package com.partisiablockchain.server.rest.shard;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.Block;
import com.partisiablockchain.blockchain.BlockchainLedger;
import com.partisiablockchain.blockchain.BlockchainLedgerHelper;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.blockchain.ImmutableChainState;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.server.rest.chain.LedgerChainServiceTest;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.secata.stream.SafeDataOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Test. */
public final class LedgerShardServiceTest {

  private Map<String, BlockchainLedger> ledgers;

  private ShardService service;

  ConcurrentHashMap<MemoryStorage.PathAndHash, byte[]> data;

  private static final String SHARD_0 = "Shard0";
  private static final String SHARD_1 = "Shard1";

  private static final Hash EMPTY =
      Hash.fromString("0000000000000000000000000000000000000000000000000000000000000000");

  /**
   * Setup.
   *
   * @throws IOException exception.
   */
  @BeforeEach
  public void setup() throws IOException {
    data = new ConcurrentHashMap<>();
    RootDirectory rootDirectory =
        MemoryStorage.createRootDirectory(Files.createTempDirectory("myDirectory").toFile(), data);
    BlockchainLedger shard0 =
        BlockchainLedgerHelper.createLedger(
            rootDirectory,
            BlockchainLedgerHelper.createNetworkConfig(),
            BlockchainLedgerHelper.createChainState(),
            "Shard1");

    BlockchainLedger shard1 =
        BlockchainLedgerHelper.createLedger(
            rootDirectory, BlockchainLedgerHelper.createNetworkConfig(), mutable -> {}, "Shard1");
    ledgers = Map.of(SHARD_0, shard0, SHARD_1, shard1);
    service = new LedgerShardService(ledgers);
  }

  @Test
  void getShardState() {
    ShardState shardState0 = service.getShardState(SHARD_0);
    ImmutableChainState chainState0 = ledgers.get(SHARD_0).getChainState();
    assertThat(shardState0.governanceVersion()).isEqualTo(chainState0.getGovernanceVersion());
    assertThat(shardState0.plugins().get(ChainPluginType.ACCOUNT).isEmpty()).isFalse();
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString(
            shardState0
                .plugins()
                .get(ChainPluginType.ACCOUNT)
                .get("contracts")
                .get(0)
                .get("key")
                .textValue());
    assertThat(contractAddress).isEqualTo(LedgerChainServiceTest.TEST_CONTRACT_ADDRESS);

    ShardState shardState1 = service.getShardState(SHARD_1);
    ImmutableChainState chainState1 = ledgers.get(SHARD_1).getChainState();
    assertThat(shardState1.governanceVersion()).isEqualTo(chainState1.getGovernanceVersion());
  }

  @Test
  void getShardBlock() {
    BlockchainLedger shard0 = ledgers.get(SHARD_0);
    Block latestBlock = shard0.getLatestBlock();
    ShardBlock shardBlock = service.getShardBlock(SHARD_0, latestBlock.identifier());
    assertThat(shardBlock).isNotNull();
    assertThat(shardBlock.blockTime()).isEqualTo(latestBlock.getBlockTime());
    assertThat(shardBlock.parentBlock()).isEqualTo(latestBlock.getParentBlock());
    assertThat(shardBlock.committeeId()).isEqualTo(latestBlock.getCommitteeId());
    assertThat(shardBlock.producer()).isEqualTo(latestBlock.getProducerIndex());
    assertThat(shardBlock.productionTime()).isEqualTo(latestBlock.getProductionTime());
    ShardBlock unknownBlock =
        service.getShardBlock(SHARD_0, Hash.create(s -> s.writeString("Unknown block")));
    assertThat(unknownBlock).isNull();
    ShardBlock latestShardBlock = service.getShardBlock(SHARD_0, null);
    assertThat(latestShardBlock.identifier()).isEqualTo(latestBlock.identifier());
  }

  @Test
  void getShardTransactionNotFinalized() {
    BlockchainLedger shard0 = ledgers.get(SHARD_0);
    SignedTransaction transaction =
        LedgerChainServiceTest.createMintForChainState(shard0, TestObjects.KEY_PAIR_ONE, 1000);
    boolean valid = shard0.addPendingTransaction(transaction);
    assertThat(valid).isTrue();
    ShardTransaction shardPendingTransaction =
        service.getShardTransaction(SHARD_0, transaction.identifier());
    // Transaction is appended, but not included in any blocks
    assertThat(shardPendingTransaction).isNotNull();
    assertThat(shardPendingTransaction.executionStatus()).isNull();
    assertThat(shardPendingTransaction.isEvent()).isFalse();
    assertThat(shardPendingTransaction.identifier()).isEqualTo(transaction.identifier());
    BlockchainLedgerHelper.appendBlock(shard0, List.of(transaction), List.of());
    ShardTransaction invalidHash = service.getShardTransaction(SHARD_0, EMPTY);
    assertThat(invalidHash).isNull();
    ShardTransaction shardTransaction =
        service.getShardTransaction(SHARD_0, transaction.identifier());
    // Transaction is included in block, but not finalized
    assertThat(shardTransaction).isNotNull();
    assertThat(shardTransaction.isEvent()).isFalse();
    assertThat(shardTransaction.identifier()).isEqualTo(transaction.identifier());

    assertThat(shardTransaction.executionStatus().blockId())
        .isEqualTo(shard0.getProposals().get(0).getBlock().identifier());
    assertThat(shardTransaction.executionStatus().finalized()).isFalse();
    assertThat(shardTransaction.executionStatus().failure()).isNull();
    assertThat(shardTransaction.executionStatus().success()).isTrue();
    String route = getRoute(shard0, LedgerChainServiceTest.TEST_MINT_CONTRACT_ADDRESS);
    // The transaction spawns an event
    assertThat(shardTransaction.executionStatus().events().size()).isEqualTo(1);
    ShardTransaction.ShardExecutionStatus.ShardTransactionPointer transactionPointer =
        shardTransaction.executionStatus().events().get(0);
    Hash eventIdentifier = transactionPointer.identifier();
    assertThat(transactionPointer)
        .isEqualTo(
            new ShardTransaction.ShardExecutionStatus.ShardTransactionPointer(
                eventIdentifier, route));
  }

  @Test
  void getShardTransaction() {
    BlockchainLedger shard0 = ledgers.get(SHARD_0);
    SignedTransaction transaction =
        LedgerChainServiceTest.createMintForChainState(shard0, TestObjects.KEY_PAIR_ONE, 1000);
    boolean valid = shard0.addPendingTransaction(transaction);
    assertThat(valid).isTrue();
    BlockchainLedgerHelper.appendBlock(shard0, List.of(transaction), List.of());
    BlockchainLedgerHelper.appendBlock(shard0, List.of(), List.of());
    ShardTransaction shardTransaction =
        service.getShardTransaction(SHARD_0, transaction.identifier());
    assertThat(shardTransaction).isNotNull();
    assertThat(shardTransaction.isEvent()).isFalse();
    assertThat(shardTransaction.identifier()).isEqualTo(transaction.identifier());
    assertThat(shardTransaction.content())
        .isEqualTo(
            SafeDataOutputStream.serialize(shard0.getTransaction(transaction.identifier())::write));
    assertThat(shardTransaction.executionStatus().blockId())
        .isEqualTo(shard0.getBlock(shard0.getBlockTime()).identifier());
    assertThat(shardTransaction.executionStatus().finalized()).isTrue();
    final String route = getRoute(shard0, LedgerChainServiceTest.TEST_MINT_CONTRACT_ADDRESS);
    assertThat(shardTransaction.executionStatus().failure()).isNull();
    assertThat(shardTransaction.executionStatus().success()).isTrue();
    // The transaction spawns an event
    assertThat(shardTransaction.executionStatus().events().size()).isEqualTo(1);
    ShardTransaction.ShardExecutionStatus.ShardTransactionPointer transactionPointer =
        shardTransaction.executionStatus().events().get(0);
    Hash eventIdentifier = transactionPointer.identifier();
    Hash eventSpawner =
        shard0.getChainState().getExecutedState().getSpawnedEvents().getValue(eventIdentifier);
    assertThat(eventSpawner).isEqualTo(transaction.identifier());
    assertThat(transactionPointer)
        .isEqualTo(
            new ShardTransaction.ShardExecutionStatus.ShardTransactionPointer(
                eventIdentifier, route));
  }

  @Test
  void getShardTransactionEvent() {
    KeyPair producer = new KeyPair();
    ExecutableEvent event =
        BlockchainLedgerHelper.createEventToContract(
            "Shard1", "Shard1", 1L, LedgerChainServiceTest.TEST_CONTRACT_ADDRESS);
    BlockchainLedger shard1 = ledgers.get(SHARD_1);
    FloodableEvent floodableEvent =
        BlockchainLedgerHelper.createFloodableEvent(shard1.getChainId(), producer, "Shard1", event)
            .get(0);
    BlockchainLedgerHelper.addPendingEvent(shard1, producer, true, event);
    BlockchainLedgerHelper.appendBlock(shard1, List.of(), List.of(floodableEvent));
    BlockchainLedgerHelper.appendBlock(shard1, List.of(), List.of());
    BlockchainLedgerHelper.appendBlock(shard1, List.of(), List.of());
    ShardTransaction shardTransaction = service.getShardTransaction(SHARD_1, event.identifier());
    assertThat(shardTransaction).isNotNull();
    assertThat(shardTransaction.isEvent()).isTrue();
    assertThat(shardTransaction.content())
        .isEqualTo(
            SafeDataOutputStream.serialize(shard1.getTransaction(event.identifier())::write));
    assertThat(shardTransaction.identifier()).isEqualTo(event.identifier());
    ShardTransaction.ShardExecutionStatus executionStatus = shardTransaction.executionStatus();
    assertThat(executionStatus.success()).isTrue();
    assertThat(executionStatus.finalized()).isTrue();
    assertThat(executionStatus.failure()).isNull();
    assertThat(executionStatus.blockId()).isEqualTo(shard1.getBlock(1L).identifier());
    assertThat(executionStatus.events().size()).isEqualTo(0);
  }

  @Test
  void getFailedTransaction() {
    KeyPair producer = new KeyPair();
    ExecutableEvent event =
        BlockchainLedgerHelper.createEventToContract(
            "Shard1",
            "Shard1",
            1L,
            BlockchainAddress.fromString("02A000000000000000000000000000000000000002"));
    BlockchainLedger shard1 = ledgers.get(SHARD_1);
    FloodableEvent floodableEvent =
        BlockchainLedgerHelper.createFloodableEvent(shard1.getChainId(), producer, "Shard1", event)
            .get(0);
    BlockchainLedgerHelper.addPendingEvent(shard1, producer, true, event);
    BlockchainLedgerHelper.appendBlock(shard1, List.of(), List.of(floodableEvent));
    BlockchainLedgerHelper.appendBlock(shard1, List.of(), List.of());
    ShardTransaction shardTransaction = service.getShardTransaction(SHARD_1, event.identifier());
    assertThat(shardTransaction).isNotNull();
    ShardTransaction.ShardExecutionStatus.ShardFailure failure =
        shardTransaction.executionStatus().failure();
    assertThat(failure.errorMessage()).isEqualTo("Contract does not exist");
    assertThat(failure.stackTrace())
        .isEqualTo(shard1.getTransactionFailure(event.identifier()).getStackTrace());
  }

  @Test
  void getShardJar() {
    ImmutableChainState chainState0 = ledgers.get(SHARD_0).getChainState();
    Hash accountJar = chainState0.getPluginJarIdentifier(ChainPluginType.ACCOUNT);
    ShardJar shardJar = service.getShardJar(SHARD_0, accountJar);
    assertThat(shardJar).isNotNull();
    assertThat(shardJar.data())
        .isEqualTo(chainState0.getPluginJar(ChainPluginType.ACCOUNT).getData());
    ShardJar shardJar1 =
        service.getShardJar(SHARD_1, Hash.create(s -> s.writeString("Invalid hash")));
    assertThat(shardJar1).isNull();
    assertThat(service.getShardJar(SHARD_1, EMPTY)).isNull();
  }

  private static String getRoute(BlockchainLedger ledger, BlockchainAddress target) {
    return ledger
        .getChainState()
        .getRoutingPlugin()
        .route(ledger.getChainState().getActiveShards(), target);
  }
}
