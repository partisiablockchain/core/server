package com.partisiablockchain.server.rest;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.CloseableTest;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.dto.PluginType;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/** Test for plugin type converter. */
public final class PluginTypeConverterTest extends CloseableTest {

  @ParameterizedTest
  @MethodSource("com.partisiablockchain.blockchain.ChainPluginType#values")
  void pluginTypeConversionAreExhaustive(ChainPluginType value) {
    assertThat(PluginTypeConverter.convert(value)).isNotNull();
  }

  @Test
  public void testConversionFromChainPluginTypeToPluginType() {
    PluginTypeConverter converter = new PluginTypeConverter();
    assertThat(converter).isNotNull();

    ChainPluginType type = ChainPluginType.ACCOUNT;
    assertThat(PluginTypeConverter.convert(type)).isEqualTo(PluginType.ACCOUNT);

    type = ChainPluginType.ROUTING;
    assertThat(PluginTypeConverter.convert(type)).isEqualTo(PluginType.ROUTING);

    type = ChainPluginType.CONSENSUS;
    assertThat(PluginTypeConverter.convert(type)).isEqualTo(PluginType.CONSENSUS);

    type = ChainPluginType.SHARED_OBJECT_STORE;
    assertThat(PluginTypeConverter.convert(type)).isEqualTo(PluginType.SHARED_OBJECT_STORE);
  }

  @Test
  public void expectIllegalArgumentWhenPluginTypeInvalid() {
    ChainPluginType type = null;
    assertThatThrownBy(() -> PluginTypeConverter.convert(type))
        .isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void convert_from_chain_plugin_type_to_string() {
    PluginTypeConverter converter = new PluginTypeConverter();
    assertThat(converter).isNotNull();

    ChainPluginType type = ChainPluginType.ACCOUNT;
    assertThat(PluginTypeConverter.convertToString(type)).isEqualTo("ACCOUNT");

    type = ChainPluginType.ROUTING;
    assertThat(PluginTypeConverter.convertToString(type)).isEqualTo("ROUTING");

    type = ChainPluginType.CONSENSUS;
    assertThat(PluginTypeConverter.convertToString(type)).isEqualTo("CONSENSUS");

    type = ChainPluginType.SHARED_OBJECT_STORE;
    assertThat(PluginTypeConverter.convertToString(type)).isEqualTo("SHARED_OBJECT_STORE");
  }

  @Test
  public void expect_illigal_argument_when_plugin_type_is_invalid() {
    ChainPluginType type = null;
    assertThatThrownBy(() -> PluginTypeConverter.convertToString(type))
        .isInstanceOf(IllegalArgumentException.class);
  }
}
