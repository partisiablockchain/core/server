package com.partisiablockchain.server.tcp.server;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.ObjectCreator;
import com.secata.tools.coverage.FunctionUtility;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ServerConnectorTest {

  @Test
  public void closeTwice() {
    ServerConnector server =
        new ServerConnector(ObjectCreator.port(), FunctionUtility.noOpBiConsumer());
    Assertions.assertThat(server.isClosed()).isFalse();
    server.close();
    Assertions.assertThat(server.isClosed()).isTrue();
    server.close();
  }

  @Test
  public void closeOnSocketError() throws Exception {
    CountDownLatch latch = new CountDownLatch(1);
    ServerSocket serverSocket =
        new ServerSocket() {
          @Override
          public void close() throws IOException {
            latch.countDown();
            super.close();
          }
        };
    ServerConnector server = new ServerConnector(serverSocket, FunctionUtility.noOpBiConsumer());
    Assertions.assertThat(latch.await(1, TimeUnit.SECONDS)).isTrue();

    Assertions.assertThat(server.isClosed()).isTrue();
    Assertions.assertThat(serverSocket.isClosed()).isTrue();
  }

  @Test
  public void connect() throws Exception {
    CountDownLatch latch = new CountDownLatch(1);
    int port = ObjectCreator.port();
    ServerConnector server = new ServerConnector(port, (input, output) -> latch.countDown());
    Socket localhost = new Socket("localhost", port);
    Assertions.assertThat(latch.await(1, TimeUnit.SECONDS)).isTrue();
    localhost.close();
    server.close();
  }
}
