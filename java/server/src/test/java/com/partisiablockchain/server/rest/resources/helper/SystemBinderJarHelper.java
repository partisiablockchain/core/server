package com.partisiablockchain.server.rest.resources.helper;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.server.rest.SysContractBinder;
import com.secata.jarutil.JarBuilder;

/** Helper class for handing the system binder jar. */
public final class SystemBinderJarHelper {

  private static final byte[] SYS_BINDER = JarBuilder.buildJar(SysContractBinder.class);

  /**
   * Gets the system binder from pom.
   *
   * @return the jar as bytes
   */
  public static byte[] getSystemBinderJar() {
    return SYS_BINDER;
  }

  /**
   * Gets the system binder jar identifier.
   *
   * @return Identifier of the system binder jar
   */
  public static Hash getSystemBinderJarIdentifier() {
    return Hash.create(stream -> stream.writeDynamicBytes(getSystemBinderJar()));
  }
}
