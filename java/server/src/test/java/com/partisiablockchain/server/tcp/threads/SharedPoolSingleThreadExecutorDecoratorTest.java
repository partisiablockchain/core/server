package com.partisiablockchain.server.tcp.threads;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SharedPoolSingleThreadExecutorDecoratorTest {

  @Test
  public void shouldCall() throws Exception {
    SharedPoolSingleThreadExecutorDecorator execution =
        new SharedPoolSingleThreadExecutorDecorator("DecoratorTest");
    Executor executor = execution.get();
    CountDownLatch run = new CountDownLatch(1);
    executor.execute(run::countDown);
    Assertions.assertThat(run.await(1, TimeUnit.SECONDS)).isTrue();
  }

  @Test
  public void resource() {
    SharedPoolSingleThreadExecutorDecorator execution =
        new SharedPoolSingleThreadExecutorDecorator("DecoratorTest");
    Assertions.assertThat(execution.resources()).hasSize(1);
  }
}
