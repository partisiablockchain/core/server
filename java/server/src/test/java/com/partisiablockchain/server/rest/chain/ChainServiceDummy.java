package com.partisiablockchain.server.rest.chain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.ChainPluginType;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateObjectMapper;
import com.partisiablockchain.server.rest.chain.ChainState.ChainFeature;
import com.partisiablockchain.server.rest.chain.ChainState.ChainPlugin;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/** Dummy implementation for chain service. */
public final class ChainServiceDummy implements ChainService {

  private final ObjectMapper mapper = StateObjectMapper.createObjectMapper();
  private long governanceVersion = 0L;
  public final String shardId = "ShardId";
  public final BlockchainAddress notFoundChainAccountAddress =
      BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeInt(1)));
  public final BlockchainAddress validChainAccountAddress =
      BlockchainAddress.fromHash(BlockchainAddress.Type.ACCOUNT, Hash.create(h -> h.writeInt(2)));
  public final BlockchainAddress notFoundChainContractAddress =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV, Hash.create(h -> h.writeInt(1)));
  public final BlockchainAddress validChainContractAddress =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV, Hash.create(h -> h.writeInt(2)));
  public final Hash jarId = Hash.create(h -> h.writeInt(3));
  public final Hash binder = Hash.create(h -> h.writeInt(4));
  public final JsonNode accountJson = mapper.valueToTree("string account");
  public final byte[] serializedContract = "string contract".getBytes(StandardCharsets.UTF_8);
  public final Hash transactionHash = Hash.create(h -> h.writeInt(5));
  public final long storagelength = 1L;
  public final byte[] abi = new byte[4];

  @Override
  public ChainState getChainState(Long blockTime) {
    String chainId = "CHAIN ID";
    List<String> shards = Stream.of(null, "Shard").toList();

    ChainFeature chainFeatureState = new ChainFeature("featureName", "featureValue");
    List<ChainFeature> features = List.of(chainFeatureState);

    ChainPlugin chainPluginState = new ChainPlugin(jarId, mapper.valueToTree("state"));
    Map<ChainPluginType, ChainPlugin> plugins = Map.of(ChainPluginType.ACCOUNT, chainPluginState);

    return new ChainState(chainId, governanceVersion, shards, features, plugins);
  }

  long setGovernanceVersion(long newValue) {
    governanceVersion = newValue;
    return governanceVersion;
  }

  @Override
  public ChainAccount getChainAccount(BlockchainAddress address, Long blockTime) {
    if (address.equals(notFoundChainAccountAddress)) {
      return null;
    }

    return new ChainAccount(shardId, accountJson, 0L);
  }

  @Override
  public ChainContract getChainContract(BlockchainAddress address, Long blockTime) {
    if (address.equals(notFoundChainContractAddress)) {
      return null;
    }

    return new ChainContract(
        shardId,
        serializedContract,
        validChainContractAddress,
        jarId,
        binder,
        accountJson,
        storagelength,
        abi);
  }

  @Override
  public ChainTransactionPointer putChainTransaction(byte[] payload) {
    if (payload.length == 1) {
      return null;
    }
    return new ChainTransactionPointer(transactionHash, shardId);
  }

  @Override
  public byte[] getChainContractAvlValue(
      BlockchainAddress address, int treeId, byte[] key, Long blockTime) {
    Objects.requireNonNull(address);
    Objects.requireNonNull(key);
    return new byte[] {42, 43, 44};
  }

  @Override
  public List<Map.Entry<byte[], byte[]>> getAvlNextN(
      BlockchainAddress address, int treeId, byte[] key, int n, int skip, Long blockTime) {
    Objects.requireNonNull(address);
    return List.of(
        Map.entry(new byte[] {1}, new byte[] {2}), Map.entry(new byte[] {3}, new byte[] {4}));
  }

  @Override
  public List<byte[]> getAvlModifiedKeys(
      BlockchainAddress address, int treeId, long blockTime1, long blockTime2, byte[] key, int n) {
    Objects.requireNonNull(address);
    return List.of(new byte[] {1}, new byte[] {2}, new byte[] {3});
  }

  @Override
  public int getAvlInformation(BlockchainAddress address, int treeId, Long blockTime) {
    return 10;
  }
}
