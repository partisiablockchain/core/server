package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import java.math.BigInteger;
import java.util.List;

/** Test. */
public final class TestObjects {

  public static final Hash EMPTY_HASH = Hash.create(s -> {});

  public static final BlockchainAddress ACCOUNT_ONE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");

  public static final BlockchainAddress ACCOUNT_TWO =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  public static final BlockchainAddress ACCOUNT_THREE =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");

  public static final BlockchainAddress ACCOUNT_FOUR =
      BlockchainAddress.fromString("000000000000000000000000000000000000000004");

  public static final KeyPair KEY_PAIR_ONE = new KeyPair(BigInteger.ONE);
  public static final KeyPair KEY_ACCOUNT_GOV = new KeyPair(BigInteger.TWO);
  public static final BlockchainAddress ACCOUNT_GOV = KEY_ACCOUNT_GOV.getPublic().createAddress();

  public static final BlockchainAddress CONTRACT_MPC_TOKEN =
      BlockchainAddress.fromString("04FF00000000000000000000000000000000000003");

  public static final List<BlockchainAddress> GOV_SHARD_ADDRESSES = List.of(ACCOUNT_GOV);

  /**
   * Create a hash from a number.
   *
   * @param number the number to append.
   * @return the resulting hash.
   */
  public static Hash hashNumber(Number number) {
    return Hash.create(
        stream -> {
          if (number instanceof Long) {
            stream.writeLong(number.longValue());
          } else if (number instanceof Integer) {
            stream.writeInt(number.intValue());
          } else if (number instanceof Byte) {
            stream.writeByte(number.intValue());
          }
        });
  }
}
