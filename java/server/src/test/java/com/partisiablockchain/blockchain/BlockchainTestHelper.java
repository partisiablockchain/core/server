package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.ObjectCreator;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.genesis.GenesisStorage;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.flooding.NetworkConfig;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.storage.MemoryStorage;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/** Test helper. */
public final class BlockchainTestHelper {

  public final BlockchainLedger blockchain;

  /**
   * Construct a new helper.
   *
   * @param root the folder to use as data directory
   * @param closeOnExit callback to register closeable resources
   */
  public BlockchainTestHelper(File root, Consumer<AutoCloseable> closeOnExit) {
    this(rootDir(root), closeOnExit);
  }

  /**
   * Construct a new helper.
   *
   * @param root the folder to use as data directory
   * @param initialState callback used to populate the initial state
   * @param closeOnExit callback to register closeable resources
   */
  public BlockchainTestHelper(
      File root, Consumer<MutableChainState> initialState, Consumer<AutoCloseable> closeOnExit) {
    this(rootDir(root), initialState, closeOnExit);
  }

  /**
   * Creates a helper with the supplied folder.
   *
   * @param rootDirectory the folder to use as data directory
   */
  private BlockchainTestHelper(RootDirectory rootDirectory, Consumer<AutoCloseable> closeOnExit) {
    this(rootDirectory, StateHelper::initial, closeOnExit);
  }

  /**
   * Creates a helper with the supplied folder and initial state.
   *
   * @param rootDirectory the folder to use as data directory
   * @param initialState the state to initialize the blockchain with
   */
  private BlockchainTestHelper(
      RootDirectory rootDirectory,
      Consumer<MutableChainState> initialState,
      Consumer<AutoCloseable> closeOnExit) {
    this.blockchain =
        BlockchainLedger.createForTest(
            rootDirectory,
            mutableChainState -> {
              initialState.accept(mutableChainState);
              mutableChainState.createAccount(TestObjects.ACCOUNT_FOUR);
              mutableChainState.createAccount(TestObjects.ACCOUNT_THREE);
            },
            new BlockChainTestNetwork(),
            null);
    closeOnExit.accept(blockchain);
  }

  /**
   * Create a new helper with a fixed chain id.
   *
   * @param chainId chain id
   */
  public BlockchainTestHelper(File root, String chainId, Consumer<AutoCloseable> closeOnExit) {
    RootDirectory rootDirectory = MemoryStorage.createRootDirectory(root);
    this.blockchain =
        BlockchainLedger.createBlockChain(
            new NetworkConfig(new Address("localhost", 0), FunctionUtility.nullSupplier(), null),
            rootDirectory,
            createDefaultGenesis(chainId));
    closeOnExit.accept(blockchain);
  }

  private static RootDirectory rootDir(File root) {
    return MemoryStorage.createRootDirectory(root);
  }

  /**
   * Returns a blockchain ledger with events added to spawned events, and executed events where all
   * executed events map to false.
   *
   * @param root root file
   * @param events events to add
   * @return blockchain ledger with events added to initial state.
   */
  public static BlockchainLedger createLedgerWithEvents(File root, Hash... events) {
    Address nodeAddress = Address.parseAddress("address:" + ObjectCreator.port());
    return BlockchainLedger.createBlockChain(
        new NetworkConfig(nodeAddress, () -> nodeAddress, nodeAddress),
        rootDir(root),
        StateHelper.initialWithEvents(StateHelper::initial, events));
  }

  /**
   * Create default genesis state file.
   *
   * @param chainId the chain id to use
   * @return the absolute path of the created file
   */
  public static String createDefaultGenesis(String chainId) {
    BlockchainAddress root =
        BlockchainAddress.fromString("0035e97a5e078a5a0f28ec96d547bfee9ace803ac0");
    GenesisStorage storage = new GenesisStorage();
    MutableChainState state = MutableChainState.emptyMaster(new ChainStateCache(storage));
    state.createAccount(root);
    ImmutableChainState immutableChainState =
        state.asImmutable(chainId, null, AvlTree.create(), AvlTree.create());

    Hash hash =
        immutableChainState.saveExecutedState(
            s -> new StateSerializer(storage, true).write(s, ExecutedState.class).hash());

    File genesisFile =
        ExceptionConverter.call(
            () -> File.createTempFile("genesis", ".zip"), "Unable to save genesis");
    genesisFile.deleteOnExit();

    Block block =
        new Block(
            System.currentTimeMillis(), 0L, 0L, Block.GENESIS_PARENT, hash, List.of(), List.of());
    storage.persistGenesis(genesisFile, new FinalBlock(block, new byte[0]));
    return genesisFile.getAbsolutePath();
  }

  /** Append a new block to the contained blockchain. */
  public Block appendBlock(
      KeyPair keyPair, List<SignedTransaction> transactions, List<FloodableEvent> events) {
    long productionTime =
        Math.max(System.currentTimeMillis(), blockchain.getLatestBlock().getProductionTime() + 1);
    return appendBlock(keyPair, transactions, events, blockchain, productionTime);
  }

  /** Append a new block to the contained blockchain with the given productionTime. */
  public Block appendBlock(
      KeyPair keyPair, List<SignedTransaction> transactions, long productionTime) {
    return appendBlock(keyPair, transactions, List.of(), blockchain, productionTime);
  }

  /**
   * Append a new block to the contained blockchain with the given productionTime and producerIndex.
   */
  public Block appendBlock(
      KeyPair keyPair,
      List<SignedTransaction> transactions,
      long productionTime,
      int producerIndex) {
    return appendBlock(keyPair, transactions, List.of(), blockchain, productionTime, producerIndex);
  }

  /** Append a new block to the contained blockchain without events. */
  public Block appendBlock(KeyPair keyPair, List<SignedTransaction> transactions) {
    return appendBlock(keyPair, transactions, List.of());
  }

  /** Append a new block to the contained blockchain. */
  private static Block appendBlock(
      KeyPair keyPair,
      List<SignedTransaction> transactions,
      List<FloodableEvent> events,
      BlockchainLedger blockchain,
      long productionTime) {
    transactions.forEach(blockchain::addPendingTransaction);
    events.forEach(blockchain::addPendingEvent);
    List<ExecutableEvent> executableEvents =
        events.stream().map(FloodableEvent::getExecutableEvent).toList();
    FinalBlock finalBlock =
        createBlock(keyPair, transactions, executableEvents, blockchain, productionTime);
    appendBlock(blockchain, finalBlock);
    return finalBlock.getBlock();
  }

  /** Append a new block to the contained blockchain. */
  private static Block appendBlock(
      KeyPair keyPair,
      List<SignedTransaction> transactions,
      List<FloodableEvent> events,
      BlockchainLedger blockchain,
      long productionTime,
      int producerIndex) {
    transactions.forEach(blockchain::addPendingTransaction);
    events.forEach(blockchain::addPendingEvent);
    List<ExecutableEvent> executableEvents =
        events.stream().map(FloodableEvent::getExecutableEvent).toList();

    FinalBlock finalBlock =
        createBlock(
            keyPair, transactions, executableEvents, blockchain, productionTime, producerIndex);
    appendBlock(blockchain, finalBlock);
    return finalBlock.getBlock();
  }

  private static void appendBlock(BlockchainLedger blockchain, FinalBlock finalBlock) {
    blockchain.appendBlock(finalBlock);
  }

  /** Create a block. */
  private static FinalBlock createBlock(
      KeyPair keyPair,
      List<SignedTransaction> transactions,
      List<ExecutableEvent> events,
      BlockchainLedger blockchain,
      long productionTime) {
    return createBlock(keyPair, transactions, events, blockchain, productionTime, -1);
  }

  /** Create a block. */
  private static FinalBlock createBlock(
      KeyPair keyPair,
      List<SignedTransaction> transactions,
      List<ExecutableEvent> events,
      BlockchainLedger blockchain,
      long productionTime,
      int producerIndex) {
    List<Hash> trxHashes =
        transactions.stream().map(SignedTransaction::identifier).collect(Collectors.toList());
    List<Hash> eventHashes =
        events.stream().map(ExecutableEvent::identifier).collect(Collectors.toList());
    Hash previousStateHash = blockchain.latest().getState().getHash();
    Block block =
        new Block(
            productionTime,
            blockchain.getLatestBlock().getBlockTime() + 1,
            blockchain.getLatestBlock().getBlockTime(),
            blockchain.getLatestBlock().identifier(),
            previousStateHash,
            eventHashes,
            trxHashes,
            producerIndex);
    return new FinalBlock(block, SafeDataOutputStream.serialize(keyPair.sign(block.identifier())));
  }

  public List<Hash> getPendingTransactions() {
    return new ArrayList<>(blockchain.getPending().keySet());
  }
}
