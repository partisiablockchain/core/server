package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.Block.GENESIS_PARENT;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.genesis.GenesisStorage;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.server.rest.resources.helper.SystemBinderJarHelper;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.File;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

/** Test. */
public final class StateHelper {

  public static final BlockchainAddress DEPLOY =
      BlockchainAddress.fromHash(
          BlockchainAddress.Type.CONTRACT_GOV, Hash.create(s -> s.writeString("DeployInTest")));

  /** Create the initial chain state. */
  public static void initial(MutableChainState state) {
    BlockchainAddress initialProducer = new KeyPair(BigInteger.ONE).getPublic().createAddress();
    state.createAccount(initialProducer);
    state.createContract(
        DEPLOY,
        CoreContractState.create(
            state.saveJar(SystemBinderJarHelper.getSystemBinderJar()),
            state.saveJar(JarBuilder.buildJar(DeployContract.class)),
            state.saveJar(new byte[0]),
            0));
    state.setContractState(DEPLOY, null);
  }

  private static String setUpGenesis(Consumer<MutableChainState> stateBuilder, Hash... events) {
    File genesis = ExceptionConverter.call(() -> File.createTempFile("genesis", ".zip"), "");
    genesis.deleteOnExit();

    GenesisStorage storage = new GenesisStorage();
    ChainStateCache context = new ChainStateCache(storage);

    MutableChainState genesisState = MutableChainState.emptyMaster(context);
    stateBuilder.accept(genesisState);
    AvlTree<Hash, Hash> spawnedEvents =
        AvlTree.create(
            Arrays.stream(events).collect(Collectors.toMap(Function.identity(), Hash::create)));
    AvlTree<Hash, Boolean> executedEvents =
        AvlTree.create(Arrays.stream(events).collect(Collectors.toMap(Hash::create, (a) -> false)));
    Hash stateHash =
        genesisState
            .asImmutable("Shard1", "Shard1", spawnedEvents, executedEvents)
            .saveExecutedState(
                s -> {
                  StateSerializer stateSerializer = new StateSerializer(storage, true);
                  return stateSerializer.write(s, ExecutedState.class).hash();
                });
    Block block = new Block(0L, 1L, 2L, GENESIS_PARENT, stateHash, List.of(), List.of());
    storage.persistGenesis(genesis, new FinalBlock(block, new byte[0]));
    return genesis.getAbsolutePath();
  }

  static String initialWithEvents(Consumer<MutableChainState> state, Hash... events) {
    return setUpGenesis(state, events);
  }

  /** Add the supplied events to the executed state. */
  public static ImmutableChainState withEvent(
      MutableChainState state, String chainId, String subChainId, Hash... events) {
    AvlTree<Hash, Hash> spawnedEvents =
        AvlTree.create(
            Arrays.stream(events).collect(Collectors.toMap(Function.identity(), Hash::create)));
    return state.asImmutable(chainId, subChainId, spawnedEvents, AvlTree.create());
  }

  /** Contract used to deploy / remove a contract on chain. */
  public static final class DeployContract extends SysContract<StateVoid> {

    /** Create a remove transaction. */
    public static InteractWithContractTransaction remove(BlockchainAddress address) {
      return InteractWithContractTransaction.create(
          DEPLOY,
          SafeDataOutputStream.serialize(
              s -> {
                s.writeBoolean(true);
                address.write(s);
              }));
    }

    /** Create deploy transaction. */
    public static InteractWithContractTransaction deploy(
        BlockchainAddress address, byte[] binder, byte[] contract) {
      return InteractWithContractTransaction.create(
          DEPLOY,
          SafeDataOutputStream.serialize(
              s -> {
                s.writeBoolean(false);
                address.write(s);
                s.writeDynamicBytes(binder);
                s.writeDynamicBytes(contract);
              }));
    }

    @Override
    public StateVoid onInvoke(
        SysContractContext context, StateVoid state, SafeDataInputStream rpc) {
      boolean remove = rpc.readBoolean();
      if (remove) {
        context.getInvocationCreator().removeContract(BlockchainAddress.read(rpc));
      } else {
        context
            .getInvocationCreator()
            .deployContract(BlockchainAddress.read(rpc))
            .withBinderJar(rpc.readDynamicBytes())
            .withContractJar(rpc.readDynamicBytes())
            .allocateCost(0)
            .withPayload(s -> {})
            .send();
      }
      return null;
    }
  }
}
