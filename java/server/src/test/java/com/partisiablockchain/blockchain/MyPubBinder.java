package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.serialization.StateVoid;
import java.util.Objects;

/** Test. */
public final class MyPubBinder extends AbstractPubBinder<StateVoid> {

  /**
   * Construct a new binder.
   *
   * @param contract the associated contract
   */
  public MyPubBinder(PubContract<StateVoid> contract) {
    Objects.requireNonNull(contract);
  }

  @Override
  public Class<StateVoid> getStateClass() {
    return StateVoid.class;
  }
}
