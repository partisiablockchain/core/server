package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.blockchain.Block.GENESIS_PARENT;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.ObjectCreator;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.blockchain.contract.CoreContractState;
import com.partisiablockchain.blockchain.contract.CreateContractTransaction;
import com.partisiablockchain.blockchain.contract.InteractWithContractTransaction;
import com.partisiablockchain.blockchain.genesis.GenesisStorage;
import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.blockchain.transaction.CoreTransactionPart;
import com.partisiablockchain.blockchain.transaction.EventTransaction;
import com.partisiablockchain.blockchain.transaction.ExecutableEvent;
import com.partisiablockchain.blockchain.transaction.FloodableEvent;
import com.partisiablockchain.blockchain.transaction.SignedTransaction;
import com.partisiablockchain.blockchain.transaction.SyncEvent;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.flooding.Address;
import com.partisiablockchain.flooding.NetworkConfig;
import com.partisiablockchain.serialization.SerializationResult;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.serialization.StateSerializer;
import com.partisiablockchain.server.rest.chain.LedgerChainServiceTest;
import com.partisiablockchain.server.rest.resources.helper.SystemBinderJarHelper;
import com.partisiablockchain.storage.RootDirectory;
import com.partisiablockchain.tree.AvlTree;
import com.secata.jarutil.JarBuilder;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import com.secata.tools.coverage.FunctionUtility;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.assertj.core.api.Assertions;

/** Helper class to create BlockchainLedger, block events etc. */
public final class BlockchainLedgerHelper {

  private static final AtomicInteger port = new AtomicInteger(10000);

  /**
   * Get an unused port.
   *
   * @return a port.
   */
  public static int getFreshPort() {
    return port.getAndAdd(100);
  }

  /**
   * Returns a blockchain ledger.
   *
   * @param rootDataDirectory the ledgers root directory
   * @param network network
   * @param stateBuilder state builder for genesis state
   * @param subChain ledgers sub chain
   * @param events Executed events to start out with.
   * @return blockchain ledger
   */
  public static BlockchainLedger createLedger(
      RootDirectory rootDataDirectory,
      NetworkConfig network,
      Consumer<MutableChainState> stateBuilder,
      String subChain,
      Hash... events) {

    return BlockchainLedger.createBlockChain(
        network, rootDataDirectory, setUpGenesis("ChainId", subChain, stateBuilder, 0, events));
  }

  /**
   * Returns a blockchain ledger with given chainId.
   *
   * @param rootDataDirectory the ledgers root directory
   * @param network network
   * @param stateBuilder state builder for genesis state
   * @param chainId the ledgers' chain id
   * @param subChain ledgers sub chain
   * @param events Executed events to start out with.
   * @return blockchain ledger
   */
  public static BlockchainLedger createLedger(
      RootDirectory rootDataDirectory,
      NetworkConfig network,
      Consumer<MutableChainState> stateBuilder,
      String chainId,
      String subChain,
      Hash... events) {

    return BlockchainLedger.createBlockChain(
        network, rootDataDirectory, setUpGenesis(chainId, subChain, stateBuilder, 0, events));
  }

  /**
   * Creates a genesis file.
   *
   * @param chainId chain id
   * @param subChain sub chain
   * @param stateBuilder state builder
   * @param productionTime start production time for genesis
   * @param events events to add to genesis
   * @return path to genesis file
   */
  public static String setUpGenesis(
      String chainId,
      String subChain,
      Consumer<MutableChainState> stateBuilder,
      long productionTime,
      Hash... events) {
    File genesis = ExceptionConverter.call(() -> File.createTempFile("genesis", ".zip"), "");

    GenesisStorage storage = new GenesisStorage();
    ChainStateCache context = new ChainStateCache(storage);

    MutableChainState genesisState = MutableChainState.emptyMaster(context);
    stateBuilder.accept(genesisState);
    AvlTree<Hash, Hash> spawnedEvents =
        AvlTree.create(
            Arrays.stream(events).collect(Collectors.toMap(Function.identity(), Hash::create)));
    Hash stateHash =
        genesisState
            .asImmutable(chainId, subChain, spawnedEvents, AvlTree.create())
            .saveExecutedState(
                s -> {
                  StateSerializer stateSerializer = new StateSerializer(storage, true);
                  return stateSerializer.write(s, ExecutedState.class).hash();
                });
    Block block =
        new Block(
            productionTime, 0L, 2L, GENESIS_PARENT, stateHash, List.of(events), List.of(events));
    storage.persistGenesis(genesis, new FinalBlock(block, new byte[0]));
    return genesis.getAbsolutePath();
  }

  private static Block createBlock(ImmutableChainState state) {
    return new Block(
        System.currentTimeMillis(),
        3L,
        2L,
        Hash.create(s -> {}),
        state.getHash(),
        List.of(),
        List.of());
  }

  /**
   * Creates a list of floodable events.
   *
   * @param chainId chain id
   * @param producer producer
   * @param sendingShard sending shard
   * @param events events to create floodable events from
   * @return list of floodable events.
   */
  public static List<FloodableEvent> createFloodableEvent(
      String chainId, KeyPair producer, String sendingShard, ExecutableEvent... events) {
    StateStorageRaw storage = ObjectCreator.createMemoryStateStorage();
    ImmutableChainState state =
        StateHelper.withEvent(
            MutableChainState.emptyMaster(new ChainStateCache(storage)),
            chainId,
            sendingShard,
            Arrays.stream(events).map(ExecutableEvent::identifier).toArray(Hash[]::new));
    state.saveExecutedState(
        s -> new StateSerializer(storage, true).write(s, ExecutedState.class).hash());

    Block newBlock = createBlock(state);
    FinalBlock finalBlock =
        new FinalBlock(
            newBlock, SafeDataOutputStream.serialize(producer.sign(newBlock.identifier())));
    List<FloodableEvent> floodableEvents = new ArrayList<>();
    for (ExecutableEvent event : events) {
      floodableEvents.add(FloodableEvent.create(event, finalBlock, storage));
    }
    return floodableEvents;
  }

  /**
   * Adds a pending event to the ledger.
   *
   * @param ledger the ledger
   * @param producer producer to sign
   * @param result expected result
   * @param events events to add.
   */
  public static void addPendingEvent(
      BlockchainLedger ledger, KeyPair producer, boolean result, ExecutableEvent... events) {
    List<FloodableEvent> floodableEvents =
        createFloodableEvent(ledger.getChainId(), producer, "Shard1", events);
    for (FloodableEvent fe : floodableEvents) {
      Assertions.assertThat(ledger.addPendingEvent(fe)).isEqualTo(result);
    }
  }

  /**
   * Returns a network config.
   *
   * @return network config.
   */
  public static NetworkConfig createNetworkConfig() {
    Address address = Address.parseAddress("someAddress:" + getFreshPort());
    return new NetworkConfig(address, () -> address, address);
  }

  /**
   * Returns a consumer of mutable chain state, with dummy routing, consensus and account plugins.
   *
   * @param routeToShard Increase the outbound count for given shard.
   * @return mutable chain state
   */
  public static Consumer<MutableChainState> createChainState(String... routeToShard) {
    return createChainState(List.of(), routeToShard);
  }

  /**
   * Returns a consumer of mutable chain state, with dummy routing, consensus and account plugins
   * and enabled features.
   *
   * @param features to enable
   * @param routeToShard Increase the outbound count for given shard
   * @return mutable chain state
   */
  public static Consumer<MutableChainState> createChainState(
      List<String> features, String... routeToShard) {
    return mutable -> {
      addRouting(mutable, routeToShard);
      addPlugins(mutable);
      createAccountsAndContracts(mutable);
      clearSync(mutable);
      addFeatures(mutable, features);
    };
  }

  private static void addFeatures(MutableChainState mutable, List<String> features) {
    for (String feature : features) {
      mutable.setFeature(feature, "OK");
    }
  }

  /**
   * Add routing to a chain state.
   *
   * @param mutable the state
   * @param routeToShard the routing
   */
  public static void addRouting(MutableChainState mutable, String... routeToShard) {
    for (String shard : routeToShard) {
      mutable.routeToShard(shard);
    }
    mutable.addActiveShard(FunctionUtility.noOpBiConsumer(), "Shard1", "Shard1");
  }

  /**
   * Add some default plugins to a chain state.
   *
   * @param mutable the state
   */
  public static void addPlugins(MutableChainState mutable) {
    mutable.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.CONSENSUS,
        ConsensusPluginHelper.createJar(),
        SafeDataOutputStream.serialize(TestObjects.KEY_PAIR_ONE.getPublic().createAddress()));
    mutable.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ACCOUNT,
        FeePluginHelper.createJar(),
        new byte[0]);
    mutable.setPlugin(
        FunctionUtility.noOpBiConsumer(),
        null,
        ChainPluginType.ROUTING,
        RoutingPluginHelper.createJar(),
        new byte[0]);
  }

  /**
   * Add some default accounts and contracts to a chain state.
   *
   * @param mutable the state
   */
  public static void createAccountsAndContracts(MutableChainState mutable) {
    mutable.createAccount(TestObjects.KEY_PAIR_ONE.getPublic().createAddress());
    mutable.createAccount(TestObjects.ACCOUNT_ONE);
    mutable.createAccount(TestObjects.ACCOUNT_GOV);

    byte[] testChainContract = JarBuilder.buildJar(LedgerChainServiceTest.TestContract.class);
    createContract(
        LedgerChainServiceTest.TEST_CONTRACT_ADDRESS,
        () -> testChainContract,
        () -> new LedgerChainServiceTest.TestContractState(10),
        mutable);
    byte[] testMintChainContract =
        JarBuilder.buildJar(LedgerChainServiceTest.MintTokensChainServiceContract.class);
    createContract(
        LedgerChainServiceTest.TEST_MINT_CONTRACT_ADDRESS,
        () -> testMintChainContract,
        () -> null,
        mutable);
  }

  private static void clearSync(MutableChainState builder) {
    for (String activeShard : builder.getActiveShards()) {
      clearSync(builder, activeShard);
    }
    clearSync(builder, null);
  }

  private static void clearSync(MutableChainState builder, String activeShard) {
    ShardNonces shardNonce = builder.getShardNonce(activeShard);
    if (shardNonce.isMissingSync()) {
      builder.incomingSync(activeShard, new SyncEvent(List.of(), List.of(), List.of()));
    }
  }

  /**
   * Creates a contract on the blockchain ledgers mutable chain state.
   *
   * @param address address
   * @param contractJar contract jar
   * @param initialState initial state of the contract
   * @param state blockchain ledgers mutable chain state.
   */
  public static void createContract(
      BlockchainAddress address,
      Supplier<byte[]> contractJar,
      Supplier<StateSerializable> initialState,
      MutableChainState state) {
    byte[] jar = contractJar.get();
    Hash contractIdentifier = state.saveJar(jar);
    Hash abiIdentifier = state.saveJar(new byte[0]);
    CoreContractState contractState =
        CoreContractState.create(
            state.saveJar(getBinder(address.getType())),
            contractIdentifier,
            abiIdentifier,
            jar.length);
    state.createContract(address, contractState);
    ContractState stateContract = state.getContract(address);
    SerializationResult write = state.serializer.write(initialState.get());
    state.setContract(address, stateContract.withHashAndSize(write.hash(), write.totalByteCount()));
    state.contractCreated(address, 0);
  }

  private static byte[] getBinder(BlockchainAddress.Type type) {
    byte[] sysBinder = SystemBinderJarHelper.getSystemBinderJar();
    if (type == BlockchainAddress.Type.CONTRACT_SYSTEM) {
      return sysBinder;
    } else if (type == BlockchainAddress.Type.CONTRACT_GOV) {
      return sysBinder;
    } else {
      throw new IllegalArgumentException("Unable to handle contract type: " + type);
    }
  }

  /**
   * Creates an event to the given blockchain address.
   *
   * @param originShard origin shard
   * @param destination destination shard.
   * @param nonce nonce
   * @param recipient recipient of event
   * @return event.
   */
  public static ExecutableEvent createEventToContract(
      String originShard, String destination, long nonce, BlockchainAddress recipient) {
    return createEvent(
        originShard,
        destination,
        nonce,
        BlockchainAddress.fromString("000000000000000000000000000000000000000004"),
        recipient);
  }

  private static ExecutableEvent createEvent(
      String originShard,
      String destination,
      long nonce,
      BlockchainAddress sender,
      BlockchainAddress recipient) {
    return new ExecutableEvent(
        originShard,
        EventTransaction.createStandalone(
            Hash.create(s -> {}),
            sender,
            0L,
            InteractWithContractTransaction.create(recipient, new byte[0]),
            new ShardRoute(destination, nonce),
            0L,
            0L));
  }

  static FinalBlock createFinalBlock(Block block) {
    byte[] signature =
        SafeDataOutputStream.serialize(TestObjects.KEY_PAIR_ONE.sign(block.identifier())::write);
    return new FinalBlock(block, signature);
  }

  /**
   * Appends a block to the ledger.
   *
   * @param ledger to append block to
   * @param transactions list of transactions in the block
   * @param events list of events in the block
   */
  public static void appendBlock(
      BlockchainLedger ledger, List<SignedTransaction> transactions, List<FloodableEvent> events) {
    Hash stateHash;
    Block latestBlock;
    if (ledger.getProposals().size() > 0) {
      BlockchainLedger.BlockAndState blockAndState = ledger.getProposals().get(0);
      stateHash = blockAndState.getState().getHash();
      latestBlock = blockAndState.getBlock();
    } else {
      stateHash = ledger.getChainState().getHash();
      latestBlock = ledger.getLatestBlock();
    }
    Block block =
        new Block(
            Math.max(System.currentTimeMillis(), latestBlock.getProductionTime()) + 1,
            latestBlock.getBlockTime() + 1,
            2,
            latestBlock.identifier(),
            stateHash,
            events.stream()
                .map(event -> event.getExecutableEvent().identifier())
                .collect(Collectors.toList()),
            transactions.stream().map(SignedTransaction::identifier).collect(Collectors.toList()));
    for (SignedTransaction signedTransaction : transactions) {
      ledger.addPendingTransaction(signedTransaction);
    }
    for (FloodableEvent event : events) {
      ledger.addPendingEvent(event);
    }
    ledger.appendBlock(createFinalBlock(block));
    Assertions.assertThat(
            ledger.getPossibleHeads().stream().map(BlockAndStateWithParent::currentBlock).toList())
        .describedAs("Block was not appended to the ledger")
        .contains(block);
  }

  /**
   * Appends block to the ledger, with empty transactions and events.
   *
   * @param ledger to append block to
   */
  public static void appendBlock(BlockchainLedger ledger) {
    appendBlock(ledger, List.of(), List.of());
  }

  /**
   * Creates a signed transaction to the contract, on the given ledger by the sender.
   *
   * @param ledger transaction being sent on
   * @param contract to send transaction to
   * @param sender of transaction
   * @param rpc transaction rpc
   * @return signed transaction
   */
  public static SignedTransaction createInteractWithContractTransaction(
      BlockchainLedger ledger,
      BlockchainAddress contract,
      KeyPair sender,
      Consumer<SafeDataOutputStream> rpc) {
    CoreTransactionPart core =
        CoreTransactionPart.create(
            ledger.getAccountState(sender.getPublic().createAddress()).getNonce(),
            Long.MAX_VALUE / 10,
            100_000);

    InteractWithContractTransaction interact =
        InteractWithContractTransaction.create(contract, SafeDataOutputStream.serialize(rpc));
    return SignedTransaction.create(core, interact).sign(sender, ledger.getChainId());
  }

  /**
   * Create a deployment event.
   *
   * @param originShard origin shard
   * @param destination destination shard
   * @param nonce nonce
   * @param sender sender
   * @param contractAddress the newly deployed contract's address
   * @param contractJar the contract jar
   * @param rpc rpc for the contract's onCreate
   * @return the deployment event
   */
  public static ExecutableEvent createDeployEvent(
      String originShard,
      String destination,
      long nonce,
      BlockchainAddress sender,
      BlockchainAddress contractAddress,
      byte[] contractJar,
      byte[] rpc) {
    return new ExecutableEvent(
        originShard,
        EventTransaction.createStandalone(
            Hash.create(s -> {}),
            sender,
            0L,
            CreateContractTransaction.create(
                contractAddress, getBinder(contractAddress.getType()), contractJar, null, rpc),
            new ShardRoute(destination, nonce),
            0L,
            0L));
  }
}
