package com.partisiablockchain.blockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.binder.BinderEvent;
import com.partisiablockchain.binder.BinderInteraction;
import com.partisiablockchain.binder.BinderResult;
import com.partisiablockchain.binder.sys.SysBinderContext;
import com.partisiablockchain.binder.sys.SysBinderContract;
import com.partisiablockchain.binder.zk.ZkBinderContext;
import com.partisiablockchain.binder.zk.ZkBinderContract;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractType;
import com.partisiablockchain.contract.pub.PubContract;
import com.partisiablockchain.contract.sys.SysContract;
import com.partisiablockchain.contract.zk.ZkClosed;
import com.partisiablockchain.contract.zk.ZkContract;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.jarutil.JarBuilder;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test. */
public final class ClassLoaderHelperTest {

  @Test
  public void createWasm() {
    ClassLoaderHelper classLoaderHelper = new ClassLoaderHelper(Collections.emptySet());
    Assertions.assertThat(
            classLoaderHelper.blockchainContract(
                ContractType.PUBLIC,
                new LargeByteArray(JarBuilder.buildJar(MyPubWasmBinder.class)),
                new LargeByteArray(new byte[] {0, 97, 115, 109})))
        .isNotNull();
  }

  @Test
  public void createPublic() {
    ClassLoaderHelper classLoaderHelper = new ClassLoaderHelper(Collections.emptySet());
    Assertions.assertThat(
            classLoaderHelper.blockchainContract(
                ContractType.PUBLIC,
                new LargeByteArray(JarBuilder.buildJar(MyPubBinder.class)),
                new LargeByteArray(JarBuilder.buildJar(MyPubContract.class))))
        .isNotNull();
  }

  @Test
  public void createSystem() {
    ClassLoaderHelper classLoaderHelper = new ClassLoaderHelper(Collections.emptySet());
    Assertions.assertThat(
            classLoaderHelper.blockchainContract(
                ContractType.SYSTEM,
                new LargeByteArray(JarBuilder.buildJar(MySystemBinder.class)),
                new LargeByteArray(JarBuilder.buildJar(MySysContract.class))))
        .isNotNull();
  }

  @Test
  public void createGovernance() {
    ClassLoaderHelper classLoaderHelper = new ClassLoaderHelper(Collections.emptySet());
    Assertions.assertThat(
            classLoaderHelper.blockchainContract(
                ContractType.GOVERNANCE,
                new LargeByteArray(JarBuilder.buildJar(MySystemBinder.class)),
                new LargeByteArray(JarBuilder.buildJar(MySysContract.class))))
        .isNotNull();
  }

  @Test
  public void createZk() {
    ClassLoaderHelper classLoaderHelper = new ClassLoaderHelper(Collections.emptySet());
    Assertions.assertThat(
            classLoaderHelper.blockchainContract(
                ContractType.ZERO_KNOWLEDGE,
                new LargeByteArray(JarBuilder.buildJar(MyZkBinder.class)),
                new LargeByteArray(JarBuilder.buildJar(MyZkContract.class))))
        .isNotNull();
  }

  @Test
  public void unknownContractType() {
    ClassLoaderHelper classLoaderHelper = new ClassLoaderHelper(Collections.emptySet());
    Assertions.assertThatThrownBy(
            () ->
                classLoaderHelper.blockchainContract(
                    null,
                    new LargeByteArray(JarBuilder.buildJar(MyZkBinder.class)),
                    new LargeByteArray(JarBuilder.buildJar(MyZkContract.class))))
        .hasMessageContaining("Unknown contract type: ");
  }

  @Test
  public void shouldCacheClassLoaders() {
    ClassLoaderHelper classLoaderHelper = new ClassLoaderHelper(Collections.emptySet());
    LargeByteArray jar = new LargeByteArray(JarBuilder.buildJar(MyZkContract.class));
    JarClassLoader loader = classLoaderHelper.jarClassLoader(jar);
    JarClassLoader reloaded = classLoaderHelper.jarClassLoader(jar);
    Assertions.assertThat(reloaded).isSameAs(loader);
  }

  @Test
  public void namespacesAreAddedToJarClassLoader() {
    ClassLoaderHelper classLoaderHelper =
        new ClassLoaderHelper(Set.of("com.partisiablockchain.blockchain"));
    LargeByteArray binderJar = new LargeByteArray(JarBuilder.buildJar(MyPubBinder.class));
    JarClassLoader binderLoader = classLoaderHelper.jarClassLoader(binderJar);

    Assertions.assertThat(binderLoader.getSystemLoadedNamespaces())
        .containsExactly("com.partisiablockchain.blockchain");

    LargeByteArray contractJar = new LargeByteArray(JarBuilder.buildJar(MyPubContract.class));
    JarClassLoader contractLoader =
        classLoaderHelper.contractClassLoader(contractJar, binderLoader);

    Assertions.assertThat(contractLoader.getSystemLoadedNamespaces())
        .containsExactly("com.partisiablockchain.blockchain");
  }

  @Test
  public void shouldCacheContractClassLoaders() {
    ClassLoaderHelper classLoaderHelper = new ClassLoaderHelper(Collections.emptySet());
    JarClassLoader systemBinder =
        JarClassLoader.builder(JarBuilder.buildJar(MySystemBinder.class)).build();
    LargeByteArray jar = new LargeByteArray(JarBuilder.buildJar(MySysContract.class));
    JarClassLoader loaded = classLoaderHelper.contractClassLoader(jar, systemBinder);
    JarClassLoader reloaded = classLoaderHelper.contractClassLoader(jar, systemBinder);

    Assertions.assertThat(reloaded).isSameAs(loaded);
  }

  /** Test. */
  public static final class MyPubContract extends PubContract<StateVoid> {}

  /** Test. */
  public static final class MySystemBinder implements SysBinderContract<StateVoid> {

    /**
     * Construct a new binder.
     *
     * @param contract the associated contract
     */
    public MySystemBinder(SysContract<StateVoid> contract) {
      Objects.requireNonNull(contract);
    }

    @Override
    public Class<StateVoid> getStateClass() {
      return StateVoid.class;
    }

    @Override
    public BinderResult<StateVoid, BinderEvent> create(SysBinderContext context, byte[] rpc) {
      return null;
    }

    @Override
    public BinderResult<StateVoid, BinderEvent> invoke(
        SysBinderContext context, StateVoid state, byte[] rpc) {
      return null;
    }

    @Override
    public BinderResult<StateVoid, BinderEvent> callback(
        SysBinderContext context, StateVoid state, CallbackContext callbackContext, byte[] rpc) {
      return null;
    }

    @Override
    public StateVoid upgrade(StateAccessor oldState, byte[] rpc) {
      return null;
    }
  }

  /** Test. */
  public static final class MySysContract extends SysContract<StateVoid> {}

  /** Test. */
  public static final class MyZkBinder implements ZkBinderContract<StateVoid> {

    private final ZkContract<?, ?, ?, ?> zkContract;

    /**
     * Construct a new binder.
     *
     * @param contract the associated contract
     */
    public MyZkBinder(ZkContract<?, ?, ?, ?> contract) {
      zkContract = Objects.requireNonNull(contract);
    }

    @Override
    public Class<StateVoid> getStateClass() {
      return StateVoid.class;
    }

    @Override
    public BinderResult<StateVoid, BinderInteraction> create(ZkBinderContext context, byte[] rpc) {
      return null;
    }

    @Override
    public BinderResult<StateVoid, BinderInteraction> invoke(
        ZkBinderContext context, StateVoid state, byte[] rpc) {
      return null;
    }

    @Override
    public BinderResult<StateVoid, BinderInteraction> callback(
        ZkBinderContext context, StateVoid state, CallbackContext callbackContext, byte[] rpc) {
      return null;
    }

    @Override
    public List<Class<?>> getStateClassTypeParameters() {
      return List.of();
    }

    @Override
    public ZkContract<?, ?, ?, ?> getContract() {
      return zkContract;
    }
  }

  /** Test. */
  public static final class MyZkContract
      extends ZkContract<StateVoid, StateVoid, ZkClosed<StateVoid>, Integer> {

    /** Construct a new contract. */
    public MyZkContract() {
      super(123);
    }
  }
}
