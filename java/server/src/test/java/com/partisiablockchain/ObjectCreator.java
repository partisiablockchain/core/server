package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.blockchain.storage.StateStorageRaw;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;

/** Test. */
public final class ObjectCreator {

  private static final AtomicInteger port = new AtomicInteger(6001);

  /**
   * Allocate the next port.
   *
   * @return next port
   */
  public static int port() {
    return port.getAndIncrement();
  }

  /** Create memory state storage. */
  public static MemoryStateStorage createMemoryStateStorage() {
    return new MemoryStateStorage();
  }

  /** Create an input stream that blocks after having read the supplied data. */
  public static BlockingInput blockingInput(CloseableTest registration) {
    BlockingInput input = new BlockingInput();
    registration.register(input);
    return input;
  }

  /** Create an input stream that blocks after having read the supplied data. */
  public static BlockingInput blockingInput(CloseableTest registration, byte[] dataBeforeBlock) {
    BlockingInput input = new BlockingInput(dataBeforeBlock);
    registration.register(input);
    return input;
  }

  /** Test. */
  @SuppressWarnings("InputStreamSlowMultibyteRead")
  public static final class BlockingInput extends InputStream {

    private final byte[] data;
    private int index = 0;
    private boolean closed = false;

    BlockingInput(byte[] dataBeforeBlock) {
      this.data = dataBeforeBlock;
    }

    BlockingInput() {
      this(new byte[0]);
    }

    @Override
    public synchronized int read() throws IOException {
      if (index < data.length) {
        int result = Byte.toUnsignedInt(data[index]);
        index++;
        return result;
      }
      while (!closed) {
        ExceptionConverter.run(this::wait, "Interrupt");
      }
      throw new EOFException("expected");
    }

    @Override
    public synchronized void close() {
      closed = true;
      this.notifyAll();
    }

    public boolean isClosed() {
      return closed;
    }
  }

  /** Test. */
  public static final class MemoryStateStorage implements StateStorageRaw {

    private final Map<Hash, byte[]> serialized = new HashMap<>();

    /**
     * Remove an entry from the storage.
     *
     * @param hash the entry to remove
     */
    public void remove(Hash hash) {
      serialized.remove(hash);
    }

    @Override
    public synchronized boolean write(Hash hash, Consumer<SafeDataOutputStream> writer) {
      if (serialized.containsKey(hash)) {
        return false;
      }
      serialized.put(
          hash,
          SafeDataOutputStream.serialize(t -> ExceptionConverter.run(() -> writer.accept(t), "")));
      return true;
    }

    @Override
    public synchronized <S> S read(Hash hash, Function<SafeDataInputStream, S> reader) {
      if (serialized.containsKey(hash)) {
        SafeDataInputStream bytes = SafeDataInputStream.createFromBytes(serialized.get(hash));
        return ExceptionConverter.call(() -> reader.apply(bytes), "");
      } else {
        return null;
      }
    }

    @Override
    public synchronized byte[] read(Hash hash) {
      return serialized.get(hash);
    }
  }
}
