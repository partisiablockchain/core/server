package com.partisiablockchain;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.io.TempDir;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Test. */
public abstract class CloseableTest {

  private static final Logger logger = LoggerFactory.getLogger(CloseableTest.class);

  private final List<AutoCloseable> toClose = new ArrayList<>();

  @TempDir private File tmp;

  /**
   * Register a thread that should be interrupted when the test ends.
   *
   * @param thread the thread
   * @return the thread
   */
  protected Thread register(Thread thread) {
    toClose.add(thread::interrupt);
    return thread;
  }

  /**
   * Register a closeable that should be closed after the test.
   *
   * @param closeable the closeable to close
   * @param <T> the type of the closeable
   * @return the closeable
   */
  public <T extends AutoCloseable> T register(T closeable) {
    toClose.add(closeable);
    return closeable;
  }

  /**
   * Get the temporary folder used by this test.
   *
   * @return the folder
   */
  protected File newFolder() {
    return tmp;
  }

  /** Close anything that has been registered for closure. */
  @AfterEach
  public void tearDown() {
    for (AutoCloseable autoCloseable : toClose) {
      try {
        autoCloseable.close();
      } catch (Exception e) {
        logger.info("Ignored close exception");
      }
    }
  }
}
