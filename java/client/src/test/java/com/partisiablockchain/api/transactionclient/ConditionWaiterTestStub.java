package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.function.Predicate;
import java.util.function.Supplier;

/** Condition waiter test implementation, allows to check the number of sleeps. */
final class ConditionWaiterTestStub implements ConditionWaiter {

  private int numberOfSleeps;
  private long timeWaited;

  ConditionWaiterTestStub() {}

  @Override
  public <T> T waitForCondition(
      Supplier<T> valueSupplier,
      Predicate<T> valuePredicate,
      long timeoutMs,
      Supplier<String> errorMessage) {
    T value;
    try {
      value = valueSupplier.get();
    } catch (Exception ignored) {
      value = null;
    }
    while ((value == null || !valuePredicate.test(value)) && numberOfSleeps < 7) {
      this.numberOfSleeps++;
      timeWaited += timeoutMs;
      try {
        value = valueSupplier.get();
      } catch (Exception ignored) {
        value = null;
      }
    }
    return value;
  }

  void assertHasWaitedWithoutResult(long duration) {
    assertThat(numberOfSleeps >= 7).isTrue();
    assertThat(timeWaited).isEqualTo(duration * numberOfSleeps);
  }
}
