package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.Test;

/** Tests for condition waiter. */
public final class ConditionWaiterTest {

  @Test
  void nonPositiveTimeoutsResultsInNoWaiting() {
    int startTime = 42;
    TickingTimeSupport timeSupport = new TickingTimeSupport(startTime);
    final ConditionWaiter waiter = ConditionWaiterImpl.createForTest(timeSupport);
    AtomicInteger counter = new AtomicInteger(0);

    // Run test with negative timeout
    assertThatThrownBy(() -> waiter.waitForCondition(counter::incrementAndGet, i -> false, -1, ""))
        .hasMessage("");
    assertThat(timeSupport.totalSleepTime).isEqualTo(0);

    // Repeat test with 0 timeout
    assertThatThrownBy(() -> waiter.waitForCondition(counter::incrementAndGet, i -> false, 0, ""))
        .hasMessage("");
    assertThat(timeSupport.totalSleepTime).isEqualTo(0);
  }

  @Test
  void checksConditionOnceWhenWaitingNonPositiveSeconds() {
    TickingTimeSupport tickingTimeSupport = new TickingTimeSupport(42);

    // Run test with negative timeout
    final ConditionWaiter waiter = ConditionWaiterImpl.createForTest(tickingTimeSupport);
    AtomicInteger counter = new AtomicInteger(0);
    final long negativeWaitTime = -1;
    assertThatThrownBy(
            () ->
                waiter.waitForCondition(counter::incrementAndGet, i -> false, negativeWaitTime, ""))
        .hasMessage("");
    assertThat(counter.get()).isEqualTo(1);

    // Repeat test with zero
    AtomicInteger newCounter = new AtomicInteger(0);
    final long zeroWaitTime = 0;
    assertThatThrownBy(
            () ->
                waiter.waitForCondition(newCounter::incrementAndGet, i -> false, zeroWaitTime, ""))
        .hasMessage("");
    assertThat(newCounter.get()).isEqualTo(1);
  }

  /** Test the default waiter which uses system time. */
  @Test
  void testSystemWaiter() {
    final ConditionWaiter waiter = ConditionWaiterImpl.create();
    AtomicInteger counter = new AtomicInteger(0);
    final Integer result =
        waiter.waitForCondition(counter::incrementAndGet, val -> val == 2, 1000, "time-out");
    assertThat(result).isEqualTo(2);
  }

  /** When the condition is fulfilled from the start sleep is not called. */
  @Test
  void testNoSleepsWhenStartingFulfilled() {
    final TimeSupportTestStub timeSupport = new TimeSupportTestStub(0);
    final ConditionWaiter waiter = ConditionWaiterImpl.createForTest(timeSupport);
    final Integer result = waiter.waitForCondition(() -> 7, val -> val == 7, 100, "time-out");
    assertThat(result).isEqualTo(7);
    assertThat(timeSupport.numberOfSleeps).isEqualTo(0);
  }

  @Test
  public void sleepInterruptionThrowsException() {
    ConditionWaiterImpl.TimeSupport throwingTimeSupport =
        new ConditionWaiterImpl.TimeSupport() {
          @Override
          public long now() {
            return 0;
          }

          @Override
          public void sleep(long ms) {
            throw new RuntimeException();
          }
        };
    ConditionWaiter waiter = ConditionWaiterImpl.createForTest(throwingTimeSupport);
    assertThatThrownBy(() -> waiter.waitForCondition(() -> 0, i -> false, 1000, "not done"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Sleep interrupted");
  }

  /**
   * Testing that we do not do busy-waiting. We sleep in an incrementing fashion of 0.1, 0.2, 0.3,
   * and lastly 0.4 seconds in the final check. This sums to 1 second, which corresponds to the
   * 1-second (1000 milliseconds) timeout, we provide the function which yield 5 rounds of sleeps,
   * since we sleep 0 seconds in the final round.
   */
  @Test
  public void ensureDelayBetweenResamples() {
    TimeSupportTestStub timeSupport = new TimeSupportTestStub(0);
    ConditionWaiter waiter = ConditionWaiterImpl.createForTest(timeSupport);
    assertThatThrownBy(() -> waiter.waitForCondition(() -> 0, i -> false, 1000, "timed out"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("timed out");

    assertThat(timeSupport.numberOfSleeps).isEqualTo(5);
    assertThat(timeSupport.now()).isEqualTo(1000);
  }

  /**
   * Test that we keep checking even if the value supplier fails with an exception on some attempts.
   */
  @Test
  public void shouldSurviveErrorsFromValueSupplier() {
    AtomicInteger counter = new AtomicInteger(0);
    TimeSupportTestStub timeSupport = new TimeSupportTestStub(0);
    ConditionWaiter waiter = ConditionWaiterImpl.createForTest(timeSupport);
    Integer result =
        waiter.waitForCondition(
            () -> {
              int i = counter.incrementAndGet();
              if (i == 1) {
                throw new RuntimeException("Expected");
              }
              return i;
            },
            i -> i == 2,
            1000,
            "time-out");

    // We expect to be finished after the second iteration/checking round.
    assertThat(result).isEqualTo(2);
    assertThat(timeSupport.numberOfSleeps).isEqualTo(1);
    assertThat(timeSupport.now()).isEqualTo(100);
  }

  /**
   * Test that we do one final check of the condition, when the "current time" is equal to the
   * timeout, before throwing an error.
   */
  @Test
  public void testTimingBoundary() {
    TimeSupportTestStub timeSupport = new TimeSupportTestStub(0);
    ConditionWaiter waiter = ConditionWaiterImpl.createForTest(timeSupport);
    AtomicInteger integer = new AtomicInteger(0);

    assertThatThrownBy(
            () -> waiter.waitForCondition(integer::incrementAndGet, i -> false, 100, "time-out"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("time-out");

    // We expect to be finished after the second iteration/checking round.
    assertThat(integer.get()).isEqualTo(2);
    assertThat(timeSupport.now()).isEqualTo(100);
  }

  /** Test that the fulfilled conditions correctly results in the wait ending. */
  @Test
  public void timeoutForEndlessLoop() {
    AtomicInteger counter = new AtomicInteger(0);
    TimeSupportTestStub timeSupport = new TimeSupportTestStub(0);
    ConditionWaiter waiter = ConditionWaiterImpl.createForTest(timeSupport);

    waiter.waitForCondition(counter::incrementAndGet, val -> val == 5, Long.MAX_VALUE, "time-out");
    assertThat(counter.get()).isEqualTo(5);
    assertThat(timeSupport.numberOfSleeps).isEqualTo(4);
    assertThat(timeSupport.now()).isEqualTo(1000);
  }

  private static final class TickingTimeSupport implements ConditionWaiterImpl.TimeSupport {
    public long now;
    public long totalSleepTime;

    TickingTimeSupport(long now) {
      this.now = now;
      totalSleepTime = 0;
    }

    @Override
    public long now() {
      return now++;
    }

    @Override
    public void sleep(long ms) {
      totalSleepTime += ms;
    }
  }
}
