package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import java.util.Arrays;
import java.util.Objects;
import org.junit.jupiter.api.Test;

/** Test. */
public final class TransactionTest {

  @Test
  public void equalsAndHashCode() {
    BlockchainAddress controlAddress = TestObjects.ACCOUNT_ONE;
    byte[] controlRpc = {1};
    Transaction control = Transaction.create(controlAddress, controlRpc);
    Transaction structurallyEquivalent = Transaction.create(controlAddress, controlRpc);
    Transaction differentAddress = Transaction.create(TestObjects.ACCOUNT_TWO, controlRpc);
    Transaction differentRpc = Transaction.create(controlAddress, new byte[] {2});

    Object object = new Object();
    Transaction nullTransaction = null;

    assertThat(control.equals(structurallyEquivalent)).isTrue();
    assertThat(control.hashCode()).isEqualTo(structurallyEquivalent.hashCode());
    assertThat(control.equals(differentAddress)).isFalse();
    assertThat(control.hashCode()).isNotEqualTo(differentAddress.hashCode());
    assertThat(control.equals(differentRpc)).isFalse();
    assertThat(control.equals(object)).isFalse();
    assertThat(control.equals(nullTransaction)).isFalse();

    assertThat(control.hashCode())
        .isEqualTo(31 * Objects.hash(controlAddress) + Arrays.hashCode(controlRpc));
  }
}
