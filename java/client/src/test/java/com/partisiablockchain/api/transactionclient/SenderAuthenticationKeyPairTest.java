package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.KeyPair;
import java.math.BigInteger;
import org.junit.jupiter.api.Test;

/** Tests for TransactionSenderKeyPair. */
public final class SenderAuthenticationKeyPairTest {

  @Test
  public void senderAuthenticationKeyPair() {
    KeyPair keyPair = new KeyPair(new BigInteger(new byte[] {42}));
    SenderAuthenticationKeyPair senderAuthenticationKeyPair =
        new SenderAuthenticationKeyPair(keyPair);
    assertThat(senderAuthenticationKeyPair.getKeyPair()).isEqualTo(keyPair);
    assertThat(senderAuthenticationKeyPair.getAddress()).isNotNull();
  }

  @Test
  public void transactionSenderFromString() {
    SenderAuthenticationKeyPair senderAuthenticationKeyPair =
        SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY);
    KeyPair keyPair = senderAuthenticationKeyPair.getKeyPair();
    assertThat(keyPair.getPrivateKey()).isEqualTo(new BigInteger(TestObjects.PRIVATE_KEY, 16));
  }
}
