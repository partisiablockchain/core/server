package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.TestObjects;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.api.ShardControllerApi;
import com.partisiablockchain.api.transactionclient.model.Account;
import com.partisiablockchain.api.transactionclient.model.Block;
import com.partisiablockchain.api.transactionclient.model.Chain;
import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import com.partisiablockchain.api.transactionclient.model.ExecutionStatus;
import com.partisiablockchain.api.transactionclient.model.SerializedTransaction;
import com.partisiablockchain.api.transactionclient.model.TransactionPointer;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import jakarta.ws.rs.BadRequestException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/** Tests for {@link BlockchainTransactionClient}. */
public final class BlockchainTransactionClientTest {

  private ConditionWaiterTestStub timeSupport;
  private SenderAuthenticationKeyPair authentication;
  private long startTime = 1_000_000L;
  BlockchainTransactionClient client;

  @BeforeEach
  void init() throws ApiException {
    authentication = SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY);
    timeSupport = new ConditionWaiterTestStub();
    client = createClient();
  }

  private ChainControllerApi createChainControllerWithDefaultPointer() {
    TransactionPointer defaultPointer =
        new TransactionPointer()
            .identifier(Hash.fromString("a".repeat(64)))
            .destinationShardId("Shard0");
    return new ChainControllerStub(defaultPointer);
  }

  private ShardControllerApi createShardController() {
    return createShardController(null);
  }

  private ShardControllerApi createShardController(ExecutedTransaction transaction) {
    ShardControllerApi shardController = new ShardControllerStub(transaction);
    return shardController;
  }

  private BlockchainTransactionClient createClient() throws ApiException {
    ShardAndChainControllerTransactionClient transactionClient =
        new ShardAndChainControllerTransactionClient(
            createChainControllerWithDefaultPointer(), createShardController());

    return BlockchainTransactionClient.createForTest(
        transactionClient,
        authentication,
        () -> startTime,
        timeSupport,
        BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
        BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
  }

  /**
   * Tests that we wait for transactions according to their validity duration. Waits for a
   * transaction that never arrives. The time waited should correspond to the validity duration of a
   * transaction, regardless of current time.
   */
  @Test
  public void testWaitTimeInWaitForInclusionInBlock() throws ApiException {
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    SentTransaction sentTransaction = client.signAndSend(transaction, 1);
    assertThat(sentTransaction.signedTransaction().getValidToTime())
        .isEqualTo(startTime + BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION);
    client.waitForInclusionInBlock(sentTransaction);

    timeSupport.assertHasWaitedWithoutResult(
        BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION);
  }

  /** Send a transaction with a valid signature. */
  @Test
  void sendValidTransaction() throws ApiException {
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    SentTransaction sentTransaction = client.signAndSend(transaction, 1);

    // validate the transaction by recovering the sender
    SignedTransaction signedTransaction = sentTransaction.signedTransaction();
    byte[] bytes = SafeDataOutputStream.serialize(signedTransaction::write);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(bytes);
    String chainId = ((ChainControllerApi) new ChainControllerStub()).getChain(null).getChainId();
    SignedTransaction recoveredTransaction = SignedTransaction.read(chainId, stream);
    BlockchainAddress recoveredSender = recoveredTransaction.recoverSender();

    assertThat(recoveredSender).isEqualTo(authentication.getAddress());
  }

  /**
   * Tests that the time waited for a transaction is only dependent on the validity duration at the
   * time of creation. As such, we expect to wait a total of 3 minutes with the default duration
   * regardless of changes to the transaction validity duration, post signing.
   */
  @Test
  void transactionsAreAffectedByValidityDuration() throws ApiException {
    BlockchainTransactionClient client =
        BlockchainTransactionClient.createForTest(
            new ShardAndChainControllerTransactionClient(
                createChainControllerWithDefaultPointer(), createShardController()),
            SenderAuthenticationKeyPair.fromString(TestObjects.PRIVATE_KEY),
            () -> startTime,
            timeSupport,
            Integer.MIN_VALUE,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);

    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    SentTransaction sentTransaction = client.signAndSend(transaction, 1);

    // Set new validity duration for future transactions
    client.waitForInclusionInBlock(sentTransaction);

    timeSupport.assertHasWaitedWithoutResult(Integer.MIN_VALUE);
  }

  /**
   * {@link BlockchainTransactionClient} can be created directly from the underlying API
   * controllers.
   */
  @Test
  public void createFromControllers() throws ApiException {
    ChainControllerApi chainController = new ChainControllerStub();
    ShardControllerApi shardController = createShardController();
    ShardAndChainControllerTransactionClient transactionClient =
        new ShardAndChainControllerTransactionClient(chainController, shardController);
    BlockchainTransactionClient expectedClient =
        BlockchainTransactionClient.createForTest(
            transactionClient,
            authentication,
            System::currentTimeMillis,
            ConditionWaiterImpl.create(),
            BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
    BlockchainTransactionClient created =
        BlockchainTransactionClient.create(chainController, shardController, authentication);

    assertThat(created).usingRecursiveComparison().isEqualTo(expectedClient);
  }

  @DisplayName("Will continue sending transactions until the blockchain accepts the transaction")
  @Test
  void sendTransactionFourTimesBeforeGoingThrough() throws ApiException {
    final Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    final AtomicInteger timeSupportCounter = new AtomicInteger();
    final ShardControllerStub shardController = new ShardControllerStub(null);
    final ChainControllerStub chainController = new ChainControllerStub();
    final SignedTransaction signedTransaction =
        SignedTransaction.create(
            authentication,
            chainController.getAccount("unused", 0L).getNonce(),
            1_000L,
            1,
            chainController.getChain(0L).getChainId(),
            transaction);
    ExecutedTransaction executedTransaction =
        new ExecutedTransaction().identifier(Hash.fromString("aa".repeat(32)));
    CurrentTime timeSupportStub =
        () -> {
          int callCounter = timeSupportCounter.getAndIncrement();
          if (callCounter == 8) {
            chainController.addTransactionPointerForTransaction(
                new SerializedTransaction()
                    .payload(SafeDataOutputStream.serialize(signedTransaction::write)));
            shardController.addTransaction(executedTransaction);
          }
          return 1L;
        };
    BlockchainTransactionClient client =
        setupTransactionClient(
            chainController, shardController, timeSupportStub, new ConditionWaiterTestStub());
    ExecutedTransaction actualTransaction =
        client.ensureInclusionInBlock(
            transaction, 1, Instant.ofEpochMilli(1_000L), Duration.ofSeconds(1));
    assertThat(actualTransaction).isEqualTo(executedTransaction);
    assertThat(timeSupportCounter.get()).isEqualTo(10);
  }

  @DisplayName("Sending a failing transaction without retrying results in an exception.")
  @Test
  void sendTransactionWithoutRetrying() {
    final Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    BlockchainTransactionClient client =
        setupTransactionClient(
            new ChainControllerStub(),
            new ShardControllerStub(null),
            () -> startTime,
            new ConditionWaiterTestStub());
    assertThatThrownBy(() -> client.signAndSend(transaction, 100L))
        .isInstanceOf(BadRequestException.class)
        .hasMessage("The received transaction is invalid in the current state of the blockchain.");
  }

  @Test
  @DisplayName(
      "BlockchainTransactionClient can be created directly from a host URL, and an authentication"
          + " provider.")
  public void createFromHostUrl() throws ApiException {
    final BlockchainTransactionClient created =
        BlockchainTransactionClient.create("https://example.org", authentication);
    assertThat(created).isNotNull();
  }

  @Test
  public void waitForEmptyEvents() throws ApiException {
    var transactionWithoutInnerEvent =
        new ExecutedTransaction()
            .executionStatus(new ExecutionStatus().success(true).finalized(true).events(List.of()))
            .isEvent(true);
    ShardAndChainControllerTransactionClient transactionClientInner =
        new ShardAndChainControllerTransactionClient(
            new ChainControllerStub(), createShardController(transactionWithoutInnerEvent));
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            transactionClientInner,
            authentication,
            () -> 0,
            new ConditionWaiterTestStub(),
            100,
            100);

    TransactionTree executedTransactionTree =
        transactionClient.waitForSpawnedEvents(transactionWithoutInnerEvent);

    assertThat(executedTransactionTree.events().isEmpty()).isTrue();
  }

  @Test
  public void waitForEvents() throws ApiException {
    var transactionWithoutInnerEvent =
        new ExecutedTransaction()
            .executionStatus(new ExecutionStatus().events(List.of()).finalized(true));

    TransactionPointer innerEvent =
        new TransactionPointer()
            .identifier(
                Hash.fromString("abababababababababababababababababababababababababababababababab"))
            .destinationShardId("Shard0");

    var transactionWithInnerEvent =
        new ExecutedTransaction()
            .executionStatus(new ExecutionStatus().events(List.of(innerEvent)).finalized(true));

    ShardAndChainControllerTransactionClient transactionClientInner =
        new ShardAndChainControllerTransactionClient(
            new ChainControllerStub(), createShardController(transactionWithoutInnerEvent));
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            transactionClientInner,
            authentication,
            () -> 0,
            new ConditionWaiterTestStub(),
            100,
            100);

    TransactionTree tree = transactionClient.waitForSpawnedEvents(transactionWithInnerEvent);
    assertThat(tree.events().isEmpty()).isFalse();
    assertThat(tree.events().get(0)).isEqualTo(transactionWithoutInnerEvent);
  }

  @Test
  public void waitForEventsOfSentTransaction() throws ApiException {
    var transactionWithoutInnerEvent =
        new ExecutedTransaction()
            .isEvent(false)
            .identifier(Hash.fromString("b".repeat(64)))
            .executionStatus(new ExecutionStatus().events(List.of()).finalized(true));

    TransactionPointer innerEvent =
        new TransactionPointer()
            .identifier(Hash.fromString("b".repeat(64)))
            .destinationShardId("Shard0");

    var transactionWithInnerEvent =
        new ExecutedTransaction()
            .content("a".repeat(64).getBytes(StandardCharsets.UTF_8))
            .identifier(Hash.fromString("a".repeat(64)))
            .executionStatus(new ExecutionStatus().events(List.of(innerEvent)).finalized(true));

    byte[] content = transactionWithInnerEvent.getContent();
    Transaction transaction =
        Transaction.create(
            BlockchainAddress.fromHash(
                BlockchainAddress.Type.CONTRACT_PUBLIC, transactionWithInnerEvent.getIdentifier()),
            content);

    ShardControllerStub shardController = new ShardControllerStub();
    shardController.addTransaction(transactionWithInnerEvent);
    shardController.addTransaction(transactionWithoutInnerEvent);

    ShardAndChainControllerTransactionClient transactionClientInner =
        new ShardAndChainControllerTransactionClient(
            createChainControllerWithDefaultPointer(), shardController);

    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            transactionClientInner,
            authentication,
            () -> 0,
            new ConditionWaiterTestStub(),
            100,
            100);

    SentTransaction sentTransaction = transactionClient.signAndSend(transaction, 100L);
    TransactionTree tree = transactionClient.waitForSpawnedEvents(sentTransaction);

    assertThat(tree.events().isEmpty()).isFalse();
    assertThat(tree.events().get(0)).isEqualTo(transactionWithoutInnerEvent);
  }

  @Test
  public void transactionValidityShouldBe123Milliseconds() throws ApiException {
    ShardAndChainControllerTransactionClient transactionClientInner =
        new ShardAndChainControllerTransactionClient(
            new ChainControllerStub(), createShardController());
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            transactionClientInner,
            authentication,
            () -> 0,
            new ConditionWaiterTestStub(),
            123,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);

    SignedTransaction signedTransaction =
        transactionClient.sign(Transaction.create(TestObjects.CONTRACT_ONE, new byte[] {0}), 0);
    assertThat(signedTransaction.getValidToTime()).isEqualTo(123);
  }

  @Test
  public void createSignedTransaction() throws ApiException {
    ShardAndChainControllerTransactionClient transactionClientInner =
        new ShardAndChainControllerTransactionClient(
            new ChainControllerStub(), createShardController());
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            transactionClientInner,
            authentication,
            () -> 0,
            new ConditionWaiterTestStub(),
            123,
            BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);

    SignedTransaction signedTransaction =
        transactionClient.sign(Transaction.create(TestObjects.CONTRACT_ONE, new byte[] {0}), 0);

    assertThat(signedTransaction.getNonce()).isEqualTo(765399);
    assertThat(signedTransaction.recoverSender()).isEqualTo(authentication.getAddress());
  }

  @Test
  public void errorInTxLookupGivesError() throws ApiException {
    TransactionPointer innerEvent =
        new TransactionPointer()
            .identifier(
                Hash.fromString("abababababababababababababababababababababababababababababababab"))
            .destinationShardId("Shard0");

    var transactionWithInnerEvent =
        new ExecutedTransaction()
            .executionStatus(new ExecutionStatus().events(List.of(innerEvent)));

    ConditionWaiter conditionWaiter =
        new ConditionWaiter() {
          @Override
          public <T> T waitForCondition(
              Supplier<T> valueSupplier,
              Predicate<T> valuePredicate,
              long timeoutMs,
              Supplier<String> errorMessage) {
            throw new RuntimeException(errorMessage.get());
          }
        };

    ShardAndChainControllerTransactionClient transactionClientInner =
        new ShardAndChainControllerTransactionClient(
            new ChainControllerStub(), createShardController());
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            transactionClientInner,
            authentication,
            () -> 0,
            conditionWaiter,
            BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
            1);

    assertThatThrownBy(() -> transactionClient.waitForSpawnedEvents(transactionWithInnerEvent))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining("was not included in a block at shard")
        .hasMessageContaining(innerEvent.getIdentifier().toString());
  }

  /** WaitForInclusionInBlock throws an error if the waited upon transaction never gets executed. */
  @Test
  void waitOnPending() throws ApiException {
    var pendingTransaction = new ExecutedTransaction();

    assertWaitingOnFailingTransaction(pendingTransaction);
  }

  /**
   * WaitForInclusionInBlock throws an error if the waited upon transaction never gets finalized.
   */
  @Test
  void waitOnNonFinalized() throws ApiException {
    var nonFinalized =
        new ExecutedTransaction().executionStatus(new ExecutionStatus().finalized(false));

    assertWaitingOnFailingTransaction(nonFinalized);
  }

  /**
   * Asserts that waitForInclusionInBlock fails when returning failingTransaction.
   *
   * @param failingTransaction the executed transaction that the api stub returns.
   */
  private void assertWaitingOnFailingTransaction(ExecutedTransaction failingTransaction)
      throws ApiException {
    ConditionWaiter conditionWaiter =
        new ConditionWaiter() {
          @Override
          public <T> T waitForCondition(
              Supplier<T> valueSupplier,
              Predicate<T> valuePredicate,
              long timeoutMs,
              Supplier<String> errorMessage) {
            T value = valueSupplier.get();
            if (!valuePredicate.test(value)) {
              throw new RuntimeException(errorMessage.get());
            }
            return value;
          }
        };

    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);
    ShardAndChainControllerTransactionClient transactionClientInner =
        new ShardAndChainControllerTransactionClient(
            createChainControllerWithDefaultPointer(), createShardController(failingTransaction));
    BlockchainTransactionClient transactionClient =
        BlockchainTransactionClient.createForTest(
            transactionClientInner,
            authentication,
            () -> 0,
            conditionWaiter,
            BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
            1);

    SentTransaction sentTransaction = client.signAndSend(transaction, 1);

    assertThatThrownBy(() -> transactionClient.waitForInclusionInBlock(sentTransaction))
        .isInstanceOf(RuntimeException.class)
        .hasMessageContaining(
            "Transaction 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' "
                + "was not included in a block at shard 'Shard0'");
  }

  /** A signed and sent transaction is included in a block immediately. */
  @Test
  void transactionIsIncludedImmediately() throws ApiException {
    ExecutedTransaction expectedTransaction =
        new ExecutedTransaction().executionStatus(new ExecutionStatus().finalized(true));
    long startTime = 1_000_000L;
    BlockchainTransactionClient txclient =
        setupTransactionClient(
            createChainControllerWithDefaultPointer(),
            new ShardControllerStub(expectedTransaction),
            () -> startTime,
            getOrThrowConditionWaiter());
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    ExecutedTransaction actualTransaction =
        txclient.ensureInclusionInBlock(transaction, 1, Instant.ofEpochMilli(startTime + 1));
    assertThat(actualTransaction).isEqualTo(expectedTransaction);
  }

  /**
   * We wait for a transaction to be included in a block that is slow to be finalized, before
   * retrying.
   */
  @Test
  void transactionIsIncludedInSlowBlock() throws ApiException {
    ExecutedTransaction expectedTransaction = new ExecutedTransaction();
    long startTime = 1_000_000L;
    long beforeTime = startTime + 500L;
    AtomicInteger waitConditionCalls = new AtomicInteger(0);
    ShardControllerStub innerClient = new ShardControllerStub(expectedTransaction);
    ConditionWaiter customWaiter =
        new ConditionWaiter() {
          @Override
          public <T> T waitForCondition(
              Supplier<T> valueSupplier,
              Predicate<T> valuePredicate,
              long timeoutMs,
              Supplier<String> errorMessage) {
            int numberOfCalls = waitConditionCalls.incrementAndGet();
            if (numberOfCalls == 1) { // signAndSend transaction
              return valueSupplier.get();
            } else if (numberOfCalls == 2) { // Waiting for transaction
              throw new RuntimeException(errorMessage.get());
            } else if (numberOfCalls == 3) { // Waiting for latest block
              innerClient.setProductionTimeOfLatestFinalBlock(beforeTime - 1);
              T blockState = valueSupplier.get();
              assertThat(valuePredicate.test(blockState)).isFalse();

              innerClient.setProductionTimeOfLatestFinalBlock(beforeTime);
              blockState = valueSupplier.get();
              assertThat(valuePredicate.test(blockState)).isFalse();

              innerClient.setProductionTimeOfLatestFinalBlock(beforeTime + 1);
              blockState = valueSupplier.get();
              assertThat(valuePredicate.test(blockState)).isTrue();

              return valueSupplier.get();
            } else {
              throw new RuntimeException("Should not have happened!");
            }
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(
            createChainControllerWithDefaultPointer(), innerClient, () -> startTime, customWaiter);
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    ExecutedTransaction actualTransaction =
        client.ensureInclusionInBlock(transaction, 1, Instant.ofEpochMilli(beforeTime));
    assertThat(actualTransaction).isEqualTo(expectedTransaction);
  }

  /** A transaction is re-signed and sent if first attempt to include fails. */
  @Test
  void transactionInclusionIsRetried() throws ApiException {
    ExecutedTransaction expectedTransaction =
        new ExecutedTransaction().identifier(Hash.fromString("aa".repeat(32)));
    long startTime = 1_000_000L;
    long beforeTime = startTime + 500L;
    long blockWaitTime = 600_000;
    AtomicInteger waitConditionCalls = new AtomicInteger(0);
    ShardControllerStub innerClient = new ShardControllerStub(null);
    ConditionWaiter customWaiter =
        new ConditionWaiter() {
          @Override
          public <T> T waitForCondition(
              Supplier<T> valueSupplier,
              Predicate<T> valuePredicate,
              long timeoutMs,
              Supplier<String> errorMessage) {
            int numberOfCalls = waitConditionCalls.incrementAndGet();

            if (numberOfCalls == 1) { // signAndSend transaction first time
              return valueSupplier.get();
            } else if (numberOfCalls == 2) { // Waiting for transaction
              throw new RuntimeException(errorMessage.get());
            } else if (numberOfCalls == 3) { // Waiting for latest block
              assertThat(timeoutMs).isEqualTo(beforeTime + 600_000);
              return valueSupplier.get();
            } else if (numberOfCalls == 4) { // signAndSend transaction second time
              return valueSupplier.get();
            } else if (numberOfCalls == 5) { // Waiting for transaction
              innerClient.addTransaction(expectedTransaction);
              return valueSupplier.get();
            } else {
              throw new RuntimeException("Should not have happened!");
            }
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(
            createChainControllerWithDefaultPointer(), innerClient, () -> startTime, customWaiter);
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    ExecutedTransaction actualTransaction =
        client.ensureInclusionInBlock(
            transaction, 1, Instant.ofEpochMilli(beforeTime), Duration.ofMillis(blockWaitTime));
    assertThat(actualTransaction).isEqualTo(expectedTransaction);
  }

  /** If we are unable to get latest block before timeout we do not retry sending again. */
  @Test
  void latestBlockFailureAbortsResending() {
    ExecutedTransaction expectedTransaction = new ExecutedTransaction();
    long startTime = 1_000_000L;
    BlockchainTransactionClient client =
        setupTransactionClient(
            createChainControllerWithDefaultPointer(),
            new ShardControllerStub(expectedTransaction),
            () -> startTime,
            getOrThrowConditionWaiter());
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    Instant before = Instant.ofEpochMilli(startTime + 500);
    assertThatThrownBy(() -> client.ensureInclusionInBlock(transaction, 1, before))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Unable to get block with production time greater than 1000500 for shard"
                + " ShardId[name=Shard0]");
  }

  @DisplayName("Will only try to resend transactions until include before.")
  @Test
  void willOnlyRetrySendingTransactionUntilIncludeBefore() {
    AtomicInteger timeSupportCalls = new AtomicInteger(0);
    CurrentTime customTime =
        () -> {
          int numberOfCalls = timeSupportCalls.incrementAndGet();
          if (numberOfCalls <= 3) { // First round
            return startTime;
          } else if (numberOfCalls <= 5) { // second round
            return startTime + 500;
          } else {
            throw new RuntimeException("Should not happen!");
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(
            new ChainControllerStub(),
            new ShardControllerStub(),
            customTime,
            getOrAssertTimeoutConditionWaiter(500));
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    Instant before = Instant.ofEpochMilli(startTime + 500);
    assertThatThrownBy(() -> client.ensureInclusionInBlock(transaction, 1, before))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Unable to push transaction to chain");
  }

  /** If the applicable until time limit expires, we do not retry sending transaction. */
  @Test
  void applicableUntilExpires() {
    long startTime = 1_000_000L;
    long beforeTime = startTime + 500L;
    AtomicInteger timeSupportCalls = new AtomicInteger(0);
    ShardControllerStub innerClient = new ShardControllerStub(null);
    innerClient.setProductionTimeOfLatestFinalBlock(beforeTime + 1);
    CurrentTime customTime =
        () -> {
          int numberOfCalls = timeSupportCalls.incrementAndGet();
          if (numberOfCalls <= 3) { // First round
            return startTime;
          } else if (numberOfCalls <= 8) { // second round
            return beforeTime - 1;
          } else if (numberOfCalls == 9) {
            return beforeTime;
          } else {
            throw new RuntimeException("Should not happen!");
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(
            createChainControllerWithDefaultPointer(),
            innerClient,
            customTime,
            getOrThrowConditionWaiter());
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    assertThatThrownBy(
            () -> client.ensureInclusionInBlock(transaction, 1, Instant.ofEpochMilli(beforeTime)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transaction was not included in block before 1970-01-01T00:16:40.500Z");
  }

  /** If applicable until time is in the past we do not try to sign and send. */
  @Test
  void applicableUntilTimeInThePast() {
    long startTime = 1_000_000L;
    long beforeTime = startTime + 500L;
    AtomicInteger timeSupportCalls = new AtomicInteger(0);
    CurrentTime customTime =
        () -> {
          int numberOfCalls = timeSupportCalls.incrementAndGet();
          if (numberOfCalls == 1) {
            return beforeTime + 1500;
          } else {
            throw new RuntimeException("Should not happen!");
          }
        };
    BlockchainTransactionClient client =
        setupTransactionClient(
            new ChainControllerStub(),
            new ShardControllerStub(null),
            customTime,
            getOrThrowConditionWaiter());
    Transaction transaction = Transaction.create(TestObjects.CONTRACT_ONE, new byte[42]);

    assertThatThrownBy(
            () -> client.ensureInclusionInBlock(transaction, 1, Instant.ofEpochMilli(beforeTime)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transaction was not included in block before 1970-01-01T00:16:40.500Z");
  }

  private BlockchainTransactionClient setupTransactionClient(
      ChainControllerApi chainController,
      ShardControllerApi shardController,
      CurrentTime time,
      ConditionWaiter conditionWaiter) {
    return BlockchainTransactionClient.createForTest(
        new ShardAndChainControllerTransactionClient(chainController, shardController),
        authentication,
        time,
        conditionWaiter,
        BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
        BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT);
  }

  private ConditionWaiter getOrAssertTimeoutConditionWaiter(long timeout) {
    return new ConditionWaiter() {
      @Override
      public <T> T waitForCondition(
          Supplier<T> valueSupplier,
          Predicate<T> valuePredicate,
          long timeoutMs,
          Supplier<String> errorMessage) {
        T value = valueSupplier.get();
        if (valuePredicate.test(value)) {
          return value;
        } else {
          assertThat(timeoutMs).isEqualTo(timeout);
          throw new RuntimeException(errorMessage.get());
        }
      }
    };
  }

  private ConditionWaiter getOrThrowConditionWaiter() {
    return new ConditionWaiter() {
      @Override
      public <T> T waitForCondition(
          Supplier<T> valueSupplier,
          Predicate<T> valuePredicate,
          long timeoutMs,
          Supplier<String> errorMessage) {
        T value = valueSupplier.get();
        if (valuePredicate.test(value)) {
          return value;
        } else {
          throw new RuntimeException(errorMessage.get());
        }
      }
    };
  }

  static final class ChainControllerStub extends ChainControllerApi {

    private HashMap<Hash, TransactionPointer> transactions;
    private TransactionPointer defaultTransactionPointer;

    ChainControllerStub() {
      this(null);
    }

    ChainControllerStub(TransactionPointer defaultTransactionPointer) {
      this.defaultTransactionPointer = defaultTransactionPointer;
      this.transactions = new HashMap<>();
    }

    @Override
    public Account getAccount(String address, Long blockTime) {
      return new Account().nonce(765399L);
    }

    @Override
    public Chain getChain(Long blockTime) {
      return new Chain().chainId("Shard0");
    }

    void addTransactionPointerForTransaction(SerializedTransaction transaction) {
      transactions.put(
          Hash.create(s -> s.write(transaction.getPayload())),
          new TransactionPointer()
              .identifier(Hash.fromString("a".repeat(64)))
              .destinationShardId("Shard0"));
    }

    @Override
    public TransactionPointer putTransaction(SerializedTransaction serializedTransaction) {
      TransactionPointer transactionPointer =
          transactions.getOrDefault(
              Hash.create(s -> s.write(serializedTransaction.getPayload())),
              defaultTransactionPointer);
      if (transactionPointer == null) {
        throw new BadRequestException(
            "The received transaction is invalid in the current state of the blockchain.");
      }
      return transactionPointer;
    }
  }

  static final class ShardControllerStub extends ShardControllerApi {
    private HashMap<String, ExecutedTransaction> transactions;
    private ExecutedTransaction defaultTransaction;
    private long latestBlockProductionTime;

    ShardControllerStub() {
      this(null);
    }

    ShardControllerStub(ExecutedTransaction transaction) {
      this.transactions = new HashMap<>();
      this.defaultTransaction = transaction;
      this.latestBlockProductionTime = 0;
    }

    void addTransaction(ExecutedTransaction transaction) {
      transactions.put(transaction.getIdentifier().toString(), transaction);
    }

    @Override
    public ExecutedTransaction getTransaction(String shardId, String transactionId)
        throws ApiException {
      ExecutedTransaction orDefault = transactions.getOrDefault(transactionId, defaultTransaction);
      if (orDefault == null) {
        throw new ApiException("404 not found");
      }
      return orDefault;
    }

    public void setProductionTimeOfLatestFinalBlock(long productionTime) {
      this.latestBlockProductionTime = productionTime;
    }

    @Override
    public Block getLatestBlock(String shardId) {
      return new Block().productionTime(latestBlockProductionTime);
    }
  }
}
