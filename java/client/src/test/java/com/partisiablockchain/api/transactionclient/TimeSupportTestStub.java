package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Time support that has a static clock and increments it by adding its sleep duration. It also
 * records number of times slept.
 */
public final class TimeSupportTestStub implements ConditionWaiterImpl.TimeSupport {

  private long now;

  public int numberOfSleeps;

  /** Creates a new time support for testing. */
  public TimeSupportTestStub(long now) {
    this.now = now;
    this.numberOfSleeps = 0;
  }

  @Override
  public long now() {
    return now;
  }

  @Override
  public void sleep(long ms) {
    numberOfSleeps++;
    now += ms;
  }
}
