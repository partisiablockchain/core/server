package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.api.transactionclient.model.TransactionPointer;
import com.partisiablockchain.crypto.Hash;
import org.junit.jupiter.api.Test;

/** Tests for SentTransaction. */
public final class SentTransactionTest {

  @Test
  public void sentTransactionWrapsTransactionPointerPair() {
    SignedTransaction someSignedTransaction =
        SignedTransactionTest.toSigned(Transaction.create(TestObjects.ACCOUNT_ONE, new byte[99]));
    TransactionPointer somePointer =
        new TransactionPointer()
            .identifier(
                Hash.fromString("0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a0a"))
            .destinationShardId("Shard0");
    SentTransaction sentTransaction = new SentTransaction(someSignedTransaction, somePointer);
    assertThat(sentTransaction.signedTransaction()).isEqualTo(someSignedTransaction);
    assertThat(sentTransaction.transactionPointer()).isEqualTo(somePointer);
  }
}
