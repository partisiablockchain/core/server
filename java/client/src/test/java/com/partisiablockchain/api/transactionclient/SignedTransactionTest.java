package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.TestObjects;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import org.junit.jupiter.api.Test;

/** Test. */
public final class SignedTransactionTest {

  private static final SenderAuthentication keyPair =
      new SenderAuthenticationKeyPair(TestObjects.KEY_ONE);
  private static final String chainId = "SpecificChainIDLocatable";

  @Test
  public void equalsAndHashCode() {
    // Create control
    Transaction transaction = Transaction.create(TestObjects.ACCOUNT_ONE, new byte[99]);
    SignedTransaction control = SignedTransaction.create(keyPair, 2, 3, 2, chainId, transaction);
    SignedTransaction controlReference = control;
    SignedTransaction structurallyEquivalent =
        SignedTransaction.create(keyPair, 2, 3, 2, chainId, transaction);

    SignedTransaction differentInAllAspects =
        SignedTransaction.create(
            keyPair,
            42,
            42,
            42,
            "chanId",
            Transaction.create(TestObjects.ACCOUNT_ONE, new byte[98]));
    SignedTransaction differentChainId =
        SignedTransaction.create(keyPair, 2, 3, 2, "chanId", transaction);

    SignedTransaction.InnerPart inner = new SignedTransaction.InnerPart(2, 3, 2, transaction);
    SignedTransaction sameSignatureDifferentHash =
        new SignedTransaction(
            keyPair.sign(SafeDataOutputStream.serialize(inner::write), chainId),
            Hash.create(stream -> stream.writeString("abc")),
            inner);

    Object object = new Object();
    SignedTransaction nullTransaction = null;

    assertThat(control.equals(controlReference)).isTrue();
    assertThat(control.equals(structurallyEquivalent)).isTrue();
    assertThat(control.hashCode()).isEqualTo(structurallyEquivalent.hashCode());
    assertThat(control.equals(differentInAllAspects)).isFalse();
    assertThat(control.hashCode()).isNotEqualTo(differentInAllAspects.hashCode());
    assertThat(control.equals(differentChainId)).isFalse();
    assertThat(control.equals(sameSignatureDifferentHash)).isFalse();
    assertThat(control.equals(object)).isFalse();
    assertThat(control.equals(nullTransaction)).isFalse();
  }

  @Test
  public void innerEqualsAndHashCode() {
    Transaction transaction = Transaction.create(TestObjects.ACCOUNT_ONE, new byte[99]);
    SignedTransaction.InnerPart control = new SignedTransaction.InnerPart(0, 0, 0, transaction);
    SignedTransaction.InnerPart controlReference = control;
    SignedTransaction.InnerPart structurallyEquivalent =
        new SignedTransaction.InnerPart(0, 0, 0, transaction);
    SignedTransaction.InnerPart differentNonce =
        new SignedTransaction.InnerPart(42, 0, 0, transaction);
    SignedTransaction.InnerPart differentValidToTime =
        new SignedTransaction.InnerPart(0, 42, 0, transaction);
    SignedTransaction.InnerPart differentCost =
        new SignedTransaction.InnerPart(0, 0, 42, transaction);
    SignedTransaction.InnerPart differentTransaction =
        new SignedTransaction.InnerPart(
            0, 0, 0, Transaction.create(TestObjects.ACCOUNT_ONE, new byte[98]));

    Object object = new Object();
    SignedTransaction.InnerPart nullInner = null;

    assertThat(control.equals(controlReference)).isTrue();
    assertThat(control.equals(structurallyEquivalent)).isTrue();
    assertThat(control.hashCode()).isEqualTo(structurallyEquivalent.hashCode());
    assertThat(control.equals(differentNonce)).isFalse();
    assertThat(control.hashCode()).isNotEqualTo(differentNonce.hashCode());
    assertThat(control.equals(differentValidToTime)).isFalse();
    assertThat(control.equals(differentCost)).isFalse();
    assertThat(control.equals(differentTransaction)).isFalse();
    assertThat(control.equals(object)).isFalse();
    assertThat(control.equals(nullInner)).isFalse();
  }

  @Test
  public void hashInteraction() {
    Transaction dto = Transaction.create(TestObjects.CONTRACT_ONE, new byte[88]);

    SignedTransaction signedDto = toSigned(dto);
    assertThat(signedDto.getHash().toString())
        .isEqualTo("1327ca0532f47d508886e4268a32eb7f55449913ec8c92bec036b675771c16a7");
  }

  @Test
  public void innerSenderPays() {
    byte[] rpc = new byte[88];
    Transaction expected = Transaction.create(TestObjects.CONTRACT_ONE, rpc);

    SignedTransaction signedDto = toSigned(expected);
    Transaction actual = signedDto.getInner();
    assertThat(actual).isSameAs(expected);

    assertThat(actual.getAddress()).isEqualTo(TestObjects.CONTRACT_ONE);
    assertThat(actual.getRpc()).containsExactly(rpc);
  }

  @Test
  public void innerContractPays() {
    byte[] rpc = new byte[88];
    Transaction expected = Transaction.create(TestObjects.CONTRACT_TWO, rpc);

    SignedTransaction signedDto = toSigned(expected);
    Transaction actual = signedDto.getInner();
    assertThat(actual).isSameAs(expected);

    assertThat(actual.getAddress()).isEqualTo(TestObjects.CONTRACT_TWO);
    assertThat(actual.getRpc()).containsExactly(rpc);
  }

  @Test
  public void serializeInteraction() {
    Transaction interact = Transaction.create(TestObjects.ACCOUNT_ONE, new byte[99]);
    SignedTransaction dto = toSigned(interact);

    SafeDataInputStream stream =
        SafeDataInputStream.createFromBytes(SafeDataOutputStream.serialize(dto::write));

    SignedTransaction read = SignedTransaction.read(chainId, stream);
    assertThat(read).usingRecursiveComparison().isEqualTo(dto);

    assertThat(
            Transaction.read(
                SafeDataInputStream.createFromBytes(SafeDataOutputStream.serialize(interact))))
        .usingRecursiveComparison()
        .isEqualTo(interact);
  }

  static SignedTransaction toSigned(Transaction dto) {
    return SignedTransaction.create(keyPair, 2, 3, 2, chainId, dto);
  }

  @Test
  public void identifier() {
    SignedTransaction dto = toSigned(Transaction.create(TestObjects.ACCOUNT_ONE, new byte[99]));

    assertThat(dto.identifier().toString())
        .isEqualTo("dcc528ceaaafdb06c8312bd2aed5cdf257e1f9a277ad708eea9dff107aef575d");
  }

  /** Can get the gas cost of a signed transaction. */
  @Test
  public void gasCost() {
    SignedTransaction dto = toSigned(Transaction.create(TestObjects.ACCOUNT_ONE, new byte[99]));
    assertThat(dto.getGasCost()).isEqualTo(2L);
  }
}
