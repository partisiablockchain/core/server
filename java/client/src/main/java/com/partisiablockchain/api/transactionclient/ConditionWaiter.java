package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Generic wait interface allowing the creation of asynchronous calls and wait for these. The user
 * must provide a value getter and a value predicate, the value getter returns the current value,
 * the value predicate whether the value is "complete" and the wait is over.
 */
public interface ConditionWaiter {

  /**
   * Waits for the condition dictated by the input predicate until it is met. Times out by if the
   * condition is not met within a specified time-out. Sleeps in an incrementing fashion between
   * checks of the condition until a maximum of 30 seconds between iterations to avoid busy-waiting.
   *
   * @param <T> The type of the values that the condition waits for.
   * @param valueSupplier The supplier of (possibly) updated values for the condition.
   * @param valuePredicate The condition that the value must meet before waiting is over.
   * @param timeoutMs The time to wait for in milliseconds.
   * @param errorMessage The lazy initialized error message to throw in case of timing out.
   * @return The final value returned by the supplier, which meets the condition.
   * @throws RuntimeException If timing out.
   */
  <T> T waitForCondition(
      Supplier<T> valueSupplier,
      Predicate<T> valuePredicate,
      long timeoutMs,
      Supplier<String> errorMessage);

  /**
   * Waits for the condition dictated by the input predicate until it is met. Times out by if the
   * condition is not met within a specified time-out. Sleeps in an incrementing fashion between
   * checks of the condition until a maximum of 30 seconds between iterations to avoid busy-waiting.
   *
   * @param <T> The type of the values that the condition waits for.
   * @param valueSupplier The supplier of (possibly) updated values for the condition.
   * @param valuePredicate The condition that the value must meet before waiting is over.
   * @param timeoutMs The time to wait for in milliseconds.
   * @param errorMessage The error message to throw in case of timing out.
   * @return The final value returned by the supplier, which meets the condition.
   * @throws RuntimeException If timing out.
   */
  default <T> T waitForCondition(
      Supplier<T> valueSupplier, Predicate<T> valuePredicate, long timeoutMs, String errorMessage) {
    return waitForCondition(valueSupplier, valuePredicate, timeoutMs, () -> errorMessage);
  }
}
