package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import com.partisiablockchain.api.transactionclient.model.SerializedTransaction;
import com.partisiablockchain.api.transactionclient.model.TransactionPointer;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import com.partisiablockchain.crypto.Hash;

/**
 * A client for getting the information to create a transaction, and send it after creation and
 * signing.
 */
public interface BlockchainClientForTransaction {

  /**
   * Get the chain ID for the running chain.
   *
   * @return the chain ID string.
   * @throws ApiException on io error
   */
  String getChainId() throws ApiException;

  /**
   * Get the current nonce to use when signing a transaction for the given account.
   *
   * @param blockchainAddress the address to get the nonce for.
   * @return the current nonce to use for signing.
   * @throws ApiException on io error
   */
  long getNonce(BlockchainAddress blockchainAddress) throws ApiException;

  /**
   * Retrieve an executed transaction from the blockchain.
   *
   * @param shardId The name of the shard to interact with.
   * @param transactionIdentifier The transaction hash.
   * @return The retrieved transaction.
   * @throws ApiException on io error
   */
  ExecutedTransaction getTransaction(ShardId shardId, Hash transactionIdentifier)
      throws ApiException;

  /**
   * Get the production time from the latest created block of a specific shard.
   *
   * @param shardId the id of the shard.
   * @return the production time of the latest created block.
   * @throws ApiException on io error
   */
  long getLatestProductionTime(ShardId shardId) throws ApiException;

  /**
   * Send a signed transaction to the chain and return a pointer to the transaction.
   *
   * @param serializedTransaction the transaction to send.
   * @return a pointer to the transaction on chain.
   * @throws ApiException on io error
   */
  TransactionPointer putTransaction(SerializedTransaction serializedTransaction)
      throws ApiException;
}
