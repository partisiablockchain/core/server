package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Arrays;
import java.util.Objects;

/** A transaction for a contract on the blockchain with an RPC payload. */
public final class Transaction implements DataStreamSerializable {
  private final BlockchainAddress address;
  private final byte[] rpc;

  private Transaction(BlockchainAddress address, byte[] rpc) {
    this.address = address;
    this.rpc = rpc;
  }

  /**
   * Create a new interaction.
   *
   * @param address the target contract
   * @param rpc the rpc for the interaction
   * @return the created interaction
   */
  public static Transaction create(BlockchainAddress address, byte[] rpc) {
    return new Transaction(address, rpc);
  }

  /**
   * Get the address of this interaction.
   *
   * @return the address
   */
  public BlockchainAddress getAddress() {
    return address;
  }

  /**
   * Get rpc of this interaction.
   *
   * @return the rpc
   */
  public byte[] getRpc() {
    return rpc.clone();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transaction that = (Transaction) o;
    return address.equals(that.address) && Arrays.equals(rpc, that.rpc);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(address);
    result = 31 * result + Arrays.hashCode(rpc);
    return result;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    address.write(stream);
    stream.writeInt(rpc.length);
    stream.write(rpc);
  }

  /**
   * Read a transaction from stream.
   *
   * @param stream the stream to read from
   * @return the read transaction
   */
  public static Transaction read(SafeDataInputStream stream) {
    BlockchainAddress contractId = BlockchainAddress.read(stream);
    byte[] payload = stream.readBytes(stream.readInt());

    return create(contractId, payload);
  }
}
