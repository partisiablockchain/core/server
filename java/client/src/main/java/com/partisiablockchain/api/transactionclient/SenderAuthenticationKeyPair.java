package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.KeyPair;
import com.partisiablockchain.crypto.Signature;
import java.math.BigInteger;
import java.util.Objects;

/**
 * Sender authentication based on a key-pair, consisting of a public and private key. The sender
 * address is computed from the public key.
 */
public final class SenderAuthenticationKeyPair implements SenderAuthentication {
  private final KeyPair keyPair;

  /**
   * Creates authentication for a sender.
   *
   * @param keyPair The public and private key of the sender. Not nullable.
   */
  public SenderAuthenticationKeyPair(KeyPair keyPair) {
    this.keyPair = Objects.requireNonNull(keyPair);
  }

  /**
   * Creates a sender's authentication from a private key.
   *
   * @param privateKey The private key to create a key pair from, written as a value in hexidecimal.
   * @return authentication for a transaction sender.
   */
  public static SenderAuthenticationKeyPair fromString(String privateKey) {
    return new SenderAuthenticationKeyPair(new KeyPair(new BigInteger(privateKey, 16)));
  }

  @Override
  public BlockchainAddress getAddress() {
    return keyPair.getPublic().createAddress();
  }

  @Override
  public Signature sign(byte[] transactionPayload, String chainId) {
    Hash hash = SignedTransaction.calculateHash(transactionPayload, chainId);
    return keyPair.sign(hash);
  }

  /**
   * Get the key-pair.
   *
   * @return The key pair. Never null.
   */
  public KeyPair getKeyPair() {
    return keyPair;
  }
}
