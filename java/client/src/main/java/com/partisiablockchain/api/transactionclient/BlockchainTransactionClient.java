package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.api.ShardControllerApi;
import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import com.partisiablockchain.api.transactionclient.model.SerializedTransaction;
import com.partisiablockchain.api.transactionclient.model.TransactionPointer;
import com.partisiablockchain.api.transactionclient.utils.ApiClient;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import com.partisiablockchain.crypto.Hash;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.ExceptionConverter;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** A client that supports signing, sending and waiting for transactions on the blockchain. */
public final class BlockchainTransactionClient {

  private static final Logger logger = LoggerFactory.getLogger(BlockchainTransactionClient.class);

  /** Authentication of the sender that will sign transactions. */
  private final SenderAuthentication authentication;

  private final BlockchainClientForTransaction transactionClient;

  /** Support for working with time (now and sleeping). */
  private final CurrentTime timeSupport;

  private final ConditionWaiter waiter;

  /** The number of milliseconds a signed transaction is valid for inclusion in a block. */
  private final long transactionValidityDuration;

  /**
   * The number of milliseconds the client will wait for a spawned event to be included in a block
   * before timing out.
   */
  private final long spawnedEventTimeout;

  /**
   * Default signed transaction vilidity time in milliseconds. This is three minutes in
   * milliseconds.
   */
  public static final long DEFAULT_TRANSACTION_VALIDITY_DURATION = 180_000L;

  /**
   * Default timeout for spawned event to be inclusion in a block. This is ten minutes in
   * milliseconds.
   */
  public static final long DEFAULT_SPAWNED_EVENT_TIMEOUT = 600_000L;

  private BlockchainTransactionClient(
      BlockchainClientForTransaction transactionClient,
      SenderAuthentication authentication,
      CurrentTime timeSupport,
      ConditionWaiter waiter,
      long transactionValidityDuration,
      long spawnedEventTimeout) {
    this.transactionClient = Objects.requireNonNull(transactionClient);
    this.authentication = Objects.requireNonNull(authentication);
    this.timeSupport = Objects.requireNonNull(timeSupport);
    this.waiter = Objects.requireNonNull(waiter);
    this.transactionValidityDuration = transactionValidityDuration;
    this.spawnedEventTimeout = spawnedEventTimeout;
  }

  /**
   * Create {@link BlockchainTransactionClient}.
   *
   * @param chainController the client used for communicating with the chain api
   * @param shardController the client used for communicating with the shard api
   * @param authentication authentication of the sender that will sign transactions
   * @return A {@link BlockchainTransactionClient}
   */
  public static BlockchainTransactionClient create(
      ChainControllerApi chainController,
      ShardControllerApi shardController,
      SenderAuthentication authentication) {
    return create(
        chainController,
        shardController,
        authentication,
        DEFAULT_TRANSACTION_VALIDITY_DURATION,
        DEFAULT_SPAWNED_EVENT_TIMEOUT);
  }

  /**
   * Create a {@link BlockchainTransactionClient}.
   *
   * @param chainController the client used for communicating with the chain api
   * @param shardController the client used for communicating with the shard api
   * @param authentication authentication of the sender that will sign transactions
   * @param transactionValidityInMillis the number of milliseconds a signed transaction is valid for
   *     inclusion in a block.
   * @param spawnedEventTimeoutInMillis the number of milliseconds the client will wait for a
   *     spawned event to be included in a block before timing out.
   * @return A {@link BlockchainTransactionClient}
   */
  public static BlockchainTransactionClient create(
      ChainControllerApi chainController,
      ShardControllerApi shardController,
      SenderAuthentication authentication,
      long transactionValidityInMillis,
      long spawnedEventTimeoutInMillis) {
    return create(
        new ShardAndChainControllerTransactionClient(chainController, shardController),
        authentication,
        transactionValidityInMillis,
        spawnedEventTimeoutInMillis);
  }

  /**
   * Create a {@link BlockchainTransactionClient}.
   *
   * @param transactionClient the client used for communicating with the blockchain api
   * @param authentication authentication of the sender that will sign transactions
   * @param transactionValidityInMillis the number of milliseconds a signed transaction is valid for
   *     inclusion in a block.
   * @param spawnedEventTimeoutInMillis the number of milliseconds the client will wait for a
   *     spawned event to be included in a block before timing out.
   * @return A {@link BlockchainTransactionClient}
   */
  public static BlockchainTransactionClient create(
      BlockchainClientForTransaction transactionClient,
      SenderAuthentication authentication,
      long transactionValidityInMillis,
      long spawnedEventTimeoutInMillis) {
    return new BlockchainTransactionClient(
        transactionClient,
        authentication,
        CurrentTime.SYSTEM,
        ConditionWaiterImpl.create(),
        transactionValidityInMillis,
        spawnedEventTimeoutInMillis);
  }

  /**
   * Create a {@link BlockchainTransactionClient} with default validity duration and timeouts.
   *
   * @param baseUrl The base url of the server to be a client to.
   * @param authentication authentication of the sender that will sign transactions
   * @return A {@link BlockchainTransactionClient}
   */
  public static BlockchainTransactionClient create(
      String baseUrl, SenderAuthentication authentication) {
    return create(
        baseUrl,
        authentication,
        DEFAULT_TRANSACTION_VALIDITY_DURATION,
        DEFAULT_SPAWNED_EVENT_TIMEOUT);
  }

  /**
   * Create a {@link BlockchainTransactionClient}.
   *
   * @param baseUrl The base url of the server to be a client to.
   * @param authentication authentication of the sender that will sign transactions
   * @param transactionValidityInMillis the number of milliseconds a signed transaction is valid for
   *     inclusion in a block.
   * @param spawnedEventTimeoutInMillis the number of milliseconds the client will wait for a
   *     spawned event to be included in a block before timing out.
   * @return A {@link BlockchainTransactionClient}
   */
  public static BlockchainTransactionClient create(
      String baseUrl,
      SenderAuthentication authentication,
      long transactionValidityInMillis,
      long spawnedEventTimeoutInMillis) {
    final var apiClient = new ApiClient();
    apiClient.setBasePath(baseUrl);
    final ChainControllerApi chainControllerApi = new ChainControllerApi(apiClient);
    final ShardControllerApi shardControllerApi = new ShardControllerApi(apiClient);
    return create(
        chainControllerApi,
        shardControllerApi,
        authentication,
        transactionValidityInMillis,
        spawnedEventTimeoutInMillis);
  }

  static BlockchainTransactionClient createForTest(
      BlockchainClientForTransaction transactionClient,
      SenderAuthentication authentication,
      CurrentTime timeSupport,
      ConditionWaiter conditionWaiter,
      long transactionValidityDuration,
      long spawnedEventTimeout) {
    return new BlockchainTransactionClient(
        transactionClient,
        authentication,
        timeSupport,
        conditionWaiter,
        transactionValidityDuration,
        spawnedEventTimeout);
  }

  /**
   * Sign a transaction in preparation for sending it to the blockchain. The signed transaction
   * expires after {@link #transactionValidityDuration} millis or if the nonce of the signer
   * changes.
   *
   * @param transaction the transaction to sign
   * @param gasCost the amount of gas to allocate for executing the transaction.
   * @return a signed transaction corresponding to the passed transaction.
   * @throws ApiException on io error
   */
  public SignedTransaction sign(Transaction transaction, long gasCost) throws ApiException {
    return sign(transaction, gasCost, timeSupport.now() + transactionValidityDuration);
  }

  private SignedTransaction sign(Transaction transaction, long gasCost, long applicableUntil)
      throws ApiException {
    BlockchainAddress sender = authentication.getAddress();
    long nonce = transactionClient.getNonce(sender);
    String chainId = transactionClient.getChainId();
    long validToTime = timeSupport.now() + transactionValidityDuration;
    long effectiveValidToTime = Math.min(validToTime, applicableUntil);
    return SignedTransaction.create(
        authentication, nonce, effectiveValidToTime, gasCost, chainId, transaction);
  }

  /**
   * Sends a signed transaction to the blockchain for execution and inclusion in a block.
   *
   * @param signedTransaction the signed transaction to send.
   * @return a sent transaction corresponding to the signed transaction.
   * @throws ApiException on io error
   */
  public SentTransaction send(SignedTransaction signedTransaction) throws ApiException {
    byte[] bytes = SafeDataOutputStream.serialize(signedTransaction::write);

    SerializedTransaction serializedTransaction = new SerializedTransaction().payload(bytes);
    TransactionPointer transactionPointer = transactionClient.putTransaction(serializedTransaction);
    return new SentTransaction(signedTransaction, transactionPointer);
  }

  /**
   * Sign and send a transaction to the blockchain for execution.
   *
   * <p>Will not retry signing and sending if the transaction were rejected. This could be due to
   * reused nonce.
   *
   * @param transaction the transaction to sign and send
   * @param gasCost the amount of gas to allocate for executing the transaction.
   * @return a sent transaction corresponding to the signed transaction.
   * @throws ApiException on io error
   */
  public SentTransaction signAndSend(Transaction transaction, long gasCost) throws ApiException {
    return send(sign(transaction, gasCost, timeSupport.now() + transactionValidityDuration));
  }

  /**
   * Sign and sends a transaction to the blockchain for execution.
   *
   * @param transaction the transaction to sign and send
   * @param gasCost the amount of gas to allocate for executing the transaction
   * @return a sent transaction corresponding to the signed transaction, or null if and an exception
   *     was caught.
   */
  private SentTransaction signAndSend(Transaction transaction, long gasCost, long applicableUntil) {
    try {
      return send(sign(transaction, gasCost, applicableUntil));
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Waits for a sent transaction to be included in a block on the blockchain.
   *
   * @param sentTransaction a transaction that has been sent to the blockchain.
   * @return the executed state of the transaction.
   */
  public ExecutedTransaction waitForInclusionInBlock(SentTransaction sentTransaction) {
    ShardId senderShard = new ShardId(sentTransaction.transactionPointer().getDestinationShardId());
    Hash transactionIdentifier = sentTransaction.transactionPointer().getIdentifier();
    long validToTime = sentTransaction.signedTransaction().getValidToTime();
    long timeToWait = validToTime - timeSupport.now();

    return waitForTransaction(senderShard, transactionIdentifier, timeToWait);
  }

  /**
   * Recursively wait for the inclusion of all events spawned by the transaction and the
   * transaction's events.
   *
   * @param transaction an executed transaction.
   * @return the execution status for all recursive events spawned by the transaction.
   */
  public TransactionTree waitForSpawnedEvents(ExecutedTransaction transaction) {
    List<ExecutedTransaction> events = new ArrayList<>();
    ArrayDeque<TransactionPointer> pendingEvents =
        new ArrayDeque<>(transaction.getExecutionStatus().getEvents());
    while (!pendingEvents.isEmpty()) {
      TransactionPointer nextEvent = pendingEvents.removeFirst();
      Hash identifier = nextEvent.getIdentifier();
      final ShardId shardId = new ShardId(nextEvent.getDestinationShardId());
      ExecutedTransaction executed = waitForTransaction(shardId, identifier, spawnedEventTimeout);
      logger.debug(
          "Execution of {} spawned event {}",
          nextEvent,
          executed.getExecutionStatus().getEvents().size());
      events.add(executed);
      pendingEvents.addAll(executed.getExecutionStatus().getEvents());
    }
    return new TransactionTree(transaction, events);
  }

  /**
   * Recursively wait for the inclusion of all events spawned by the transaction and the
   * transaction's events.
   *
   * @param sentTransaction a transaction that has been sent to the blockchain.
   * @return the execution status for all recursive events spawned by the transaction.
   */
  public TransactionTree waitForSpawnedEvents(SentTransaction sentTransaction) {
    final ExecutedTransaction executedTransaction = waitForInclusionInBlock(sentTransaction);
    return waitForSpawnedEvents(executedTransaction);
  }

  /**
   * Try to ensure that the given transaction is included in a final block before the specified time
   * has passed. Will try to resend the transaction until it has been included in a block or the
   * specified time has passed. If the transaction has not been included, or we are unable to
   * determine inclusion, due to network issues, an exception is thrown.
   *
   * @param transaction The transaction to sign and send
   * @param transactionGasCost The amount of gas to allocate for executing the transaction
   * @param includeBefore The UTC timestamp after which the transaction will no longer tried to be
   *     sent
   * @return The executed state of the transaction
   * @throws ApiException on io error
   */
  public ExecutedTransaction ensureInclusionInBlock(
      Transaction transaction, long transactionGasCost, Instant includeBefore) throws ApiException {
    return ensureInclusionInBlock(
        transaction, transactionGasCost, includeBefore, Duration.ofMinutes(30));
  }

  /**
   * Try to ensure that the given transaction is included in a final block before the specified time
   * has passed. Will try to resend the transaction until it has been included in a block or the
   * specified time has passed. If the transaction has not been included, or we are unable to
   * determine inclusion, due to network issues, an exception is thrown.
   *
   * <p>To determine that a tried transaction has not been included in a block, we must wait until
   * the last valid block that could have included the transaction has been finalized. The caller of
   * this function must specify the amount of time to wait for the last valid block after validity
   * time of the tried transaction has expired.
   *
   * @param transaction The transaction to sign and send
   * @param transactionGasCost The amount of gas to allocate for executing the transaction
   * @param includeBefore The UTC timestamp after which the transaction will no longer tried to be
   *     sent
   * @param lastValidBlockWaitTime The amount of time to wait for the last valid block that a tried
   *     transaction can be included in
   * @return The executed state of the transaction
   * @throws ApiException on io error
   */
  public ExecutedTransaction ensureInclusionInBlock(
      Transaction transaction,
      long transactionGasCost,
      Instant includeBefore,
      Duration lastValidBlockWaitTime)
      throws ApiException {
    long applicableUntil = includeBefore.toEpochMilli();
    while (applicableUntil > timeSupport.now()) {
      SentTransaction sentTransaction =
          waiter.waitForCondition(
              () -> signAndSend(transaction, transactionGasCost, applicableUntil),
              Objects::nonNull,
              applicableUntil - timeSupport.now(),
              "Unable to push transaction to chain");
      ExecutedTransaction executedTransaction =
          waitForExecutedTransaction(sentTransaction, lastValidBlockWaitTime);
      if (executedTransaction != null) {
        return executedTransaction;
      }
    }
    throw new RuntimeException("Transaction was not included in block before " + includeBefore);
  }

  private ExecutedTransaction waitForExecutedTransaction(
      SentTransaction sentTransaction, Duration latestBlockWait) throws ApiException {
    try {
      return waitForInclusionInBlock(sentTransaction);
    } catch (Exception ignore) {
      return waitForLastPossibleBlockToInclude(sentTransaction, latestBlockWait);
    }
  }

  private ExecutedTransaction waitForLastPossibleBlockToInclude(
      SentTransaction sentTransaction, Duration latestBlockWait) throws ApiException {
    ShardId shardId = new ShardId(sentTransaction.transactionPointer().getDestinationShardId());
    long validToTime = sentTransaction.signedTransaction().getValidToTime();
    long timeout = latestBlockWait.toMillis() + validToTime;
    waiter.waitForCondition(
        () -> ExceptionConverter.call(() -> transactionClient.getLatestProductionTime(shardId)),
        (productionTime) -> productionTime > validToTime,
        timeout,
        () ->
            String.format(
                "Unable to get block with production time greater than %d for shard %s",
                validToTime, shardId));
    Hash transactionIdentifier = sentTransaction.transactionPointer().getIdentifier();
    try {
      return transactionClient.getTransaction(shardId, transactionIdentifier);
    } catch (Exception ignore) {
      return null;
    }
  }

  /**
   * Waits for the execution of a single transaction or event to be finalized.
   *
   * @param shardId the shard to wait at.
   * @param transactionIdentifier the identifier of the transaction to wait for.
   * @param timeout how long to wait for the transaction before throwing an exception (in
   *     milliseconds).
   * @return the transaction being waited upon, post-execution.
   */
  private ExecutedTransaction waitForTransaction(
      ShardId shardId, Hash transactionIdentifier, long timeout) {
    return waiter.waitForCondition(
        () ->
            ExceptionConverter.call(
                () -> transactionClient.getTransaction(shardId, transactionIdentifier)),
        tx -> tx.getExecutionStatus() != null && tx.getExecutionStatus().getFinalized(),
        timeout,
        () ->
            String.format(
                "Transaction '%s' was not included in a block at shard '%s'",
                transactionIdentifier, shardId.name()));
  }
}
