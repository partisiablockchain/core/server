package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Signature;

/**
 * Authentication for a sender of a transaction. Sending transactions requires a blockchain address
 * and the ability to sign messages.
 */
public interface SenderAuthentication {

  /**
   * Get the address of the sender.
   *
   * @return The blockchain address of the sender.
   */
  BlockchainAddress getAddress();

  /**
   * Sign a transaction.
   *
   * @param transactionPayload payload of the transaction
   * @param chainId the chain id
   * @return the signature of the message
   */
  Signature sign(byte[] transactionPayload, String chainId);
}
