package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import java.util.List;

/**
 * Status for a transaction and the whole tree of spawned events.
 *
 * @param transaction the transaction
 * @param events list of spawned events from the transaction
 */
public record TransactionTree(ExecutedTransaction transaction, List<ExecutedTransaction> events) {

  /**
   * Check if the executed transaction tree includes any transactions or events which did not
   * succeed.
   *
   * @return true if the transaction tree includes any failures, false if all succeeded.
   */
  public boolean hasFailures() {
    if (!transaction().getExecutionStatus().getSuccess()) {
      return true;
    }
    for (ExecutedTransaction executedEvent : events()) {
      if (!executedEvent.getExecutionStatus().getSuccess()) {
        return true;
      }
    }
    return false;
  }
}
