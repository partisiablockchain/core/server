package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.crypto.Signature;
import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import java.util.Objects;

/**
 * A transaction that has been signed by a private key. A signed transaction is ready to be sent to
 * the blockchain. A signed transaction has limited durability, since it includes information about
 * the valid-to-time and the next available nonce of the sending user.
 */
@SuppressWarnings("WeakerAccess")
public final class SignedTransaction implements DataStreamSerializable {

  private final InnerPart inner;
  private final Signature signature;
  private final Hash hash;

  /**
   * Constructor for testing only.
   *
   * @param signature The signature for this signedTransaction.
   * @param hash The hash of this signedTransaction.
   * @param inner The transaction that was signed.
   */
  SignedTransaction(Signature signature, Hash hash, InnerPart inner) {
    this.inner = inner;
    this.hash = hash;
    this.signature = signature;
  }

  /**
   * Create a signed transaction to send to PBC.
   *
   * @param senderAuthentication the signature provider to sign the transaction with.
   * @param nonce the nonce of the signing account.
   * @param validToTime the unix time that the transaction is valid to.
   * @param gasCost the amount of gas allocated to executing the transaction.
   * @param chainId the id of the chain.
   * @param transaction the transaction to sign.
   * @return the signed transaction.
   */
  public static SignedTransaction create(
      SenderAuthentication senderAuthentication,
      long nonce,
      long validToTime,
      long gasCost,
      String chainId,
      Transaction transaction) {
    InnerPart inner = new InnerPart(nonce, validToTime, gasCost, transaction);
    Signature signature =
        senderAuthentication.sign(SafeDataOutputStream.serialize(inner::write), chainId);
    Hash hash = calculateHash(inner, chainId);
    return new SignedTransaction(signature, hash, inner);
  }

  /**
   * Get the sender of the transaction. The sender address is recovered from the signature and
   * message hash.
   *
   * @return The senders address
   */
  public BlockchainAddress recoverSender() {
    return this.getSignature().recoverSender(this.getHash());
  }

  /**
   * Calculates the SHA256 hash for this transaction.
   *
   * @param chainId the id of the chain
   * @return the hash that is used when signing a transaction.
   */
  private static Hash calculateHash(InnerPart inner, String chainId) {
    return calculateHash(SafeDataOutputStream.serialize(inner::write), chainId);
  }

  /**
   * Calculate the SHA256 hash for a transaction payload and a chain id.
   *
   * @param transactionPayload a serialized transaction payload
   * @param chainId the id of the chain
   * @return the hash that is used when signing a transaction.
   */
  public static Hash calculateHash(byte[] transactionPayload, String chainId) {
    return Hash.create(
        stream -> {
          stream.write(transactionPayload);
          stream.writeString(chainId);
        });
  }

  /**
   * Get the hash of this transaction.
   *
   * @return the hash
   */
  public Hash getHash() {
    return hash;
  }

  @Override
  public void write(SafeDataOutputStream stream) {
    signature.write(stream);
    inner.write(stream);
  }

  /**
   * Read a signed transaction from the specified stream.
   *
   * @param chainId the id of the chain to read from
   * @param stream the stream to read from
   * @return signed transaction read from stream
   */
  public static SignedTransaction read(String chainId, SafeDataInputStream stream) {
    Signature signature = Signature.read(stream);
    InnerPart inner = InnerPart.read(stream);
    return new SignedTransaction(signature, calculateHash(inner, chainId), inner);
  }

  /**
   * Get the identifier of this transaction.
   *
   * @return the identify hash
   */
  public Hash identifier() {
    return Hash.create(
        stream -> {
          hash.write(stream);
          signature.write(stream);
        });
  }

  /**
   * Get the nonce of this transaction.
   *
   * @return the nonce
   */
  public long getNonce() {
    return inner.nonce;
  }

  /**
   * Get the valid-to-time of this transaction. If the transaction is not included in a block before
   * this time it will fail.
   *
   * @return the valid to time as a unix timestamp.
   */
  public long getValidToTime() {
    return inner.validToTime;
  }

  /**
   * Get the inner transaction of this transaction.
   *
   * @return the inner
   */
  public Transaction getInner() {
    return inner.innerTransaction;
  }

  /**
   * Get the gas cost of this transaction.
   *
   * @return the gas cost
   */
  public long getGasCost() {
    return inner.gasCost;
  }

  /**
   * Get the signature of this transaction.
   *
   * @return the signature
   */
  public Signature getSignature() {
    return signature;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SignedTransaction that = (SignedTransaction) o;
    return inner.equals(that.inner) && signature.equals(that.signature) && hash.equals(that.hash);
  }

  @Override
  public int hashCode() {
    return Objects.hash(inner, signature, hash);
  }

  static final class InnerPart implements DataStreamSerializable {

    public final long nonce;
    public final long validToTime;
    public final long gasCost;
    public final Transaction innerTransaction;

    InnerPart(long nonce, long validToTime, long gasCost, Transaction innerTransaction) {

      this.nonce = nonce;
      this.validToTime = validToTime;
      this.gasCost = gasCost;
      this.innerTransaction = innerTransaction;
    }

    @Override
    public void write(SafeDataOutputStream stream) {
      stream.writeLong(nonce);
      stream.writeLong(validToTime);
      stream.writeLong(gasCost);
      innerTransaction.write(stream);
    }

    static InnerPart read(SafeDataInputStream stream) {
      long nonce = stream.readLong();
      long validToTime = stream.readLong();
      long gasCost = stream.readLong();
      Transaction transaction = Transaction.read(stream);
      return new InnerPart(nonce, validToTime, gasCost, transaction);
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      InnerPart innerPart = (InnerPart) o;
      return nonce == innerPart.nonce
          && validToTime == innerPart.validToTime
          && gasCost == innerPart.gasCost
          && innerTransaction.equals(innerPart.innerTransaction);
    }

    @Override
    public int hashCode() {
      return Objects.hash(nonce, validToTime, gasCost, innerTransaction);
    }
  }
}
