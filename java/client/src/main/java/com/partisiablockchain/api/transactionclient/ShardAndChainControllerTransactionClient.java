package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.api.transactionclient.api.ChainControllerApi;
import com.partisiablockchain.api.transactionclient.api.ShardControllerApi;
import com.partisiablockchain.api.transactionclient.model.ExecutedTransaction;
import com.partisiablockchain.api.transactionclient.model.SerializedTransaction;
import com.partisiablockchain.api.transactionclient.model.TransactionPointer;
import com.partisiablockchain.api.transactionclient.utils.ApiException;
import com.partisiablockchain.crypto.Hash;

/** Transaction client using the shard and chain controller APIs. */
public final class ShardAndChainControllerTransactionClient
    implements BlockchainClientForTransaction {

  private final ChainControllerApi chainController;
  private final ShardControllerApi shardController;

  /**
   * Create transaction client.
   *
   * @param chainController the client used for communicating with the chain api
   * @param shardController the client used for communicating with the shard api
   */
  public ShardAndChainControllerTransactionClient(
      ChainControllerApi chainController, ShardControllerApi shardController) {
    this.chainController = chainController;
    this.shardController = shardController;
  }

  @Override
  public String getChainId() throws ApiException {
    return chainController.getChain(null).getChainId();
  }

  @Override
  public long getNonce(BlockchainAddress blockchainAddress) throws ApiException {
    return chainController.getAccount(blockchainAddress.writeAsString(), null).getNonce();
  }

  @Override
  public ExecutedTransaction getTransaction(ShardId shardId, Hash transactionIdentifier)
      throws ApiException {
    return shardController.getTransaction(shardId.name(), transactionIdentifier.toString());
  }

  @Override
  public long getLatestProductionTime(ShardId shardId) throws ApiException {
    return shardController.getLatestBlock(shardId.name()).getProductionTime();
  }

  @Override
  public TransactionPointer putTransaction(SerializedTransaction serializedTransaction)
      throws ApiException {
    return chainController.putTransaction(serializedTransaction);
  }
}
