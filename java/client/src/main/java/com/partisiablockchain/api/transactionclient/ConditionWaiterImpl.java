package com.partisiablockchain.api.transactionclient;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.tools.coverage.ExceptionConverter;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Can wait for a condition to become fulfilled by repeatedly sleeping the current thread between
 * checks.
 */
public final class ConditionWaiterImpl implements ConditionWaiter {

  private static final Logger logger = LoggerFactory.getLogger(ConditionWaiterImpl.class);
  private final TimeSupport timeSupport;

  /**
   * Creates a new condition waiter.
   *
   * @param timeSupport Support for time (current time and sleep). Not nullable.
   */
  private ConditionWaiterImpl(TimeSupport timeSupport) {
    this.timeSupport = Objects.requireNonNull(timeSupport);
  }

  /**
   * Creates a new condition waiter using the system time and system sleep.
   *
   * @return The transaction waiter.
   */
  public static ConditionWaiter create() {
    return new ConditionWaiterImpl(TimeSupport.SYSTEM);
  }

  /**
   * Creates a new condition waiter.
   *
   * @param timeSupport Support for time (current time and sleep). Not nullable.
   * @return The transaction waiter.
   */
  static ConditionWaiter createForTest(TimeSupport timeSupport) {
    return new ConditionWaiterImpl(timeSupport);
  }

  @Override
  public <T> T waitForCondition(
      Supplier<T> valueSupplier,
      Predicate<T> valuePredicate,
      long timeoutMs,
      Supplier<String> errorMessage) {
    long end = timeSupport.now() + timeoutMs;
    int increasingSleep = 0;
    long remainingTime;
    do {
      T value = null;
      try {
        value = valueSupplier.get();
      } catch (Exception e) {
        logger.trace("Error updating condition, retrying in a moment", e);
      }
      if (value != null && valuePredicate.test(value)) {
        return value;
      }
      increasingSleep++;
      long sleepDurationMs = Math.min(30000, 100 * increasingSleep);
      remainingTime = Math.max(0, end - timeSupport.now());
      long effectiveSleepDuration = Math.min(sleepDurationMs, remainingTime);
      ExceptionConverter.run(() -> timeSupport.sleep(effectiveSleepDuration), "Sleep interrupted");
    } while (remainingTime > 0);
    throw new RuntimeException(errorMessage.get());
  }

  /** Support for getting the current time and sleeping. */
  interface TimeSupport {

    /** Time support that uses the underlying operating system. */
    TimeSupport SYSTEM =
        new TimeSupport() {
          @Override
          public long now() {
            return System.currentTimeMillis();
          }

          @Override
          public void sleep(long ms) throws Exception {
            Thread.sleep(ms);
          }
        };

    /**
     * Gets the current time in milliseconds.
     *
     * @return The current time in milliseconds.
     */
    long now();

    /**
     * Pauses execution of the current program for some measure of time.
     *
     * @param ms The time to stop execution in milliseconds.
     * @throws Exception An exception in case of failure when pausing the current execution.
     */
    void sleep(long ms) throws Exception;
  }
}
