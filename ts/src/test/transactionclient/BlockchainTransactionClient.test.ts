/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  BlockchainTransactionClient,
  ChainControllerApi,
  ConditionWaiter,
  ConditionWaiterImpl,
  Configuration,
  ExecutedTransaction,
  ExecutionStatus,
  SenderAuthenticationKeyPair,
  SerializedTransaction,
  ShardControllerApi,
  SignedTransaction,
  Transaction,
  TransactionPointer,
} from "../../main";
import { ShardAndChainControllerTransactionClient } from "../../main/transactionclient/ShardAndChainControllerTransactionClient";
import { TestObjects } from "./TestObjects";
import {
  ChainControllerStub,
  ConditionWaiterStub,
  getOrAssertTimeoutConditionWaiter,
  GetOrThrowConditionWaiter,
  ShardControllerStub,
} from "./TestStubs";

const authentication = SenderAuthenticationKeyPair.fromString("ab");
const startTime = 1_000_000;

/** Will wait forever if transaction never is included. */
test("waiting forever for inclusion in block", async () => {
  const waiter = new ConditionWaiterStub();
  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerStub({ identifier: "a".repeat(64), destinationShardId: "Shard0" }),
    new ShardControllerStub()
  );
  const client = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
    BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT,
    () => startTime,
    waiter
  );
  const transaction: Transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(42) };
  const sentTransaction = await client.signAndSend(transaction, 1);
  expect(sentTransaction.signedTransaction.getValidToTime()).toEqual(
    startTime + BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION
  );
  await client.waitForInclusionInBlock(sentTransaction);

  waiter.assertHasWaitedWithoutResult(
    BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION
  );
});

/** Waiting for spawned events where there are no events returns transaction tree with only the original transaction. */
test("wait for empty events", async () => {
  const transactionWithoutInnerEvent: ExecutedTransaction = {
    executionStatus: executionStatus(),
    isEvent: true,
    content: "",
    identifier: "ff".repeat(32),
  };
  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerStub(),
    new ShardControllerStub()
  );
  const client = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    100,
    100,
    () => 0,
    new ConditionWaiterStub()
  );

  const executedTransactionTree = await client.waitForSpawnedEvents(transactionWithoutInnerEvent);

  expect(executedTransactionTree.transaction).toEqual(transactionWithoutInnerEvent);
  expect(executedTransactionTree.events.length).toEqual(0);
});

/** Waiting for spawned events where there are events returns transaction tree with the original transaction and the spawned events. */
test("wait for events", async () => {
  const transactionWithoutInnerEvent: ExecutedTransaction = {
    executionStatus: executionStatus(),
    isEvent: true,
    content: "",
    identifier: "ff".repeat(32),
  };

  const innerEvent: TransactionPointer = {
    identifier: "abababababababababababababababababababababababababababababababab",
    destinationShardId: "Shard0",
  };

  const transactionWithInnerEvent: ExecutedTransaction = {
    executionStatus: executionStatus(innerEvent),
    isEvent: false,
    content: "",
    identifier: "00".repeat(32),
  };

  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerStub(),
    new ShardControllerStub(transactionWithoutInnerEvent)
  );
  const client = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    100,
    100,
    () => 0,
    new ConditionWaiterStub()
  );

  const executedTransactionTree = await client.waitForSpawnedEvents(transactionWithInnerEvent);

  expect(executedTransactionTree.transaction).toEqual(transactionWithInnerEvent);
  expect(executedTransactionTree.events.length).toEqual(1);
  expect(executedTransactionTree.events[0]).toEqual(transactionWithoutInnerEvent);
});

/** When waiting for spawned events from a sentTransaction, we will also wait for that transaction. */
test("wait for events of sent transaction", async () => {
  const transactionWithoutInnerEvent: ExecutedTransaction = {
    executionStatus: executionStatus(),
    isEvent: true,
    content: "",
    identifier: "bb".repeat(32),
  };

  const innerEvent: TransactionPointer = {
    identifier: "bb".repeat(32),
    destinationShardId: "Shard0",
  };

  const transactionWithInnerEvent: ExecutedTransaction = {
    executionStatus: executionStatus(innerEvent),
    isEvent: false,
    content: "aa".repeat(32),
    identifier: "aa".repeat(32),
  };

  const transaction: Transaction = {
    address: "02aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    rpc: Buffer.from("aa".repeat(32), "hex"),
  };

  const shardController = new ShardControllerStub();
  shardController.addTransaction(transactionWithInnerEvent);
  shardController.addTransaction(transactionWithoutInnerEvent);

  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerStub({ identifier: "a".repeat(64), destinationShardId: "Shard0" }),
    shardController
  );
  const client = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    100,
    100,
    () => 0,
    new ConditionWaiterStub()
  );

  const sentTransaction = await client.signAndSend(transaction, 100);

  const executedTransactionTree = await client.waitForSpawnedEvents(sentTransaction);

  expect(executedTransactionTree.transaction).toEqual(transactionWithInnerEvent);
  expect(executedTransactionTree.events.length).toEqual(1);
  expect(executedTransactionTree.events[0]).toEqual(transactionWithoutInnerEvent);
});

/** Can create a signed transaction. */
test("create signed transaction", async () => {
  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerStub(),
    new ShardControllerStub()
  );
  const client = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    123,
    BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT,
    () => 0,
    new ConditionWaiterStub()
  );

  const gasCost = 5000;
  const transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(1) };
  const signedTransaction = await client.sign(transaction, gasCost);
  expect(signedTransaction.getValidToTime()).toEqual(123);
  expect(signedTransaction.getGasCost()).toEqual(gasCost);
  expect(signedTransaction.getNonce()).toEqual(765399);
  expect(signedTransaction.getInner()).toEqual(transaction);
});

/** When unable to get transaction after waiting, throws error. */
test("error in tx lookup gives error", async () => {
  const innerEvent: TransactionPointer = {
    identifier: "bb".repeat(32),
    destinationShardId: "Shard0",
  };

  const transactionWithInnerEvent: ExecutedTransaction = {
    executionStatus: executionStatus(innerEvent),
    isEvent: false,
    content: "aa".repeat(32),
    identifier: "aa".repeat(32),
  };

  const waiter: ConditionWaiter = {
    /** @inheritdoc */
    waitForCondition<T>(
      _valueSupplier: () => Promise<T>,
      _valuePredicate: (value: T) => boolean,
      _timeoutMs: number,
      errorMessage: () => string
    ): Promise<T> {
      throw new Error(errorMessage());
    },
  };

  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerStub(),
    new ShardControllerStub(transactionWithInnerEvent)
  );
  const client = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
    BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT,
    () => 0,
    waiter
  );

  await expect(() => client.waitForSpawnedEvents(transactionWithInnerEvent)).rejects.toThrow(
    `Transaction '${innerEvent.identifier}' was not included in a block at shard '${innerEvent.destinationShardId}'`
  );
});

/** Sending transactions has a backoff of 1 sec before sending again. */
test("Sending transaction has a backoff of 1 sec.", async () => {
  const transaction: Transaction = {
    address: "02aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    rpc: Buffer.from("aa".repeat(32), "hex"),
  };

  const expectedTx: ExecutedTransaction = {
    executionStatus: executionStatus(),
    isEvent: false,
    content: "aa".repeat(32),
    identifier: "aa".repeat(32),
  };

  const chainControllerStub = new ChainControllerStub();
  const shardControllerStub = new ShardControllerStub(expectedTx);
  let currentTimeCalls = 0;
  const signedTransaction = SignedTransaction.create(
    authentication,
    765399,
    1000,
    1,
    "Chain",
    transaction
  );
  const bytes = (await signedTransaction).serialize();

  const serializedTransaction: SerializedTransaction = { payload: bytes.toString("base64") };
  const customTime = () => {
    const numberOfCalls = ++currentTimeCalls;
    if (numberOfCalls == 9) {
      chainControllerStub.addTransactionPointerForTransaction({ serializedTransaction });
      shardControllerStub.addTransaction(expectedTx);
    }

    return 1;
  };
  const client = setupTransactionClient(
    chainControllerStub,
    shardControllerStub,
    new ConditionWaiterStub(),
    customTime
  );
  const actualTx = await client.ensureInclusionInBlock(transaction, 1, 1000, 1000);
  expect(actualTx).toEqual(expectedTx);
});

/** Creating tx client through create call uses default validity, timeout, currentTime and condition waiter. */
test("create transaction client", () => {
  const client = BlockchainTransactionClient.create("http://localhost", authentication);
  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerApi(new Configuration({ basePath: "http://localhost" })),
    new ShardControllerApi(new Configuration({ basePath: "http://localhost" }))
  );
  const control = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
    BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT,
    () => new Date().getTime(),
    ConditionWaiterImpl.create()
  );
  expect(JSON.stringify(client)).toEqual(JSON.stringify(control));
});

/** Creating tx client through create call with validity and timeout uses default currentTime and condition waiter. */
test("create transaction client with validity and timeout", () => {
  const client = BlockchainTransactionClient.create("http://localhost", authentication, 100, 200);
  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerApi(new Configuration({ basePath: "http://localhost" })),
    new ShardControllerApi(new Configuration({ basePath: "http://localhost" }))
  );
  const control = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    100,
    200,
    () => new Date().getTime(),
    ConditionWaiterImpl.create()
  );
  expect(JSON.stringify(client)).toEqual(JSON.stringify(control));
});

/** Creating tx client using custom blockchain client for transaction. */
test("create transaction client using custom blockchain client", () => {
  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerApi(new Configuration({ basePath: "http://localhost" })),
    new ShardControllerApi(new Configuration({ basePath: "http://localhost" }))
  );
  const client = BlockchainTransactionClient.createForCustomApi(
    transactionClient,
    authentication,
    100,
    200
  );
  const control = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    100,
    200,
    () => new Date().getTime(),
    ConditionWaiterImpl.create()
  );
  expect(JSON.stringify(client)).toEqual(JSON.stringify(control));
});

/** Creating tx client using custom blockchain client for transaction with default validity and event timeout. */
test("create transaction client using custom blockchain client with default validity and timeout", () => {
  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerApi(new Configuration({ basePath: "http://localhost" })),
    new ShardControllerApi(new Configuration({ basePath: "http://localhost" }))
  );
  const client = BlockchainTransactionClient.createForCustomApi(transactionClient, authentication);
  const control = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
    BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT,
    () => new Date().getTime(),
    ConditionWaiterImpl.create()
  );
  expect(JSON.stringify(client)).toEqual(JSON.stringify(control));
});

test("test default system time", () => {
  jest.useFakeTimers();
  jest.setSystemTime(500);
  expect(BlockchainTransactionClient.DEFAULT_CURRENT_TIME()).toBe(500);
});

/** WaitForInclusionInBlock throws an error if the waited upon transaction never gets executed. */
test("wait on pending transaction", async () => {
  const pendingTransaction: ExecutedTransaction = {
    isEvent: false,
    content: "",
    identifier: "aa".repeat(32),
  };

  await assertWaitingOnFailingTransaction(pendingTransaction);
});

/**
 * WaitForInclusionInBlock throws an error if the waited upon transaction never gets finalized.
 */
test("wait on non finalized transaction", async () => {
  const nonFinalized: ExecutedTransaction = {
    executionStatus: {
      success: true,
      finalized: false,
      events: [],
      transactionCost: {
        allocatedForEvents: 0,
        cpu: 0,
        networkFees: {},
        paidByContract: 0,
        remaining: 0,
      },
      blockId: "0",
    },
    isEvent: false,
    content: "",
    identifier: "aa".repeat(32),
  };

  await assertWaitingOnFailingTransaction(nonFinalized);
});

/**
 * Asserts that waitForInclusionInBlock fails when returning failingTransaction.
 * @param failingTransaction the executed transaction that the api stub returns.
 */
async function assertWaitingOnFailingTransaction(failingTransaction: ExecutedTransaction) {
  const conditionWaiter: ConditionWaiter = {
    /** @inheritdoc */
    async waitForCondition<T>(
      valueSupplier: () => Promise<T>,
      valuePredicate: (value: T) => boolean,
      timeoutMs: number,
      errorMessage: () => string
    ): Promise<T> {
      const value = await valueSupplier();
      if (!valuePredicate(value)) {
        throw new Error(errorMessage());
      }
      return value;
    },
  };
  const transactionClient = new ShardAndChainControllerTransactionClient(
    new ChainControllerStub({ identifier: "a".repeat(64), destinationShardId: "Shard0" }),
    new ShardControllerStub(failingTransaction)
  );
  const txClient = BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
    BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT,
    () => startTime,
    conditionWaiter
  );
  const transaction: Transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(42) };
  const sentTransaction = await txClient.signAndSend(transaction, 1);
  await expect(() => txClient.waitForInclusionInBlock(sentTransaction)).rejects.toThrow(
    "Transaction 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa' " +
      "was not included in a block at shard 'Shard0'"
  );
}

// Feature: Testing ensureInclusionInBlock

/** A signed and sent transaction is included in a block immediately. */
test("transaction is include immediately", async () => {
  const expectedTx: ExecutedTransaction = {
    executionStatus: executionStatus(),
    isEvent: false,
    content: "",
    identifier: "aa".repeat(32),
  };
  const client = setupTransactionClient(
    new ChainControllerStub({ identifier: "a".repeat(64), destinationShardId: "Shard0" }),
    new ShardControllerStub(expectedTx),
    new GetOrThrowConditionWaiter()
  );
  const transaction: Transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(42) };

  const actualTx = await client.ensureInclusionInBlock(transaction, 1, startTime + 1);
  expect(actualTx).toEqual(expectedTx);
});

/**
 * We wait for a transaction to be included in a block that is slow to be finalized, before
 * retrying.
 */
test("transaction is included in slow block", async () => {
  const expectedTx: ExecutedTransaction = {
    executionStatus: executionStatus(),
    isEvent: false,
    content: "",
    identifier: "aa".repeat(32),
  };
  const beforeTime = startTime + 500;
  let waitConditionCalls = 0;
  const shardController = new ShardControllerStub(expectedTx);
  const customWaiter: ConditionWaiter = {
    /** @inheritdoc */
    async waitForCondition<T>(
      valueSupplier: () => Promise<T>,
      valuePredicate: (value: T) => boolean,
      timeoutMs: number,
      errorMessage: () => string
    ): Promise<T> {
      const numberOfCalls = ++waitConditionCalls;

      if (numberOfCalls === 1) {
        // signAndSendRetryable transaction
        return valueSupplier();
      } else if (numberOfCalls === 2) {
        // Waiting for transaction
        throw new Error(errorMessage());
      } else if (numberOfCalls == 3) {
        // Waiting for latest block
        shardController.setProductionTimeOfLatestFinalBlock(beforeTime - 1);
        let blockState: T = await valueSupplier();
        expect(valuePredicate(blockState)).toEqual(false);

        shardController.setProductionTimeOfLatestFinalBlock(beforeTime);
        blockState = await valueSupplier();
        expect(valuePredicate(blockState)).toEqual(false);

        shardController.setProductionTimeOfLatestFinalBlock(beforeTime + 1);
        blockState = await valueSupplier();
        expect(valuePredicate(blockState)).toEqual(true);

        return valueSupplier();
      } else {
        throw new Error("Should not have happened");
      }
    },
  };
  const client = setupTransactionClient(
    new ChainControllerStub({ identifier: "a".repeat(64), destinationShardId: "Shard0" }),
    shardController,
    customWaiter
  );
  const transaction: Transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(42) };

  const actualTx = await client.ensureInclusionInBlock(transaction, 1, beforeTime);
  expect(actualTx).toEqual(expectedTx);
});

/** A transaction is re-signed and sent if first attempt to include fails. */
test("transaction inclusion is retried", async () => {
  const expectedTx: ExecutedTransaction = {
    executionStatus: executionStatus(),
    isEvent: false,
    content: "",
    identifier: "aa".repeat(32),
  };
  const beforeTime = startTime + 500;
  const blockWaitTime = 600_000;
  let waitConditionCalls = 0;
  const shardController = new ShardControllerStub();
  const customWaiter: ConditionWaiter = {
    /** @inheritdoc */
    async waitForCondition<T>(
      valueSupplier: () => Promise<T>,
      valuePredicate: (value: T) => boolean,
      timeoutMs: number,
      errorMessage: () => string
    ): Promise<T> {
      const numberOfCalls = ++waitConditionCalls;
      if (numberOfCalls === 1) {
        // signAndSendRetryable transaction
        return valueSupplier();
      } else if (numberOfCalls === 2) {
        // Waiting for transaction
        throw new Error(errorMessage());
      } else if (numberOfCalls == 3) {
        // Waiting for latest block
        expect(timeoutMs).toEqual(beforeTime + 600_000);
        return valueSupplier();
      } else if (numberOfCalls === 4) {
        // signAndSendRetryable transaction
        return valueSupplier();
      } else if (numberOfCalls == 5) {
        // Waiting for transaction
        shardController.addTransaction(expectedTx);
        return valueSupplier();
      } else {
        throw new Error("Should not have happened");
      }
    },
  };
  const client = setupTransactionClient(
    new ChainControllerStub({ identifier: "a".repeat(64), destinationShardId: "Shard0" }),
    shardController,
    customWaiter
  );
  const transaction: Transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(42) };

  const actualTx = await client.ensureInclusionInBlock(transaction, 1, beforeTime, blockWaitTime);
  expect(actualTx).toEqual(expectedTx);
});

/** If we are unable to get latest block before timeout we do not retry sending again. */
test("latest block failure aborts resending", async () => {
  const expectedTx: ExecutedTransaction = {
    executionStatus: executionStatus(),
    isEvent: false,
    content: "",
    identifier: "aa".repeat(32),
  };
  const beforeTime = startTime + 500;
  const shardController = new ShardControllerStub(expectedTx);
  let waitConditionCalls = 0;
  const customWaiter: ConditionWaiter = {
    /** @inheritdoc */
    async waitForCondition<T>(
      valueSupplier: () => Promise<T>,
      _valuePredicate: (value: T) => boolean,
      _timeoutMs: number,
      errorMessage: () => string
    ): Promise<T> {
      const callCounter = ++waitConditionCalls;
      if (callCounter == 1) {
        return valueSupplier();
      }
      throw new Error(errorMessage());
    },
  };
  const client = setupTransactionClient(
    new ChainControllerStub({ identifier: "a".repeat(64), destinationShardId: "Shard0" }),
    shardController,
    customWaiter
  );
  const transaction: Transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(42) };

  await expect(client.ensureInclusionInBlock(transaction, 1, beforeTime)).rejects.toThrow(
    "Unable to get block with production time greater than 1000500 for shard" + " Shard0"
  );
});

/** If the applicable until time limit expires, we do not retry sending transaction. */
test("applicable until expires", async () => {
  const beforeTime = startTime + 500;

  const shardController = new ShardControllerStub();
  shardController.setProductionTimeOfLatestFinalBlock(beforeTime + 1);
  let currentTimeCalls = 0;
  const customTime = () => {
    const numberOfCalls = ++currentTimeCalls;
    if (numberOfCalls <= 3) {
      // First round
      return startTime;
    } else if (numberOfCalls <= 8) {
      // second round
      return beforeTime - 1;
    } else if (numberOfCalls == 9) {
      return beforeTime;
    } else {
      throw new Error("Should not happen!");
    }
  };

  const client = setupTransactionClient(
    new ChainControllerStub({ identifier: "a".repeat(64), destinationShardId: "Shard0" }),
    shardController,
    new GetOrThrowConditionWaiter(),
    customTime
  );
  const transaction: Transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(42) };

  await expect(client.ensureInclusionInBlock(transaction, 1, beforeTime)).rejects.toThrow(
    "Transaction was not included in block before 1000500"
  );
});

/** If applicable until time is in the past we do not try to sign and send. */
test("applicable until expires", async () => {
  const beforeTime = startTime + 500;

  let currentTimeCalls = 0;
  const customTime = () => {
    const numberOfCalls = ++currentTimeCalls;
    if (numberOfCalls == 1) {
      return beforeTime + 1500;
    } else {
      throw new Error("Should not happen!");
    }
  };

  const client = setupTransactionClient(
    new ChainControllerStub({ identifier: "a".repeat(64), destinationShardId: "Shard0" }),
    new ShardControllerStub(),
    new GetOrThrowConditionWaiter(),
    customTime
  );
  const transaction: Transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(42) };

  await expect(client.ensureInclusionInBlock(transaction, 1, beforeTime)).rejects.toThrow(
    "Transaction was not included in block before 1000500"
  );
});

/** Will only try to resend transaction until include before. */
test("Will only retry resending transactions until include before.", async () => {
  const beforeTime = startTime + 500;

  let currentTimeCalls = 0;
  const customTime = () => {
    const numberOfCalls = ++currentTimeCalls;
    if (numberOfCalls <= 3) {
      return startTime;
    } else if (numberOfCalls <= 5) {
      return beforeTime;
    } else {
      throw new Error("Should not happen!");
    }
  };

  const client = setupTransactionClient(
    new ChainControllerStub(),
    new ShardControllerStub(),
    new getOrAssertTimeoutConditionWaiter(500),
    customTime
  );
  const transaction: Transaction = { address: TestObjects.CONTRACT_ONE, rpc: Buffer.alloc(42) };

  await expect(client.ensureInclusionInBlock(transaction, 1, beforeTime)).rejects.toThrow(
    "Unable to push transaction to chain"
  );
});

/**
 * Create a transaction client for tests.
 */
function setupTransactionClient(
  chainController: ChainControllerApi,
  shardController: ShardControllerApi,
  waiter: ConditionWaiter,
  customTime?: () => number
): BlockchainTransactionClient {
  const transactionClient = new ShardAndChainControllerTransactionClient(
    chainController,
    shardController
  );
  return BlockchainTransactionClient.createForTest(
    transactionClient,
    authentication,
    BlockchainTransactionClient.DEFAULT_TRANSACTION_VALIDITY_DURATION,
    BlockchainTransactionClient.DEFAULT_SPAWNED_EVENT_TIMEOUT,
    customTime ?? (() => startTime),
    waiter
  );
}

/**
 * Create a default execution status for tests.
 * @param events events of the execution status
 */
function executionStatus(...events: TransactionPointer[]): ExecutionStatus {
  return {
    success: true,
    finalized: true,
    events,
    transactionCost: {
      allocatedForEvents: 0,
      cpu: 0,
      networkFees: {},
      paidByContract: 0,
      remaining: 0,
    },
    blockId: "0",
  };
}
