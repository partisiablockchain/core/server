/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ConditionWaiterImpl } from "../../main";
import { TimeSupport } from "../../main/transactionclient/ConditionWaiter";

/**
 * If the condition is true immediately, then the waiter returns the supplied value without
 * waiting.
 */
test("Condition is true on first attempt", async () => {
  const timeSupport = new TimeSupportStub(0);
  const waiter = ConditionWaiterImpl.createForTest(timeSupport);
  const result = await waiter.waitForCondition(
    async () => "result",
    () => true,
    1000,
    () => "Error"
  );
  expect(result).toEqual("result");
  expect(timeSupport.currentTime).toEqual(0);
  expect(timeSupport.sleeps.length).toEqual(0);
});

/**
 * If the condition is never true, then the waiter sleeps in increasing amounts until it
 * reaches timeout where it throws.
 */
test("Condition is always false", async () => {
  const timeSupport = new TimeSupportStub(0);
  const waiter = ConditionWaiterImpl.createForTest(timeSupport);
  await expect(
    waiter.waitForCondition(
      async () => "result",
      () => false,
      1200,
      () => "Error"
    )
  ).rejects.toThrow("Error");
  expect(timeSupport.currentTime).toEqual(1200);
  expect(timeSupport.sleeps).toEqual([100, 200, 300, 400, 200, 0]);
});

/**
 * If the condition becomes true after two attempts, then the waiter will sleep twice,
 * before returning a value.
 */
test("Condition is true after two waits", async () => {
  const timeSupport = new TimeSupportStub(0);
  const waiter = ConditionWaiterImpl.createForTest(timeSupport);
  let i = 0;
  const result = await waiter.waitForCondition(
    async () => ++i,
    (i) => i === 3,
    1000,
    () => "Error"
  );
  expect(result).toEqual(3);
  expect(timeSupport.currentTime).toEqual(300);
  expect(timeSupport.sleeps).toEqual([100, 200]);
});

/**
 * If the value predicate is always true, but the value supplier throws twice, then the
 * waiter will sleep twice before returning its third value.
 */
test("Continues even if value supplier throws", async () => {
  const timeSupport = new TimeSupportStub(0);
  const waiter = ConditionWaiterImpl.createForTest(timeSupport);
  let i = 0;
  const result = await waiter.waitForCondition(
    async () => {
      i++;
      if (i < 3) {
        throw new Error("Value getter error");
      }
      return i;
    },
    (_i) => true,
    1000,
    () => "Error"
  );
  expect(result).toEqual(3);
  expect(timeSupport.currentTime).toEqual(300);
  expect(timeSupport.sleeps).toEqual([100, 200]);
});

/** The default system waiter returns the correct result. */
test("Test system waiter", async () => {
  const waiter = ConditionWaiterImpl.create();
  let i: number = 0;
  const result = await waiter.waitForCondition(
    async () => ++i,
    (val) => val === 2,
    1000,
    () => "time-out"
  );
  expect(result).toEqual(2);
});

/** Time support stub for use in testing. */
class TimeSupportStub implements TimeSupport {
  /**
   * The current time this stub reports.
   */
  currentTime: number;
  /**
   * The number and amount of sleeps this stub has been called with.
   */
  readonly sleeps: number[] = [];

  /**
   * Constructor.
   * @param currentTime the current time of the stub.
   */
  constructor(currentTime: number) {
    this.currentTime = currentTime;
  }

  /** @inheritDoc */
  now(): number {
    return this.currentTime;
  }
  /** @inheritDoc */
  async sleep(ms: number): Promise<void> {
    this.sleeps.push(ms);
    this.currentTime += ms;
  }
}
