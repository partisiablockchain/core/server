/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { SenderAuthenticationKeyPair, SignedTransaction, Transaction } from "../../main";
import { TestObjects } from "./TestObjects";
import { BigEndianByteInput } from "@secata-public/bitmanipulation-ts";

const authentication = SenderAuthenticationKeyPair.fromString("ab");

/** A signed transaction is serialized according to this format https://partisiablockchain.gitlab.io/documentation/smart-contracts/transaction-binary-format.html */
test("serialize signed", async () => {
  const transaction: Transaction = {
    address: TestObjects.CONTRACT_ONE,
    rpc: Buffer.from([1, 2, 3]),
  };
  const nonce = 40;
  const validToTime = 100;
  const gasCost = 200;
  const signed = await SignedTransaction.create(
    authentication,
    nonce,
    validToTime,
    gasCost,
    "Chain id",
    transaction
  );
  const bytes = signed.serialize();

  const input = new BigEndianByteInput(bytes);

  // Signature
  input.readBytes(65);
  expect(input.readI64().toNumber()).toEqual(nonce);
  expect(input.readI64().toNumber()).toEqual(validToTime);
  expect(input.readI64().toNumber()).toEqual(gasCost);
  // Address
  expect(input.readBytes(21).toString("hex")).toEqual(TestObjects.CONTRACT_ONE);
  // rpc
  expect(input.readI32()).toEqual(3);
  expect(input.readBytes(3)).toEqual(Buffer.from([1, 2, 3]));
});

/** The identifier function calculates the identifier hash of the signed transaction. */
test("identifier", async () => {
  const transaction: Transaction = {
    address: TestObjects.CONTRACT_ONE,
    rpc: Buffer.from([1, 2, 3]),
  };
  const signed = await SignedTransaction.create(
    authentication,
    40,
    100,
    200,
    "Chain id",
    transaction
  );
  expect(signed.identifier()).toEqual(
    "a140e9abf126e693e324849c0446523a8d41140bb8c86540d093d60bb286c23e"
  );
});
