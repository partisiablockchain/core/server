/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** An executed transaction tree where all transactions and event succeeded returns false. */
import { ExecutedTransaction, TransactionTree } from "../../main";

test("transaction tree succeeded", () => {
  const transactionTree: TransactionTree = new TransactionTree(executedTransaction(true), [
    executedTransaction(true),
    executedTransaction(true),
  ]);
  expect(transactionTree.hasFailures()).toEqual(false);
});

/**
 * An executed transaction tree where either the transaction or one of the events failed returns
 * true.
 */
test("transaction tree failed", () => {
  const transactionTree: TransactionTree = new TransactionTree(executedTransaction(false), []);
  expect(transactionTree.hasFailures()).toEqual(true);
  const transactionTree2: TransactionTree = new TransactionTree(executedTransaction(true), [
    executedTransaction(true),
    executedTransaction(false),
    executedTransaction(true),
  ]);
  expect(transactionTree2.hasFailures()).toEqual(true);
});

/** Create default executed transaction. */
function executedTransaction(succeeded: boolean): ExecutedTransaction {
  return {
    executionStatus: {
      success: succeeded,
      transactionCost: {
        allocatedForEvents: 0,
        cpu: 0,
        networkFees: {},
        paidByContract: 0,
        remaining: 0,
      },
      finalized: true,
      blockId: "",
      events: [],
    },
    identifier: "",
    isEvent: false,
    content: "",
  };
}
