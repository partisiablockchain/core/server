/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  Account,
  Block,
  Chain,
  ChainControllerApi,
  ExecutedTransaction,
  GetAccountRequest,
  GetChainRequest,
  GetLatestBlockRequest,
  GetTransactionRequest,
  InitOverrideFunction,
  PutTransactionRequest,
  ShardControllerApi,
  TransactionPointer,
} from "../../main";
import { ConditionWaiter } from "../../main/transactionclient/ConditionWaiter";

/**
 * A condition waiter stub.
 */
export class ConditionWaiterStub implements ConditionWaiter {
  private numberOfSleeps = 0;
  private timeWaited = 0;

  /** @inheritDoc */
  async waitForCondition<T>(
    valueSupplier: () => Promise<T>,
    valuePredicate: (value: T) => boolean,
    timeoutMs: number,
    _errorMessage: () => string
  ): Promise<T> {
    let value: T | undefined;
    try {
      value = await valueSupplier();
    } catch (_) {
      value = undefined;
    }
    while ((value === undefined || !valuePredicate(value)) && this.numberOfSleeps < 7) {
      this.numberOfSleeps++;
      this.timeWaited += timeoutMs;
      try {
        value = await valueSupplier();
      } catch (_) {
        value = undefined;
      }
    }
    return value!;
  }

  /**
   * Assert that this stub has waited the maximum amount of time.
   * @param duration the expected amount of sleep per wait
   */
  assertHasWaitedWithoutResult(duration: number) {
    expect(this.numberOfSleeps >= 7).toEqual(true);
    expect(this.timeWaited).toEqual(duration * this.numberOfSleeps);
  }
}

/**
 * A condition waiter stub that returns the value immediately if the value predicate holds,
 * otherwise throws.
 */
export class GetOrThrowConditionWaiter implements ConditionWaiter {
  /** @inheritDoc */
  async waitForCondition<T>(
    valueSupplier: () => Promise<T>,
    valuePredicate: (value: T) => boolean,
    timeoutMs: number,
    errorMessage: () => string
  ): Promise<T> {
    const value = await valueSupplier();
    if (valuePredicate(value)) {
      return value;
    } else {
      throw new Error(errorMessage());
    }
  }
}

/**
 * A condition waiter stub that returns the value immediately if the value predicate holds,
 * otherwise checks the timeout is the supplied expected timeout and throws and exception.
 */
export class getOrAssertTimeoutConditionWaiter implements ConditionWaiter {
  /**
   * The expected timeout of the condition waiter
   */
  expectedTimeout: number;

  /**
   * Constructor for the timeout to assert with
   * @param expectedTimeout expected timeout of the condition waiter.
   */
  constructor(expectedTimeout: number) {
    this.expectedTimeout = expectedTimeout;
  }

  /** @inheritDoc */
  async waitForCondition<T>(
    valueSupplier: () => Promise<T>,
    valuePredicate: (value: T) => boolean,
    timeoutMs: number,
    errorMessage: () => string
  ): Promise<T> {
    const value = await valueSupplier();
    if (valuePredicate(value)) {
      return value;
    } else {
      expect(this.expectedTimeout).toEqual(timeoutMs);
      throw new Error(errorMessage());
    }
  }
}

/**
 * A stub for the chain controller
 */
export class ChainControllerStub extends ChainControllerApi {
  /**
   * Map of PutTransactionRequest payload to a transaction pointer.
   */
  transactions: Map<string, TransactionPointer> = new Map<string, TransactionPointer>();

  /**
   * Default transaction pointer to return if no transaction pointer is present in the transaction
   * map.
   */
  defaultTransactionPointer: TransactionPointer | undefined;

  /**
   * Constructor for ChainControllerStub allowing to define the default transaction pointer.
   * @param defaultTransactionPointer default transaction pointer to use.
   */
  constructor(defaultTransactionPointer?: TransactionPointer) {
    super();
    this.defaultTransactionPointer = defaultTransactionPointer;
  }

  /** @inheritDoc */
  override async getAccount(
    _requestParameters: GetAccountRequest,
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<Account> {
    return { nonce: 765399, shardId: "Shard0", account: {} };
  }

  /** @inheritDoc */
  override async getChain(
    _requestParameters: GetChainRequest = {},
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<Chain> {
    return {
      chainId: "Chain",
      governanceVersion: 1,
      shards: ["Shard0"],
      features: [],
      plugins: {},
    };
  }

  /**
   * Sets a transaction pointer for a given transaction put request.
   * @param transaction to set transaction pointer for.
   */
  addTransactionPointerForTransaction(transaction: PutTransactionRequest) {
    this.transactions.set(transaction.serializedTransaction.payload, {
      identifier: "a".repeat(64),
      destinationShardId: "Shard0",
    });
  }

  /** @inheritDoc */
  override async putTransaction(
    requestParameters: PutTransactionRequest,
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<TransactionPointer> {
    const transactionPointer = this.transactions.get(
      requestParameters.serializedTransaction.payload
    );
    if (transactionPointer === undefined && this.defaultTransactionPointer === undefined) {
      throw new Error(
        "The received transaction is invalid in the current state of the blockchain."
      );
    } else if (this.defaultTransactionPointer !== undefined) {
      return this.defaultTransactionPointer;
    }
    return transactionPointer!;
  }
}

/**
 * Stub for the shard controller.
 */
export class ShardControllerStub extends ShardControllerApi {
  private readonly transactions: Map<string, ExecutedTransaction>;
  private readonly defaultTransaction?: ExecutedTransaction;
  private latestBlockProductionTime: number;

  /**
   * Constructor.
   * @param defaultTransaction the default transaction the stub returns on getTransaction
   */
  constructor(defaultTransaction?: ExecutedTransaction) {
    super();
    this.transactions = new Map();
    this.defaultTransaction = defaultTransaction;
    this.latestBlockProductionTime = 0;
  }

  /**
   * Add a transaction the stub can return.
   * @param transaction transaction to add.
   */
  addTransaction(transaction: ExecutedTransaction) {
    this.transactions.set(transaction.identifier, transaction);
  }

  /** @inheritDoc */
  override async getTransaction(
    requestParameters: GetTransactionRequest,
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<ExecutedTransaction> {
    const executed =
      this.transactions.get(requestParameters.transactionId) ?? this.defaultTransaction;
    if (executed === undefined) {
      throw new Error("404 not found");
    }
    return executed;
  }

  /**
   * Set the production time that the stub returns for getLatestBlock.
   * @param productionTime new production time
   */
  setProductionTimeOfLatestFinalBlock(productionTime: number): void {
    this.latestBlockProductionTime = productionTime;
  }

  /** @inheritDoc */
  override async getLatestBlock(
    _requestParameters: GetLatestBlockRequest,
    _initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<Block> {
    return {
      productionTime: this.latestBlockProductionTime,
      blockTime: 0,
      parentBlock: "",
      identifier: "",
      events: [],
      committeeId: 0,
      transactions: [],
      producer: 0,
      state: "",
    };
  }
}
