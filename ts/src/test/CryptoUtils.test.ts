/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ec as Elliptic } from "elliptic";
import { CryptoUtils } from "../main/CryptoUtils";

test("key to address", () => {
  const privateKey = "1";
  const keyPair = CryptoUtils.privateKeyToKeypair(privateKey);
  expect(CryptoUtils.keyPairToAccountAddress(keyPair)).toEqual(
    "0035e97a5e078a5a0f28ec96d547bfee9ace803ac0"
  );
});

test("signature to buffer with 31 byte s value test", () => {
  const privateKey = "07f18365046dd1445d72f796c75748dbfa2b69d5cd2db295dd1b6b42d85ca713";
  const hash = "9a16f2c40583ca831df266e68339f719c2ff304e5c455611f6d940d968d64c31";
  const buffer = Buffer.from(hash, "hex");
  const expectedSignature =
    "00" +
    "ed68c4457d0c187c7218699b8dddf161f38a40099e3b874b684b8c377d3091e3" +
    "00e1aa0f050cfd9ed08d95468b3b915c6a030c84aee2020c84375c60c8f12d2d";

  const signature = CryptoUtils.privateKeyToKeypair(privateKey).sign(buffer);
  const serializedSignature = CryptoUtils.signatureToBuffer(signature);
  expect(serializedSignature.toString("hex")).toEqual(expectedSignature);
});

test("signature without recovery param", () => {
  const signature = { recoveryParam: null } as Elliptic.Signature;
  expect(() => CryptoUtils.signatureToBuffer(signature)).toThrowError("Recovery parameter is null");
});

test("hash buffer", () => {
  const buffer = CryptoUtils.hashBuffer(Buffer.alloc(8));
  expect(buffer.toString("hex")).toBe(
    "af5570f5a1810b7af78caf4bc70a660f0df51e42baf91d4de5b2328de0e83dfc"
  );
});
