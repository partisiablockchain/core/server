/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Generic wait interface allowing the creation of asynchronous calls and wait for these. The user
 * must provide a value getter and a value predicate, the value getter returns the current value,
 * the value predicate whether the value is "complete" and the wait is over.
 */
export interface ConditionWaiter {
  /**
   * Waits for the condition dictated by the input predicate until it is met. Times out by if the
   * condition is not met within a specified time-out. Sleeps in an incrementing fashion between
   * checks of the condition until a maximum of 30 seconds between iterations to avoid busy-waiting.
   * @param valueSupplier The supplier of (possibly) updated values for the condition.
   * @param valuePredicate The condition that the value must meet before waiting is over.
   * @param timeoutMs The time to wait for in milliseconds.
   * @param errorMessage The lazy initialized error message to throw in case of timing out.
   * @returns The final value returned by the supplier, which meets the condition.
   */
  waitForCondition<T>(
    valueSupplier: () => Promise<T>,
    valuePredicate: (value: T) => boolean,
    timeoutMs: number,
    errorMessage: () => string
  ): Promise<T>;
}

/**
 * Can wait for a condition to become fulfilled by repeatedly sleeping the current thread between
 * checks.
 */
export class ConditionWaiterImpl implements ConditionWaiter {
  private readonly timeSupport: TimeSupport;

  private constructor(timeSupport: TimeSupport) {
    this.timeSupport = timeSupport;
  }

  /**
   * @inheritdoc
   */
  async waitForCondition<T>(
    valueSupplier: () => Promise<T>,
    valuePredicate: (value: T) => boolean,
    timeoutMs: number,
    errorMessage: () => string
  ): Promise<T> {
    const end = this.timeSupport.now() + timeoutMs;
    let increasingSleep = 0;
    let remainingTime;
    do {
      let value: T | undefined = undefined;
      try {
        value = await valueSupplier();
      } catch (error) {
        // Empty try again
      }
      if (value !== undefined && valuePredicate(value)) {
        return value;
      }
      increasingSleep++;
      const sleepDurationMs = Math.min(30000, 100 * increasingSleep);
      remainingTime = Math.max(0, end - this.timeSupport.now());
      const effectiveSleepDuration = Math.min(sleepDurationMs, remainingTime);
      await this.timeSupport.sleep(effectiveSleepDuration);
    } while (remainingTime > 0);
    throw new Error(errorMessage());
  }

  /**
   * Creates a new condition waiter using the system time and system sleep.
   * @returns The transaction waiter.
   */
  public static create(): ConditionWaiterImpl {
    return new ConditionWaiterImpl(TimeSupportSystem);
  }

  /**
   * Creates a new condition waiter.
   * @param timeSupport Support for time (current time and sleep).
   * @returns The transaction waiter.
   */
  public static createForTest(timeSupport: TimeSupport): ConditionWaiterImpl {
    return new ConditionWaiterImpl(timeSupport);
  }
}

/** Support for getting the current time and sleeping. */
export interface TimeSupport {
  /**
   * Gets the current time in milliseconds.
   * @returns The current time in milliseconds.
   */
  now(): number;
  /**
   * Pauses execution of the current program for some measure of time.
   * @param ms The time to stop execution in milliseconds.
   * @returns an empty promise that resolves after the duration has passed
   */
  sleep(ms: number): Promise<void>;
}

/** Time support that uses the underlying operating system. */
export const TimeSupportSystem: TimeSupport = {
  /** @inheritDoc */
  now(): number {
    return new Date().getTime();
  },
  /** @inheritDoc */
  sleep(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  },
};
