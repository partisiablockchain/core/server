/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import BN from "bn.js";
import { BigEndianByteOutput } from "@secata-public/bitmanipulation-ts";
import { CryptoUtils } from "../CryptoUtils";
import { Hash, SenderAuthentication, Signature, Transaction } from "./types";

/**
 * A transaction that has been signed by a private key. A signed transaction is ready to be sent to
 * the blockchain. A signed transaction has limited durability, since it includes information about
 * the valid-to-time and the next available nonce of the sending user.
 */
export class SignedTransaction {
  private readonly inner: InnerPart;
  private readonly signature: Signature;
  private readonly hash: Hash;

  private constructor(inner: InnerPart, signature: Signature, hash: Hash) {
    this.inner = inner;
    this.signature = signature;
    this.hash = hash;
  }

  /**
   * Create a signed transaction to send to PBC.
   * @param senderAuthentication the sender authentication to sign the transaction with.
   * @param nonce the nonce of the signing account.
   * @param validToTime the unix time that the transaction is valid to.
   * @param gasCost the amount of gas allocated to executing the transaction.
   * @param chainId the id of the chain.
   * @param transaction the transaction to sign.
   * @returns the signed transaction.
   */
  public static async create(
    senderAuthentication: SenderAuthentication,
    nonce: number,
    validToTime: number,
    gasCost: number,
    chainId: string,
    transaction: Transaction
  ): Promise<SignedTransaction> {
    const inner: InnerPart = { nonce, validToTime, gasCost, innerTransaction: transaction };
    const transactionPayload = serializeInnerPart(inner);
    const hash = SignedTransaction.calculateHash(transactionPayload, chainId);
    const signature = await senderAuthentication.sign(transactionPayload, chainId);
    return new SignedTransaction(inner, signature, hash);
  }

  /**
   * Calculate the SHA256 hash for a transaction payload and a chain id.
   * @param transactionPayload a serialized transaction payload
   * @param chainId the id of the chain
   * @returns the hash that is used when signing a transaction.
   */
  public static calculateHash(transactionPayload: Buffer, chainId: string) {
    return CryptoUtils.hashBuffers([
      transactionPayload,
      BigEndianByteOutput.serialize((out) => out.writeString(chainId)),
    ]).toString("hex");
  }

  /**
   * Serialize the signed transaction into bytes.
   * @returns the serialized bytes.
   */
  public serialize(): Buffer {
    return Buffer.concat([Buffer.from(this.signature, "hex"), serializeInnerPart(this.inner)]);
  }

  /**
   * Get the identifier of this transaction.
   * @returns the identify hash
   */
  public identifier(): Hash {
    return CryptoUtils.hashBuffers([
      Buffer.from(this.hash, "hex"),
      Buffer.from(this.signature, "hex"),
    ]).toString("hex");
  }

  /**
   * Get the nonce of this transaction.
   * @returns the nonce
   */
  public getNonce(): number {
    return this.inner.nonce;
  }

  /**
   * Get the valid-to-time of this transaction. If the transaction is not included in a block before
   * this time it will fail.
   * @returns the valid to time as a unix timestamp.
   */
  public getValidToTime(): number {
    return this.inner.validToTime;
  }

  /**
   * Get the gas cost of this transaction.
   * @returns the gas cost
   */
  public getGasCost(): number {
    return this.inner.gasCost;
  }

  /**
   * Get the inner transaction of this transaction.
   * @returns the inner
   */
  public getInner(): Transaction {
    return this.inner.innerTransaction;
  }
}

/** The inner part of a signed transaction. */
interface InnerPart {
  /** The nonce of the sender */
  nonce: number;
  /** The time this transaction is valid to. */
  validToTime: number;
  /** The gas cost of the transaction */
  gasCost: number;
  /** The input transaction. */
  innerTransaction: Transaction;
}

/** Serialize the inner part of a signed transaction to bytes. */
function serializeInnerPart(innerPart: InnerPart): Buffer {
  return BigEndianByteOutput.serialize((out) => {
    out.writeI64(new BN(innerPart.nonce));
    out.writeI64(new BN(innerPart.validToTime));
    out.writeI64(new BN(innerPart.gasCost));
    out.writeBytes(Buffer.from(innerPart.innerTransaction.address, "hex"));
    out.writeI32(innerPart.innerTransaction.rpc.length);
    out.writeBytes(innerPart.innerTransaction.rpc);
  });
}
