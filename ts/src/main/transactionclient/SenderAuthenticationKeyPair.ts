/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ec as Elliptic } from "elliptic";
import { BlockchainAddress, SenderAuthentication, Signature } from "./types";
import { CryptoUtils } from "../CryptoUtils";
import { SignedTransaction } from "./SignedTransaction";

/**
 * Sender authentication based on a key-pair, consisting of a public and private key. The sender
 * address is computed from the public key.
 */
export class SenderAuthenticationKeyPair implements SenderAuthentication {
  private readonly keyPair: Elliptic.KeyPair;

  /**
   * Creates authentication for a sender.
   * @param keyPair The public and private key of the sender. Not nullable.
   */
  public constructor(keyPair: Elliptic.KeyPair) {
    this.keyPair = keyPair;
  }

  /**
   * Creates a sender's authentication from a private key.
   * @param privateKey The private key to create a key pair from, written as a value in hexidecimal.
   * @returns authentication for a transaction sender.
   */
  public static fromString(privateKey: string): SenderAuthenticationKeyPair {
    return new SenderAuthenticationKeyPair(CryptoUtils.privateKeyToKeypair(privateKey));
  }

  /** @inheritDoc */
  public getAddress(): BlockchainAddress {
    return CryptoUtils.keyPairToAccountAddress(this.keyPair);
  }

  /** @inheritDoc */
  public async sign(transactionPayload: Buffer, chainId: string): Promise<Signature> {
    const hash = SignedTransaction.calculateHash(transactionPayload, chainId);
    return CryptoUtils.signatureToBuffer(this.keyPair.sign(hash)).toString("hex");
  }
}
