/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ExecutedTransaction } from "../generated/openapi";

/** Status for a transaction and the whole tree of spawned events. */
export class TransactionTree {
  /** The original transaction. */
  public readonly transaction: ExecutedTransaction;
  /** List of spawned events from the transaction. */
  public readonly events: ExecutedTransaction[];

  /**
   * Create new TransactionTree.
   * @param transaction the original transaction.
   * @param events list of spawned events from the transaction.
   */
  constructor(transaction: ExecutedTransaction, events: ExecutedTransaction[]) {
    this.transaction = transaction;
    this.events = events;
  }

  /**
   * Check if the executed transaction tree includes any transactions or events which did not
   * succeed.
   * @returns true if the transaction tree includes any failures, false if all succeeded.
   */
  public hasFailures(): boolean {
    if (!this.transaction.executionStatus!.success) {
      return true;
    }
    for (const event of this.events) {
      if (!event.executionStatus!.success) {
        return true;
      }
    }
    return false;
  }
}
