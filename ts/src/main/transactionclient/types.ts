/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { SignedTransaction } from "./SignedTransaction";
import {
  ExecutedTransaction,
  SerializedTransaction,
  TransactionPointer,
} from "../generated/openapi";

/** Hash as a 32 byte long hex string. */
export type Hash = string;
/** BlockchainAddress as 21 byte long hex string. */
export type BlockchainAddress = string;
/** Signature as 65 byte long hex string. */
export type Signature = string;

/**
 * Authentication for a sender of a transaction. Sending transactions requires a blockchain address
 * and the ability to sign messages.
 */
export interface SenderAuthentication {
  /**
   * Get the address of the sender.
   * @returns The blockchain address of the sender.
   */
  getAddress(): BlockchainAddress;

  /**
   * Sign a transaction.
   * @param transactionPayload payload of the transaction
   * @param chainId the chain id
   * @returns the signature of the message
   */
  sign(transactionPayload: Buffer, chainId: string): Promise<Signature>;
}

/** A transaction. */
export interface Transaction {
  /** The address of the receiver of the transaction. */
  address: BlockchainAddress;
  /** The rpc of the transaction. */
  rpc: Buffer;
}

/**
 * A transaction that has been sent to the blockchain for execution.
 */
export interface SentTransaction {
  /** The signed transaction that was sent. */
  signedTransaction: SignedTransaction;
  /** A pointer to the signed transaction that was sent. */
  transactionPointer: TransactionPointer;
}

/**
 * A client for getting the information to create a transaction, and send it after creation and
 * signing.
 */
export interface BlockchainClientForTransaction {
  /**
   * Get the chain ID for the running chain.
   * @returns the chain ID string.
   */
  getChainId(): Promise<string>;

  /**
   * Get the current nonce to use when signing a transaction for the given account.
   * @param blockchainAddress the address to get the nonce for.
   * @returns the current nonce to use for signing.
   */
  getNonce(blockchainAddress: BlockchainAddress): Promise<number>;

  /**
   * Retrieve an executed transaction from the blockchain.
   * @param shardId The name of the shard to interact with.
   * @param transactionId The transaction hash.
   * @returns The retrieved transaction.
   */
  getTransaction(shardId: string, transactionId: Hash): Promise<ExecutedTransaction>;

  /**
   * Get the production time from the latest created block of a specific shard.
   * @param shardId the id of the shard.
   * @returns the production time of the latest created block.
   */
  getLatestProductionTime(shardId: string): Promise<number>;

  /**
   * Send a signed transaction to the chain and return a pointer to the transaction.
   * @param serializedTransaction the transaction to send.
   * @returns a pointer to the transaction on chain.
   */
  putTransaction(serializedTransaction: SerializedTransaction): Promise<TransactionPointer>;
}
