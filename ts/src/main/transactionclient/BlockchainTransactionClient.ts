/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  ChainControllerApi,
  Configuration,
  ExecutedTransaction,
  SerializedTransaction,
  ShardControllerApi,
  TransactionPointer,
} from "../generated/openapi";
import { ConditionWaiter, ConditionWaiterImpl } from "./ConditionWaiter";
import { ShardAndChainControllerTransactionClient } from "./ShardAndChainControllerTransactionClient";
import { SignedTransaction } from "./SignedTransaction";
import { TransactionTree } from "./TransactionTree";
import {
  BlockchainClientForTransaction,
  Hash,
  SenderAuthentication,
  SentTransaction,
  Transaction,
} from "./types";

/** A client that supports signing, sending and waiting for transactions on the blockchain. */
export class BlockchainTransactionClient {
  private readonly authentication: SenderAuthentication;
  private readonly transactionClient: BlockchainClientForTransaction;

  private readonly transactionValidityDuration: number;
  private readonly spawnedEventTimeout: number;

  private readonly currentTime: () => number;
  private readonly conditionWaiter: ConditionWaiter;

  /**
   * Default signed transaction vilidity time in milliseconds. This is three minutes in
   * milliseconds.
   */
  public static readonly DEFAULT_TRANSACTION_VALIDITY_DURATION = 180000;

  /**
   * Default timeout for spawned event to be inclusion in a block. This is ten minutes in
   * milliseconds.
   */
  public static readonly DEFAULT_SPAWNED_EVENT_TIMEOUT = 600000;

  /**
   * Default current time is system time.
   */
  public static readonly DEFAULT_CURRENT_TIME = () => new Date().getTime();

  private constructor(
    transactionClient: BlockchainClientForTransaction,
    authentication: SenderAuthentication,
    transactionValidityDuration: number,
    spawnedEventTimeout: number,
    currentTime: () => number,
    conditionWaiter: ConditionWaiter
  ) {
    this.authentication = authentication;
    this.transactionClient = transactionClient;
    this.transactionValidityDuration = transactionValidityDuration;
    this.spawnedEventTimeout = spawnedEventTimeout;
    this.currentTime = currentTime;
    this.conditionWaiter = conditionWaiter;
  }

  /**
   * Create a blockchain transaction client.
   * @param baseUrl the base url of the chain to connect to
   * @param authentication authentication of the sender that will sign transactions
   * @param transactionValidityInMillis the number of milliseconds a signed transaction is valid for
   *     inclusion in a block.
   * @param spawnedEventTimeoutInMillis the number of milliseconds the client will wait for a
   *     spawned event to be included in a block before timing out.
   * @returns A blockchain transaction client
   */
  public static create(
    baseUrl: string,
    authentication: SenderAuthentication,
    transactionValidityInMillis?: number,
    spawnedEventTimeoutInMillis?: number
  ): BlockchainTransactionClient {
    const chainControllerApi = new ChainControllerApi(new Configuration({ basePath: baseUrl }));
    const shardControllerApi = new ShardControllerApi(new Configuration({ basePath: baseUrl }));
    const transactionClient = new ShardAndChainControllerTransactionClient(
      chainControllerApi,
      shardControllerApi
    );
    return new BlockchainTransactionClient(
      transactionClient,
      authentication,
      transactionValidityInMillis ?? this.DEFAULT_TRANSACTION_VALIDITY_DURATION,
      spawnedEventTimeoutInMillis ?? this.DEFAULT_SPAWNED_EVENT_TIMEOUT,
      this.DEFAULT_CURRENT_TIME,
      ConditionWaiterImpl.create()
    );
  }

  /**
   * Create a blockchain transaction client for a custom API.
   * @param transactionClient the client used for communicating with the blockchain api
   * @param authentication authentication of the sender that will sign transactions
   * @param transactionValidityInMillis the number of milliseconds a signed transaction is valid for
   *     inclusion in a block.
   * @param spawnedEventTimeoutInMillis the number of milliseconds the client will wait for a
   *     spawned event to be included in a block before timing out.
   * @returns A blockchain transaction client
   */
  public static createForCustomApi(
    transactionClient: BlockchainClientForTransaction,
    authentication: SenderAuthentication,
    transactionValidityInMillis?: number,
    spawnedEventTimeoutInMillis?: number
  ): BlockchainTransactionClient {
    return new BlockchainTransactionClient(
      transactionClient,
      authentication,
      transactionValidityInMillis ?? this.DEFAULT_TRANSACTION_VALIDITY_DURATION,
      spawnedEventTimeoutInMillis ?? this.DEFAULT_SPAWNED_EVENT_TIMEOUT,
      this.DEFAULT_CURRENT_TIME,
      ConditionWaiterImpl.create()
    );
  }

  /**
   * Create a blockchain transaction client used for testing.
   * @param transactionClient the client used for communicating with the blockchain api
   * @param authentication authentication of the sender that will sign transactions
   * @param transactionValidityDuration the number of milliseconds a signed transaction is valid for
   *      inclusion in a block.
   * @param spawnedEventTimeout the number of milliseconds the client will wait for a
   *     spawned event to be included in a block before timing out. @param currentTime
   * @param currentTime support for injecting custom time
   * @param conditionWaiter support for injecting custom condition waiter
   * @returns A blockchain transaction client
   */
  static createForTest(
    transactionClient: BlockchainClientForTransaction,
    authentication: SenderAuthentication,
    transactionValidityDuration: number,
    spawnedEventTimeout: number,
    currentTime: () => number,
    conditionWaiter: ConditionWaiter
  ): BlockchainTransactionClient {
    return new BlockchainTransactionClient(
      transactionClient,
      authentication,
      transactionValidityDuration,
      spawnedEventTimeout,
      currentTime,
      conditionWaiter
    );
  }

  /**
   * Sign a transaction in preparation for sending it to the blockchain. The signed transaction
   * expires after {@link #transactionValidityDuration} millis or if the nonce of the signer
   * changes.
   * @param transaction the transaction to sign
   * @param gasCost the amount of gas to allocate for executing the transaction.
   * @param applicableUntil the latest time this transaction is allowed
   * @returns a signed transaction corresponding to the passed transaction.
   */
  public async sign(
    transaction: Transaction,
    gasCost: number,
    applicableUntil?: number
  ): Promise<SignedTransaction> {
    const sender = this.authentication.getAddress();
    const nonce = await this.transactionClient.getNonce(sender);
    const chainId = await this.transactionClient.getChainId();
    let validToTime = this.currentTime() + this.transactionValidityDuration;
    if (applicableUntil !== undefined) {
      validToTime = Math.min(validToTime, applicableUntil);
    }
    return SignedTransaction.create(
      this.authentication,
      nonce,
      validToTime,
      gasCost,
      chainId,
      transaction
    );
  }

  /**
   * Sends a signed transaction to the blockchain for execution and inclusion in a block.
   * @param signedTransaction the signed transaction to send.
   * @returns a sent transaction corresponding to the signed transaction.
   */
  public async send(signedTransaction: SignedTransaction): Promise<SentTransaction> {
    const bytes = signedTransaction.serialize();

    const serializedTransaction: SerializedTransaction = { payload: bytes.toString("base64") };
    const transactionPointer = await this.transactionClient.putTransaction(serializedTransaction);
    return { signedTransaction, transactionPointer };
  }

  /**
   * Sign and send a transaction to the blockchain for execution.
   * @param transaction the transaction to sign and send
   * @param gasCost the amount of gas to allocate for executing the transaction.
   * @returns a sent transaction corresponding to the signed transaction.
   */
  public async signAndSend(transaction: Transaction, gasCost: number): Promise<SentTransaction> {
    return await this.send(await this.sign(transaction, gasCost));
  }

  /**
   * Waits for a sent transaction to be included in a block on the blockchain.
   * @param sentTransaction a transaction that has been sent to the blockchain.
   * @returns the executed state of the transaction.
   */
  public async waitForInclusionInBlock(
    sentTransaction: SentTransaction
  ): Promise<ExecutedTransaction> {
    const senderShard = sentTransaction.transactionPointer.destinationShardId;
    const transactionIdentifier: Hash = sentTransaction.transactionPointer.identifier;
    const validToTime = sentTransaction.signedTransaction.getValidToTime();
    const timeToWait = validToTime - this.currentTime();

    return this.waitForTransaction(senderShard, transactionIdentifier, timeToWait);
  }

  /**
   * Recursively wait for the inclusion of all events spawned by the transaction and the
   * transaction's events.
   * @param transaction the transaction to wait for
   * @returns the execution status for all recursive events spawned by the transaction.
   */
  public async waitForSpawnedEvents(
    transaction: ExecutedTransaction | SentTransaction
  ): Promise<TransactionTree> {
    let executedTransaction = transaction;
    if ("signedTransaction" in executedTransaction) {
      executedTransaction = await this.waitForInclusionInBlock(executedTransaction);
    }
    const events: ExecutedTransaction[] = [];
    const executionStatus = executedTransaction.executionStatus!;
    const pendingEvents = [...executionStatus.events];
    while (pendingEvents.length !== 0) {
      const nextEvent = pendingEvents.shift() as TransactionPointer;
      const identifier: Hash = nextEvent.identifier;
      const shardId = nextEvent.destinationShardId;
      const executed = await this.waitForTransaction(shardId, identifier, this.spawnedEventTimeout);
      events.push(executed);
      const executedExecutionStatus = executed.executionStatus!;
      pendingEvents.push(...executedExecutionStatus.events);
    }
    return new TransactionTree(executedTransaction, events);
  }

  /**
   * Try to ensure that the given transaction is included in a final block before the specified time
   * has passed. Will try to resend the transaction until it has been included in a block or the
   * specified time has passed. If the transaction has not been included, or we are unable to
   * determine inclusion, due to network issues, an exception is thrown.
   *
   * <p>To determine that a tried transaction has not been included in a block, we must wait until
   * the last valid block that could have included the transaction has been finalized. The caller of
   * this function can specify the amount of time to wait for the last valid block after validity
   * time of the tried transaction has expired.
   * @param transaction The transaction to sign and send
   * @param transactionGasCost The amount of gas to allocate for executing the transaction
   * @param includeBefore The UTC timestamp after which the transaction will no longer tried to be
   *     sent
   * @param lastValidBlockWaitTime The amount of time to wait for the last valid block that a tried
   *     transaction can be included in
   * @returns The executed state of the transaction
   * @throws ApiException on io error
   */
  public async ensureInclusionInBlock(
    transaction: Transaction,
    transactionGasCost: number,
    includeBefore: number,
    lastValidBlockWaitTime?: number
  ): Promise<ExecutedTransaction> {
    // Default is 30 days
    const effectiveLastValidBlockWaitTime = lastValidBlockWaitTime ?? 2_592_000_000;
    while (includeBefore > this.currentTime()) {
      const sentTransaction = await this.conditionWaiter.waitForCondition(
        async () => await this.signAndSendRetryable(transaction, transactionGasCost, includeBefore),
        (sentTransaction) => sentTransaction !== undefined,
        includeBefore - this.currentTime(),
        () => "Unable to push transaction to chain"
      );
      const executedTransaction = await this.waitForExecutedTransaction(
        sentTransaction!,
        effectiveLastValidBlockWaitTime
      );
      if (executedTransaction !== undefined) {
        return executedTransaction;
      }
    }
    throw new Error("Transaction was not included in block before " + includeBefore);
  }

  /**
   * Sign and sends the transaction.
   * @param transaction the transaction to sign and send
   * @param transactionGasCost the transaction gas cost
   * @param applicableUntil the latest time this transaction is allowed
   * @returns  a {@link SentTransaction} corresponding to the signed transaction or null if an
   *   {@link Error} was thrown
   */
  private async signAndSendRetryable(
    transaction: Transaction,
    transactionGasCost: number,
    applicableUntil: number
  ): Promise<SentTransaction | undefined> {
    try {
      return await this.send(await this.sign(transaction, transactionGasCost, applicableUntil));
    } catch (_) {
      /* empty */
    }
  }

  private async waitForExecutedTransaction(
    sentTransaction: SentTransaction,
    latestBlockWait: number
  ): Promise<ExecutedTransaction | undefined> {
    try {
      return await this.waitForInclusionInBlock(sentTransaction);
    } catch (_) {
      return await this.waitForLastPossibleBlockToInclude(sentTransaction, latestBlockWait);
    }
  }

  private async waitForLastPossibleBlockToInclude(
    sentTransaction: SentTransaction,
    latestBlockWait: number
  ): Promise<ExecutedTransaction | undefined> {
    const shardId = sentTransaction.transactionPointer.destinationShardId;
    const validToTime = sentTransaction.signedTransaction.getValidToTime();
    const timeOut = latestBlockWait + validToTime;
    await this.conditionWaiter.waitForCondition(
      async () => await this.transactionClient.getLatestProductionTime(shardId),
      (productionTime) => productionTime > validToTime,
      timeOut,
      () =>
        `Unable to get block with production time greater than ${validToTime} for shard ${shardId}`
    );
    const transactionIdentifier = sentTransaction.transactionPointer.identifier;

    // Stryker disable BlockStatement
    try {
      return await this.transactionClient.getTransaction(shardId, transactionIdentifier);
    } catch (_) {
      return undefined;
    }
  }

  // Stryker restore BlockStatement

  private waitForTransaction(
    shardId: string,
    transactionId: Hash,
    timeout: number
  ): Promise<ExecutedTransaction> {
    return this.conditionWaiter.waitForCondition(
      () => this.transactionClient.getTransaction(shardId, transactionId),
      (value) => value.executionStatus !== undefined && value.executionStatus.finalized,
      timeout,
      () => `Transaction '${transactionId}' was not included in a block at shard '${shardId}'`
    );
  }
}
