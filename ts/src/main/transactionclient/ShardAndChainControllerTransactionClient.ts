/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import {
  ChainControllerApi,
  ExecutedTransaction,
  SerializedTransaction,
  ShardControllerApi,
  TransactionPointer,
} from "../generated/openapi";
import { BlockchainAddress, BlockchainClientForTransaction } from "./types";

/** Transaction client using the shard and chain controller APIs. */
export class ShardAndChainControllerTransactionClient implements BlockchainClientForTransaction {
  private readonly chainController: ChainControllerApi;
  private readonly shardController: ShardControllerApi;

  /**
   * Create transaction client.
   * @param chainController the client used for communicating with the chain api
   * @param shardController the client used for communicating with the shard api
   */
  constructor(chainController: ChainControllerApi, shardController: ShardControllerApi) {
    this.chainController = chainController;
    this.shardController = shardController;
  }

  /** @inheritDoc */
  public async getChainId(): Promise<string> {
    return (await this.chainController.getChain()).chainId;
  }

  /** @inheritDoc */
  public async getNonce(blockchainAddress: BlockchainAddress): Promise<number> {
    return (await this.chainController.getAccount({ address: blockchainAddress })).nonce;
  }

  /** @inheritDoc */
  public getTransaction(shardId: string, transactionId: string): Promise<ExecutedTransaction> {
    return this.shardController.getTransaction({ shardId, transactionId });
  }

  /** @inheritDoc */
  public async getLatestProductionTime(shardId: string): Promise<number> {
    return (await this.shardController.getLatestBlock({ shardId })).productionTime;
  }

  /** @inheritDoc */
  public putTransaction(serializedTransaction: SerializedTransaction): Promise<TransactionPointer> {
    return this.chainController.putTransaction({ serializedTransaction });
  }
}
