/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { ec as Elliptic } from "elliptic";
import { sha256 } from "hash.js";

const ec = new Elliptic("secp256k1");

/**
 * Serializes a signature into byte.
 * @param signature the signature.
 * @returns the bytes.
 */
function signatureToBuffer(signature: Elliptic.Signature): Buffer {
  if (signature.recoveryParam == null) {
    throw new Error("Recovery parameter is null");
  }
  return Buffer.concat([
    Buffer.from([signature.recoveryParam]),
    signature.r.toArrayLike(Buffer, "be", 32),
    signature.s.toArrayLike(Buffer, "be", 32),
  ]);
}

/**
 * Computes the account address based on a key pair.
 * @param keyPair the keypair of the account.
 * @returns the address of the account.
 */
function keyPairToAccountAddress(keyPair: Elliptic.KeyPair): string {
  const publicKey = keyPair.getPublic(false, "array");
  const hash = sha256();
  hash.update(publicKey);
  return "00" + hash.digest("hex").substring(24);
}

/**
 * Creates a keypair based on the private key.
 * @param privateKey the private key as a hex string.
 * @returns the keypair.
 */
function privateKeyToKeypair(privateKey: string): Elliptic.KeyPair {
  return ec.keyFromPrivate(privateKey);
}

/**
 * Hashes the buffers.
 * @param buffers the buffers to be hashed.
 * @returns the hash.
 */
function hashBuffers(buffers: Buffer[]): Buffer {
  const hash = sha256();

  for (const buffer of buffers) {
    hash.update(buffer);
  }

  return Buffer.from(hash.digest());
}

/**
 * Hashes the buffer.
 * @param buffer the buffer to be hashed.
 * @returns the hash.
 */
function hashBuffer(buffer: Buffer): Buffer {
  return hashBuffers([buffer]);
}

/** A collection of useful crypto functions. */
export const CryptoUtils = {
  signatureToBuffer,
  keyPairToAccountAddress,
  privateKeyToKeypair,
  hashBuffers,
  hashBuffer,
};
