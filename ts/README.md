# Blockchain Clients

This library contains three different clients for interaction with the Partisia Blockchain. These are

- `ChainControllerApi` - Used to get information of contracts and accounts from the blockchain
- `ShardControllerApi` - Used to get information of blocks and transaction from the blockchain
- `BlockchainTransactionClient` - Client used to help build, sign, and send transactions to the blockchain

The `ChainControllerApi` and `ShardControllerApi` clients are generated from the OpenAPI spec
found [here](https://reader.partisiablockchain.com/openapi).
The `BlockchainTransactionClient` is instead built on top of the other clients and uses them to build, sign and send
transactions.

## ChainControllerApi

You can create a `ChainControllerApi` targeting a specific reader url as follows

```ts
const chainController = new ChainControllerApi(
  new Configuration({basePath: "https://node1.testnet.partisiablockchain.com"})
);
```

### Get generic chain information

```ts
const chain = await chainController.getChain();
```

### Get the latest state of a contract

```ts
const state = await chainController.getContract({
  address: "02e1e09358e543e8b1cf97d5e19d5b287983cee8f6",
});
```

### Get an AvlTreeMap value from a contract

```ts
const avlValue = await chainController.getContractAvlValue({
  address: "02e1e09358e543e8b1cf97d5e19d5b287983cee8f6",
  treeId: 0,
  key: "00e72e44eab933faaf1fd4ce94bb57e08bff98a1ed"
});
```

### Get account data

```ts
const account = await chainController.getAccount({
  address: "02e1e09358e543e8b1cf97d5e19d5b287983cee8f6",
});
```

## ShardControllerApi

A `ShardControllerApi` is created the same way as a `ChainControllerApi`

```ts
const shardController = new ShardControllerApi(
  new Configuration({basePath: "https://node1.testnet.partisiablockchain.com"})
);
```

### Get latest block for a specific shard

```ts
const block = await shardController.getLatestBlock({shardId: "Shard0"});
```

### Get transaction information

```ts
const transaction = await shardController.getTransaction({
  shardId: "Shard0",
  transactionId: "9356779565b80548a1cd07800d2e2508e530f39cbd7eb71e057acf7bcb18ce0b",
});
```

## BlockchainTransactionClient

When creating a transaction client you have to provide a way to generate signature. This is done through the
interface `SenderAuthentication`. The most basic implementation of this is the `SenderAuthenticationKeyPair` which uses
a private key to sign with. Other ways to sign includes wallets such as Metamask Snap, or Parti Wallet.

```ts
const senderAuthentication = SenderAuthenticationKeyPair.fromString(privateKey);
const transactionSender = BlockchainTransactionClient.create(
  "https://node1.testnet.partisiablockchain.com",
  senderAuthentication
);
```

### Sign and send a transaction

```ts
const transaction: Transaction = {address: contractAddress, rpc: actionRpc};
const sentTransaction = await transactionSender.signAndSend(transaction, gasCost);
```

### Wait for spawned events of a transaction

```ts
const transactionTree = await transactionSender.waitForSpawnedEvents(sentTransaction);
```